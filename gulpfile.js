var gulp = require('gulp');
var fs = require("fs");
var browserify = require("browserify");
var babelify = require("babelify");
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var less = require('gulp-less');
var path = require('path');
var webserver = require('gulp-webserver');

gulp.task('webserver', function() {
  gulp.src('./public')
    .pipe(webserver({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});

gulp.task('es6', function() {
	browserify({ debug: true })
	.transform('babelify',  { sourceMaps: false })
	.require("./app/app.js", { entry: true })
	.bundle()
	.on('error', gutil.log)
	.pipe(source('bundle.js'))
	.pipe(gulp.dest('./public/'));
});

gulp.task('less', function () {
  return gulp.src('./app/less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('watch',function() {
	gulp.watch(['./app/**/*.js'],['es6']);
	gulp.watch(['./app/**/*.less'],['less']);
});

gulp.task('default', ['es6', 'less', 'webserver','watch']);
