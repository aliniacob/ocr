import synaptic from 'synaptic';

export default class Crs {
	constructor(cfg) {
		this.cfg = Object.assign({
			'fileUpload': 'file-load',
			'contrastTreshold': 100
		}, cfg)
		this.lettersCoords = [];
		this.synaptic = synaptic;
		this.fileInput = document.getElementById(this.cfg.fileUpload);

		this.canvas = document.createElement('canvas');
		this.ctx = this.canvas.getContext('2d');
		this.canvas.id = 'original';

		this.processCanvas = document.createElement('canvas');
		this.processCtx = this.processCanvas.getContext('2d');
		this.processCanvas.id = 'test';

		this.imageProCanvas = document.createElement('canvas');
		this.imageProCtx = this.imageProCanvas.getContext('2d');
		this.imageProCanvas.id = 'processed-image';


		this.readImages();
		this.buildCanvas();

		this.fileInput.parentElement.appendChild(this.canvas);
		this.fileInput.parentElement.appendChild(this.processCanvas);
		this.fileInput.parentElement.appendChild(this.imageProCanvas);
	}

	buildCanvas(baseSix) {
		const img = new Image();


		img.onload = () => {
			let w = img.naturalWidth;
			let h = img.naturalHeight;

			this.canvas.width = w;
			this.canvas.height = h;

			this.processCanvas.width = w;
			this.processCanvas.height = h;

			this.imageProCanvas.width = w;
			this.imageProCanvas.height = h;

			this.ctx.drawImage(img, 0, 0);
			this.imageData = this.ctx.getImageData(0, 0, w, h);

			this.iterateData();
		};
		img.src = './testA.png';
		img.src = baseSix;
	}


	threshold() {
		let d = this.imageData.data;

		for (let i = 0; i < d.length; i += 4) {
			let r = d[i];
			let g = d[i + 1];
			let b = d[i + 2];

			d[i + 3] < 255 ? d[i + 3] = 255 : d[i + 3] = d[i + 3];

			let v = (0.2126 * r + 0.7152 * g + 0.0722 * b >= 150) ? 255 : 0;

			d[i] = d[i + 1] = d[i + 2] = v;
		}
		this.processCtx.putImageData(this.imageData, 0, 0);
		return this.imageData;
	}



	getVariation(pxa, pxb) {
		let rv = Math.abs(pxa.r - pxb.r);
		let gv = Math.abs(pxa.g - pxb.g);
		let bv = Math.abs(pxa.b - pxb.b);
		let av = Math.abs(pxa.a - pxb.a);

		let sumVariation = (rv + gv + bv + av) / 4;

		let variation = {
			sumVariation
		};

		if (pxa.c && pxb.c) {
			variation.a = pxa.c;
			variation.b = pxb.c;
		};

		return variation;
	}

	getHitRegionPixels(x, y, w, h, hitColor) {
		let dataW = this.imageData.width;
		let dataH = this.imageData.height;

		let hitRegions = {
			't': false,  //Top     border
			'r': false,  //Right   border
			'b': false,  //Bottom  border
			'l': false,  //Left    border
			'a': false   //Overall status
		};


		//Push top square pixels
		const tStart = (y * dataW + x) * 4;
		const tStop = tStart + (w * 4);

		for (let t = tStart; t < tStop; t += 4) {
			let hits = this.getVariation(hitColor, {
				'r': this.imageData.data[t],
				'g': this.imageData.data[t + 1],
				'b': this.imageData.data[t + 2],
				'a': this.imageData.data[t + 3]
			});

			if (hits.sumVariation < 5) {
				hitRegions.t = true;
				hitRegions.a = true;
			}

		}


		//Push right square pixels
		const rStart = tStart + (w - 1) * 4;
		const rStop = rStart + h * dataW * 4;

		for (let r = rStart; r < rStop; r += dataW * 4) {
			let hits = this.getVariation(hitColor, {
				'r': this.imageData.data[r],
				'g': this.imageData.data[r + 1],
				'b': this.imageData.data[r + 2],
				'a': this.imageData.data[r + 3]
			});

			if (hits.sumVariation < 5) {
				hitRegions.r = true;
				hitRegions.a = true;
			}

		}

		//Push bottom square pixels
		const bStart = ((y + h - 1) * dataW + x) * 4;
		const bStop = bStart + w * 4;

		for (let b = bStart; b < bStop; b += 4) {
			let hits = this.getVariation(hitColor, {
				'r': this.imageData.data[b],
				'g': this.imageData.data[b + 1],
				'b': this.imageData.data[b + 2],
				'a': this.imageData.data[b + 3]
			});

			if (hits.sumVariation < 5) {
				hitRegions.b = true;
				hitRegions.a = true;
			}

		}

		//Push left square pixels
		const lStart = tStart;
		const lStop = lStart + dataW * h * 4;

		for (let l = lStart; l < lStop; l += dataW * 4) {
			let hits = this.getVariation(hitColor, {
				'r': this.imageData.data[l],
				'g': this.imageData.data[l + 1],
				'b': this.imageData.data[l + 2],
				'a': this.imageData.data[l + 3]
			});

			if (hits.sumVariation < 5) {
				hitRegions.l = true;
				hitRegions.a = true;
			}
		}

		return hitRegions;
	}


	analyzeHitRegion(pxa, pxb, variation) {
		let cX = pxb.c.x;
		let cY = pxb.c.y;

		let cW = 2;
		let cH = 2;

		let hasHit = true;
		let testCase = {
			'init': false,
			'success': 0,
			'val': {
				'h': 0
			}
		};

		while (hasHit) {
			let regions = this.getHitRegionPixels(cX, cY, cW, cH, pxb);
			if (regions.t) {
				--cY;
				++cH;
			}
			if (regions.r) {
				++cW;
			}
			if (regions.b) {
				++cH;
			}
			if (regions.l) {
				--cX;
				++cW;
			}
			hasHit = regions.a;

			//Dot case up
			if (!regions.a && Math.abs((cH - cW)) < 5 ) {
				if (!testCase.init) {
					testCase.init = true;
					testCase.val.h = cH;
				}

				hasHit = true;
				cH += 2;
			}
			//If test case was started monitor ROI height
			if (testCase.init && regions.b) {
				testCase.val.h = cH;
			}

		}

		if (testCase.init) {
			cH = testCase.val.h;
		}

		this.imageProCtx.rect(cX, cY, cW, cH);
		this.imageProCtx.stroke();

		return [cX + 1, cY + 1, cW - 1, cH - 1]
	}

	checkSkip(fp) {
		let mustSkip = 0;

		if (this.lettersCoords.length < 1) {
			return 0;
		}

		this.lettersCoords.forEach(l => {
			//If current point is inbetween return a match
			let machfX = fp.x >= l[0] && fp.x <= l[0] + l[2];
			let machfY = fp.y >= l[1] && fp.y <= l[1] + l[3];

			if (machfX && machfY) {
				mustSkip = l[3];
			}
		});

		return mustSkip;
	}

	iterateData() {
		let pxdata = this.threshold().data;
		let w = this.imageData.width;
		let h = this.imageData.height;
		// return;
		for (let x = w; x >= 0; x -= 2) {
		// for (let x = 0; x < w ; x = 2) {
			for (let y = 0; y < h - 1; y += 1) {

				const tpStart = (x + y * w) * 4;
				const bpStart = (x + (y + 1) * w) * 4;

				//First pixel
				const fPx = {
					'r': pxdata[tpStart],
					'g': pxdata[tpStart + 1],
					'b': pxdata[tpStart + 2],
					'a': pxdata[tpStart + 3],
					'c': {'x': x, 'y': y}
				};

				//Second pixel (right)
				const sPx = {
					'r': pxdata[bpStart],
					'g': pxdata[bpStart + 1],
					'b': pxdata[bpStart + 2],
					'a': pxdata[bpStart + 3],
					'c': {'x': x, 'y': y + 1}
				};

				let skip = this.checkSkip(fPx.c);
				if (skip > 0) {
					y += skip;
					continue;
				}
				const pxvar = this.getVariation(fPx, sPx);

				// this.processCtx.fillStyle = 'red';
				// this.processCtx.fillRect(x, y, 1, 1);
				if (pxvar.sumVariation > this.cfg.contrastTreshold) {
					let coords = this.analyzeHitRegion(fPx, sPx, pxvar.sumVariation);
					this.lettersCoords.push(coords);
					y = coords[1] - 1;
				}
			}
		}
		//Remove duplicates
		this.letters = [...new Set(
			this.lettersCoords.map(l => `${l[0]},${l[1]},${l[2]},${l[3]}`))
		].map(l => {
			l = l.split(',');
			return [
				parseFloat(l[0]),
				parseFloat(l[1]),
				parseFloat(l[2]),
				parseFloat(l[3])
			]
		});

		console.log(this.letters);

	}

	buildImage() {
		//Must iterate to process multiple files
		this.buildCanvas(this.imageArray[0]);
	}

	readImages() {
		this.fileInput.addEventListener('change', (e) => {
			this.imageArray = [];

			const tgt = e.target || window.event.srcElement;
			const files = tgt.files;

			// FileReader support
			if (FileReader && files && files.length) {
				let fr = new FileReader();
				let index = 1;

				fr.onload = () => {
					this.imageArray.push(fr.result);

					if (index < files.length) {
						fr.readAsDataURL(files[index]);
						index += 1;
					} else {
						this.buildImage();
					}
				}

				fr.readAsDataURL(files[0]);
			} else {
				alert('Please update browser');
			}
		})
	}

};
