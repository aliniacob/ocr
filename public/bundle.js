(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _Crs = require('./js/Crs');

var _Crs2 = _interopRequireDefault(_Crs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

new _Crs2.default({
	'fileUpload': 'file-load'
});
},{"./js/Crs":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _synaptic = require('synaptic');

var _synaptic2 = _interopRequireDefault(_synaptic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Crs = function () {
	function Crs(cfg) {
		(0, _classCallCheck3.default)(this, Crs);

		this.cfg = Object.assign({
			'fileUpload': 'file-load',
			'contrastTreshold': 100
		}, cfg);
		this.lettersCoords = [];
		this.synaptic = _synaptic2.default;
		this.fileInput = document.getElementById(this.cfg.fileUpload);

		this.canvas = document.createElement('canvas');
		this.ctx = this.canvas.getContext('2d');
		this.canvas.id = 'original';

		this.processCanvas = document.createElement('canvas');
		this.processCtx = this.processCanvas.getContext('2d');
		this.processCanvas.id = 'test';

		this.imageProCanvas = document.createElement('canvas');
		this.imageProCtx = this.imageProCanvas.getContext('2d');
		this.imageProCanvas.id = 'processed-image';

		this.readImages();
		this.buildCanvas();

		this.fileInput.parentElement.appendChild(this.canvas);
		this.fileInput.parentElement.appendChild(this.processCanvas);
		this.fileInput.parentElement.appendChild(this.imageProCanvas);
	}

	(0, _createClass3.default)(Crs, [{
		key: 'buildCanvas',
		value: function buildCanvas(baseSix) {
			var _this = this;

			var img = new Image();

			img.onload = function () {
				var w = img.naturalWidth;
				var h = img.naturalHeight;

				_this.canvas.width = w;
				_this.canvas.height = h;

				_this.processCanvas.width = w;
				_this.processCanvas.height = h;

				_this.imageProCanvas.width = w;
				_this.imageProCanvas.height = h;

				_this.ctx.drawImage(img, 0, 0);
				_this.imageData = _this.ctx.getImageData(0, 0, w, h);

				_this.iterateData();
			};
			img.src = './testA.png';
			img.src = baseSix;
		}
	}, {
		key: 'threshold',
		value: function threshold() {
			var d = this.imageData.data;

			for (var i = 0; i < d.length; i += 4) {
				var r = d[i];
				var g = d[i + 1];
				var b = d[i + 2];

				d[i + 3] < 255 ? d[i + 3] = 255 : d[i + 3] = d[i + 3];

				var v = 0.2126 * r + 0.7152 * g + 0.0722 * b >= 150 ? 255 : 0;

				d[i] = d[i + 1] = d[i + 2] = v;
			}
			this.processCtx.putImageData(this.imageData, 0, 0);
			return this.imageData;
		}
	}, {
		key: 'getVariation',
		value: function getVariation(pxa, pxb) {
			var rv = Math.abs(pxa.r - pxb.r);
			var gv = Math.abs(pxa.g - pxb.g);
			var bv = Math.abs(pxa.b - pxb.b);
			var av = Math.abs(pxa.a - pxb.a);

			var sumVariation = (rv + gv + bv + av) / 4;

			var variation = {
				sumVariation: sumVariation
			};

			if (pxa.c && pxb.c) {
				variation.a = pxa.c;
				variation.b = pxb.c;
			};

			return variation;
		}
	}, {
		key: 'getHitRegionPixels',
		value: function getHitRegionPixels(x, y, w, h, hitColor) {
			var dataW = this.imageData.width;
			var dataH = this.imageData.height;

			var hitRegions = {
				't': false, //Top     border
				'r': false, //Right   border
				'b': false, //Bottom  border
				'l': false, //Left    border
				'a': false //Overall status
			};

			//Push top square pixels
			var tStart = (y * dataW + x) * 4;
			var tStop = tStart + w * 4;

			for (var t = tStart; t < tStop; t += 4) {
				var hits = this.getVariation(hitColor, {
					'r': this.imageData.data[t],
					'g': this.imageData.data[t + 1],
					'b': this.imageData.data[t + 2],
					'a': this.imageData.data[t + 3]
				});

				if (hits.sumVariation < 5) {
					hitRegions.t = true;
					hitRegions.a = true;
				}
			}

			//Push right square pixels
			var rStart = tStart + (w - 1) * 4;
			var rStop = rStart + h * dataW * 4;

			for (var r = rStart; r < rStop; r += dataW * 4) {
				var _hits = this.getVariation(hitColor, {
					'r': this.imageData.data[r],
					'g': this.imageData.data[r + 1],
					'b': this.imageData.data[r + 2],
					'a': this.imageData.data[r + 3]
				});

				if (_hits.sumVariation < 5) {
					hitRegions.r = true;
					hitRegions.a = true;
				}
			}

			//Push bottom square pixels
			var bStart = ((y + h - 1) * dataW + x) * 4;
			var bStop = bStart + w * 4;

			for (var b = bStart; b < bStop; b += 4) {
				var _hits2 = this.getVariation(hitColor, {
					'r': this.imageData.data[b],
					'g': this.imageData.data[b + 1],
					'b': this.imageData.data[b + 2],
					'a': this.imageData.data[b + 3]
				});

				if (_hits2.sumVariation < 5) {
					hitRegions.b = true;
					hitRegions.a = true;
				}
			}

			//Push left square pixels
			var lStart = tStart;
			var lStop = lStart + dataW * h * 4;

			for (var l = lStart; l < lStop; l += dataW * 4) {
				var _hits3 = this.getVariation(hitColor, {
					'r': this.imageData.data[l],
					'g': this.imageData.data[l + 1],
					'b': this.imageData.data[l + 2],
					'a': this.imageData.data[l + 3]
				});

				if (_hits3.sumVariation < 5) {
					hitRegions.l = true;
					hitRegions.a = true;
				}
			}

			return hitRegions;
		}
	}, {
		key: 'analyzeHitRegion',
		value: function analyzeHitRegion(pxa, pxb, variation) {
			var cX = pxb.c.x;
			var cY = pxb.c.y;

			var cW = 2;
			var cH = 2;

			var hasHit = true;
			var testCase = {
				'init': false,
				'success': 0,
				'val': {
					'h': 0
				}
			};

			while (hasHit) {
				var regions = this.getHitRegionPixels(cX, cY, cW, cH, pxb);
				if (regions.t) {
					--cY;
					++cH;
				}
				if (regions.r) {
					++cW;
				}
				if (regions.b) {
					++cH;
				}
				if (regions.l) {
					--cX;
					++cW;
				}
				hasHit = regions.a;

				//Dot case up
				if (!regions.a && Math.abs(cH - cW) < 5) {
					if (!testCase.init) {
						testCase.init = true;
						testCase.val.h = cH;
					}

					hasHit = true;
					cH += 2;
				}
				//If test case was started monitor ROI height
				if (testCase.init && regions.b) {
					testCase.val.h = cH;
				}
			}

			if (testCase.init) {
				cH = testCase.val.h;
			}

			this.imageProCtx.rect(cX, cY, cW, cH);
			this.imageProCtx.stroke();

			return [cX + 1, cY + 1, cW - 1, cH - 1];
		}
	}, {
		key: 'checkSkip',
		value: function checkSkip(fp) {
			var mustSkip = 0;

			if (this.lettersCoords.length < 1) {
				return 0;
			}

			this.lettersCoords.forEach(function (l) {
				//If current point is inbetween return a match
				var machfX = fp.x >= l[0] && fp.x <= l[0] + l[2];
				var machfY = fp.y >= l[1] && fp.y <= l[1] + l[3];

				if (machfX && machfY) {
					mustSkip = l[3];
				}
			});

			return mustSkip;
		}
	}, {
		key: 'iterateData',
		value: function iterateData() {
			var pxdata = this.threshold().data;
			var w = this.imageData.width;
			var h = this.imageData.height;
			// return;
			for (var x = w; x >= 0; x -= 2) {
				// for (let x = 0; x < w ; x = 2) {
				for (var y = 0; y < h - 1; y += 1) {

					var tpStart = (x + y * w) * 4;
					var bpStart = (x + (y + 1) * w) * 4;

					//First pixel
					var fPx = {
						'r': pxdata[tpStart],
						'g': pxdata[tpStart + 1],
						'b': pxdata[tpStart + 2],
						'a': pxdata[tpStart + 3],
						'c': { 'x': x, 'y': y }
					};

					//Second pixel (right)
					var sPx = {
						'r': pxdata[bpStart],
						'g': pxdata[bpStart + 1],
						'b': pxdata[bpStart + 2],
						'a': pxdata[bpStart + 3],
						'c': { 'x': x, 'y': y + 1 }
					};

					var skip = this.checkSkip(fPx.c);
					if (skip > 0) {
						y += skip;
						continue;
					}
					var pxvar = this.getVariation(fPx, sPx);

					// this.processCtx.fillStyle = 'red';
					// this.processCtx.fillRect(x, y, 1, 1);
					if (pxvar.sumVariation > this.cfg.contrastTreshold) {
						var coords = this.analyzeHitRegion(fPx, sPx, pxvar.sumVariation);
						this.lettersCoords.push(coords);
						y = coords[1] - 1;
					}
				}
			}
			//Remove duplicates
			this.letters = [].concat((0, _toConsumableArray3.default)(new Set(this.lettersCoords.map(function (l) {
				return l[0] + ',' + l[1] + ',' + l[2] + ',' + l[3];
			})))).map(function (l) {
				l = l.split(',');
				return [parseFloat(l[0]), parseFloat(l[1]), parseFloat(l[2]), parseFloat(l[3])];
			});

			console.log(this.letters);
		}
	}, {
		key: 'buildImage',
		value: function buildImage() {
			//Must iterate to process multiple files
			this.buildCanvas(this.imageArray[0]);
		}
	}, {
		key: 'readImages',
		value: function readImages() {
			var _this2 = this;

			this.fileInput.addEventListener('change', function (e) {
				_this2.imageArray = [];

				var tgt = e.target || window.event.srcElement;
				var files = tgt.files;

				// FileReader support
				if (FileReader && files && files.length) {
					(function () {
						var fr = new FileReader();
						var index = 1;

						fr.onload = function () {
							_this2.imageArray.push(fr.result);

							if (index < files.length) {
								fr.readAsDataURL(files[index]);
								index += 1;
							} else {
								_this2.buildImage();
							}
						};

						fr.readAsDataURL(files[0]);
					})();
				} else {
					alert('Please update browser');
				}
			});
		}
	}]);
	return Crs;
}();

exports.default = Crs;
;
},{"babel-runtime/helpers/classCallCheck":5,"babel-runtime/helpers/createClass":6,"babel-runtime/helpers/toConsumableArray":7,"synaptic":66}],3:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/array/from"), __esModule: true };
},{"core-js/library/fn/array/from":8}],4:[function(require,module,exports){
module.exports = { "default": require("core-js/library/fn/object/define-property"), __esModule: true };
},{"core-js/library/fn/object/define-property":9}],5:[function(require,module,exports){
"use strict";

exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};
},{}],6:[function(require,module,exports){
"use strict";

exports.__esModule = true;

var _defineProperty = require("../core-js/object/define-property");

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();
},{"../core-js/object/define-property":4}],7:[function(require,module,exports){
"use strict";

exports.__esModule = true;

var _from = require("../core-js/array/from");

var _from2 = _interopRequireDefault(_from);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  } else {
    return (0, _from2.default)(arr);
  }
};
},{"../core-js/array/from":3}],8:[function(require,module,exports){
require('../../modules/es6.string.iterator');
require('../../modules/es6.array.from');
module.exports = require('../../modules/_core').Array.from;
},{"../../modules/_core":15,"../../modules/es6.array.from":59,"../../modules/es6.string.iterator":61}],9:[function(require,module,exports){
require('../../modules/es6.object.define-property');
var $Object = require('../../modules/_core').Object;
module.exports = function defineProperty(it, key, desc){
  return $Object.defineProperty(it, key, desc);
};
},{"../../modules/_core":15,"../../modules/es6.object.define-property":60}],10:[function(require,module,exports){
module.exports = function(it){
  if(typeof it != 'function')throw TypeError(it + ' is not a function!');
  return it;
};
},{}],11:[function(require,module,exports){
var isObject = require('./_is-object');
module.exports = function(it){
  if(!isObject(it))throw TypeError(it + ' is not an object!');
  return it;
};
},{"./_is-object":31}],12:[function(require,module,exports){
// false -> Array#indexOf
// true  -> Array#includes
var toIObject = require('./_to-iobject')
  , toLength  = require('./_to-length')
  , toIndex   = require('./_to-index');
module.exports = function(IS_INCLUDES){
  return function($this, el, fromIndex){
    var O      = toIObject($this)
      , length = toLength(O.length)
      , index  = toIndex(fromIndex, length)
      , value;
    // Array#includes uses SameValueZero equality algorithm
    if(IS_INCLUDES && el != el)while(length > index){
      value = O[index++];
      if(value != value)return true;
    // Array#toIndex ignores holes, Array#includes - not
    } else for(;length > index; index++)if(IS_INCLUDES || index in O){
      if(O[index] === el)return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};
},{"./_to-index":50,"./_to-iobject":52,"./_to-length":53}],13:[function(require,module,exports){
// getting tag from 19.1.3.6 Object.prototype.toString()
var cof = require('./_cof')
  , TAG = require('./_wks')('toStringTag')
  // ES3 wrong here
  , ARG = cof(function(){ return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function(it, key){
  try {
    return it[key];
  } catch(e){ /* empty */ }
};

module.exports = function(it){
  var O, T, B;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (T = tryGet(O = Object(it), TAG)) == 'string' ? T
    // builtinTag case
    : ARG ? cof(O)
    // ES3 arguments fallback
    : (B = cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
};
},{"./_cof":14,"./_wks":57}],14:[function(require,module,exports){
var toString = {}.toString;

module.exports = function(it){
  return toString.call(it).slice(8, -1);
};
},{}],15:[function(require,module,exports){
var core = module.exports = {version: '2.4.0'};
if(typeof __e == 'number')__e = core; // eslint-disable-line no-undef
},{}],16:[function(require,module,exports){
'use strict';
var $defineProperty = require('./_object-dp')
  , createDesc      = require('./_property-desc');

module.exports = function(object, index, value){
  if(index in object)$defineProperty.f(object, index, createDesc(0, value));
  else object[index] = value;
};
},{"./_object-dp":39,"./_property-desc":44}],17:[function(require,module,exports){
// optional / simple context binding
var aFunction = require('./_a-function');
module.exports = function(fn, that, length){
  aFunction(fn);
  if(that === undefined)return fn;
  switch(length){
    case 1: return function(a){
      return fn.call(that, a);
    };
    case 2: return function(a, b){
      return fn.call(that, a, b);
    };
    case 3: return function(a, b, c){
      return fn.call(that, a, b, c);
    };
  }
  return function(/* ...args */){
    return fn.apply(that, arguments);
  };
};
},{"./_a-function":10}],18:[function(require,module,exports){
// 7.2.1 RequireObjectCoercible(argument)
module.exports = function(it){
  if(it == undefined)throw TypeError("Can't call method on  " + it);
  return it;
};
},{}],19:[function(require,module,exports){
// Thank's IE8 for his funny defineProperty
module.exports = !require('./_fails')(function(){
  return Object.defineProperty({}, 'a', {get: function(){ return 7; }}).a != 7;
});
},{"./_fails":23}],20:[function(require,module,exports){
var isObject = require('./_is-object')
  , document = require('./_global').document
  // in old IE typeof document.createElement is 'object'
  , is = isObject(document) && isObject(document.createElement);
module.exports = function(it){
  return is ? document.createElement(it) : {};
};
},{"./_global":24,"./_is-object":31}],21:[function(require,module,exports){
// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');
},{}],22:[function(require,module,exports){
var global    = require('./_global')
  , core      = require('./_core')
  , ctx       = require('./_ctx')
  , hide      = require('./_hide')
  , PROTOTYPE = 'prototype';

var $export = function(type, name, source){
  var IS_FORCED = type & $export.F
    , IS_GLOBAL = type & $export.G
    , IS_STATIC = type & $export.S
    , IS_PROTO  = type & $export.P
    , IS_BIND   = type & $export.B
    , IS_WRAP   = type & $export.W
    , exports   = IS_GLOBAL ? core : core[name] || (core[name] = {})
    , expProto  = exports[PROTOTYPE]
    , target    = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE]
    , key, own, out;
  if(IS_GLOBAL)source = name;
  for(key in source){
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if(own && key in exports)continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? (function(C){
      var F = function(a, b, c){
        if(this instanceof C){
          switch(arguments.length){
            case 0: return new C;
            case 1: return new C(a);
            case 2: return new C(a, b);
          } return new C(a, b, c);
        } return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
    // make static versions for prototype methods
    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if(IS_PROTO){
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if(type & $export.R && expProto && !expProto[key])hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library` 
module.exports = $export;
},{"./_core":15,"./_ctx":17,"./_global":24,"./_hide":26}],23:[function(require,module,exports){
module.exports = function(exec){
  try {
    return !!exec();
  } catch(e){
    return true;
  }
};
},{}],24:[function(require,module,exports){
// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self : Function('return this')();
if(typeof __g == 'number')__g = global; // eslint-disable-line no-undef
},{}],25:[function(require,module,exports){
var hasOwnProperty = {}.hasOwnProperty;
module.exports = function(it, key){
  return hasOwnProperty.call(it, key);
};
},{}],26:[function(require,module,exports){
var dP         = require('./_object-dp')
  , createDesc = require('./_property-desc');
module.exports = require('./_descriptors') ? function(object, key, value){
  return dP.f(object, key, createDesc(1, value));
} : function(object, key, value){
  object[key] = value;
  return object;
};
},{"./_descriptors":19,"./_object-dp":39,"./_property-desc":44}],27:[function(require,module,exports){
module.exports = require('./_global').document && document.documentElement;
},{"./_global":24}],28:[function(require,module,exports){
module.exports = !require('./_descriptors') && !require('./_fails')(function(){
  return Object.defineProperty(require('./_dom-create')('div'), 'a', {get: function(){ return 7; }}).a != 7;
});
},{"./_descriptors":19,"./_dom-create":20,"./_fails":23}],29:[function(require,module,exports){
// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = require('./_cof');
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function(it){
  return cof(it) == 'String' ? it.split('') : Object(it);
};
},{"./_cof":14}],30:[function(require,module,exports){
// check on default Array iterator
var Iterators  = require('./_iterators')
  , ITERATOR   = require('./_wks')('iterator')
  , ArrayProto = Array.prototype;

module.exports = function(it){
  return it !== undefined && (Iterators.Array === it || ArrayProto[ITERATOR] === it);
};
},{"./_iterators":36,"./_wks":57}],31:[function(require,module,exports){
module.exports = function(it){
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};
},{}],32:[function(require,module,exports){
// call something on iterator step with safe closing on error
var anObject = require('./_an-object');
module.exports = function(iterator, fn, value, entries){
  try {
    return entries ? fn(anObject(value)[0], value[1]) : fn(value);
  // 7.4.6 IteratorClose(iterator, completion)
  } catch(e){
    var ret = iterator['return'];
    if(ret !== undefined)anObject(ret.call(iterator));
    throw e;
  }
};
},{"./_an-object":11}],33:[function(require,module,exports){
'use strict';
var create         = require('./_object-create')
  , descriptor     = require('./_property-desc')
  , setToStringTag = require('./_set-to-string-tag')
  , IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
require('./_hide')(IteratorPrototype, require('./_wks')('iterator'), function(){ return this; });

module.exports = function(Constructor, NAME, next){
  Constructor.prototype = create(IteratorPrototype, {next: descriptor(1, next)});
  setToStringTag(Constructor, NAME + ' Iterator');
};
},{"./_hide":26,"./_object-create":38,"./_property-desc":44,"./_set-to-string-tag":46,"./_wks":57}],34:[function(require,module,exports){
'use strict';
var LIBRARY        = require('./_library')
  , $export        = require('./_export')
  , redefine       = require('./_redefine')
  , hide           = require('./_hide')
  , has            = require('./_has')
  , Iterators      = require('./_iterators')
  , $iterCreate    = require('./_iter-create')
  , setToStringTag = require('./_set-to-string-tag')
  , getPrototypeOf = require('./_object-gpo')
  , ITERATOR       = require('./_wks')('iterator')
  , BUGGY          = !([].keys && 'next' in [].keys()) // Safari has buggy iterators w/o `next`
  , FF_ITERATOR    = '@@iterator'
  , KEYS           = 'keys'
  , VALUES         = 'values';

var returnThis = function(){ return this; };

module.exports = function(Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED){
  $iterCreate(Constructor, NAME, next);
  var getMethod = function(kind){
    if(!BUGGY && kind in proto)return proto[kind];
    switch(kind){
      case KEYS: return function keys(){ return new Constructor(this, kind); };
      case VALUES: return function values(){ return new Constructor(this, kind); };
    } return function entries(){ return new Constructor(this, kind); };
  };
  var TAG        = NAME + ' Iterator'
    , DEF_VALUES = DEFAULT == VALUES
    , VALUES_BUG = false
    , proto      = Base.prototype
    , $native    = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT]
    , $default   = $native || getMethod(DEFAULT)
    , $entries   = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined
    , $anyNative = NAME == 'Array' ? proto.entries || $native : $native
    , methods, key, IteratorPrototype;
  // Fix native
  if($anyNative){
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base));
    if(IteratorPrototype !== Object.prototype){
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if(!LIBRARY && !has(IteratorPrototype, ITERATOR))hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if(DEF_VALUES && $native && $native.name !== VALUES){
    VALUES_BUG = true;
    $default = function values(){ return $native.call(this); };
  }
  // Define iterator
  if((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])){
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG]  = returnThis;
  if(DEFAULT){
    methods = {
      values:  DEF_VALUES ? $default : getMethod(VALUES),
      keys:    IS_SET     ? $default : getMethod(KEYS),
      entries: $entries
    };
    if(FORCED)for(key in methods){
      if(!(key in proto))redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};
},{"./_export":22,"./_has":25,"./_hide":26,"./_iter-create":33,"./_iterators":36,"./_library":37,"./_object-gpo":41,"./_redefine":45,"./_set-to-string-tag":46,"./_wks":57}],35:[function(require,module,exports){
var ITERATOR     = require('./_wks')('iterator')
  , SAFE_CLOSING = false;

try {
  var riter = [7][ITERATOR]();
  riter['return'] = function(){ SAFE_CLOSING = true; };
  Array.from(riter, function(){ throw 2; });
} catch(e){ /* empty */ }

module.exports = function(exec, skipClosing){
  if(!skipClosing && !SAFE_CLOSING)return false;
  var safe = false;
  try {
    var arr  = [7]
      , iter = arr[ITERATOR]();
    iter.next = function(){ return {done: safe = true}; };
    arr[ITERATOR] = function(){ return iter; };
    exec(arr);
  } catch(e){ /* empty */ }
  return safe;
};
},{"./_wks":57}],36:[function(require,module,exports){
module.exports = {};
},{}],37:[function(require,module,exports){
module.exports = true;
},{}],38:[function(require,module,exports){
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject    = require('./_an-object')
  , dPs         = require('./_object-dps')
  , enumBugKeys = require('./_enum-bug-keys')
  , IE_PROTO    = require('./_shared-key')('IE_PROTO')
  , Empty       = function(){ /* empty */ }
  , PROTOTYPE   = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function(){
  // Thrash, waste and sodomy: IE GC bug
  var iframe = require('./_dom-create')('iframe')
    , i      = enumBugKeys.length
    , lt     = '<'
    , gt     = '>'
    , iframeDocument;
  iframe.style.display = 'none';
  require('./_html').appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while(i--)delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties){
  var result;
  if(O !== null){
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty;
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};

},{"./_an-object":11,"./_dom-create":20,"./_enum-bug-keys":21,"./_html":27,"./_object-dps":40,"./_shared-key":47}],39:[function(require,module,exports){
var anObject       = require('./_an-object')
  , IE8_DOM_DEFINE = require('./_ie8-dom-define')
  , toPrimitive    = require('./_to-primitive')
  , dP             = Object.defineProperty;

exports.f = require('./_descriptors') ? Object.defineProperty : function defineProperty(O, P, Attributes){
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if(IE8_DOM_DEFINE)try {
    return dP(O, P, Attributes);
  } catch(e){ /* empty */ }
  if('get' in Attributes || 'set' in Attributes)throw TypeError('Accessors not supported!');
  if('value' in Attributes)O[P] = Attributes.value;
  return O;
};
},{"./_an-object":11,"./_descriptors":19,"./_ie8-dom-define":28,"./_to-primitive":55}],40:[function(require,module,exports){
var dP       = require('./_object-dp')
  , anObject = require('./_an-object')
  , getKeys  = require('./_object-keys');

module.exports = require('./_descriptors') ? Object.defineProperties : function defineProperties(O, Properties){
  anObject(O);
  var keys   = getKeys(Properties)
    , length = keys.length
    , i = 0
    , P;
  while(length > i)dP.f(O, P = keys[i++], Properties[P]);
  return O;
};
},{"./_an-object":11,"./_descriptors":19,"./_object-dp":39,"./_object-keys":43}],41:[function(require,module,exports){
// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has         = require('./_has')
  , toObject    = require('./_to-object')
  , IE_PROTO    = require('./_shared-key')('IE_PROTO')
  , ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function(O){
  O = toObject(O);
  if(has(O, IE_PROTO))return O[IE_PROTO];
  if(typeof O.constructor == 'function' && O instanceof O.constructor){
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectProto : null;
};
},{"./_has":25,"./_shared-key":47,"./_to-object":54}],42:[function(require,module,exports){
var has          = require('./_has')
  , toIObject    = require('./_to-iobject')
  , arrayIndexOf = require('./_array-includes')(false)
  , IE_PROTO     = require('./_shared-key')('IE_PROTO');

module.exports = function(object, names){
  var O      = toIObject(object)
    , i      = 0
    , result = []
    , key;
  for(key in O)if(key != IE_PROTO)has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while(names.length > i)if(has(O, key = names[i++])){
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};
},{"./_array-includes":12,"./_has":25,"./_shared-key":47,"./_to-iobject":52}],43:[function(require,module,exports){
// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys       = require('./_object-keys-internal')
  , enumBugKeys = require('./_enum-bug-keys');

module.exports = Object.keys || function keys(O){
  return $keys(O, enumBugKeys);
};
},{"./_enum-bug-keys":21,"./_object-keys-internal":42}],44:[function(require,module,exports){
module.exports = function(bitmap, value){
  return {
    enumerable  : !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable    : !(bitmap & 4),
    value       : value
  };
};
},{}],45:[function(require,module,exports){
module.exports = require('./_hide');
},{"./_hide":26}],46:[function(require,module,exports){
var def = require('./_object-dp').f
  , has = require('./_has')
  , TAG = require('./_wks')('toStringTag');

module.exports = function(it, tag, stat){
  if(it && !has(it = stat ? it : it.prototype, TAG))def(it, TAG, {configurable: true, value: tag});
};
},{"./_has":25,"./_object-dp":39,"./_wks":57}],47:[function(require,module,exports){
var shared = require('./_shared')('keys')
  , uid    = require('./_uid');
module.exports = function(key){
  return shared[key] || (shared[key] = uid(key));
};
},{"./_shared":48,"./_uid":56}],48:[function(require,module,exports){
var global = require('./_global')
  , SHARED = '__core-js_shared__'
  , store  = global[SHARED] || (global[SHARED] = {});
module.exports = function(key){
  return store[key] || (store[key] = {});
};
},{"./_global":24}],49:[function(require,module,exports){
var toInteger = require('./_to-integer')
  , defined   = require('./_defined');
// true  -> String#at
// false -> String#codePointAt
module.exports = function(TO_STRING){
  return function(that, pos){
    var s = String(defined(that))
      , i = toInteger(pos)
      , l = s.length
      , a, b;
    if(i < 0 || i >= l)return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
      ? TO_STRING ? s.charAt(i) : a
      : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};
},{"./_defined":18,"./_to-integer":51}],50:[function(require,module,exports){
var toInteger = require('./_to-integer')
  , max       = Math.max
  , min       = Math.min;
module.exports = function(index, length){
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};
},{"./_to-integer":51}],51:[function(require,module,exports){
// 7.1.4 ToInteger
var ceil  = Math.ceil
  , floor = Math.floor;
module.exports = function(it){
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};
},{}],52:[function(require,module,exports){
// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = require('./_iobject')
  , defined = require('./_defined');
module.exports = function(it){
  return IObject(defined(it));
};
},{"./_defined":18,"./_iobject":29}],53:[function(require,module,exports){
// 7.1.15 ToLength
var toInteger = require('./_to-integer')
  , min       = Math.min;
module.exports = function(it){
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};
},{"./_to-integer":51}],54:[function(require,module,exports){
// 7.1.13 ToObject(argument)
var defined = require('./_defined');
module.exports = function(it){
  return Object(defined(it));
};
},{"./_defined":18}],55:[function(require,module,exports){
// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = require('./_is-object');
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function(it, S){
  if(!isObject(it))return it;
  var fn, val;
  if(S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it)))return val;
  if(typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it)))return val;
  if(!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it)))return val;
  throw TypeError("Can't convert object to primitive value");
};
},{"./_is-object":31}],56:[function(require,module,exports){
var id = 0
  , px = Math.random();
module.exports = function(key){
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};
},{}],57:[function(require,module,exports){
var store      = require('./_shared')('wks')
  , uid        = require('./_uid')
  , Symbol     = require('./_global').Symbol
  , USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function(name){
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;
},{"./_global":24,"./_shared":48,"./_uid":56}],58:[function(require,module,exports){
var classof   = require('./_classof')
  , ITERATOR  = require('./_wks')('iterator')
  , Iterators = require('./_iterators');
module.exports = require('./_core').getIteratorMethod = function(it){
  if(it != undefined)return it[ITERATOR]
    || it['@@iterator']
    || Iterators[classof(it)];
};
},{"./_classof":13,"./_core":15,"./_iterators":36,"./_wks":57}],59:[function(require,module,exports){
'use strict';
var ctx            = require('./_ctx')
  , $export        = require('./_export')
  , toObject       = require('./_to-object')
  , call           = require('./_iter-call')
  , isArrayIter    = require('./_is-array-iter')
  , toLength       = require('./_to-length')
  , createProperty = require('./_create-property')
  , getIterFn      = require('./core.get-iterator-method');

$export($export.S + $export.F * !require('./_iter-detect')(function(iter){ Array.from(iter); }), 'Array', {
  // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
  from: function from(arrayLike/*, mapfn = undefined, thisArg = undefined*/){
    var O       = toObject(arrayLike)
      , C       = typeof this == 'function' ? this : Array
      , aLen    = arguments.length
      , mapfn   = aLen > 1 ? arguments[1] : undefined
      , mapping = mapfn !== undefined
      , index   = 0
      , iterFn  = getIterFn(O)
      , length, result, step, iterator;
    if(mapping)mapfn = ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
    // if object isn't iterable or it's array with default iterator - use simple case
    if(iterFn != undefined && !(C == Array && isArrayIter(iterFn))){
      for(iterator = iterFn.call(O), result = new C; !(step = iterator.next()).done; index++){
        createProperty(result, index, mapping ? call(iterator, mapfn, [step.value, index], true) : step.value);
      }
    } else {
      length = toLength(O.length);
      for(result = new C(length); length > index; index++){
        createProperty(result, index, mapping ? mapfn(O[index], index) : O[index]);
      }
    }
    result.length = index;
    return result;
  }
});

},{"./_create-property":16,"./_ctx":17,"./_export":22,"./_is-array-iter":30,"./_iter-call":32,"./_iter-detect":35,"./_to-length":53,"./_to-object":54,"./core.get-iterator-method":58}],60:[function(require,module,exports){
var $export = require('./_export');
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !require('./_descriptors'), 'Object', {defineProperty: require('./_object-dp').f});
},{"./_descriptors":19,"./_export":22,"./_object-dp":39}],61:[function(require,module,exports){
'use strict';
var $at  = require('./_string-at')(true);

// 21.1.3.27 String.prototype[@@iterator]()
require('./_iter-define')(String, 'String', function(iterated){
  this._t = String(iterated); // target
  this._i = 0;                // next index
// 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function(){
  var O     = this._t
    , index = this._i
    , point;
  if(index >= O.length)return {value: undefined, done: true};
  point = $at(O, index);
  this._i += point.length;
  return {value: point, done: false};
});
},{"./_iter-define":34,"./_string-at":49}],62:[function(require,module,exports){
// import
var Layer   = require('./layer')
,   Network = require('./network')
,   Trainer = require('./trainer')

/*******************************************************************************************
                                        ARCHITECT
*******************************************************************************************/

// Collection of useful built-in architectures
var Architect = {

  // Multilayer Perceptron
  Perceptron: function Perceptron() {

    var args = Array.prototype.slice.call(arguments); // convert arguments to Array
    if (args.length < 3)
      throw new Error("not enough layers (minimum 3) !!");

    var inputs = args.shift(); // first argument
    var outputs = args.pop(); // last argument
    var layers = args; // all the arguments in the middle

    var input = new Layer(inputs);
    var hidden = [];
    var output = new Layer(outputs);

    var previous = input;

    // generate hidden layers
    for (var level in layers) {
      var size = layers[level];
      var layer = new Layer(size);
      hidden.push(layer);
      previous.project(layer);
      previous = layer;
    }
    previous.project(output);

    // set layers of the neural network
    this.set({
      input: input,
      hidden: hidden,
      output: output
    });

    // trainer for the network
    this.trainer = new Trainer(this);
  },

  // Multilayer Long Short-Term Memory
  LSTM: function LSTM() {

    var args = Array.prototype.slice.call(arguments); // convert arguments to array
    if (args.length < 3)
      throw new Error("not enough layers (minimum 3) !!");

    var last = args.pop();
    var option = {
      peepholes: Layer.connectionType.ALL_TO_ALL,
      hiddenToHidden: false,
      outputToHidden: false,
      outputToGates: false,
      inputToOutput: true,
    };
    if (typeof last != 'number') {
      var outputs = args.pop();
      if (last.hasOwnProperty('peepholes'))
        option.peepholes = last.peepholes;
      if (last.hasOwnProperty('hiddenToHidden'))
        option.hiddenToHidden = last.hiddenToHidden;
      if (last.hasOwnProperty('outputToHidden'))
        option.outputToHidden = last.outputToHidden;
      if (last.hasOwnProperty('outputToGates'))
        option.outputToGates = last.outputToGates;
      if (last.hasOwnProperty('inputToOutput'))
        option.inputToOutput = last.inputToOutput;
    } else
      var outputs = last;

    var inputs = args.shift();
    var layers = args;

    var inputLayer = new Layer(inputs);
    var hiddenLayers = [];
    var outputLayer = new Layer(outputs);

    var previous = null;

    // generate layers
    for (var layer in layers) {
      // generate memory blocks (memory cell and respective gates)
      var size = layers[layer];

      var inputGate = new Layer(size).set({
        bias: 1
      });
      var forgetGate = new Layer(size).set({
        bias: 1
      });
      var memoryCell = new Layer(size);
      var outputGate = new Layer(size).set({
        bias: 1
      });

      hiddenLayers.push(inputGate);
      hiddenLayers.push(forgetGate);
      hiddenLayers.push(memoryCell);
      hiddenLayers.push(outputGate);

      // connections from input layer
      var input = inputLayer.project(memoryCell);
      inputLayer.project(inputGate);
      inputLayer.project(forgetGate);
      inputLayer.project(outputGate);

      // connections from previous memory-block layer to this one
      if (previous != null) {
        var cell = previous.project(memoryCell);
        previous.project(inputGate);
        previous.project(forgetGate);
        previous.project(outputGate);
      }

      // connections from memory cell
      var output = memoryCell.project(outputLayer);

      // self-connection
      var self = memoryCell.project(memoryCell);

      // hidden to hidden recurrent connection
      if (option.hiddenToHidden)
        memoryCell.project(memoryCell, Layer.connectionType.ALL_TO_ELSE);

      // out to hidden recurrent connection
      if (option.outputToHidden)
        outputLayer.project(memoryCell);

      // out to gates recurrent connection
      if (option.outputToGates) {
        outputLayer.project(inputGate);
        outputLayer.project(outputGate);
        outputLayer.project(forgetGate);
      }

      // peepholes
      memoryCell.project(inputGate, option.peepholes);
      memoryCell.project(forgetGate, option.peepholes);
      memoryCell.project(outputGate, option.peepholes);

      // gates
      inputGate.gate(input, Layer.gateType.INPUT);
      forgetGate.gate(self, Layer.gateType.ONE_TO_ONE);
      outputGate.gate(output, Layer.gateType.OUTPUT);
      if (previous != null)
        inputGate.gate(cell, Layer.gateType.INPUT);

      previous = memoryCell;
    }

    // input to output direct connection
    if (option.inputToOutput)
      inputLayer.project(outputLayer);

    // set the layers of the neural network
    this.set({
      input: inputLayer,
      hidden: hiddenLayers,
      output: outputLayer
    });

    // trainer
    this.trainer = new Trainer(this);
  },

  // Liquid State Machine
  Liquid: function Liquid(inputs, hidden, outputs, connections, gates) {

    // create layers
    var inputLayer = new Layer(inputs);
    var hiddenLayer = new Layer(hidden);
    var outputLayer = new Layer(outputs);

    // make connections and gates randomly among the neurons
    var neurons = hiddenLayer.neurons();
    var connectionList = [];

    for (var i = 0; i < connections; i++) {
      // connect two random neurons
      var from = Math.random() * neurons.length | 0;
      var to = Math.random() * neurons.length | 0;
      var connection = neurons[from].project(neurons[to]);
      connectionList.push(connection);
    }

    for (var j = 0; j < gates; j++) {
      // pick a random gater neuron
      var gater = Math.random() * neurons.length | 0;
      // pick a random connection to gate
      var connection = Math.random() * connectionList.length | 0;
      // let the gater gate the connection
      neurons[gater].gate(connectionList[connection]);
    }

    // connect the layers
    inputLayer.project(hiddenLayer);
    hiddenLayer.project(outputLayer);

    // set the layers of the network
    this.set({
      input: inputLayer,
      hidden: [hiddenLayer],
      output: outputLayer
    });

    // trainer
    this.trainer = new Trainer(this);
  },

  Hopfield: function Hopfield(size) {

    var inputLayer = new Layer(size);
    var outputLayer = new Layer(size);

    inputLayer.project(outputLayer, Layer.connectionType.ALL_TO_ALL);

    this.set({
      input: inputLayer,
      hidden: [],
      output: outputLayer
    });

    var trainer = new Trainer(this);

    var proto = Architect.Hopfield.prototype;

    proto.learn = proto.learn || function(patterns)
    {
      var set = [];
      for (var p in patterns)
        set.push({
          input: patterns[p],
          output: patterns[p]
        });

      return trainer.train(set, {
        iterations: 500000,
        error: .00005,
        rate: 1
      });
    };

    proto.feed = proto.feed || function(pattern)
    {
      var output = this.activate(pattern);

      var pattern = [];
      for (var i in output)
        pattern[i] = output[i] > .5 ? 1 : 0;

      return pattern;
    }
  }
}

// Extend prototype chain (so every architectures is an instance of Network)
for (var architecture in Architect) {
  Architect[architecture].prototype = new Network();
  Architect[architecture].prototype.constructor = Architect[architecture];
}

// export
if (module) module.exports = Architect;
},{"./layer":63,"./network":64,"./trainer":67}],63:[function(require,module,exports){
// export
if (module) module.exports = Layer;

// import
var Neuron  = require('./neuron')
,   Network = require('./network')

/*******************************************************************************************
                                            LAYER
*******************************************************************************************/

function Layer(size, label) {
  this.size = size | 0;
  this.list = [];
  this.label = label || null;
  this.connectedTo = [];

  while (size--) {
    var neuron = new Neuron();
    this.list.push(neuron);
  }
}

Layer.prototype = {

  // activates all the neurons in the layer
  activate: function(input) {

    var activations = [];

    if (typeof input != 'undefined') {
      if (input.length != this.size)
        throw new Error("INPUT size and LAYER size must be the same to activate!");

      for (var id in this.list) {
        var neuron = this.list[id];
        var activation = neuron.activate(input[id]);
        activations.push(activation);
      }
    } else {
      for (var id in this.list) {
        var neuron = this.list[id];
        var activation = neuron.activate();
        activations.push(activation);
      }
    }
    return activations;
  },

  // propagates the error on all the neurons of the layer
  propagate: function(rate, target) {

    if (typeof target != 'undefined') {
      if (target.length != this.size)
        throw new Error("TARGET size and LAYER size must be the same to propagate!");

      for (var id = this.list.length - 1; id >= 0; id--) {
        var neuron = this.list[id];
        neuron.propagate(rate, target[id]);
      }
    } else {
      for (var id = this.list.length - 1; id >= 0; id--) {
        var neuron = this.list[id];
        neuron.propagate(rate);
      }
    }
  },

  // projects a connection from this layer to another one
  project: function(layer, type, weights) {

    if (layer instanceof Network)
      layer = layer.layers.input;

    if (layer instanceof Layer) {
      if (!this.connected(layer))
        return new Layer.connection(this, layer, type, weights);
    } else
      throw new Error("Invalid argument, you can only project connections to LAYERS and NETWORKS!");


  },

  // gates a connection betwenn two layers
  gate: function(connection, type) {

    if (type == Layer.gateType.INPUT) {
      if (connection.to.size != this.size)
        throw new Error("GATER layer and CONNECTION.TO layer must be the same size in order to gate!");

      for (var id in connection.to.list) {
        var neuron = connection.to.list[id];
        var gater = this.list[id];
        for (var input in neuron.connections.inputs) {
          var gated = neuron.connections.inputs[input];
          if (gated.ID in connection.connections)
            gater.gate(gated);
        }
      }
    } else if (type == Layer.gateType.OUTPUT) {
      if (connection.from.size != this.size)
        throw new Error("GATER layer and CONNECTION.FROM layer must be the same size in order to gate!");

      for (var id in connection.from.list) {
        var neuron = connection.from.list[id];
        var gater = this.list[id];
        for (var projected in neuron.connections.projected) {
          var gated = neuron.connections.projected[projected];
          if (gated.ID in connection.connections)
            gater.gate(gated);
        }
      }
    } else if (type == Layer.gateType.ONE_TO_ONE) {
      if (connection.size != this.size)
        throw new Error("The number of GATER UNITS must be the same as the number of CONNECTIONS to gate!");

      for (var id in connection.list) {
        var gater = this.list[id];
        var gated = connection.list[id];
        gater.gate(gated);
      }
    }
    connection.gatedfrom.push({layer: this, type: type});
  },

  // true or false whether the whole layer is self-connected or not
  selfconnected: function() {

    for (var id in this.list) {
      var neuron = this.list[id];
      if (!neuron.selfconnected())
        return false;
    }
    return true;
  },

  // true of false whether the layer is connected to another layer (parameter) or not
  connected: function(layer) {
    // Check if ALL to ALL connection
    var connections = 0;
    for (var here in this.list) {
      for (var there in layer.list) {
        var from = this.list[here];
        var to = layer.list[there];
        var connected = from.connected(to);
        if (connected.type == 'projected')
          connections++;
      }
    }
    if (connections == this.size * layer.size)
      return Layer.connectionType.ALL_TO_ALL;

    // Check if ONE to ONE connection
    connections = 0;
    for (var neuron in this.list) {
      var from = this.list[neuron];
      var to = layer.list[neuron];
      var connected = from.connected(to);
      if (connected.type == 'projected')
        connections++;
    }
    if (connections == this.size)
      return Layer.connectionType.ONE_TO_ONE;
  },

  // clears all the neuorns in the layer
  clear: function() {
    for (var id in this.list) {
      var neuron = this.list[id];
      neuron.clear();
    }
  },

  // resets all the neurons in the layer
  reset: function() {
    for (var id in this.list) {
      var neuron = this.list[id];
      neuron.reset();
    }
  },

  // returns all the neurons in the layer (array)
  neurons: function() {
    return this.list;
  },

  // adds a neuron to the layer
  add: function(neuron) {
    this.neurons[neuron.ID] = neuron || new Neuron();
    this.list.push(neuron);
    this.size++;
  },

  set: function(options) {
    options = options || {};

    for (var i in this.list) {
      var neuron = this.list[i];
      if (options.label)
        neuron.label = options.label + '_' + neuron.ID;
      if (options.squash)
        neuron.squash = options.squash;
      if (options.bias)
        neuron.bias = options.bias;
    }
    return this;
  }
}

// represents a connection from one layer to another, and keeps track of its weight and gain
Layer.connection = function LayerConnection(fromLayer, toLayer, type, weights) {
  this.ID = Layer.connection.uid();
  this.from = fromLayer;
  this.to = toLayer;
  this.selfconnection = toLayer == fromLayer;
  this.type = type;
  this.connections = {};
  this.list = [];
  this.size = 0;
  this.gatedfrom = [];

  if (typeof this.type == 'undefined')
  {
    if (fromLayer == toLayer)
      this.type = Layer.connectionType.ONE_TO_ONE;
    else
      this.type = Layer.connectionType.ALL_TO_ALL;
  }

  if (this.type == Layer.connectionType.ALL_TO_ALL ||
      this.type == Layer.connectionType.ALL_TO_ELSE) {
    for (var here in this.from.list) {
      for (var there in this.to.list) {
        var from = this.from.list[here];
        var to = this.to.list[there];
        if(this.type == Layer.connectionType.ALL_TO_ELSE && from == to)
          continue;
        var connection = from.project(to, weights);

        this.connections[connection.ID] = connection;
        this.size = this.list.push(connection);
      }
    }
  } else if (this.type == Layer.connectionType.ONE_TO_ONE) {

    for (var neuron in this.from.list) {
      var from = this.from.list[neuron];
      var to = this.to.list[neuron];
      var connection = from.project(to, weights);

      this.connections[connection.ID] = connection;
      this.size = this.list.push(connection);
    }
  }

  fromLayer.connectedTo.push(this);
}

// types of connections
Layer.connectionType = {};
Layer.connectionType.ALL_TO_ALL = "ALL TO ALL";
Layer.connectionType.ONE_TO_ONE = "ONE TO ONE";
Layer.connectionType.ALL_TO_ELSE = "ALL TO ELSE";

// types of gates
Layer.gateType = {};
Layer.gateType.INPUT = "INPUT";
Layer.gateType.OUTPUT = "OUTPUT";
Layer.gateType.ONE_TO_ONE = "ONE TO ONE";

(function() {
  var connections = 0;
  Layer.connection.uid = function() {
    return connections++;
  }
})();

},{"./network":64,"./neuron":65}],64:[function(require,module,exports){
// export
if (module) module.exports = Network;

// import
var Neuron  = require('./neuron')
,   Layer   = require('./layer')
,   Trainer = require('./trainer')

/*******************************************************************************************
                                         NETWORK
*******************************************************************************************/

function Network(layers) {
  if (typeof layers != 'undefined') {
    this.layers = layers || {
      input: null,
      hidden: {},
      output: null
    };
    this.optimized = null;
  }
}
Network.prototype = {

  // feed-forward activation of all the layers to produce an ouput
  activate: function(input) {

    if (this.optimized === false)
    {
      this.layers.input.activate(input);
      for (var layer in this.layers.hidden)
        this.layers.hidden[layer].activate();
      return this.layers.output.activate();
    }
    else
    {
      if (this.optimized == null)
        this.optimize();
      return this.optimized.activate(input);
    }
  },

  // back-propagate the error thru the network
  propagate: function(rate, target) {

    if (this.optimized === false)
    {
      this.layers.output.propagate(rate, target);
      var reverse = [];
      for (var layer in this.layers.hidden)
        reverse.push(this.layers.hidden[layer]);
      reverse.reverse();
      for (var layer in reverse)
        reverse[layer].propagate(rate);
    }
    else
    {
      if (this.optimized == null)
        this.optimize();
      this.optimized.propagate(rate, target);
    }
  },

  // project a connection to another unit (either a network or a layer)
  project: function(unit, type, weights) {

    if (this.optimized)
      this.optimized.reset();

    if (unit instanceof Network)
      return this.layers.output.project(unit.layers.input, type, weights);

    if (unit instanceof Layer)
      return this.layers.output.project(unit, type, weights);

    throw new Error("Invalid argument, you can only project connections to LAYERS and NETWORKS!");
  },

  // let this network gate a connection
  gate: function(connection, type) {
    if (this.optimized)
      this.optimized.reset();
    this.layers.output.gate(connection, type);
  },

  // clear all elegibility traces and extended elegibility traces (the network forgets its context, but not what was trained)
  clear: function() {

    this.restore();

    var inputLayer = this.layers.input,
      outputLayer = this.layers.output;

    inputLayer.clear();
    for (var layer in this.layers.hidden) {
      var hiddenLayer = this.layers.hidden[layer];
      hiddenLayer.clear();
    }
    outputLayer.clear();

    if (this.optimized)
      this.optimized.reset();
  },

  // reset all weights and clear all traces (ends up like a new network)
  reset: function() {

    this.restore();

    var inputLayer = this.layers.input,
      outputLayer = this.layers.output;

    inputLayer.reset();
    for (var layer in this.layers.hidden) {
      var hiddenLayer = this.layers.hidden[layer];
      hiddenLayer.reset();
    }
    outputLayer.reset();

    if (this.optimized)
      this.optimized.reset();
  },

  // hardcodes the behaviour of the whole network into a single optimized function
  optimize: function() {

    var that = this;
    var optimized = {};
    var neurons = this.neurons();

    for (var i in neurons) {
      var neuron = neurons[i].neuron;
      var layer = neurons[i].layer;
      while (neuron.neuron)
        neuron = neuron.neuron;
      optimized = neuron.optimize(optimized, layer);
    }
    for (var i in optimized.propagation_sentences)
      optimized.propagation_sentences[i].reverse();
    optimized.propagation_sentences.reverse();

    var hardcode = "";
    hardcode += "var F = Float64Array ? new Float64Array(" + optimized.memory +
      ") : []; ";
    for (var i in optimized.variables)
      hardcode += "F[" + optimized.variables[i].id + "] = " + (optimized.variables[
        i].value || 0) + "; ";
    hardcode += "var activate = function(input){\n";
    for (var i in optimized.inputs)
      hardcode += "F[" + optimized.inputs[i] + "] = input[" + i + "]; ";
    for (var currentLayer in optimized.activation_sentences) {
      if (optimized.activation_sentences[currentLayer].length > 0) {
        for (var currentNeuron in optimized.activation_sentences[currentLayer]) {
          hardcode += optimized.activation_sentences[currentLayer][currentNeuron].join(" ");
          hardcode += optimized.trace_sentences[currentLayer][currentNeuron].join(" ");
        }
      }
    }
    hardcode += " var output = []; "
    for (var i in optimized.outputs)
      hardcode += "output[" + i + "] = F[" + optimized.outputs[i] + "]; ";
    hardcode += "return output; }; "
    hardcode += "var propagate = function(rate, target){\n";
    hardcode += "F[" + optimized.variables.rate.id + "] = rate; ";
    for (var i in optimized.targets)
      hardcode += "F[" + optimized.targets[i] + "] = target[" + i + "]; ";
    for (var currentLayer in optimized.propagation_sentences)
      for (var currentNeuron in optimized.propagation_sentences[currentLayer])
        hardcode += optimized.propagation_sentences[currentLayer][currentNeuron].join(" ") + " ";
    hardcode += " };\n";
    hardcode +=
      "var ownership = function(memoryBuffer){\nF = memoryBuffer;\nthis.memory = F;\n};\n";
    hardcode +=
      "return {\nmemory: F,\nactivate: activate,\npropagate: propagate,\nownership: ownership\n};";
    hardcode = hardcode.split(";").join(";\n");

    var constructor = new Function(hardcode);

    var network = constructor();
    network.data = {
      variables: optimized.variables,
      activate: optimized.activation_sentences,
      propagate: optimized.propagation_sentences,
      trace: optimized.trace_sentences,
      inputs: optimized.inputs,
      outputs: optimized.outputs,
      check_activation: this.activate,
      check_propagation: this.propagate
    }

    network.reset = function() {
      if (that.optimized) {
        that.optimized = null;
        that.activate = network.data.check_activation;
        that.propagate = network.data.check_propagation;
      }
    }

    this.optimized = network;
    this.activate = network.activate;
    this.propagate = network.propagate;
  },

  // restores all the values from the optimized network the their respective objects in order to manipulate the network
  restore: function() {
    if (!this.optimized)
      return;

    var optimized = this.optimized;

    var getValue = function() {
      var args = Array.prototype.slice.call(arguments);

      var unit = args.shift();
      var prop = args.pop();

      var id = prop + '_';
      for (var property in args)
        id += args[property] + '_';
      id += unit.ID;

      var memory = optimized.memory;
      var variables = optimized.data.variables;

      if (id in variables)
        return memory[variables[id].id];
      return 0;
    }

    var list = this.neurons();

    // link id's to positions in the array
    var ids = {};
    for (var i in list) {
      var neuron = list[i].neuron;
      while (neuron.neuron)
        neuron = neuron.neuron;

      neuron.state = getValue(neuron, 'state');
      neuron.old = getValue(neuron, 'old');
      neuron.activation = getValue(neuron, 'activation');
      neuron.bias = getValue(neuron, 'bias');

      for (var input in neuron.trace.elegibility)
        neuron.trace.elegibility[input] = getValue(neuron, 'trace',
          'elegibility', input);

      for (var gated in neuron.trace.extended)
        for (var input in neuron.trace.extended[gated])
          neuron.trace.extended[gated][input] = getValue(neuron, 'trace',
            'extended', gated, input);
    }

    // get connections
    for (var i in list) {
      var neuron = list[i].neuron;
      while (neuron.neuron)
        neuron = neuron.neuron;

      for (var j in neuron.connections.projected) {
        var connection = neuron.connections.projected[j];
        connection.weight = getValue(connection, 'weight');
        connection.gain = getValue(connection, 'gain');
      }
    }
  },

  // returns all the neurons in the network
  neurons: function() {

    var neurons = [];

    var inputLayer = this.layers.input.neurons(),
      outputLayer = this.layers.output.neurons();

    for (var neuron in inputLayer)
      neurons.push({
        neuron: inputLayer[neuron],
        layer: 'input'
      });

    for (var layer in this.layers.hidden) {
      var hiddenLayer = this.layers.hidden[layer].neurons();
      for (var neuron in hiddenLayer)
        neurons.push({
          neuron: hiddenLayer[neuron],
          layer: layer
        });
    }
    for (var neuron in outputLayer)
      neurons.push({
        neuron: outputLayer[neuron],
        layer: 'output'
      });

    return neurons;
  },

  // returns number of inputs of the network
  inputs: function() {
    return this.layers.input.size;
  },

  // returns number of outputs of hte network
  outputs: function() {
    return this.layers.output.size;
  },

  // sets the layers of the network
  set: function(layers) {

    this.layers = layers;
    if (this.optimized)
      this.optimized.reset();
  },

  setOptimize: function(bool){
    this.restore();
    if (this.optimized)
      this.optimized.reset();
    this.optimized = bool? null : false;
  },

  // returns a json that represents all the neurons and connections of the network
  toJSON: function(ignoreTraces) {

    this.restore();

    var list = this.neurons();
    var neurons = [];
    var connections = [];

    // link id's to positions in the array
    var ids = {};
    for (var i in list) {
      var neuron = list[i].neuron;
      while (neuron.neuron)
        neuron = neuron.neuron;
      ids[neuron.ID] = i;

      var copy = {
        trace: {
          elegibility: {},
          extended: {}
        },
        state: neuron.state,
        old: neuron.old,
        activation: neuron.activation,
        bias: neuron.bias,
        layer: list[i].layer
      };

      copy.squash = neuron.squash == Neuron.squash.LOGISTIC ? "LOGISTIC" :
        neuron.squash == Neuron.squash.TANH ? "TANH" :
        neuron.squash == Neuron.squash.IDENTITY ? "IDENTITY" :
        neuron.squash == Neuron.squash.HLIM ? "HLIM" :
        null;

      neurons.push(copy);
    }

    // get connections
    for (var i in list) {
      var neuron = list[i].neuron;
      while (neuron.neuron)
        neuron = neuron.neuron;

      for (var j in neuron.connections.projected) {
        var connection = neuron.connections.projected[j];
        connections.push({
          from: ids[connection.from.ID],
          to: ids[connection.to.ID],
          weight: connection.weight,
          gater: connection.gater ? ids[connection.gater.ID] : null,
        });
      }
      if (neuron.selfconnected())
        connections.push({
          from: ids[neuron.ID],
          to: ids[neuron.ID],
          weight: neuron.selfconnection.weight,
          gater: neuron.selfconnection.gater ? ids[neuron.selfconnection.gater.ID] : null,
        });
    }

    return {
      neurons: neurons,
      connections: connections
    }
  },

  // export the topology into dot language which can be visualized as graphs using dot
  /* example: ... console.log(net.toDotLang());
              $ node example.js > example.dot
              $ dot example.dot -Tpng > out.png
  */
  toDot: function(edgeConnection) {
    if (! typeof edgeConnection)
      edgeConnection = false;
    var code = "digraph nn {\n    rankdir = BT\n";
    var layers = [this.layers.input].concat(this.layers.hidden, this.layers.output);
    for (var layer in layers) {
      for (var to in layers[layer].connectedTo) { // projections
        var connection = layers[layer].connectedTo[to];
        var layerTo = connection.to;
        var size = connection.size;
        var layerID = layers.indexOf(layers[layer]);
        var layerToID = layers.indexOf(layerTo);
        /* http://stackoverflow.com/questions/26845540/connect-edges-with-graph-dot
         * DOT does not support edge-to-edge connections
         * This workaround produces somewhat weird graphs ...
        */
        if ( edgeConnection) {
          if (connection.gatedfrom.length) {
            var fakeNode = "fake" + layerID + "_" + layerToID;
            code += "    " + fakeNode +
              " [label = \"\", shape = point, width = 0.01, height = 0.01]\n";
            code += "    " + layerID + " -> " + fakeNode + " [label = " + size + ", arrowhead = none]\n";
            code += "    " + fakeNode + " -> " + layerToID + "\n";
          } else
            code += "    " + layerID + " -> " + layerToID + " [label = " + size + "]\n";
          for (var from in connection.gatedfrom) { // gatings
            var layerfrom = connection.gatedfrom[from].layer;
            var layerfromID = layers.indexOf(layerfrom);
            code += "    " + layerfromID + " -> " + fakeNode + " [color = blue]\n";
          }
        } else {
          code += "    " + layerID + " -> " + layerToID + " [label = " + size + "]\n";
          for (var from in connection.gatedfrom) { // gatings
            var layerfrom = connection.gatedfrom[from].layer;
            var layerfromID = layers.indexOf(layerfrom);
            code += "    " + layerfromID + " -> " + layerToID + " [color = blue]\n";
          }
        }
      }
    }
    code += "}\n";
    return {
      code: code,
      link: "https://chart.googleapis.com/chart?chl=" + escape(code.replace("/ /g", "+")) + "&cht=gv"
    }
  },

  // returns a function that works as the activation of the network and can be used without depending on the library
  standalone: function() {
    if (!this.optimized)
      this.optimize();

    var data = this.optimized.data;

    // build activation function
    var activation = "function (input) {\n";

    // build inputs
    for (var i in data.inputs)
      activation += "F[" + data.inputs[i] + "] = input[" + i + "];\n";

    // build network activation
    for (var neuron in data.activate) { // shouldn't this be layer?
      for (var sentence in data.activate[neuron])
        activation += data.activate[neuron][sentence].join('') + "\n";
    }

    // build outputs
    activation += "var output = [];\n";
    for (var i in data.outputs)
      activation += "output[" + i + "] = F[" + data.outputs[i] + "];\n";
    activation += "return output;\n}";

    // reference all the positions in memory
    var memory = activation.match(/F\[(\d+)\]/g);
    var dimension = 0;
    var ids = {};
    for (var address in memory) {
      var tmp = memory[address].match(/\d+/)[0];
      if (!(tmp in ids)) {
        ids[tmp] = dimension++;
      }
    }
    var hardcode = "F = {\n";
    for (var i in ids)
      hardcode += ids[i] + ": " + this.optimized.memory[i] + ",\n";
    hardcode = hardcode.substring(0, hardcode.length - 2) + "\n};\n";
    hardcode = "var run = " + activation.replace(/F\[(\d+)]/g, function(
      index) {
      return 'F[' + ids[index.match(/\d+/)[0]] + ']'
    }).replace("{\n", "{\n" + hardcode + "") + ";\n";
    hardcode += "return run";

    // return standalone function
    return new Function(hardcode)();
  },


  // Return a HTML5 WebWorker specialized on training the network stored in `memory`.
  // Train based on the given dataSet and options.
  // The worker returns the updated `memory` when done.
  worker: function(memory, set, options) {

    // Copy the options and set defaults (options might be different for each worker)
    var workerOptions = {};
    if(options) workerOptions = options;
    workerOptions.rate = options.rate || .2;
    workerOptions.iterations = options.iterations || 100000;
    workerOptions.error = options.error || .005;
    workerOptions.cost = options.cost || null;
    workerOptions.crossValidate = options.crossValidate || null;

    // Cost function might be different for each worker
    costFunction = "var cost = " + (options && options.cost || this.cost || Trainer.cost.MSE) + ";\n";
    var workerFunction = Network.getWorkerSharedFunctions();
    workerFunction = workerFunction.replace(/var cost = options && options\.cost \|\| this\.cost \|\| Trainer\.cost\.MSE;/g, costFunction);

    // Set what we do when training is finished
    workerFunction = workerFunction.replace('return results;',
                      'postMessage({action: "done", message: results, memoryBuffer: F}, [F.buffer]);');

    // Replace log with postmessage
    workerFunction = workerFunction.replace("console.log('iterations', iterations, 'error', error, 'rate', currentRate)",
              "postMessage({action: 'log', message: {\n" +
                  "iterations: iterations,\n" +
                  "error: error,\n" +
                  "rate: currentRate\n" +
                "}\n" +
              "})");

    // Replace schedule with postmessage
    workerFunction = workerFunction.replace("abort = this.schedule.do({ error: error, iterations: iterations, rate: currentRate })",
              "postMessage({action: 'schedule', message: {\n" +
                  "iterations: iterations,\n" +
                  "error: error,\n" +
                  "rate: currentRate\n" +
                "}\n" +
              "})");

    if (!this.optimized)
      this.optimize();

    var hardcode = "var inputs = " + this.optimized.data.inputs.length + ";\n";
    hardcode += "var outputs = " + this.optimized.data.outputs.length + ";\n";
    hardcode += "var F =  new Float64Array([" + this.optimized.memory.toString() + "]);\n";
    hardcode += "var activate = " + this.optimized.activate.toString() + ";\n";
    hardcode += "var propagate = " + this.optimized.propagate.toString() + ";\n";
    hardcode +=
        "onmessage = function(e) {\n" +
          "if (e.data.action == 'startTraining') {\n" +
            "train(" + JSON.stringify(set) + "," + JSON.stringify(workerOptions) + ");\n" +
          "}\n" +
        "}";

    var workerSourceCode = workerFunction + '\n' + hardcode;
    var blob = new Blob([workerSourceCode]);
    var blobURL = window.URL.createObjectURL(blob);

    return new Worker(blobURL);
  },

  // returns a copy of the network
  clone: function() {
    return Network.fromJSON(this.toJSON());
  }
};

/**
 * Creates a static String to store the source code of the functions
 *  that are identical for all the workers (train, _trainSet, test)
 *
 * @return {String} Source code that can train a network inside a worker.
 * @static
 */
Network.getWorkerSharedFunctions = function() {
  // If we already computed the source code for the shared functions
  if(typeof Network._SHARED_WORKER_FUNCTIONS !== 'undefined')
    return Network._SHARED_WORKER_FUNCTIONS;

  // Otherwise compute and return the source code
  // We compute them by simply copying the source code of the train, _trainSet and test functions
  //  using the .toString() method

  // Load and name the train function
  var train_f = Trainer.prototype.train.toString();
  train_f = train_f.replace('function (set', 'function train(set') + '\n';

  // Load and name the _trainSet function
  var _trainSet_f = Trainer.prototype._trainSet.toString().replace(/this.network./g, '');
  _trainSet_f = _trainSet_f.replace('function (set', 'function _trainSet(set') + '\n';
  _trainSet_f = _trainSet_f.replace('this.crossValidate', 'crossValidate');
  _trainSet_f = _trainSet_f.replace('crossValidate = true', 'crossValidate = { }');

  // Load and name the test function
  var test_f = Trainer.prototype.test.toString().replace(/this.network./g, '');
  test_f = test_f.replace('function (set', 'function test(set') + '\n';

  return Network._SHARED_WORKER_FUNCTIONS = train_f + _trainSet_f + test_f;
};

// rebuild a network that has been stored in a json using the method toJSON()
Network.fromJSON = function(json) {

  var neurons = [];

  var layers = {
    input: new Layer(),
    hidden: [],
    output: new Layer()
  };

  for (var i in json.neurons) {
    var config = json.neurons[i];

    var neuron = new Neuron();
    neuron.trace.elegibility = {};
    neuron.trace.extended = {};
    neuron.state = config.state;
    neuron.old = config.old;
    neuron.activation = config.activation;
    neuron.bias = config.bias;
    neuron.squash = config.squash in Neuron.squash ? Neuron.squash[config.squash] : Neuron.squash.LOGISTIC;
    neurons.push(neuron);

    if (config.layer == 'input')
      layers.input.add(neuron);
    else if (config.layer == 'output')
      layers.output.add(neuron);
    else {
      if (typeof layers.hidden[config.layer] == 'undefined')
        layers.hidden[config.layer] = new Layer();
      layers.hidden[config.layer].add(neuron);
    }
  }

  for (var i in json.connections) {
    var config = json.connections[i];
    var from = neurons[config.from];
    var to = neurons[config.to];
    var weight = config.weight;
    var gater = neurons[config.gater];

    var connection = from.project(to, weight);
    if (gater)
      gater.gate(connection);
  }

  return new Network(layers);
};

},{"./layer":63,"./neuron":65,"./trainer":67}],65:[function(require,module,exports){
// export
if (module) module.exports = Neuron;

/******************************************************************************************
                                         NEURON
*******************************************************************************************/

function Neuron() {
  this.ID = Neuron.uid();
  this.label = null;
  this.connections = {
    inputs: {},
    projected: {},
    gated: {}
  };
  this.error = {
    responsibility: 0,
    projected: 0,
    gated: 0
  };
  this.trace = {
    elegibility: {},
    extended: {},
    influences: {}
  };
  this.state = 0;
  this.old = 0;
  this.activation = 0;
  this.selfconnection = new Neuron.connection(this, this, 0); // weight = 0 -> not connected
  this.squash = Neuron.squash.LOGISTIC;
  this.neighboors = {};
  this.bias = Math.random() * .2 - .1;
}

Neuron.prototype = {

  // activate the neuron
  activate: function(input) {
    // activation from enviroment (for input neurons)
    if (typeof input != 'undefined') {
      this.activation = input;
      this.derivative = 0;
      this.bias = 0;
      return this.activation;
    }

    // old state
    this.old = this.state;

    // eq. 15
    this.state = this.selfconnection.gain * this.selfconnection.weight *
      this.state + this.bias;

    for (var i in this.connections.inputs) {
      var input = this.connections.inputs[i];
      this.state += input.from.activation * input.weight * input.gain;
    }

    // eq. 16
    this.activation = this.squash(this.state);

    // f'(s)
    this.derivative = this.squash(this.state, true);

    // update traces
    var influences = [];
    for (var id in this.trace.extended) {
      // extended elegibility trace
      var neuron = this.neighboors[id];

      // if gated neuron's selfconnection is gated by this unit, the influence keeps track of the neuron's old state
      var influence = neuron.selfconnection.gater == this ? neuron.old : 0;

      // index runs over all the incoming connections to the gated neuron that are gated by this unit
      for (var incoming in this.trace.influences[neuron.ID]) { // captures the effect that has an input connection to this unit, on a neuron that is gated by this unit
        influence += this.trace.influences[neuron.ID][incoming].weight *
          this.trace.influences[neuron.ID][incoming].from.activation;
      }
      influences[neuron.ID] = influence;
    }

    for (var i in this.connections.inputs) {
      var input = this.connections.inputs[i];

      // elegibility trace - Eq. 17
      this.trace.elegibility[input.ID] = this.selfconnection.gain * this.selfconnection
        .weight * this.trace.elegibility[input.ID] + input.gain * input.from
        .activation;

      for (var id in this.trace.extended) {
        // extended elegibility trace
        var xtrace = this.trace.extended[id];
        var neuron = this.neighboors[id];
        var influence = influences[neuron.ID];

        // eq. 18
        xtrace[input.ID] = neuron.selfconnection.gain * neuron.selfconnection
          .weight * xtrace[input.ID] + this.derivative * this.trace.elegibility[
            input.ID] * influence;
      }
    }

    //  update gated connection's gains
    for (var connection in this.connections.gated) {
      this.connections.gated[connection].gain = this.activation;
    }

    return this.activation;
  },

  // back-propagate the error
  propagate: function(rate, target) {
    // error accumulator
    var error = 0;

    // whether or not this neuron is in the output layer
    var isOutput = typeof target != 'undefined';

    // output neurons get their error from the enviroment
    if (isOutput)
      this.error.responsibility = this.error.projected = target - this.activation; // Eq. 10

    else // the rest of the neuron compute their error responsibilities by backpropagation
    {
      // error responsibilities from all the connections projected from this neuron
      for (var id in this.connections.projected) {
        var connection = this.connections.projected[id];
        var neuron = connection.to;
        // Eq. 21
        error += neuron.error.responsibility * connection.gain * connection.weight;
      }

      // projected error responsibility
      this.error.projected = this.derivative * error;

      error = 0;
      // error responsibilities from all the connections gated by this neuron
      for (var id in this.trace.extended) {
        var neuron = this.neighboors[id]; // gated neuron
        var influence = neuron.selfconnection.gater == this ? neuron.old : 0; // if gated neuron's selfconnection is gated by this neuron

        // index runs over all the connections to the gated neuron that are gated by this neuron
        for (var input in this.trace.influences[id]) { // captures the effect that the input connection of this neuron have, on a neuron which its input/s is/are gated by this neuron
          influence += this.trace.influences[id][input].weight * this.trace.influences[
            neuron.ID][input].from.activation;
        }
        // eq. 22
        error += neuron.error.responsibility * influence;
      }

      // gated error responsibility
      this.error.gated = this.derivative * error;

      // error responsibility - Eq. 23
      this.error.responsibility = this.error.projected + this.error.gated;
    }

    // learning rate
    rate = rate || .1;

    // adjust all the neuron's incoming connections
    for (var id in this.connections.inputs) {
      var input = this.connections.inputs[id];

      // Eq. 24
      var gradient = this.error.projected * this.trace.elegibility[input.ID];
      for (var id in this.trace.extended) {
        var neuron = this.neighboors[id];
        gradient += neuron.error.responsibility * this.trace.extended[
          neuron.ID][input.ID];
      }
      input.weight += rate * gradient; // adjust weights - aka learn
    }

    // adjust bias
    this.bias += rate * this.error.responsibility;
  },

  project: function(neuron, weight) {
    // self-connection
    if (neuron == this) {
      this.selfconnection.weight = 1;
      return this.selfconnection;
    }

    // check if connection already exists
    var connected = this.connected(neuron);
    if (connected && connected.type == "projected") {
      // update connection
      if (typeof weight != 'undefined')
        connected.connection.weight = weight;
      // return existing connection
      return connected.connection;
    } else {
      // create a new connection
      var connection = new Neuron.connection(this, neuron, weight);
    }

    // reference all the connections and traces
    this.connections.projected[connection.ID] = connection;
    this.neighboors[neuron.ID] = neuron;
    neuron.connections.inputs[connection.ID] = connection;
    neuron.trace.elegibility[connection.ID] = 0;

    for (var id in neuron.trace.extended) {
      var trace = neuron.trace.extended[id];
      trace[connection.ID] = 0;
    }

    return connection;
  },

  gate: function(connection) {
    // add connection to gated list
    this.connections.gated[connection.ID] = connection;

    var neuron = connection.to;
    if (!(neuron.ID in this.trace.extended)) {
      // extended trace
      this.neighboors[neuron.ID] = neuron;
      var xtrace = this.trace.extended[neuron.ID] = {};
      for (var id in this.connections.inputs) {
        var input = this.connections.inputs[id];
        xtrace[input.ID] = 0;
      }
    }

    // keep track
    if (neuron.ID in this.trace.influences)
      this.trace.influences[neuron.ID].push(connection);
    else
      this.trace.influences[neuron.ID] = [connection];

    // set gater
    connection.gater = this;
  },

  // returns true or false whether the neuron is self-connected or not
  selfconnected: function() {
    return this.selfconnection.weight !== 0;
  },

  // returns true or false whether the neuron is connected to another neuron (parameter)
  connected: function(neuron) {
    var result = {
      type: null,
      connection: false
    };

    if (this == neuron) {
      if (this.selfconnected()) {
        result.type = 'selfconnection';
        result.connection = this.selfconnection;
        return result;
      } else
        return false;
    }

    for (var type in this.connections) {
      for (var connection in this.connections[type]) {
        var connection = this.connections[type][connection];
        if (connection.to == neuron) {
          result.type = type;
          result.connection = connection;
          return result;
        } else if (connection.from == neuron) {
          result.type = type;
          result.connection = connection;
          return result;
        }
      }
    }

    return false;
  },

  // clears all the traces (the neuron forgets it's context, but the connections remain intact)
  clear: function() {

    for (var trace in this.trace.elegibility)
      this.trace.elegibility[trace] = 0;

    for (var trace in this.trace.extended)
      for (var extended in this.trace.extended[trace])
        this.trace.extended[trace][extended] = 0;

    this.error.responsibility = this.error.projected = this.error.gated = 0;
  },

  // all the connections are randomized and the traces are cleared
  reset: function() {
    this.clear();

    for (var type in this.connections)
      for (var connection in this.connections[type])
        this.connections[type][connection].weight = Math.random() * .2 - .1;
    this.bias = Math.random() * .2 - .1;

    this.old = this.state = this.activation = 0;
  },

  // hardcodes the behaviour of the neuron into an optimized function
  optimize: function(optimized, layer) {

    optimized = optimized || {};
    var store_activation = [];
    var store_trace = [];
    var store_propagation = [];
    var varID = optimized.memory || 0;
    var neurons = optimized.neurons || 1;
    var inputs = optimized.inputs || [];
    var targets = optimized.targets || [];
    var outputs = optimized.outputs || [];
    var variables = optimized.variables || {};
    var activation_sentences = optimized.activation_sentences || [];
    var trace_sentences = optimized.trace_sentences || [];
    var propagation_sentences = optimized.propagation_sentences || [];
    var layers = optimized.layers || { __count: 0, __neuron: 0 };

    // allocate sentences
    var allocate = function(store){
      var allocated = layer in layers && store[layers.__count];
      if (!allocated)
      {
        layers.__count = store.push([]) - 1;
        layers[layer] = layers.__count;
      }
    };
    allocate(activation_sentences);
    allocate(trace_sentences);
    allocate(propagation_sentences);
    var currentLayer = layers.__count;

    // get/reserve space in memory by creating a unique ID for a variablel
    var getVar = function() {
      var args = Array.prototype.slice.call(arguments);

      if (args.length == 1) {
        if (args[0] == 'target') {
          var id = 'target_' + targets.length;
          targets.push(varID);
        } else
          var id = args[0];
        if (id in variables)
          return variables[id];
        return variables[id] = {
          value: 0,
          id: varID++
        };
      } else {
        var extended = args.length > 2;
        if (extended)
          var value = args.pop();

        var unit = args.shift();
        var prop = args.pop();

        if (!extended)
          var value = unit[prop];

        var id = prop + '_';
        for (var property in args)
          id += args[property] + '_';
        id += unit.ID;
        if (id in variables)
          return variables[id];

        return variables[id] = {
          value: value,
          id: varID++
        };
      }
    };

    // build sentence
    var buildSentence = function() {
      var args = Array.prototype.slice.call(arguments);
      var store = args.pop();
      var sentence = "";
      for (var i in args)
        if (typeof args[i] == 'string')
          sentence += args[i];
        else
          sentence += 'F[' + args[i].id + ']';

      store.push(sentence + ';');
    };

    // helper to check if an object is empty
    var isEmpty = function(obj) {
      for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
          return false;
      }
      return true;
    };

    // characteristics of the neuron
    var noProjections = isEmpty(this.connections.projected);
    var noGates = isEmpty(this.connections.gated);
    var isInput = layer == 'input' ? true : isEmpty(this.connections.inputs);
    var isOutput = layer == 'output' ? true : noProjections && noGates;

    // optimize neuron's behaviour
    var rate = getVar('rate');
    var activation = getVar(this, 'activation');
    if (isInput)
      inputs.push(activation.id);
    else {
      activation_sentences[currentLayer].push(store_activation);
      trace_sentences[currentLayer].push(store_trace);
      propagation_sentences[currentLayer].push(store_propagation);
      var old = getVar(this, 'old');
      var state = getVar(this, 'state');
      var bias = getVar(this, 'bias');
      if (this.selfconnection.gater)
        var self_gain = getVar(this.selfconnection, 'gain');
      if (this.selfconnected())
        var self_weight = getVar(this.selfconnection, 'weight');
      buildSentence(old, ' = ', state, store_activation);
      if (this.selfconnected())
        if (this.selfconnection.gater)
          buildSentence(state, ' = ', self_gain, ' * ', self_weight, ' * ',
            state, ' + ', bias, store_activation);
        else
          buildSentence(state, ' = ', self_weight, ' * ', state, ' + ',
            bias, store_activation);
      else
        buildSentence(state, ' = ', bias, store_activation);
      for (var i in this.connections.inputs) {
        var input = this.connections.inputs[i];
        var input_activation = getVar(input.from, 'activation');
        var input_weight = getVar(input, 'weight');
        if (input.gater)
          var input_gain = getVar(input, 'gain');
        if (this.connections.inputs[i].gater)
          buildSentence(state, ' += ', input_activation, ' * ',
            input_weight, ' * ', input_gain, store_activation);
        else
          buildSentence(state, ' += ', input_activation, ' * ',
            input_weight, store_activation);
      }
      var derivative = getVar(this, 'derivative');
      switch (this.squash) {
        case Neuron.squash.LOGISTIC:
          buildSentence(activation, ' = (1 / (1 + Math.exp(-', state, ')))',
            store_activation);
          buildSentence(derivative, ' = ', activation, ' * (1 - ',
            activation, ')', store_activation);
          break;
        case Neuron.squash.TANH:
          var eP = getVar('aux');
          var eN = getVar('aux_2');
          buildSentence(eP, ' = Math.exp(', state, ')', store_activation);
          buildSentence(eN, ' = 1 / ', eP, store_activation);
          buildSentence(activation, ' = (', eP, ' - ', eN, ') / (', eP, ' + ', eN, ')', store_activation);
          buildSentence(derivative, ' = 1 - (', activation, ' * ', activation, ')', store_activation);
          break;
        case Neuron.squash.IDENTITY:
          buildSentence(activation, ' = ', state, store_activation);
          buildSentence(derivative, ' = 1', store_activation);
          break;
        case Neuron.squash.HLIM:
          buildSentence(activation, ' = +(', state, ' > 0)', store_activation);
          buildSentence(derivative, ' = 1', store_activation);
        case Neuron.squash.RELU:
          buildSentence(activation, ' = ', state, ' > 0 ? ', state, ' : 0', store_activation);
          buildSentence(derivative, ' = ', state, ' > 0 ? 1 : 0', store_activation);
          break;
      }

      for (var id in this.trace.extended) {
        // calculate extended elegibility traces in advance

        var neuron = this.neighboors[id];
        var influence = getVar('influences[' + neuron.ID + ']');
        var neuron_old = getVar(neuron, 'old');
        var initialized = false;
        if (neuron.selfconnection.gater == this)
        {
          buildSentence(influence, ' = ', neuron_old, store_trace);
          initialized = true;
        }
        for (var incoming in this.trace.influences[neuron.ID]) {
          var incoming_weight = getVar(this.trace.influences[neuron.ID]
            [incoming], 'weight');
          var incoming_activation = getVar(this.trace.influences[neuron.ID]
            [incoming].from, 'activation');

          if (initialized)
            buildSentence(influence, ' += ', incoming_weight, ' * ', incoming_activation, store_trace);
          else {
            buildSentence(influence, ' = ', incoming_weight, ' * ', incoming_activation, store_trace);
            initialized = true;
          }
        }
      }

      for (var i in this.connections.inputs) {
        var input = this.connections.inputs[i];
        if (input.gater)
          var input_gain = getVar(input, 'gain');
        var input_activation = getVar(input.from, 'activation');
        var trace = getVar(this, 'trace', 'elegibility', input.ID, this.trace
          .elegibility[input.ID]);
        if (this.selfconnected()) {
          if (this.selfconnection.gater) {
            if (input.gater)
              buildSentence(trace, ' = ', self_gain, ' * ', self_weight,
                ' * ', trace, ' + ', input_gain, ' * ', input_activation,
                store_trace);
            else
              buildSentence(trace, ' = ', self_gain, ' * ', self_weight,
                ' * ', trace, ' + ', input_activation, store_trace);
          } else {
            if (input.gater)
              buildSentence(trace, ' = ', self_weight, ' * ', trace, ' + ',
                input_gain, ' * ', input_activation, store_trace);
            else
              buildSentence(trace, ' = ', self_weight, ' * ', trace, ' + ',
                input_activation, store_trace);
          }
        } else {
          if (input.gater)
            buildSentence(trace, ' = ', input_gain, ' * ', input_activation,
              store_trace);
          else
            buildSentence(trace, ' = ', input_activation, store_trace);
        }
        for (var id in this.trace.extended) {
          // extended elegibility trace
          var neuron = this.neighboors[id];
          var influence = getVar('influences[' + neuron.ID + ']');

          var trace = getVar(this, 'trace', 'elegibility', input.ID, this.trace
            .elegibility[input.ID]);
          var xtrace = getVar(this, 'trace', 'extended', neuron.ID, input.ID,
            this.trace.extended[neuron.ID][input.ID]);
          if (neuron.selfconnected())
            var neuron_self_weight = getVar(neuron.selfconnection, 'weight');
          if (neuron.selfconnection.gater)
            var neuron_self_gain = getVar(neuron.selfconnection, 'gain');
          if (neuron.selfconnected())
            if (neuron.selfconnection.gater)
              buildSentence(xtrace, ' = ', neuron_self_gain, ' * ',
                neuron_self_weight, ' * ', xtrace, ' + ', derivative, ' * ',
                trace, ' * ', influence, store_trace);
            else
              buildSentence(xtrace, ' = ', neuron_self_weight, ' * ',
                xtrace, ' + ', derivative, ' * ', trace, ' * ',
                influence, store_trace);
          else
            buildSentence(xtrace, ' = ', derivative, ' * ', trace, ' * ',
              influence, store_trace);
        }
      }
      for (var connection in this.connections.gated) {
        var gated_gain = getVar(this.connections.gated[connection], 'gain');
        buildSentence(gated_gain, ' = ', activation, store_activation);
      }
    }
    if (!isInput) {
      var responsibility = getVar(this, 'error', 'responsibility', this.error
        .responsibility);
      if (isOutput) {
        var target = getVar('target');
        buildSentence(responsibility, ' = ', target, ' - ', activation,
          store_propagation);
        for (var id in this.connections.inputs) {
          var input = this.connections.inputs[id];
          var trace = getVar(this, 'trace', 'elegibility', input.ID, this.trace
            .elegibility[input.ID]);
          var input_weight = getVar(input, 'weight');
          buildSentence(input_weight, ' += ', rate, ' * (', responsibility,
            ' * ', trace, ')', store_propagation);
        }
        outputs.push(activation.id);
      } else {
        if (!noProjections && !noGates) {
          var error = getVar('aux');
          for (var id in this.connections.projected) {
            var connection = this.connections.projected[id];
            var neuron = connection.to;
            var connection_weight = getVar(connection, 'weight');
            var neuron_responsibility = getVar(neuron, 'error',
              'responsibility', neuron.error.responsibility);
            if (connection.gater) {
              var connection_gain = getVar(connection, 'gain');
              buildSentence(error, ' += ', neuron_responsibility, ' * ',
                connection_gain, ' * ', connection_weight,
                store_propagation);
            } else
              buildSentence(error, ' += ', neuron_responsibility, ' * ',
                connection_weight, store_propagation);
          }
          var projected = getVar(this, 'error', 'projected', this.error.projected);
          buildSentence(projected, ' = ', derivative, ' * ', error,
            store_propagation);
          buildSentence(error, ' = 0', store_propagation);
          for (var id in this.trace.extended) {
            var neuron = this.neighboors[id];
            var influence = getVar('aux_2');
            var neuron_old = getVar(neuron, 'old');
            if (neuron.selfconnection.gater == this)
              buildSentence(influence, ' = ', neuron_old, store_propagation);
            else
              buildSentence(influence, ' = 0', store_propagation);
            for (var input in this.trace.influences[neuron.ID]) {
              var connection = this.trace.influences[neuron.ID][input];
              var connection_weight = getVar(connection, 'weight');
              var neuron_activation = getVar(connection.from, 'activation');
              buildSentence(influence, ' += ', connection_weight, ' * ',
                neuron_activation, store_propagation);
            }
            var neuron_responsibility = getVar(neuron, 'error',
              'responsibility', neuron.error.responsibility);
            buildSentence(error, ' += ', neuron_responsibility, ' * ',
              influence, store_propagation);
          }
          var gated = getVar(this, 'error', 'gated', this.error.gated);
          buildSentence(gated, ' = ', derivative, ' * ', error,
            store_propagation);
          buildSentence(responsibility, ' = ', projected, ' + ', gated,
            store_propagation);
          for (var id in this.connections.inputs) {
            var input = this.connections.inputs[id];
            var gradient = getVar('aux');
            var trace = getVar(this, 'trace', 'elegibility', input.ID, this
              .trace.elegibility[input.ID]);
            buildSentence(gradient, ' = ', projected, ' * ', trace,
              store_propagation);
            for (var id in this.trace.extended) {
              var neuron = this.neighboors[id];
              var neuron_responsibility = getVar(neuron, 'error',
                'responsibility', neuron.error.responsibility);
              var xtrace = getVar(this, 'trace', 'extended', neuron.ID,
                input.ID, this.trace.extended[neuron.ID][input.ID]);
              buildSentence(gradient, ' += ', neuron_responsibility, ' * ',
                xtrace, store_propagation);
            }
            var input_weight = getVar(input, 'weight');
            buildSentence(input_weight, ' += ', rate, ' * ', gradient,
              store_propagation);
          }

        } else if (noGates) {
          buildSentence(responsibility, ' = 0', store_propagation);
          for (var id in this.connections.projected) {
            var connection = this.connections.projected[id];
            var neuron = connection.to;
            var connection_weight = getVar(connection, 'weight');
            var neuron_responsibility = getVar(neuron, 'error',
              'responsibility', neuron.error.responsibility);
            if (connection.gater) {
              var connection_gain = getVar(connection, 'gain');
              buildSentence(responsibility, ' += ', neuron_responsibility,
                ' * ', connection_gain, ' * ', connection_weight,
                store_propagation);
            } else
              buildSentence(responsibility, ' += ', neuron_responsibility,
                ' * ', connection_weight, store_propagation);
          }
          buildSentence(responsibility, ' *= ', derivative,
            store_propagation);
          for (var id in this.connections.inputs) {
            var input = this.connections.inputs[id];
            var trace = getVar(this, 'trace', 'elegibility', input.ID, this
              .trace.elegibility[input.ID]);
            var input_weight = getVar(input, 'weight');
            buildSentence(input_weight, ' += ', rate, ' * (',
              responsibility, ' * ', trace, ')', store_propagation);
          }
        } else if (noProjections) {
          buildSentence(responsibility, ' = 0', store_propagation);
          for (var id in this.trace.extended) {
            var neuron = this.neighboors[id];
            var influence = getVar('aux');
            var neuron_old = getVar(neuron, 'old');
            if (neuron.selfconnection.gater == this)
              buildSentence(influence, ' = ', neuron_old, store_propagation);
            else
              buildSentence(influence, ' = 0', store_propagation);
            for (var input in this.trace.influences[neuron.ID]) {
              var connection = this.trace.influences[neuron.ID][input];
              var connection_weight = getVar(connection, 'weight');
              var neuron_activation = getVar(connection.from, 'activation');
              buildSentence(influence, ' += ', connection_weight, ' * ',
                neuron_activation, store_propagation);
            }
            var neuron_responsibility = getVar(neuron, 'error',
              'responsibility', neuron.error.responsibility);
            buildSentence(responsibility, ' += ', neuron_responsibility,
              ' * ', influence, store_propagation);
          }
          buildSentence(responsibility, ' *= ', derivative,
            store_propagation);
          for (var id in this.connections.inputs) {
            var input = this.connections.inputs[id];
            var gradient = getVar('aux');
            buildSentence(gradient, ' = 0', store_propagation);
            for (var id in this.trace.extended) {
              var neuron = this.neighboors[id];
              var neuron_responsibility = getVar(neuron, 'error',
                'responsibility', neuron.error.responsibility);
              var xtrace = getVar(this, 'trace', 'extended', neuron.ID,
                input.ID, this.trace.extended[neuron.ID][input.ID]);
              buildSentence(gradient, ' += ', neuron_responsibility, ' * ',
                xtrace, store_propagation);
            }
            var input_weight = getVar(input, 'weight');
            buildSentence(input_weight, ' += ', rate, ' * ', gradient,
              store_propagation);
          }
        }
      }
      buildSentence(bias, ' += ', rate, ' * ', responsibility,
        store_propagation);
    }
    return {
      memory: varID,
      neurons: neurons + 1,
      inputs: inputs,
      outputs: outputs,
      targets: targets,
      variables: variables,
      activation_sentences: activation_sentences,
      trace_sentences: trace_sentences,
      propagation_sentences: propagation_sentences,
      layers: layers
    }
  }
}


// represents a connection between two neurons
Neuron.connection = function Connection(from, to, weight) {

  if (!from || !to)
    throw new Error("Connection Error: Invalid neurons");

  this.ID = Neuron.connection.uid();
  this.from = from;
  this.to = to;
  this.weight = typeof weight == 'undefined' ? Math.random() * .2 - .1 :
    weight;
  this.gain = 1;
  this.gater = null;
}


// squashing functions
Neuron.squash = {};

// eq. 5 & 5'
Neuron.squash.LOGISTIC = function(x, derivate) {
  if (!derivate)
    return 1 / (1 + Math.exp(-x));
  var fx = Neuron.squash.LOGISTIC(x);
  return fx * (1 - fx);
};
Neuron.squash.TANH = function(x, derivate) {
  if (derivate)
    return 1 - Math.pow(Neuron.squash.TANH(x), 2);
  var eP = Math.exp(x);
  var eN = 1 / eP;
  return (eP - eN) / (eP + eN);
};
Neuron.squash.IDENTITY = function(x, derivate) {
  return derivate ? 1 : x;
};
Neuron.squash.HLIM = function(x, derivate) {
  return derivate ? 1 : x > 0 ? 1 : 0;
};
Neuron.squash.RELU = function(x, derivate) {
  if (derivate)
    return x > 0 ? 1 : 0;
  return x > 0 ? x : 0;
};

// unique ID's
(function() {
  var neurons = 0;
  var connections = 0;
  Neuron.uid = function() {
    return neurons++;
  }
  Neuron.connection.uid = function() {
    return connections++;
  }
  Neuron.quantity = function() {
    return {
      neurons: neurons,
      connections: connections
    }
  }
})();

},{}],66:[function(require,module,exports){
var Synaptic = {
    Neuron: require('./neuron'),
    Layer: require('./layer'),
    Network: require('./network'),
    Trainer: require('./trainer'),
    Architect: require('./architect')
};

// CommonJS & AMD
if (typeof define !== 'undefined' && define.amd)
{
  define([], function(){ return Synaptic });
}

// Node.js
if (typeof module !== 'undefined' && module.exports)
{
  module.exports = Synaptic;
}

// Browser
if (typeof window == 'object')
{
  (function(){
    var oldSynaptic = window['synaptic'];
    Synaptic.ninja = function(){
      window['synaptic'] = oldSynaptic;
      return Synaptic;
    };
  })();

  window['synaptic'] = Synaptic;
}

},{"./architect":62,"./layer":63,"./network":64,"./neuron":65,"./trainer":67}],67:[function(require,module,exports){
// export
if (module) module.exports = Trainer;

/*******************************************************************************************
                                        TRAINER
*******************************************************************************************/

function Trainer(network, options) {
  options = options || {};
  this.network = network;
  this.rate = options.rate || .2;
  this.iterations = options.iterations || 100000;
  this.error = options.error || .005;
  this.cost = options.cost || null;
  this.crossValidate = options.crossValidate || null;
}

Trainer.prototype = {

  // trains any given set to a network
  train: function(set, options) {

    var error = 1;
    var iterations = bucketSize = 0;
    var abort = false;
    var currentRate;
    var cost = options && options.cost || this.cost || Trainer.cost.MSE;
    var crossValidate = false, testSet, trainSet;

    var start = Date.now();

    if (options) {
      if (options.shuffle) {
        //+ Jonas Raoni Soares Silva
        //@ http://jsfromhell.com/array/shuffle [v1.0]
        function shuffle(o) { //v1.0
          for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
          return o;
        };
      }
      if (options.iterations)
        this.iterations = options.iterations;
      if (options.error)
        this.error = options.error;
      if (options.rate)
        this.rate = options.rate;
      if (options.cost)
        this.cost = options.cost;
      if (options.schedule)
        this.schedule = options.schedule;
      if (options.customLog){
        // for backward compatibility with code that used customLog
        console.log('Deprecated: use schedule instead of customLog')
        this.schedule = options.customLog;
      }
      if (this.crossValidate || options.crossValidate) {
        if(!this.crossValidate) this.crossValidate = {};
        crossValidate = true;
        if (options.crossValidate.testSize)
          this.crossValidate.testSize = options.crossValidate.testSize;
        if (options.crossValidate.testError)
          this.crossValidate.testError = options.crossValidate.testError;
      }
    }

    currentRate = this.rate;
    if(Array.isArray(this.rate)) {
      var bucketSize = Math.floor(this.iterations / this.rate.length);
    }

    if(crossValidate) {
      var numTrain = Math.ceil((1 - this.crossValidate.testSize) * set.length);
      trainSet = set.slice(0, numTrain);
      testSet = set.slice(numTrain);
    }

    while ((!abort && iterations < this.iterations && error > this.error)) {
      if (crossValidate && error <= this.crossValidate.testError) {
        break;
      }

      var currentSetSize = set.length;
      error = 0;
      iterations++;

      if(bucketSize > 0) {
        var currentBucket = Math.floor(iterations / bucketSize);
        currentRate = this.rate[currentBucket] || currentRate;
      }
      
      if(typeof this.rate === 'function') {
        currentRate = this.rate(iterations, error);
      }

      if (crossValidate) {
        this._trainSet(trainSet, currentRate, cost);
        error += this.test(testSet).error;
        currentSetSize = 1;
      } else {
        error += this._trainSet(set, currentRate, cost);
        currentSetSize = set.length;
      }

      // check error
      error /= currentSetSize;

      if (options) {
        if (this.schedule && this.schedule.every && iterations %
          this.schedule.every == 0)
          abort = this.schedule.do({ error: error, iterations: iterations, rate: currentRate });
        else if (options.log && iterations % options.log == 0) {
          console.log('iterations', iterations, 'error', error, 'rate', currentRate);
        };
        if (options.shuffle)
          shuffle(set);
      }
    }

    var results = {
      error: error,
      iterations: iterations,
      time: Date.now() - start
    };

    return results;
  },

  // trains any given set to a network, using a WebWorker (only for the browser). Returns a Promise of the results.
  trainAsync: function(set, options) {
    var train = this.workerTrain.bind(this);
    return new Promise(function(resolve, reject) {
      try {
        train(set, resolve, options, true)
      } catch(e) {
        reject(e)
      }
    })
  },

  // preforms one training epoch and returns the error (private function used in this.train)
  _trainSet: function(set, currentRate, costFunction) {
    var errorSum = 0;
    for (var train in set) {
      var input = set[train].input;
      var target = set[train].output;

      var output = this.network.activate(input);
      this.network.propagate(currentRate, target);

      errorSum += costFunction(target, output);
    }
    return errorSum;
  },

  // tests a set and returns the error and elapsed time
  test: function(set, options) {

    var error = 0;
    var input, output, target;
    var cost = options && options.cost || this.cost || Trainer.cost.MSE;

    var start = Date.now();

    for (var test in set) {
      input = set[test].input;
      target = set[test].output;
      output = this.network.activate(input);
      error += cost(target, output);
    }

    error /= set.length;

    var results = {
      error: error,
      time: Date.now() - start
    };

    return results;
  },

  // trains any given set to a network using a WebWorker [deprecated: use trainAsync instead]
  workerTrain: function(set, callback, options, suppressWarning) {

    if (!suppressWarning) {
      console.warn('Deprecated: do not use `workerTrain`, use `trainAsync` instead.')
    }
    var that = this;

    if (!this.network.optimized)
      this.network.optimize();

    // Create a new worker
    var worker = this.network.worker(this.network.optimized.memory, set, options);

    // train the worker
    worker.onmessage = function(e) {
      switch(e.data.action) {
          case 'done':
            var iterations = e.data.message.iterations;
            var error = e.data.message.error;
            var time = e.data.message.time;

            that.network.optimized.ownership(e.data.memoryBuffer);

            // Done callback
            callback({
              error: error,
              iterations: iterations,
              time: time
            });

            // Delete the worker and all its associated memory
            worker.terminate();
          break;

          case 'log':
            console.log(e.data.message);

          case 'schedule':
            if (options && options.schedule && typeof options.schedule.do === 'function') {
              var scheduled = options.schedule.do
              scheduled(e.data.message)
            }
          break;
      }
    };

    // Start the worker
    worker.postMessage({action: 'startTraining'});
  },

  // trains an XOR to the network
  XOR: function(options) {

    if (this.network.inputs() != 2 || this.network.outputs() != 1)
      throw new Error("Incompatible network (2 inputs, 1 output)");

    var defaults = {
      iterations: 100000,
      log: false,
      shuffle: true,
      cost: Trainer.cost.MSE
    };

    if (options)
      for (var i in options)
        defaults[i] = options[i];

    return this.train([{
      input: [0, 0],
      output: [0]
    }, {
      input: [1, 0],
      output: [1]
    }, {
      input: [0, 1],
      output: [1]
    }, {
      input: [1, 1],
      output: [0]
    }], defaults);
  },

  // trains the network to pass a Distracted Sequence Recall test
  DSR: function(options) {
    options = options || {};

    var targets = options.targets || [2, 4, 7, 8];
    var distractors = options.distractors || [3, 5, 6, 9];
    var prompts = options.prompts || [0, 1];
    var length = options.length || 24;
    var criterion = options.success || 0.95;
    var iterations = options.iterations || 100000;
    var rate = options.rate || .1;
    var log = options.log || 0;
    var schedule = options.schedule || {};
    var cost = options.cost || this.cost || Trainer.cost.CROSS_ENTROPY;

    var trial, correct, i, j, success;
    trial = correct = i = j = success = 0;
    var error = 1,
      symbols = targets.length + distractors.length + prompts.length;

    var noRepeat = function(range, avoid) {
      var number = Math.random() * range | 0;
      var used = false;
      for (var i in avoid)
        if (number == avoid[i])
          used = true;
      return used ? noRepeat(range, avoid) : number;
    };

    var equal = function(prediction, output) {
      for (var i in prediction)
        if (Math.round(prediction[i]) != output[i])
          return false;
      return true;
    };

    var start = Date.now();

    while (trial < iterations && (success < criterion || trial % 1000 != 0)) {
      // generate sequence
      var sequence = [],
        sequenceLength = length - prompts.length;
      for (i = 0; i < sequenceLength; i++) {
        var any = Math.random() * distractors.length | 0;
        sequence.push(distractors[any]);
      }
      var indexes = [],
        positions = [];
      for (i = 0; i < prompts.length; i++) {
        indexes.push(Math.random() * targets.length | 0);
        positions.push(noRepeat(sequenceLength, positions));
      }
      positions = positions.sort();
      for (i = 0; i < prompts.length; i++) {
        sequence[positions[i]] = targets[indexes[i]];
        sequence.push(prompts[i]);
      }

      //train sequence
      var distractorsCorrect;
      var targetsCorrect = distractorsCorrect = 0;
      error = 0;
      for (i = 0; i < length; i++) {
        // generate input from sequence
        var input = [];
        for (j = 0; j < symbols; j++)
          input[j] = 0;
        input[sequence[i]] = 1;

        // generate target output
        var output = [];
        for (j = 0; j < targets.length; j++)
          output[j] = 0;

        if (i >= sequenceLength) {
          var index = i - sequenceLength;
          output[indexes[index]] = 1;
        }

        // check result
        var prediction = this.network.activate(input);

        if (equal(prediction, output))
          if (i < sequenceLength)
            distractorsCorrect++;
          else
            targetsCorrect++;
        else {
          this.network.propagate(rate, output);
        }

        error += cost(output, prediction);

        if (distractorsCorrect + targetsCorrect == length)
          correct++;
      }

      // calculate error
      if (trial % 1000 == 0)
        correct = 0;
      trial++;
      var divideError = trial % 1000;
      divideError = divideError == 0 ? 1000 : divideError;
      success = correct / divideError;
      error /= length;

      // log
      if (log && trial % log == 0)
        console.log("iterations:", trial, " success:", success, " correct:",
          correct, " time:", Date.now() - start, " error:", error);
      if (schedule.do && schedule.every && trial % schedule.every == 0)
        schedule.do({
          iterations: trial,
          success: success,
          error: error,
          time: Date.now() - start,
          correct: correct
        });
    }

    return {
      iterations: trial,
      success: success,
      error: error,
      time: Date.now() - start
    }
  },

  // train the network to learn an Embeded Reber Grammar
  ERG: function(options) {

    options = options || {};
    var iterations = options.iterations || 150000;
    var criterion = options.error || .05;
    var rate = options.rate || .1;
    var log = options.log || 500;
    var cost = options.cost || this.cost || Trainer.cost.CROSS_ENTROPY;

    // gramar node
    var Node = function() {
      this.paths = [];
    };
    Node.prototype = {
      connect: function(node, value) {
        this.paths.push({
          node: node,
          value: value
        });
        return this;
      },
      any: function() {
        if (this.paths.length == 0)
          return false;
        var index = Math.random() * this.paths.length | 0;
        return this.paths[index];
      },
      test: function(value) {
        for (var i in this.paths)
          if (this.paths[i].value == value)
            return this.paths[i];
        return false;
      }
    };

    var reberGrammar = function() {

      // build a reber grammar
      var output = new Node();
      var n1 = (new Node()).connect(output, "E");
      var n2 = (new Node()).connect(n1, "S");
      var n3 = (new Node()).connect(n1, "V").connect(n2, "P");
      var n4 = (new Node()).connect(n2, "X");
      n4.connect(n4, "S");
      var n5 = (new Node()).connect(n3, "V");
      n5.connect(n5, "T");
      n2.connect(n5, "X");
      var n6 = (new Node()).connect(n4, "T").connect(n5, "P");
      var input = (new Node()).connect(n6, "B");

      return {
        input: input,
        output: output
      }
    };

    // build an embeded reber grammar
    var embededReberGrammar = function() {
      var reber1 = reberGrammar();
      var reber2 = reberGrammar();

      var output = new Node();
      var n1 = (new Node).connect(output, "E");
      reber1.output.connect(n1, "T");
      reber2.output.connect(n1, "P");
      var n2 = (new Node).connect(reber1.input, "P").connect(reber2.input,
        "T");
      var input = (new Node).connect(n2, "B");

      return {
        input: input,
        output: output
      }

    };

    // generate an ERG sequence
    var generate = function() {
      var node = embededReberGrammar().input;
      var next = node.any();
      var str = "";
      while (next) {
        str += next.value;
        next = next.node.any();
      }
      return str;
    };

    // test if a string matches an embeded reber grammar
    var test = function(str) {
      var node = embededReberGrammar().input;
      var i = 0;
      var ch = str.charAt(i);
      while (i < str.length) {
        var next = node.test(ch);
        if (!next)
          return false;
        node = next.node;
        ch = str.charAt(++i);
      }
      return true;
    };

    // helper to check if the output and the target vectors match
    var different = function(array1, array2) {
      var max1 = 0;
      var i1 = -1;
      var max2 = 0;
      var i2 = -1;
      for (var i in array1) {
        if (array1[i] > max1) {
          max1 = array1[i];
          i1 = i;
        }
        if (array2[i] > max2) {
          max2 = array2[i];
          i2 = i;
        }
      }

      return i1 != i2;
    };

    var iteration = 0;
    var error = 1;
    var table = {
      "B": 0,
      "P": 1,
      "T": 2,
      "X": 3,
      "S": 4,
      "E": 5
    };

    var start = Date.now();
    while (iteration < iterations && error > criterion) {
      var i = 0;
      error = 0;

      // ERG sequence to learn
      var sequence = generate();

      // input
      var read = sequence.charAt(i);
      // target
      var predict = sequence.charAt(i + 1);

      // train
      while (i < sequence.length - 1) {
        var input = [];
        var target = [];
        for (var j = 0; j < 6; j++) {
          input[j] = 0;
          target[j] = 0;
        }
        input[table[read]] = 1;
        target[table[predict]] = 1;

        var output = this.network.activate(input);

        if (different(output, target))
          this.network.propagate(rate, target);

        read = sequence.charAt(++i);
        predict = sequence.charAt(i + 1);

        error += cost(target, output);
      }
      error /= sequence.length;
      iteration++;
      if (iteration % log == 0) {
        console.log("iterations:", iteration, " time:", Date.now() - start,
          " error:", error);
      }
    }

    return {
      iterations: iteration,
      error: error,
      time: Date.now() - start,
      test: test,
      generate: generate
    }
  },

  timingTask: function(options){

    if (this.network.inputs() != 2 || this.network.outputs() != 1)
      throw new Error("Invalid Network: must have 2 inputs and one output");

    if (typeof options == 'undefined')
      options = {};

    // helper
    function getSamples (trainingSize, testSize){

      // sample size
      var size = trainingSize + testSize;

      // generate samples
      var t = 0;
      var set = [];
      for (var i = 0; i < size; i++) {
        set.push({ input: [0,0], output: [0] });
      }
      while(t < size - 20) {
          var n = Math.round(Math.random() * 20);
          set[t].input[0] = 1;
          for (var j = t; j <= t + n; j++){
              set[j].input[1] = n / 20;
              set[j].output[0] = 0.5;
          }
          t += n;
          n = Math.round(Math.random() * 20);
          for (var k = t+1; k <= (t + n) &&  k < size; k++)
              set[k].input[1] = set[t].input[1];
          t += n;
      }

      // separate samples between train and test sets
      var trainingSet = []; var testSet = [];
      for (var l = 0; l < size; l++)
          (l < trainingSize ? trainingSet : testSet).push(set[l]);

      // return samples
      return {
          train: trainingSet,
          test: testSet
      }
    }

    var iterations = options.iterations || 200;
    var error = options.error || .005;
    var rate = options.rate || [.03, .02];
    var log = options.log === false ? false : options.log || 10;
    var cost = options.cost || this.cost || Trainer.cost.MSE;
    var trainingSamples = options.trainSamples || 7000;
    var testSamples = options.trainSamples || 1000;

    // samples for training and testing
    var samples = getSamples(trainingSamples, testSamples);

    // train
    var result = this.train(samples.train, {
      rate: rate,
      log: log,
      iterations: iterations,
      error: error,
      cost: cost
    });

    return {
      train: result,
      test: this.test(samples.test)
    }
  }
};

// Built-in cost functions
Trainer.cost = {
  // Eq. 9
  CROSS_ENTROPY: function(target, output)
  {
    var crossentropy = 0;
    for (var i in output)
      crossentropy -= (target[i] * Math.log(output[i]+1e-15)) + ((1-target[i]) * Math.log((1+1e-15)-output[i])); // +1e-15 is a tiny push away to avoid Math.log(0)
    return crossentropy;
  },
  MSE: function(target, output)
  {
    var mse = 0;
    for (var i in output)
      mse += Math.pow(target[i] - output[i], 2);
    return mse / output.length;
  },
  BINARY: function(target, output){
    var misses = 0;
    for (var i in output)
      misses += Math.round(target[i] * 2) != Math.round(output[i] * 2);
    return misses;
  }
}

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJhcHAvYXBwLmpzIiwiYXBwL2pzL0Nycy5qcyIsIm5vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvYXJyYXkvZnJvbS5qcyIsIm5vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIm5vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2suanMiLCJub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwibm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvaGVscGVycy90b0NvbnN1bWFibGVBcnJheS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vYXJyYXkvZnJvbS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L2RlZmluZS1wcm9wZXJ0eS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYS1mdW5jdGlvbi5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fYW4tb2JqZWN0LmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19hcnJheS1pbmNsdWRlcy5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY2xhc3NvZi5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fY29mLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jb3JlLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19jcmVhdGUtcHJvcGVydHkuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2N0eC5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZGVmaW5lZC5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZGVzY3JpcHRvcnMuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2RvbS1jcmVhdGUuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2VudW0tYnVnLWtleXMuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2V4cG9ydC5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fZmFpbHMuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2dsb2JhbC5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faGFzLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19oaWRlLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19odG1sLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pZTgtZG9tLWRlZmluZS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faW9iamVjdC5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXMtYXJyYXktaXRlci5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXMtb2JqZWN0LmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pdGVyLWNhbGwuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2l0ZXItY3JlYXRlLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19pdGVyLWRlZmluZS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9faXRlci1kZXRlY3QuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX2l0ZXJhdG9ycy5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fbGlicmFyeS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWNyZWF0ZS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWRwLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZHBzLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtZ3BvLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3Qta2V5cy1pbnRlcm5hbC5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LWtleXMuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3Byb3BlcnR5LWRlc2MuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3JlZGVmaW5lLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zZXQtdG8tc3RyaW5nLXRhZy5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2hhcmVkLWtleS5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fc2hhcmVkLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19zdHJpbmctYXQuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLWluZGV4LmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1pbnRlZ2VyLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1pb2JqZWN0LmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL190by1sZW5ndGguanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3RvLW9iamVjdC5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fdG8tcHJpbWl0aXZlLmpzIiwibm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL191aWQuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvX3drcy5qcyIsIm5vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9jb3JlLmdldC1pdGVyYXRvci1tZXRob2QuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2LmFycmF5LmZyb20uanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2Lm9iamVjdC5kZWZpbmUtcHJvcGVydHkuanMiLCJub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM2LnN0cmluZy5pdGVyYXRvci5qcyIsIm5vZGVfbW9kdWxlcy9zeW5hcHRpYy9zcmMvYXJjaGl0ZWN0LmpzIiwibm9kZV9tb2R1bGVzL3N5bmFwdGljL3NyYy9sYXllci5qcyIsIm5vZGVfbW9kdWxlcy9zeW5hcHRpYy9zcmMvbmV0d29yay5qcyIsIm5vZGVfbW9kdWxlcy9zeW5hcHRpYy9zcmMvbmV1cm9uLmpzIiwibm9kZV9tb2R1bGVzL3N5bmFwdGljL3NyYy9zeW5hcHRpYy5qcyIsIm5vZGVfbW9kdWxlcy9zeW5hcHRpYy9zcmMvdHJhaW5lci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlZQTs7QUNBQTs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTs7QUNGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0pBO0FBQ0E7QUFDQTtBQUNBOztBQ0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDSkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0pBO0FBQ0E7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0pBO0FBQ0E7QUFDQTtBQUNBOztBQ0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ05BO0FBQ0E7QUFDQTtBQUNBOztBQ0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzVEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNOQTtBQUNBO0FBQ0E7QUFDQTs7QUNIQTtBQUNBO0FBQ0E7QUFDQTs7QUNIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1BBOztBQ0FBO0FBQ0E7QUFDQTs7QUNGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUEE7QUFDQTtBQUNBOztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BCQTs7QUNBQTs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDekNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDaEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUEE7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNyQ0E7QUFDQTtBQUNBOztBQ0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDaEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcFJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3JvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1eEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgX0NycyA9IHJlcXVpcmUoJy4vanMvQ3JzJyk7XG5cbnZhciBfQ3JzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0Nycyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbm5ldyBfQ3JzMi5kZWZhdWx0KHtcblx0J2ZpbGVVcGxvYWQnOiAnZmlsZS1sb2FkJ1xufSk7IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3RvQ29uc3VtYWJsZUFycmF5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy90b0NvbnN1bWFibGVBcnJheScpO1xuXG52YXIgX3RvQ29uc3VtYWJsZUFycmF5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3RvQ29uc3VtYWJsZUFycmF5Mik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3N5bmFwdGljID0gcmVxdWlyZSgnc3luYXB0aWMnKTtcblxudmFyIF9zeW5hcHRpYzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zeW5hcHRpYyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBDcnMgPSBmdW5jdGlvbiAoKSB7XG5cdGZ1bmN0aW9uIENycyhjZmcpIHtcblx0XHQoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBDcnMpO1xuXG5cdFx0dGhpcy5jZmcgPSBPYmplY3QuYXNzaWduKHtcblx0XHRcdCdmaWxlVXBsb2FkJzogJ2ZpbGUtbG9hZCcsXG5cdFx0XHQnY29udHJhc3RUcmVzaG9sZCc6IDEwMFxuXHRcdH0sIGNmZyk7XG5cdFx0dGhpcy5sZXR0ZXJzQ29vcmRzID0gW107XG5cdFx0dGhpcy5zeW5hcHRpYyA9IF9zeW5hcHRpYzIuZGVmYXVsdDtcblx0XHR0aGlzLmZpbGVJbnB1dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKHRoaXMuY2ZnLmZpbGVVcGxvYWQpO1xuXG5cdFx0dGhpcy5jYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcblx0XHR0aGlzLmN0eCA9IHRoaXMuY2FudmFzLmdldENvbnRleHQoJzJkJyk7XG5cdFx0dGhpcy5jYW52YXMuaWQgPSAnb3JpZ2luYWwnO1xuXG5cdFx0dGhpcy5wcm9jZXNzQ2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XG5cdFx0dGhpcy5wcm9jZXNzQ3R4ID0gdGhpcy5wcm9jZXNzQ2FudmFzLmdldENvbnRleHQoJzJkJyk7XG5cdFx0dGhpcy5wcm9jZXNzQ2FudmFzLmlkID0gJ3Rlc3QnO1xuXG5cdFx0dGhpcy5pbWFnZVByb0NhbnZhcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2NhbnZhcycpO1xuXHRcdHRoaXMuaW1hZ2VQcm9DdHggPSB0aGlzLmltYWdlUHJvQ2FudmFzLmdldENvbnRleHQoJzJkJyk7XG5cdFx0dGhpcy5pbWFnZVByb0NhbnZhcy5pZCA9ICdwcm9jZXNzZWQtaW1hZ2UnO1xuXG5cdFx0dGhpcy5yZWFkSW1hZ2VzKCk7XG5cdFx0dGhpcy5idWlsZENhbnZhcygpO1xuXG5cdFx0dGhpcy5maWxlSW5wdXQucGFyZW50RWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLmNhbnZhcyk7XG5cdFx0dGhpcy5maWxlSW5wdXQucGFyZW50RWxlbWVudC5hcHBlbmRDaGlsZCh0aGlzLnByb2Nlc3NDYW52YXMpO1xuXHRcdHRoaXMuZmlsZUlucHV0LnBhcmVudEVsZW1lbnQuYXBwZW5kQ2hpbGQodGhpcy5pbWFnZVByb0NhbnZhcyk7XG5cdH1cblxuXHQoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShDcnMsIFt7XG5cdFx0a2V5OiAnYnVpbGRDYW52YXMnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBidWlsZENhbnZhcyhiYXNlU2l4KSB7XG5cdFx0XHR2YXIgX3RoaXMgPSB0aGlzO1xuXG5cdFx0XHR2YXIgaW1nID0gbmV3IEltYWdlKCk7XG5cblx0XHRcdGltZy5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHZhciB3ID0gaW1nLm5hdHVyYWxXaWR0aDtcblx0XHRcdFx0dmFyIGggPSBpbWcubmF0dXJhbEhlaWdodDtcblxuXHRcdFx0XHRfdGhpcy5jYW52YXMud2lkdGggPSB3O1xuXHRcdFx0XHRfdGhpcy5jYW52YXMuaGVpZ2h0ID0gaDtcblxuXHRcdFx0XHRfdGhpcy5wcm9jZXNzQ2FudmFzLndpZHRoID0gdztcblx0XHRcdFx0X3RoaXMucHJvY2Vzc0NhbnZhcy5oZWlnaHQgPSBoO1xuXG5cdFx0XHRcdF90aGlzLmltYWdlUHJvQ2FudmFzLndpZHRoID0gdztcblx0XHRcdFx0X3RoaXMuaW1hZ2VQcm9DYW52YXMuaGVpZ2h0ID0gaDtcblxuXHRcdFx0XHRfdGhpcy5jdHguZHJhd0ltYWdlKGltZywgMCwgMCk7XG5cdFx0XHRcdF90aGlzLmltYWdlRGF0YSA9IF90aGlzLmN0eC5nZXRJbWFnZURhdGEoMCwgMCwgdywgaCk7XG5cblx0XHRcdFx0X3RoaXMuaXRlcmF0ZURhdGEoKTtcblx0XHRcdH07XG5cdFx0XHRpbWcuc3JjID0gJy4vdGVzdEEucG5nJztcblx0XHRcdGltZy5zcmMgPSBiYXNlU2l4O1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3RocmVzaG9sZCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHRocmVzaG9sZCgpIHtcblx0XHRcdHZhciBkID0gdGhpcy5pbWFnZURhdGEuZGF0YTtcblxuXHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBkLmxlbmd0aDsgaSArPSA0KSB7XG5cdFx0XHRcdHZhciByID0gZFtpXTtcblx0XHRcdFx0dmFyIGcgPSBkW2kgKyAxXTtcblx0XHRcdFx0dmFyIGIgPSBkW2kgKyAyXTtcblxuXHRcdFx0XHRkW2kgKyAzXSA8IDI1NSA/IGRbaSArIDNdID0gMjU1IDogZFtpICsgM10gPSBkW2kgKyAzXTtcblxuXHRcdFx0XHR2YXIgdiA9IDAuMjEyNiAqIHIgKyAwLjcxNTIgKiBnICsgMC4wNzIyICogYiA+PSAxNTAgPyAyNTUgOiAwO1xuXG5cdFx0XHRcdGRbaV0gPSBkW2kgKyAxXSA9IGRbaSArIDJdID0gdjtcblx0XHRcdH1cblx0XHRcdHRoaXMucHJvY2Vzc0N0eC5wdXRJbWFnZURhdGEodGhpcy5pbWFnZURhdGEsIDAsIDApO1xuXHRcdFx0cmV0dXJuIHRoaXMuaW1hZ2VEYXRhO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2dldFZhcmlhdGlvbicsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGdldFZhcmlhdGlvbihweGEsIHB4Yikge1xuXHRcdFx0dmFyIHJ2ID0gTWF0aC5hYnMocHhhLnIgLSBweGIucik7XG5cdFx0XHR2YXIgZ3YgPSBNYXRoLmFicyhweGEuZyAtIHB4Yi5nKTtcblx0XHRcdHZhciBidiA9IE1hdGguYWJzKHB4YS5iIC0gcHhiLmIpO1xuXHRcdFx0dmFyIGF2ID0gTWF0aC5hYnMocHhhLmEgLSBweGIuYSk7XG5cblx0XHRcdHZhciBzdW1WYXJpYXRpb24gPSAocnYgKyBndiArIGJ2ICsgYXYpIC8gNDtcblxuXHRcdFx0dmFyIHZhcmlhdGlvbiA9IHtcblx0XHRcdFx0c3VtVmFyaWF0aW9uOiBzdW1WYXJpYXRpb25cblx0XHRcdH07XG5cblx0XHRcdGlmIChweGEuYyAmJiBweGIuYykge1xuXHRcdFx0XHR2YXJpYXRpb24uYSA9IHB4YS5jO1xuXHRcdFx0XHR2YXJpYXRpb24uYiA9IHB4Yi5jO1xuXHRcdFx0fTtcblxuXHRcdFx0cmV0dXJuIHZhcmlhdGlvbjtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdnZXRIaXRSZWdpb25QaXhlbHMnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBnZXRIaXRSZWdpb25QaXhlbHMoeCwgeSwgdywgaCwgaGl0Q29sb3IpIHtcblx0XHRcdHZhciBkYXRhVyA9IHRoaXMuaW1hZ2VEYXRhLndpZHRoO1xuXHRcdFx0dmFyIGRhdGFIID0gdGhpcy5pbWFnZURhdGEuaGVpZ2h0O1xuXG5cdFx0XHR2YXIgaGl0UmVnaW9ucyA9IHtcblx0XHRcdFx0J3QnOiBmYWxzZSwgLy9Ub3AgICAgIGJvcmRlclxuXHRcdFx0XHQncic6IGZhbHNlLCAvL1JpZ2h0ICAgYm9yZGVyXG5cdFx0XHRcdCdiJzogZmFsc2UsIC8vQm90dG9tICBib3JkZXJcblx0XHRcdFx0J2wnOiBmYWxzZSwgLy9MZWZ0ICAgIGJvcmRlclxuXHRcdFx0XHQnYSc6IGZhbHNlIC8vT3ZlcmFsbCBzdGF0dXNcblx0XHRcdH07XG5cblx0XHRcdC8vUHVzaCB0b3Agc3F1YXJlIHBpeGVsc1xuXHRcdFx0dmFyIHRTdGFydCA9ICh5ICogZGF0YVcgKyB4KSAqIDQ7XG5cdFx0XHR2YXIgdFN0b3AgPSB0U3RhcnQgKyB3ICogNDtcblxuXHRcdFx0Zm9yICh2YXIgdCA9IHRTdGFydDsgdCA8IHRTdG9wOyB0ICs9IDQpIHtcblx0XHRcdFx0dmFyIGhpdHMgPSB0aGlzLmdldFZhcmlhdGlvbihoaXRDb2xvciwge1xuXHRcdFx0XHRcdCdyJzogdGhpcy5pbWFnZURhdGEuZGF0YVt0XSxcblx0XHRcdFx0XHQnZyc6IHRoaXMuaW1hZ2VEYXRhLmRhdGFbdCArIDFdLFxuXHRcdFx0XHRcdCdiJzogdGhpcy5pbWFnZURhdGEuZGF0YVt0ICsgMl0sXG5cdFx0XHRcdFx0J2EnOiB0aGlzLmltYWdlRGF0YS5kYXRhW3QgKyAzXVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRpZiAoaGl0cy5zdW1WYXJpYXRpb24gPCA1KSB7XG5cdFx0XHRcdFx0aGl0UmVnaW9ucy50ID0gdHJ1ZTtcblx0XHRcdFx0XHRoaXRSZWdpb25zLmEgPSB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdC8vUHVzaCByaWdodCBzcXVhcmUgcGl4ZWxzXG5cdFx0XHR2YXIgclN0YXJ0ID0gdFN0YXJ0ICsgKHcgLSAxKSAqIDQ7XG5cdFx0XHR2YXIgclN0b3AgPSByU3RhcnQgKyBoICogZGF0YVcgKiA0O1xuXG5cdFx0XHRmb3IgKHZhciByID0gclN0YXJ0OyByIDwgclN0b3A7IHIgKz0gZGF0YVcgKiA0KSB7XG5cdFx0XHRcdHZhciBfaGl0cyA9IHRoaXMuZ2V0VmFyaWF0aW9uKGhpdENvbG9yLCB7XG5cdFx0XHRcdFx0J3InOiB0aGlzLmltYWdlRGF0YS5kYXRhW3JdLFxuXHRcdFx0XHRcdCdnJzogdGhpcy5pbWFnZURhdGEuZGF0YVtyICsgMV0sXG5cdFx0XHRcdFx0J2InOiB0aGlzLmltYWdlRGF0YS5kYXRhW3IgKyAyXSxcblx0XHRcdFx0XHQnYSc6IHRoaXMuaW1hZ2VEYXRhLmRhdGFbciArIDNdXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdGlmIChfaGl0cy5zdW1WYXJpYXRpb24gPCA1KSB7XG5cdFx0XHRcdFx0aGl0UmVnaW9ucy5yID0gdHJ1ZTtcblx0XHRcdFx0XHRoaXRSZWdpb25zLmEgPSB0cnVlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdC8vUHVzaCBib3R0b20gc3F1YXJlIHBpeGVsc1xuXHRcdFx0dmFyIGJTdGFydCA9ICgoeSArIGggLSAxKSAqIGRhdGFXICsgeCkgKiA0O1xuXHRcdFx0dmFyIGJTdG9wID0gYlN0YXJ0ICsgdyAqIDQ7XG5cblx0XHRcdGZvciAodmFyIGIgPSBiU3RhcnQ7IGIgPCBiU3RvcDsgYiArPSA0KSB7XG5cdFx0XHRcdHZhciBfaGl0czIgPSB0aGlzLmdldFZhcmlhdGlvbihoaXRDb2xvciwge1xuXHRcdFx0XHRcdCdyJzogdGhpcy5pbWFnZURhdGEuZGF0YVtiXSxcblx0XHRcdFx0XHQnZyc6IHRoaXMuaW1hZ2VEYXRhLmRhdGFbYiArIDFdLFxuXHRcdFx0XHRcdCdiJzogdGhpcy5pbWFnZURhdGEuZGF0YVtiICsgMl0sXG5cdFx0XHRcdFx0J2EnOiB0aGlzLmltYWdlRGF0YS5kYXRhW2IgKyAzXVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRpZiAoX2hpdHMyLnN1bVZhcmlhdGlvbiA8IDUpIHtcblx0XHRcdFx0XHRoaXRSZWdpb25zLmIgPSB0cnVlO1xuXHRcdFx0XHRcdGhpdFJlZ2lvbnMuYSA9IHRydWU7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0Ly9QdXNoIGxlZnQgc3F1YXJlIHBpeGVsc1xuXHRcdFx0dmFyIGxTdGFydCA9IHRTdGFydDtcblx0XHRcdHZhciBsU3RvcCA9IGxTdGFydCArIGRhdGFXICogaCAqIDQ7XG5cblx0XHRcdGZvciAodmFyIGwgPSBsU3RhcnQ7IGwgPCBsU3RvcDsgbCArPSBkYXRhVyAqIDQpIHtcblx0XHRcdFx0dmFyIF9oaXRzMyA9IHRoaXMuZ2V0VmFyaWF0aW9uKGhpdENvbG9yLCB7XG5cdFx0XHRcdFx0J3InOiB0aGlzLmltYWdlRGF0YS5kYXRhW2xdLFxuXHRcdFx0XHRcdCdnJzogdGhpcy5pbWFnZURhdGEuZGF0YVtsICsgMV0sXG5cdFx0XHRcdFx0J2InOiB0aGlzLmltYWdlRGF0YS5kYXRhW2wgKyAyXSxcblx0XHRcdFx0XHQnYSc6IHRoaXMuaW1hZ2VEYXRhLmRhdGFbbCArIDNdXG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdGlmIChfaGl0czMuc3VtVmFyaWF0aW9uIDwgNSkge1xuXHRcdFx0XHRcdGhpdFJlZ2lvbnMubCA9IHRydWU7XG5cdFx0XHRcdFx0aGl0UmVnaW9ucy5hID0gdHJ1ZTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gaGl0UmVnaW9ucztcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdhbmFseXplSGl0UmVnaW9uJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gYW5hbHl6ZUhpdFJlZ2lvbihweGEsIHB4YiwgdmFyaWF0aW9uKSB7XG5cdFx0XHR2YXIgY1ggPSBweGIuYy54O1xuXHRcdFx0dmFyIGNZID0gcHhiLmMueTtcblxuXHRcdFx0dmFyIGNXID0gMjtcblx0XHRcdHZhciBjSCA9IDI7XG5cblx0XHRcdHZhciBoYXNIaXQgPSB0cnVlO1xuXHRcdFx0dmFyIHRlc3RDYXNlID0ge1xuXHRcdFx0XHQnaW5pdCc6IGZhbHNlLFxuXHRcdFx0XHQnc3VjY2Vzcyc6IDAsXG5cdFx0XHRcdCd2YWwnOiB7XG5cdFx0XHRcdFx0J2gnOiAwXG5cdFx0XHRcdH1cblx0XHRcdH07XG5cblx0XHRcdHdoaWxlIChoYXNIaXQpIHtcblx0XHRcdFx0dmFyIHJlZ2lvbnMgPSB0aGlzLmdldEhpdFJlZ2lvblBpeGVscyhjWCwgY1ksIGNXLCBjSCwgcHhiKTtcblx0XHRcdFx0aWYgKHJlZ2lvbnMudCkge1xuXHRcdFx0XHRcdC0tY1k7XG5cdFx0XHRcdFx0KytjSDtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAocmVnaW9ucy5yKSB7XG5cdFx0XHRcdFx0KytjVztcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAocmVnaW9ucy5iKSB7XG5cdFx0XHRcdFx0KytjSDtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAocmVnaW9ucy5sKSB7XG5cdFx0XHRcdFx0LS1jWDtcblx0XHRcdFx0XHQrK2NXO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGhhc0hpdCA9IHJlZ2lvbnMuYTtcblxuXHRcdFx0XHQvL0RvdCBjYXNlIHVwXG5cdFx0XHRcdGlmICghcmVnaW9ucy5hICYmIE1hdGguYWJzKGNIIC0gY1cpIDwgNSkge1xuXHRcdFx0XHRcdGlmICghdGVzdENhc2UuaW5pdCkge1xuXHRcdFx0XHRcdFx0dGVzdENhc2UuaW5pdCA9IHRydWU7XG5cdFx0XHRcdFx0XHR0ZXN0Q2FzZS52YWwuaCA9IGNIO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdGhhc0hpdCA9IHRydWU7XG5cdFx0XHRcdFx0Y0ggKz0gMjtcblx0XHRcdFx0fVxuXHRcdFx0XHQvL0lmIHRlc3QgY2FzZSB3YXMgc3RhcnRlZCBtb25pdG9yIFJPSSBoZWlnaHRcblx0XHRcdFx0aWYgKHRlc3RDYXNlLmluaXQgJiYgcmVnaW9ucy5iKSB7XG5cdFx0XHRcdFx0dGVzdENhc2UudmFsLmggPSBjSDtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRpZiAodGVzdENhc2UuaW5pdCkge1xuXHRcdFx0XHRjSCA9IHRlc3RDYXNlLnZhbC5oO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLmltYWdlUHJvQ3R4LnJlY3QoY1gsIGNZLCBjVywgY0gpO1xuXHRcdFx0dGhpcy5pbWFnZVByb0N0eC5zdHJva2UoKTtcblxuXHRcdFx0cmV0dXJuIFtjWCArIDEsIGNZICsgMSwgY1cgLSAxLCBjSCAtIDFdO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2NoZWNrU2tpcCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNoZWNrU2tpcChmcCkge1xuXHRcdFx0dmFyIG11c3RTa2lwID0gMDtcblxuXHRcdFx0aWYgKHRoaXMubGV0dGVyc0Nvb3Jkcy5sZW5ndGggPCAxKSB7XG5cdFx0XHRcdHJldHVybiAwO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLmxldHRlcnNDb29yZHMuZm9yRWFjaChmdW5jdGlvbiAobCkge1xuXHRcdFx0XHQvL0lmIGN1cnJlbnQgcG9pbnQgaXMgaW5iZXR3ZWVuIHJldHVybiBhIG1hdGNoXG5cdFx0XHRcdHZhciBtYWNoZlggPSBmcC54ID49IGxbMF0gJiYgZnAueCA8PSBsWzBdICsgbFsyXTtcblx0XHRcdFx0dmFyIG1hY2hmWSA9IGZwLnkgPj0gbFsxXSAmJiBmcC55IDw9IGxbMV0gKyBsWzNdO1xuXG5cdFx0XHRcdGlmIChtYWNoZlggJiYgbWFjaGZZKSB7XG5cdFx0XHRcdFx0bXVzdFNraXAgPSBsWzNdO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdFx0cmV0dXJuIG11c3RTa2lwO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2l0ZXJhdGVEYXRhJyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gaXRlcmF0ZURhdGEoKSB7XG5cdFx0XHR2YXIgcHhkYXRhID0gdGhpcy50aHJlc2hvbGQoKS5kYXRhO1xuXHRcdFx0dmFyIHcgPSB0aGlzLmltYWdlRGF0YS53aWR0aDtcblx0XHRcdHZhciBoID0gdGhpcy5pbWFnZURhdGEuaGVpZ2h0O1xuXHRcdFx0Ly8gcmV0dXJuO1xuXHRcdFx0Zm9yICh2YXIgeCA9IHc7IHggPj0gMDsgeCAtPSAyKSB7XG5cdFx0XHRcdC8vIGZvciAobGV0IHggPSAwOyB4IDwgdyA7IHggPSAyKSB7XG5cdFx0XHRcdGZvciAodmFyIHkgPSAwOyB5IDwgaCAtIDE7IHkgKz0gMSkge1xuXG5cdFx0XHRcdFx0dmFyIHRwU3RhcnQgPSAoeCArIHkgKiB3KSAqIDQ7XG5cdFx0XHRcdFx0dmFyIGJwU3RhcnQgPSAoeCArICh5ICsgMSkgKiB3KSAqIDQ7XG5cblx0XHRcdFx0XHQvL0ZpcnN0IHBpeGVsXG5cdFx0XHRcdFx0dmFyIGZQeCA9IHtcblx0XHRcdFx0XHRcdCdyJzogcHhkYXRhW3RwU3RhcnRdLFxuXHRcdFx0XHRcdFx0J2cnOiBweGRhdGFbdHBTdGFydCArIDFdLFxuXHRcdFx0XHRcdFx0J2InOiBweGRhdGFbdHBTdGFydCArIDJdLFxuXHRcdFx0XHRcdFx0J2EnOiBweGRhdGFbdHBTdGFydCArIDNdLFxuXHRcdFx0XHRcdFx0J2MnOiB7ICd4JzogeCwgJ3knOiB5IH1cblx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0Ly9TZWNvbmQgcGl4ZWwgKHJpZ2h0KVxuXHRcdFx0XHRcdHZhciBzUHggPSB7XG5cdFx0XHRcdFx0XHQncic6IHB4ZGF0YVticFN0YXJ0XSxcblx0XHRcdFx0XHRcdCdnJzogcHhkYXRhW2JwU3RhcnQgKyAxXSxcblx0XHRcdFx0XHRcdCdiJzogcHhkYXRhW2JwU3RhcnQgKyAyXSxcblx0XHRcdFx0XHRcdCdhJzogcHhkYXRhW2JwU3RhcnQgKyAzXSxcblx0XHRcdFx0XHRcdCdjJzogeyAneCc6IHgsICd5JzogeSArIDEgfVxuXHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHR2YXIgc2tpcCA9IHRoaXMuY2hlY2tTa2lwKGZQeC5jKTtcblx0XHRcdFx0XHRpZiAoc2tpcCA+IDApIHtcblx0XHRcdFx0XHRcdHkgKz0gc2tpcDtcblx0XHRcdFx0XHRcdGNvbnRpbnVlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR2YXIgcHh2YXIgPSB0aGlzLmdldFZhcmlhdGlvbihmUHgsIHNQeCk7XG5cblx0XHRcdFx0XHQvLyB0aGlzLnByb2Nlc3NDdHguZmlsbFN0eWxlID0gJ3JlZCc7XG5cdFx0XHRcdFx0Ly8gdGhpcy5wcm9jZXNzQ3R4LmZpbGxSZWN0KHgsIHksIDEsIDEpO1xuXHRcdFx0XHRcdGlmIChweHZhci5zdW1WYXJpYXRpb24gPiB0aGlzLmNmZy5jb250cmFzdFRyZXNob2xkKSB7XG5cdFx0XHRcdFx0XHR2YXIgY29vcmRzID0gdGhpcy5hbmFseXplSGl0UmVnaW9uKGZQeCwgc1B4LCBweHZhci5zdW1WYXJpYXRpb24pO1xuXHRcdFx0XHRcdFx0dGhpcy5sZXR0ZXJzQ29vcmRzLnB1c2goY29vcmRzKTtcblx0XHRcdFx0XHRcdHkgPSBjb29yZHNbMV0gLSAxO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0Ly9SZW1vdmUgZHVwbGljYXRlc1xuXHRcdFx0dGhpcy5sZXR0ZXJzID0gW10uY29uY2F0KCgwLCBfdG9Db25zdW1hYmxlQXJyYXkzLmRlZmF1bHQpKG5ldyBTZXQodGhpcy5sZXR0ZXJzQ29vcmRzLm1hcChmdW5jdGlvbiAobCkge1xuXHRcdFx0XHRyZXR1cm4gbFswXSArICcsJyArIGxbMV0gKyAnLCcgKyBsWzJdICsgJywnICsgbFszXTtcblx0XHRcdH0pKSkpLm1hcChmdW5jdGlvbiAobCkge1xuXHRcdFx0XHRsID0gbC5zcGxpdCgnLCcpO1xuXHRcdFx0XHRyZXR1cm4gW3BhcnNlRmxvYXQobFswXSksIHBhcnNlRmxvYXQobFsxXSksIHBhcnNlRmxvYXQobFsyXSksIHBhcnNlRmxvYXQobFszXSldO1xuXHRcdFx0fSk7XG5cblx0XHRcdGNvbnNvbGUubG9nKHRoaXMubGV0dGVycyk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnYnVpbGRJbWFnZScsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGJ1aWxkSW1hZ2UoKSB7XG5cdFx0XHQvL011c3QgaXRlcmF0ZSB0byBwcm9jZXNzIG11bHRpcGxlIGZpbGVzXG5cdFx0XHR0aGlzLmJ1aWxkQ2FudmFzKHRoaXMuaW1hZ2VBcnJheVswXSk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAncmVhZEltYWdlcycsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlYWRJbWFnZXMoKSB7XG5cdFx0XHR2YXIgX3RoaXMyID0gdGhpcztcblxuXHRcdFx0dGhpcy5maWxlSW5wdXQuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgZnVuY3Rpb24gKGUpIHtcblx0XHRcdFx0X3RoaXMyLmltYWdlQXJyYXkgPSBbXTtcblxuXHRcdFx0XHR2YXIgdGd0ID0gZS50YXJnZXQgfHwgd2luZG93LmV2ZW50LnNyY0VsZW1lbnQ7XG5cdFx0XHRcdHZhciBmaWxlcyA9IHRndC5maWxlcztcblxuXHRcdFx0XHQvLyBGaWxlUmVhZGVyIHN1cHBvcnRcblx0XHRcdFx0aWYgKEZpbGVSZWFkZXIgJiYgZmlsZXMgJiYgZmlsZXMubGVuZ3RoKSB7XG5cdFx0XHRcdFx0KGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0XHRcdHZhciBmciA9IG5ldyBGaWxlUmVhZGVyKCk7XG5cdFx0XHRcdFx0XHR2YXIgaW5kZXggPSAxO1xuXG5cdFx0XHRcdFx0XHRmci5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdFx0XHRcdF90aGlzMi5pbWFnZUFycmF5LnB1c2goZnIucmVzdWx0KTtcblxuXHRcdFx0XHRcdFx0XHRpZiAoaW5kZXggPCBmaWxlcy5sZW5ndGgpIHtcblx0XHRcdFx0XHRcdFx0XHRmci5yZWFkQXNEYXRhVVJMKGZpbGVzW2luZGV4XSk7XG5cdFx0XHRcdFx0XHRcdFx0aW5kZXggKz0gMTtcblx0XHRcdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdFx0XHRfdGhpczIuYnVpbGRJbWFnZSgpO1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHR9O1xuXG5cdFx0XHRcdFx0XHRmci5yZWFkQXNEYXRhVVJMKGZpbGVzWzBdKTtcblx0XHRcdFx0XHR9KSgpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdGFsZXJ0KCdQbGVhc2UgdXBkYXRlIGJyb3dzZXInKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fVxuXHR9XSk7XG5cdHJldHVybiBDcnM7XG59KCk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENycztcbjsiLCJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vYXJyYXkvZnJvbVwiKSwgX19lc01vZHVsZTogdHJ1ZSB9OyIsIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvZGVmaW5lLXByb3BlcnR5XCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHtcbiAgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpO1xuICB9XG59OyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2RlZmluZVByb3BlcnR5ID0gcmVxdWlyZShcIi4uL2NvcmUtanMvb2JqZWN0L2RlZmluZS1wcm9wZXJ0eVwiKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uICgpIHtcbiAgZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIHByb3BzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtcbiAgICAgIGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTtcbiAgICAgIGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTtcbiAgICAgIGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7XG4gICAgICAoMCwgX2RlZmluZVByb3BlcnR5Mi5kZWZhdWx0KSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gKENvbnN0cnVjdG9yLCBwcm90b1Byb3BzLCBzdGF0aWNQcm9wcykge1xuICAgIGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gICAgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7XG4gICAgcmV0dXJuIENvbnN0cnVjdG9yO1xuICB9O1xufSgpOyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2Zyb20gPSByZXF1aXJlKFwiLi4vY29yZS1qcy9hcnJheS9mcm9tXCIpO1xuXG52YXIgX2Zyb20yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZnJvbSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChhcnIpIHtcbiAgaWYgKEFycmF5LmlzQXJyYXkoYXJyKSkge1xuICAgIGZvciAodmFyIGkgPSAwLCBhcnIyID0gQXJyYXkoYXJyLmxlbmd0aCk7IGkgPCBhcnIubGVuZ3RoOyBpKyspIHtcbiAgICAgIGFycjJbaV0gPSBhcnJbaV07XG4gICAgfVxuXG4gICAgcmV0dXJuIGFycjI7XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuICgwLCBfZnJvbTIuZGVmYXVsdCkoYXJyKTtcbiAgfVxufTsiLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNi5zdHJpbmcuaXRlcmF0b3InKTtcbnJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM2LmFycmF5LmZyb20nKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLkFycmF5LmZyb207IiwicmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9lczYub2JqZWN0LmRlZmluZS1wcm9wZXJ0eScpO1xudmFyICRPYmplY3QgPSByZXF1aXJlKCcuLi8uLi9tb2R1bGVzL19jb3JlJykuT2JqZWN0O1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShpdCwga2V5LCBkZXNjKXtcbiAgcmV0dXJuICRPYmplY3QuZGVmaW5lUHJvcGVydHkoaXQsIGtleSwgZGVzYyk7XG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oaXQpe1xuICBpZih0eXBlb2YgaXQgIT0gJ2Z1bmN0aW9uJyl0aHJvdyBUeXBlRXJyb3IoaXQgKyAnIGlzIG5vdCBhIGZ1bmN0aW9uIScpO1xuICByZXR1cm4gaXQ7XG59OyIsInZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihpdCl7XG4gIGlmKCFpc09iamVjdChpdCkpdGhyb3cgVHlwZUVycm9yKGl0ICsgJyBpcyBub3QgYW4gb2JqZWN0IScpO1xuICByZXR1cm4gaXQ7XG59OyIsIi8vIGZhbHNlIC0+IEFycmF5I2luZGV4T2Zcbi8vIHRydWUgIC0+IEFycmF5I2luY2x1ZGVzXG52YXIgdG9JT2JqZWN0ID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpXG4gICwgdG9MZW5ndGggID0gcmVxdWlyZSgnLi9fdG8tbGVuZ3RoJylcbiAgLCB0b0luZGV4ICAgPSByZXF1aXJlKCcuL190by1pbmRleCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihJU19JTkNMVURFUyl7XG4gIHJldHVybiBmdW5jdGlvbigkdGhpcywgZWwsIGZyb21JbmRleCl7XG4gICAgdmFyIE8gICAgICA9IHRvSU9iamVjdCgkdGhpcylcbiAgICAgICwgbGVuZ3RoID0gdG9MZW5ndGgoTy5sZW5ndGgpXG4gICAgICAsIGluZGV4ICA9IHRvSW5kZXgoZnJvbUluZGV4LCBsZW5ndGgpXG4gICAgICAsIHZhbHVlO1xuICAgIC8vIEFycmF5I2luY2x1ZGVzIHVzZXMgU2FtZVZhbHVlWmVybyBlcXVhbGl0eSBhbGdvcml0aG1cbiAgICBpZihJU19JTkNMVURFUyAmJiBlbCAhPSBlbCl3aGlsZShsZW5ndGggPiBpbmRleCl7XG4gICAgICB2YWx1ZSA9IE9baW5kZXgrK107XG4gICAgICBpZih2YWx1ZSAhPSB2YWx1ZSlyZXR1cm4gdHJ1ZTtcbiAgICAvLyBBcnJheSN0b0luZGV4IGlnbm9yZXMgaG9sZXMsIEFycmF5I2luY2x1ZGVzIC0gbm90XG4gICAgfSBlbHNlIGZvcig7bGVuZ3RoID4gaW5kZXg7IGluZGV4KyspaWYoSVNfSU5DTFVERVMgfHwgaW5kZXggaW4gTyl7XG4gICAgICBpZihPW2luZGV4XSA9PT0gZWwpcmV0dXJuIElTX0lOQ0xVREVTIHx8IGluZGV4IHx8IDA7XG4gICAgfSByZXR1cm4gIUlTX0lOQ0xVREVTICYmIC0xO1xuICB9O1xufTsiLCIvLyBnZXR0aW5nIHRhZyBmcm9tIDE5LjEuMy42IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcoKVxudmFyIGNvZiA9IHJlcXVpcmUoJy4vX2NvZicpXG4gICwgVEFHID0gcmVxdWlyZSgnLi9fd2tzJykoJ3RvU3RyaW5nVGFnJylcbiAgLy8gRVMzIHdyb25nIGhlcmVcbiAgLCBBUkcgPSBjb2YoZnVuY3Rpb24oKXsgcmV0dXJuIGFyZ3VtZW50czsgfSgpKSA9PSAnQXJndW1lbnRzJztcblxuLy8gZmFsbGJhY2sgZm9yIElFMTEgU2NyaXB0IEFjY2VzcyBEZW5pZWQgZXJyb3JcbnZhciB0cnlHZXQgPSBmdW5jdGlvbihpdCwga2V5KXtcbiAgdHJ5IHtcbiAgICByZXR1cm4gaXRba2V5XTtcbiAgfSBjYXRjaChlKXsgLyogZW1wdHkgKi8gfVxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihpdCl7XG4gIHZhciBPLCBULCBCO1xuICByZXR1cm4gaXQgPT09IHVuZGVmaW5lZCA/ICdVbmRlZmluZWQnIDogaXQgPT09IG51bGwgPyAnTnVsbCdcbiAgICAvLyBAQHRvU3RyaW5nVGFnIGNhc2VcbiAgICA6IHR5cGVvZiAoVCA9IHRyeUdldChPID0gT2JqZWN0KGl0KSwgVEFHKSkgPT0gJ3N0cmluZycgPyBUXG4gICAgLy8gYnVpbHRpblRhZyBjYXNlXG4gICAgOiBBUkcgPyBjb2YoTylcbiAgICAvLyBFUzMgYXJndW1lbnRzIGZhbGxiYWNrXG4gICAgOiAoQiA9IGNvZihPKSkgPT0gJ09iamVjdCcgJiYgdHlwZW9mIE8uY2FsbGVlID09ICdmdW5jdGlvbicgPyAnQXJndW1lbnRzJyA6IEI7XG59OyIsInZhciB0b1N0cmluZyA9IHt9LnRvU3RyaW5nO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGl0KXtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwoaXQpLnNsaWNlKDgsIC0xKTtcbn07IiwidmFyIGNvcmUgPSBtb2R1bGUuZXhwb3J0cyA9IHt2ZXJzaW9uOiAnMi40LjAnfTtcbmlmKHR5cGVvZiBfX2UgPT0gJ251bWJlcicpX19lID0gY29yZTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bmRlZiIsIid1c2Ugc3RyaWN0JztcbnZhciAkZGVmaW5lUHJvcGVydHkgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKVxuICAsIGNyZWF0ZURlc2MgICAgICA9IHJlcXVpcmUoJy4vX3Byb3BlcnR5LWRlc2MnKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihvYmplY3QsIGluZGV4LCB2YWx1ZSl7XG4gIGlmKGluZGV4IGluIG9iamVjdCkkZGVmaW5lUHJvcGVydHkuZihvYmplY3QsIGluZGV4LCBjcmVhdGVEZXNjKDAsIHZhbHVlKSk7XG4gIGVsc2Ugb2JqZWN0W2luZGV4XSA9IHZhbHVlO1xufTsiLCIvLyBvcHRpb25hbCAvIHNpbXBsZSBjb250ZXh0IGJpbmRpbmdcbnZhciBhRnVuY3Rpb24gPSByZXF1aXJlKCcuL19hLWZ1bmN0aW9uJyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGZuLCB0aGF0LCBsZW5ndGgpe1xuICBhRnVuY3Rpb24oZm4pO1xuICBpZih0aGF0ID09PSB1bmRlZmluZWQpcmV0dXJuIGZuO1xuICBzd2l0Y2gobGVuZ3RoKXtcbiAgICBjYXNlIDE6IHJldHVybiBmdW5jdGlvbihhKXtcbiAgICAgIHJldHVybiBmbi5jYWxsKHRoYXQsIGEpO1xuICAgIH07XG4gICAgY2FzZSAyOiByZXR1cm4gZnVuY3Rpb24oYSwgYil7XG4gICAgICByZXR1cm4gZm4uY2FsbCh0aGF0LCBhLCBiKTtcbiAgICB9O1xuICAgIGNhc2UgMzogcmV0dXJuIGZ1bmN0aW9uKGEsIGIsIGMpe1xuICAgICAgcmV0dXJuIGZuLmNhbGwodGhhdCwgYSwgYiwgYyk7XG4gICAgfTtcbiAgfVxuICByZXR1cm4gZnVuY3Rpb24oLyogLi4uYXJncyAqLyl7XG4gICAgcmV0dXJuIGZuLmFwcGx5KHRoYXQsIGFyZ3VtZW50cyk7XG4gIH07XG59OyIsIi8vIDcuMi4xIFJlcXVpcmVPYmplY3RDb2VyY2libGUoYXJndW1lbnQpXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGl0KXtcbiAgaWYoaXQgPT0gdW5kZWZpbmVkKXRocm93IFR5cGVFcnJvcihcIkNhbid0IGNhbGwgbWV0aG9kIG9uICBcIiArIGl0KTtcbiAgcmV0dXJuIGl0O1xufTsiLCIvLyBUaGFuaydzIElFOCBmb3IgaGlzIGZ1bm55IGRlZmluZVByb3BlcnR5XG5tb2R1bGUuZXhwb3J0cyA9ICFyZXF1aXJlKCcuL19mYWlscycpKGZ1bmN0aW9uKCl7XG4gIHJldHVybiBPYmplY3QuZGVmaW5lUHJvcGVydHkoe30sICdhJywge2dldDogZnVuY3Rpb24oKXsgcmV0dXJuIDc7IH19KS5hICE9IDc7XG59KTsiLCJ2YXIgaXNPYmplY3QgPSByZXF1aXJlKCcuL19pcy1vYmplY3QnKVxuICAsIGRvY3VtZW50ID0gcmVxdWlyZSgnLi9fZ2xvYmFsJykuZG9jdW1lbnRcbiAgLy8gaW4gb2xkIElFIHR5cGVvZiBkb2N1bWVudC5jcmVhdGVFbGVtZW50IGlzICdvYmplY3QnXG4gICwgaXMgPSBpc09iamVjdChkb2N1bWVudCkgJiYgaXNPYmplY3QoZG9jdW1lbnQuY3JlYXRlRWxlbWVudCk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGl0KXtcbiAgcmV0dXJuIGlzID8gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChpdCkgOiB7fTtcbn07IiwiLy8gSUUgOC0gZG9uJ3QgZW51bSBidWcga2V5c1xubW9kdWxlLmV4cG9ydHMgPSAoXG4gICdjb25zdHJ1Y3RvcixoYXNPd25Qcm9wZXJ0eSxpc1Byb3RvdHlwZU9mLHByb3BlcnR5SXNFbnVtZXJhYmxlLHRvTG9jYWxlU3RyaW5nLHRvU3RyaW5nLHZhbHVlT2YnXG4pLnNwbGl0KCcsJyk7IiwidmFyIGdsb2JhbCAgICA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpXG4gICwgY29yZSAgICAgID0gcmVxdWlyZSgnLi9fY29yZScpXG4gICwgY3R4ICAgICAgID0gcmVxdWlyZSgnLi9fY3R4JylcbiAgLCBoaWRlICAgICAgPSByZXF1aXJlKCcuL19oaWRlJylcbiAgLCBQUk9UT1RZUEUgPSAncHJvdG90eXBlJztcblxudmFyICRleHBvcnQgPSBmdW5jdGlvbih0eXBlLCBuYW1lLCBzb3VyY2Upe1xuICB2YXIgSVNfRk9SQ0VEID0gdHlwZSAmICRleHBvcnQuRlxuICAgICwgSVNfR0xPQkFMID0gdHlwZSAmICRleHBvcnQuR1xuICAgICwgSVNfU1RBVElDID0gdHlwZSAmICRleHBvcnQuU1xuICAgICwgSVNfUFJPVE8gID0gdHlwZSAmICRleHBvcnQuUFxuICAgICwgSVNfQklORCAgID0gdHlwZSAmICRleHBvcnQuQlxuICAgICwgSVNfV1JBUCAgID0gdHlwZSAmICRleHBvcnQuV1xuICAgICwgZXhwb3J0cyAgID0gSVNfR0xPQkFMID8gY29yZSA6IGNvcmVbbmFtZV0gfHwgKGNvcmVbbmFtZV0gPSB7fSlcbiAgICAsIGV4cFByb3RvICA9IGV4cG9ydHNbUFJPVE9UWVBFXVxuICAgICwgdGFyZ2V0ICAgID0gSVNfR0xPQkFMID8gZ2xvYmFsIDogSVNfU1RBVElDID8gZ2xvYmFsW25hbWVdIDogKGdsb2JhbFtuYW1lXSB8fCB7fSlbUFJPVE9UWVBFXVxuICAgICwga2V5LCBvd24sIG91dDtcbiAgaWYoSVNfR0xPQkFMKXNvdXJjZSA9IG5hbWU7XG4gIGZvcihrZXkgaW4gc291cmNlKXtcbiAgICAvLyBjb250YWlucyBpbiBuYXRpdmVcbiAgICBvd24gPSAhSVNfRk9SQ0VEICYmIHRhcmdldCAmJiB0YXJnZXRba2V5XSAhPT0gdW5kZWZpbmVkO1xuICAgIGlmKG93biAmJiBrZXkgaW4gZXhwb3J0cyljb250aW51ZTtcbiAgICAvLyBleHBvcnQgbmF0aXZlIG9yIHBhc3NlZFxuICAgIG91dCA9IG93biA/IHRhcmdldFtrZXldIDogc291cmNlW2tleV07XG4gICAgLy8gcHJldmVudCBnbG9iYWwgcG9sbHV0aW9uIGZvciBuYW1lc3BhY2VzXG4gICAgZXhwb3J0c1trZXldID0gSVNfR0xPQkFMICYmIHR5cGVvZiB0YXJnZXRba2V5XSAhPSAnZnVuY3Rpb24nID8gc291cmNlW2tleV1cbiAgICAvLyBiaW5kIHRpbWVycyB0byBnbG9iYWwgZm9yIGNhbGwgZnJvbSBleHBvcnQgY29udGV4dFxuICAgIDogSVNfQklORCAmJiBvd24gPyBjdHgob3V0LCBnbG9iYWwpXG4gICAgLy8gd3JhcCBnbG9iYWwgY29uc3RydWN0b3JzIGZvciBwcmV2ZW50IGNoYW5nZSB0aGVtIGluIGxpYnJhcnlcbiAgICA6IElTX1dSQVAgJiYgdGFyZ2V0W2tleV0gPT0gb3V0ID8gKGZ1bmN0aW9uKEMpe1xuICAgICAgdmFyIEYgPSBmdW5jdGlvbihhLCBiLCBjKXtcbiAgICAgICAgaWYodGhpcyBpbnN0YW5jZW9mIEMpe1xuICAgICAgICAgIHN3aXRjaChhcmd1bWVudHMubGVuZ3RoKXtcbiAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIG5ldyBDO1xuICAgICAgICAgICAgY2FzZSAxOiByZXR1cm4gbmV3IEMoYSk7XG4gICAgICAgICAgICBjYXNlIDI6IHJldHVybiBuZXcgQyhhLCBiKTtcbiAgICAgICAgICB9IHJldHVybiBuZXcgQyhhLCBiLCBjKTtcbiAgICAgICAgfSByZXR1cm4gQy5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgfTtcbiAgICAgIEZbUFJPVE9UWVBFXSA9IENbUFJPVE9UWVBFXTtcbiAgICAgIHJldHVybiBGO1xuICAgIC8vIG1ha2Ugc3RhdGljIHZlcnNpb25zIGZvciBwcm90b3R5cGUgbWV0aG9kc1xuICAgIH0pKG91dCkgOiBJU19QUk9UTyAmJiB0eXBlb2Ygb3V0ID09ICdmdW5jdGlvbicgPyBjdHgoRnVuY3Rpb24uY2FsbCwgb3V0KSA6IG91dDtcbiAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUubWV0aG9kcy4lTkFNRSVcbiAgICBpZihJU19QUk9UTyl7XG4gICAgICAoZXhwb3J0cy52aXJ0dWFsIHx8IChleHBvcnRzLnZpcnR1YWwgPSB7fSkpW2tleV0gPSBvdXQ7XG4gICAgICAvLyBleHBvcnQgcHJvdG8gbWV0aG9kcyB0byBjb3JlLiVDT05TVFJVQ1RPUiUucHJvdG90eXBlLiVOQU1FJVxuICAgICAgaWYodHlwZSAmICRleHBvcnQuUiAmJiBleHBQcm90byAmJiAhZXhwUHJvdG9ba2V5XSloaWRlKGV4cFByb3RvLCBrZXksIG91dCk7XG4gICAgfVxuICB9XG59O1xuLy8gdHlwZSBiaXRtYXBcbiRleHBvcnQuRiA9IDE7ICAgLy8gZm9yY2VkXG4kZXhwb3J0LkcgPSAyOyAgIC8vIGdsb2JhbFxuJGV4cG9ydC5TID0gNDsgICAvLyBzdGF0aWNcbiRleHBvcnQuUCA9IDg7ICAgLy8gcHJvdG9cbiRleHBvcnQuQiA9IDE2OyAgLy8gYmluZFxuJGV4cG9ydC5XID0gMzI7ICAvLyB3cmFwXG4kZXhwb3J0LlUgPSA2NDsgIC8vIHNhZmVcbiRleHBvcnQuUiA9IDEyODsgLy8gcmVhbCBwcm90byBtZXRob2QgZm9yIGBsaWJyYXJ5YCBcbm1vZHVsZS5leHBvcnRzID0gJGV4cG9ydDsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGV4ZWMpe1xuICB0cnkge1xuICAgIHJldHVybiAhIWV4ZWMoKTtcbiAgfSBjYXRjaChlKXtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxufTsiLCIvLyBodHRwczovL2dpdGh1Yi5jb20vemxvaXJvY2svY29yZS1qcy9pc3N1ZXMvODYjaXNzdWVjb21tZW50LTExNTc1OTAyOFxudmFyIGdsb2JhbCA9IG1vZHVsZS5leHBvcnRzID0gdHlwZW9mIHdpbmRvdyAhPSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuTWF0aCA9PSBNYXRoXG4gID8gd2luZG93IDogdHlwZW9mIHNlbGYgIT0gJ3VuZGVmaW5lZCcgJiYgc2VsZi5NYXRoID09IE1hdGggPyBzZWxmIDogRnVuY3Rpb24oJ3JldHVybiB0aGlzJykoKTtcbmlmKHR5cGVvZiBfX2cgPT0gJ251bWJlcicpX19nID0gZ2xvYmFsOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmIiwidmFyIGhhc093blByb3BlcnR5ID0ge30uaGFzT3duUHJvcGVydHk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGl0LCBrZXkpe1xuICByZXR1cm4gaGFzT3duUHJvcGVydHkuY2FsbChpdCwga2V5KTtcbn07IiwidmFyIGRQICAgICAgICAgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKVxuICAsIGNyZWF0ZURlc2MgPSByZXF1aXJlKCcuL19wcm9wZXJ0eS1kZXNjJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBmdW5jdGlvbihvYmplY3QsIGtleSwgdmFsdWUpe1xuICByZXR1cm4gZFAuZihvYmplY3QsIGtleSwgY3JlYXRlRGVzYygxLCB2YWx1ZSkpO1xufSA6IGZ1bmN0aW9uKG9iamVjdCwga2V5LCB2YWx1ZSl7XG4gIG9iamVjdFtrZXldID0gdmFsdWU7XG4gIHJldHVybiBvYmplY3Q7XG59OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fZ2xvYmFsJykuZG9jdW1lbnQgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50OyIsIm1vZHVsZS5leHBvcnRzID0gIXJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgJiYgIXJlcXVpcmUoJy4vX2ZhaWxzJykoZnVuY3Rpb24oKXtcbiAgcmV0dXJuIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShyZXF1aXJlKCcuL19kb20tY3JlYXRlJykoJ2RpdicpLCAnYScsIHtnZXQ6IGZ1bmN0aW9uKCl7IHJldHVybiA3OyB9fSkuYSAhPSA3O1xufSk7IiwiLy8gZmFsbGJhY2sgZm9yIG5vbi1hcnJheS1saWtlIEVTMyBhbmQgbm9uLWVudW1lcmFibGUgb2xkIFY4IHN0cmluZ3NcbnZhciBjb2YgPSByZXF1aXJlKCcuL19jb2YnKTtcbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0KCd6JykucHJvcGVydHlJc0VudW1lcmFibGUoMCkgPyBPYmplY3QgOiBmdW5jdGlvbihpdCl7XG4gIHJldHVybiBjb2YoaXQpID09ICdTdHJpbmcnID8gaXQuc3BsaXQoJycpIDogT2JqZWN0KGl0KTtcbn07IiwiLy8gY2hlY2sgb24gZGVmYXVsdCBBcnJheSBpdGVyYXRvclxudmFyIEl0ZXJhdG9ycyAgPSByZXF1aXJlKCcuL19pdGVyYXRvcnMnKVxuICAsIElURVJBVE9SICAgPSByZXF1aXJlKCcuL193a3MnKSgnaXRlcmF0b3InKVxuICAsIEFycmF5UHJvdG8gPSBBcnJheS5wcm90b3R5cGU7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oaXQpe1xuICByZXR1cm4gaXQgIT09IHVuZGVmaW5lZCAmJiAoSXRlcmF0b3JzLkFycmF5ID09PSBpdCB8fCBBcnJheVByb3RvW0lURVJBVE9SXSA9PT0gaXQpO1xufTsiLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGl0KXtcbiAgcmV0dXJuIHR5cGVvZiBpdCA9PT0gJ29iamVjdCcgPyBpdCAhPT0gbnVsbCA6IHR5cGVvZiBpdCA9PT0gJ2Z1bmN0aW9uJztcbn07IiwiLy8gY2FsbCBzb21ldGhpbmcgb24gaXRlcmF0b3Igc3RlcCB3aXRoIHNhZmUgY2xvc2luZyBvbiBlcnJvclxudmFyIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0Jyk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGl0ZXJhdG9yLCBmbiwgdmFsdWUsIGVudHJpZXMpe1xuICB0cnkge1xuICAgIHJldHVybiBlbnRyaWVzID8gZm4oYW5PYmplY3QodmFsdWUpWzBdLCB2YWx1ZVsxXSkgOiBmbih2YWx1ZSk7XG4gIC8vIDcuNC42IEl0ZXJhdG9yQ2xvc2UoaXRlcmF0b3IsIGNvbXBsZXRpb24pXG4gIH0gY2F0Y2goZSl7XG4gICAgdmFyIHJldCA9IGl0ZXJhdG9yWydyZXR1cm4nXTtcbiAgICBpZihyZXQgIT09IHVuZGVmaW5lZClhbk9iamVjdChyZXQuY2FsbChpdGVyYXRvcikpO1xuICAgIHRocm93IGU7XG4gIH1cbn07IiwiJ3VzZSBzdHJpY3QnO1xudmFyIGNyZWF0ZSAgICAgICAgID0gcmVxdWlyZSgnLi9fb2JqZWN0LWNyZWF0ZScpXG4gICwgZGVzY3JpcHRvciAgICAgPSByZXF1aXJlKCcuL19wcm9wZXJ0eS1kZXNjJylcbiAgLCBzZXRUb1N0cmluZ1RhZyA9IHJlcXVpcmUoJy4vX3NldC10by1zdHJpbmctdGFnJylcbiAgLCBJdGVyYXRvclByb3RvdHlwZSA9IHt9O1xuXG4vLyAyNS4xLjIuMS4xICVJdGVyYXRvclByb3RvdHlwZSVbQEBpdGVyYXRvcl0oKVxucmVxdWlyZSgnLi9faGlkZScpKEl0ZXJhdG9yUHJvdG90eXBlLCByZXF1aXJlKCcuL193a3MnKSgnaXRlcmF0b3InKSwgZnVuY3Rpb24oKXsgcmV0dXJuIHRoaXM7IH0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKENvbnN0cnVjdG9yLCBOQU1FLCBuZXh0KXtcbiAgQ29uc3RydWN0b3IucHJvdG90eXBlID0gY3JlYXRlKEl0ZXJhdG9yUHJvdG90eXBlLCB7bmV4dDogZGVzY3JpcHRvcigxLCBuZXh0KX0pO1xuICBzZXRUb1N0cmluZ1RhZyhDb25zdHJ1Y3RvciwgTkFNRSArICcgSXRlcmF0b3InKTtcbn07IiwiJ3VzZSBzdHJpY3QnO1xudmFyIExJQlJBUlkgICAgICAgID0gcmVxdWlyZSgnLi9fbGlicmFyeScpXG4gICwgJGV4cG9ydCAgICAgICAgPSByZXF1aXJlKCcuL19leHBvcnQnKVxuICAsIHJlZGVmaW5lICAgICAgID0gcmVxdWlyZSgnLi9fcmVkZWZpbmUnKVxuICAsIGhpZGUgICAgICAgICAgID0gcmVxdWlyZSgnLi9faGlkZScpXG4gICwgaGFzICAgICAgICAgICAgPSByZXF1aXJlKCcuL19oYXMnKVxuICAsIEl0ZXJhdG9ycyAgICAgID0gcmVxdWlyZSgnLi9faXRlcmF0b3JzJylcbiAgLCAkaXRlckNyZWF0ZSAgICA9IHJlcXVpcmUoJy4vX2l0ZXItY3JlYXRlJylcbiAgLCBzZXRUb1N0cmluZ1RhZyA9IHJlcXVpcmUoJy4vX3NldC10by1zdHJpbmctdGFnJylcbiAgLCBnZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJy4vX29iamVjdC1ncG8nKVxuICAsIElURVJBVE9SICAgICAgID0gcmVxdWlyZSgnLi9fd2tzJykoJ2l0ZXJhdG9yJylcbiAgLCBCVUdHWSAgICAgICAgICA9ICEoW10ua2V5cyAmJiAnbmV4dCcgaW4gW10ua2V5cygpKSAvLyBTYWZhcmkgaGFzIGJ1Z2d5IGl0ZXJhdG9ycyB3L28gYG5leHRgXG4gICwgRkZfSVRFUkFUT1IgICAgPSAnQEBpdGVyYXRvcidcbiAgLCBLRVlTICAgICAgICAgICA9ICdrZXlzJ1xuICAsIFZBTFVFUyAgICAgICAgID0gJ3ZhbHVlcyc7XG5cbnZhciByZXR1cm5UaGlzID0gZnVuY3Rpb24oKXsgcmV0dXJuIHRoaXM7IH07XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oQmFzZSwgTkFNRSwgQ29uc3RydWN0b3IsIG5leHQsIERFRkFVTFQsIElTX1NFVCwgRk9SQ0VEKXtcbiAgJGl0ZXJDcmVhdGUoQ29uc3RydWN0b3IsIE5BTUUsIG5leHQpO1xuICB2YXIgZ2V0TWV0aG9kID0gZnVuY3Rpb24oa2luZCl7XG4gICAgaWYoIUJVR0dZICYmIGtpbmQgaW4gcHJvdG8pcmV0dXJuIHByb3RvW2tpbmRdO1xuICAgIHN3aXRjaChraW5kKXtcbiAgICAgIGNhc2UgS0VZUzogcmV0dXJuIGZ1bmN0aW9uIGtleXMoKXsgcmV0dXJuIG5ldyBDb25zdHJ1Y3Rvcih0aGlzLCBraW5kKTsgfTtcbiAgICAgIGNhc2UgVkFMVUVTOiByZXR1cm4gZnVuY3Rpb24gdmFsdWVzKCl7IHJldHVybiBuZXcgQ29uc3RydWN0b3IodGhpcywga2luZCk7IH07XG4gICAgfSByZXR1cm4gZnVuY3Rpb24gZW50cmllcygpeyByZXR1cm4gbmV3IENvbnN0cnVjdG9yKHRoaXMsIGtpbmQpOyB9O1xuICB9O1xuICB2YXIgVEFHICAgICAgICA9IE5BTUUgKyAnIEl0ZXJhdG9yJ1xuICAgICwgREVGX1ZBTFVFUyA9IERFRkFVTFQgPT0gVkFMVUVTXG4gICAgLCBWQUxVRVNfQlVHID0gZmFsc2VcbiAgICAsIHByb3RvICAgICAgPSBCYXNlLnByb3RvdHlwZVxuICAgICwgJG5hdGl2ZSAgICA9IHByb3RvW0lURVJBVE9SXSB8fCBwcm90b1tGRl9JVEVSQVRPUl0gfHwgREVGQVVMVCAmJiBwcm90b1tERUZBVUxUXVxuICAgICwgJGRlZmF1bHQgICA9ICRuYXRpdmUgfHwgZ2V0TWV0aG9kKERFRkFVTFQpXG4gICAgLCAkZW50cmllcyAgID0gREVGQVVMVCA/ICFERUZfVkFMVUVTID8gJGRlZmF1bHQgOiBnZXRNZXRob2QoJ2VudHJpZXMnKSA6IHVuZGVmaW5lZFxuICAgICwgJGFueU5hdGl2ZSA9IE5BTUUgPT0gJ0FycmF5JyA/IHByb3RvLmVudHJpZXMgfHwgJG5hdGl2ZSA6ICRuYXRpdmVcbiAgICAsIG1ldGhvZHMsIGtleSwgSXRlcmF0b3JQcm90b3R5cGU7XG4gIC8vIEZpeCBuYXRpdmVcbiAgaWYoJGFueU5hdGl2ZSl7XG4gICAgSXRlcmF0b3JQcm90b3R5cGUgPSBnZXRQcm90b3R5cGVPZigkYW55TmF0aXZlLmNhbGwobmV3IEJhc2UpKTtcbiAgICBpZihJdGVyYXRvclByb3RvdHlwZSAhPT0gT2JqZWN0LnByb3RvdHlwZSl7XG4gICAgICAvLyBTZXQgQEB0b1N0cmluZ1RhZyB0byBuYXRpdmUgaXRlcmF0b3JzXG4gICAgICBzZXRUb1N0cmluZ1RhZyhJdGVyYXRvclByb3RvdHlwZSwgVEFHLCB0cnVlKTtcbiAgICAgIC8vIGZpeCBmb3Igc29tZSBvbGQgZW5naW5lc1xuICAgICAgaWYoIUxJQlJBUlkgJiYgIWhhcyhJdGVyYXRvclByb3RvdHlwZSwgSVRFUkFUT1IpKWhpZGUoSXRlcmF0b3JQcm90b3R5cGUsIElURVJBVE9SLCByZXR1cm5UaGlzKTtcbiAgICB9XG4gIH1cbiAgLy8gZml4IEFycmF5I3t2YWx1ZXMsIEBAaXRlcmF0b3J9Lm5hbWUgaW4gVjggLyBGRlxuICBpZihERUZfVkFMVUVTICYmICRuYXRpdmUgJiYgJG5hdGl2ZS5uYW1lICE9PSBWQUxVRVMpe1xuICAgIFZBTFVFU19CVUcgPSB0cnVlO1xuICAgICRkZWZhdWx0ID0gZnVuY3Rpb24gdmFsdWVzKCl7IHJldHVybiAkbmF0aXZlLmNhbGwodGhpcyk7IH07XG4gIH1cbiAgLy8gRGVmaW5lIGl0ZXJhdG9yXG4gIGlmKCghTElCUkFSWSB8fCBGT1JDRUQpICYmIChCVUdHWSB8fCBWQUxVRVNfQlVHIHx8ICFwcm90b1tJVEVSQVRPUl0pKXtcbiAgICBoaWRlKHByb3RvLCBJVEVSQVRPUiwgJGRlZmF1bHQpO1xuICB9XG4gIC8vIFBsdWcgZm9yIGxpYnJhcnlcbiAgSXRlcmF0b3JzW05BTUVdID0gJGRlZmF1bHQ7XG4gIEl0ZXJhdG9yc1tUQUddICA9IHJldHVyblRoaXM7XG4gIGlmKERFRkFVTFQpe1xuICAgIG1ldGhvZHMgPSB7XG4gICAgICB2YWx1ZXM6ICBERUZfVkFMVUVTID8gJGRlZmF1bHQgOiBnZXRNZXRob2QoVkFMVUVTKSxcbiAgICAgIGtleXM6ICAgIElTX1NFVCAgICAgPyAkZGVmYXVsdCA6IGdldE1ldGhvZChLRVlTKSxcbiAgICAgIGVudHJpZXM6ICRlbnRyaWVzXG4gICAgfTtcbiAgICBpZihGT1JDRUQpZm9yKGtleSBpbiBtZXRob2RzKXtcbiAgICAgIGlmKCEoa2V5IGluIHByb3RvKSlyZWRlZmluZShwcm90bywga2V5LCBtZXRob2RzW2tleV0pO1xuICAgIH0gZWxzZSAkZXhwb3J0KCRleHBvcnQuUCArICRleHBvcnQuRiAqIChCVUdHWSB8fCBWQUxVRVNfQlVHKSwgTkFNRSwgbWV0aG9kcyk7XG4gIH1cbiAgcmV0dXJuIG1ldGhvZHM7XG59OyIsInZhciBJVEVSQVRPUiAgICAgPSByZXF1aXJlKCcuL193a3MnKSgnaXRlcmF0b3InKVxuICAsIFNBRkVfQ0xPU0lORyA9IGZhbHNlO1xuXG50cnkge1xuICB2YXIgcml0ZXIgPSBbN11bSVRFUkFUT1JdKCk7XG4gIHJpdGVyWydyZXR1cm4nXSA9IGZ1bmN0aW9uKCl7IFNBRkVfQ0xPU0lORyA9IHRydWU7IH07XG4gIEFycmF5LmZyb20ocml0ZXIsIGZ1bmN0aW9uKCl7IHRocm93IDI7IH0pO1xufSBjYXRjaChlKXsgLyogZW1wdHkgKi8gfVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGV4ZWMsIHNraXBDbG9zaW5nKXtcbiAgaWYoIXNraXBDbG9zaW5nICYmICFTQUZFX0NMT1NJTkcpcmV0dXJuIGZhbHNlO1xuICB2YXIgc2FmZSA9IGZhbHNlO1xuICB0cnkge1xuICAgIHZhciBhcnIgID0gWzddXG4gICAgICAsIGl0ZXIgPSBhcnJbSVRFUkFUT1JdKCk7XG4gICAgaXRlci5uZXh0ID0gZnVuY3Rpb24oKXsgcmV0dXJuIHtkb25lOiBzYWZlID0gdHJ1ZX07IH07XG4gICAgYXJyW0lURVJBVE9SXSA9IGZ1bmN0aW9uKCl7IHJldHVybiBpdGVyOyB9O1xuICAgIGV4ZWMoYXJyKTtcbiAgfSBjYXRjaChlKXsgLyogZW1wdHkgKi8gfVxuICByZXR1cm4gc2FmZTtcbn07IiwibW9kdWxlLmV4cG9ydHMgPSB7fTsiLCJtb2R1bGUuZXhwb3J0cyA9IHRydWU7IiwiLy8gMTkuMS4yLjIgLyAxNS4yLjMuNSBPYmplY3QuY3JlYXRlKE8gWywgUHJvcGVydGllc10pXG52YXIgYW5PYmplY3QgICAgPSByZXF1aXJlKCcuL19hbi1vYmplY3QnKVxuICAsIGRQcyAgICAgICAgID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwcycpXG4gICwgZW51bUJ1Z0tleXMgPSByZXF1aXJlKCcuL19lbnVtLWJ1Zy1rZXlzJylcbiAgLCBJRV9QUk9UTyAgICA9IHJlcXVpcmUoJy4vX3NoYXJlZC1rZXknKSgnSUVfUFJPVE8nKVxuICAsIEVtcHR5ICAgICAgID0gZnVuY3Rpb24oKXsgLyogZW1wdHkgKi8gfVxuICAsIFBST1RPVFlQRSAgID0gJ3Byb3RvdHlwZSc7XG5cbi8vIENyZWF0ZSBvYmplY3Qgd2l0aCBmYWtlIGBudWxsYCBwcm90b3R5cGU6IHVzZSBpZnJhbWUgT2JqZWN0IHdpdGggY2xlYXJlZCBwcm90b3R5cGVcbnZhciBjcmVhdGVEaWN0ID0gZnVuY3Rpb24oKXtcbiAgLy8gVGhyYXNoLCB3YXN0ZSBhbmQgc29kb215OiBJRSBHQyBidWdcbiAgdmFyIGlmcmFtZSA9IHJlcXVpcmUoJy4vX2RvbS1jcmVhdGUnKSgnaWZyYW1lJylcbiAgICAsIGkgICAgICA9IGVudW1CdWdLZXlzLmxlbmd0aFxuICAgICwgbHQgICAgID0gJzwnXG4gICAgLCBndCAgICAgPSAnPidcbiAgICAsIGlmcmFtZURvY3VtZW50O1xuICBpZnJhbWUuc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgcmVxdWlyZSgnLi9faHRtbCcpLmFwcGVuZENoaWxkKGlmcmFtZSk7XG4gIGlmcmFtZS5zcmMgPSAnamF2YXNjcmlwdDonOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXNjcmlwdC11cmxcbiAgLy8gY3JlYXRlRGljdCA9IGlmcmFtZS5jb250ZW50V2luZG93Lk9iamVjdDtcbiAgLy8gaHRtbC5yZW1vdmVDaGlsZChpZnJhbWUpO1xuICBpZnJhbWVEb2N1bWVudCA9IGlmcmFtZS5jb250ZW50V2luZG93LmRvY3VtZW50O1xuICBpZnJhbWVEb2N1bWVudC5vcGVuKCk7XG4gIGlmcmFtZURvY3VtZW50LndyaXRlKGx0ICsgJ3NjcmlwdCcgKyBndCArICdkb2N1bWVudC5GPU9iamVjdCcgKyBsdCArICcvc2NyaXB0JyArIGd0KTtcbiAgaWZyYW1lRG9jdW1lbnQuY2xvc2UoKTtcbiAgY3JlYXRlRGljdCA9IGlmcmFtZURvY3VtZW50LkY7XG4gIHdoaWxlKGktLSlkZWxldGUgY3JlYXRlRGljdFtQUk9UT1RZUEVdW2VudW1CdWdLZXlzW2ldXTtcbiAgcmV0dXJuIGNyZWF0ZURpY3QoKTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gT2JqZWN0LmNyZWF0ZSB8fCBmdW5jdGlvbiBjcmVhdGUoTywgUHJvcGVydGllcyl7XG4gIHZhciByZXN1bHQ7XG4gIGlmKE8gIT09IG51bGwpe1xuICAgIEVtcHR5W1BST1RPVFlQRV0gPSBhbk9iamVjdChPKTtcbiAgICByZXN1bHQgPSBuZXcgRW1wdHk7XG4gICAgRW1wdHlbUFJPVE9UWVBFXSA9IG51bGw7XG4gICAgLy8gYWRkIFwiX19wcm90b19fXCIgZm9yIE9iamVjdC5nZXRQcm90b3R5cGVPZiBwb2x5ZmlsbFxuICAgIHJlc3VsdFtJRV9QUk9UT10gPSBPO1xuICB9IGVsc2UgcmVzdWx0ID0gY3JlYXRlRGljdCgpO1xuICByZXR1cm4gUHJvcGVydGllcyA9PT0gdW5kZWZpbmVkID8gcmVzdWx0IDogZFBzKHJlc3VsdCwgUHJvcGVydGllcyk7XG59O1xuIiwidmFyIGFuT2JqZWN0ICAgICAgID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0JylcbiAgLCBJRThfRE9NX0RFRklORSA9IHJlcXVpcmUoJy4vX2llOC1kb20tZGVmaW5lJylcbiAgLCB0b1ByaW1pdGl2ZSAgICA9IHJlcXVpcmUoJy4vX3RvLXByaW1pdGl2ZScpXG4gICwgZFAgICAgICAgICAgICAgPSBPYmplY3QuZGVmaW5lUHJvcGVydHk7XG5cbmV4cG9ydHMuZiA9IHJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJykgPyBPYmplY3QuZGVmaW5lUHJvcGVydHkgOiBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0eShPLCBQLCBBdHRyaWJ1dGVzKXtcbiAgYW5PYmplY3QoTyk7XG4gIFAgPSB0b1ByaW1pdGl2ZShQLCB0cnVlKTtcbiAgYW5PYmplY3QoQXR0cmlidXRlcyk7XG4gIGlmKElFOF9ET01fREVGSU5FKXRyeSB7XG4gICAgcmV0dXJuIGRQKE8sIFAsIEF0dHJpYnV0ZXMpO1xuICB9IGNhdGNoKGUpeyAvKiBlbXB0eSAqLyB9XG4gIGlmKCdnZXQnIGluIEF0dHJpYnV0ZXMgfHwgJ3NldCcgaW4gQXR0cmlidXRlcyl0aHJvdyBUeXBlRXJyb3IoJ0FjY2Vzc29ycyBub3Qgc3VwcG9ydGVkIScpO1xuICBpZigndmFsdWUnIGluIEF0dHJpYnV0ZXMpT1tQXSA9IEF0dHJpYnV0ZXMudmFsdWU7XG4gIHJldHVybiBPO1xufTsiLCJ2YXIgZFAgICAgICAgPSByZXF1aXJlKCcuL19vYmplY3QtZHAnKVxuICAsIGFuT2JqZWN0ID0gcmVxdWlyZSgnLi9fYW4tb2JqZWN0JylcbiAgLCBnZXRLZXlzICA9IHJlcXVpcmUoJy4vX29iamVjdC1rZXlzJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fZGVzY3JpcHRvcnMnKSA/IE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzIDogZnVuY3Rpb24gZGVmaW5lUHJvcGVydGllcyhPLCBQcm9wZXJ0aWVzKXtcbiAgYW5PYmplY3QoTyk7XG4gIHZhciBrZXlzICAgPSBnZXRLZXlzKFByb3BlcnRpZXMpXG4gICAgLCBsZW5ndGggPSBrZXlzLmxlbmd0aFxuICAgICwgaSA9IDBcbiAgICAsIFA7XG4gIHdoaWxlKGxlbmd0aCA+IGkpZFAuZihPLCBQID0ga2V5c1tpKytdLCBQcm9wZXJ0aWVzW1BdKTtcbiAgcmV0dXJuIE87XG59OyIsIi8vIDE5LjEuMi45IC8gMTUuMi4zLjIgT2JqZWN0LmdldFByb3RvdHlwZU9mKE8pXG52YXIgaGFzICAgICAgICAgPSByZXF1aXJlKCcuL19oYXMnKVxuICAsIHRvT2JqZWN0ICAgID0gcmVxdWlyZSgnLi9fdG8tb2JqZWN0JylcbiAgLCBJRV9QUk9UTyAgICA9IHJlcXVpcmUoJy4vX3NoYXJlZC1rZXknKSgnSUVfUFJPVE8nKVxuICAsIE9iamVjdFByb3RvID0gT2JqZWN0LnByb3RvdHlwZTtcblxubW9kdWxlLmV4cG9ydHMgPSBPYmplY3QuZ2V0UHJvdG90eXBlT2YgfHwgZnVuY3Rpb24oTyl7XG4gIE8gPSB0b09iamVjdChPKTtcbiAgaWYoaGFzKE8sIElFX1BST1RPKSlyZXR1cm4gT1tJRV9QUk9UT107XG4gIGlmKHR5cGVvZiBPLmNvbnN0cnVjdG9yID09ICdmdW5jdGlvbicgJiYgTyBpbnN0YW5jZW9mIE8uY29uc3RydWN0b3Ipe1xuICAgIHJldHVybiBPLmNvbnN0cnVjdG9yLnByb3RvdHlwZTtcbiAgfSByZXR1cm4gTyBpbnN0YW5jZW9mIE9iamVjdCA/IE9iamVjdFByb3RvIDogbnVsbDtcbn07IiwidmFyIGhhcyAgICAgICAgICA9IHJlcXVpcmUoJy4vX2hhcycpXG4gICwgdG9JT2JqZWN0ICAgID0gcmVxdWlyZSgnLi9fdG8taW9iamVjdCcpXG4gICwgYXJyYXlJbmRleE9mID0gcmVxdWlyZSgnLi9fYXJyYXktaW5jbHVkZXMnKShmYWxzZSlcbiAgLCBJRV9QUk9UTyAgICAgPSByZXF1aXJlKCcuL19zaGFyZWQta2V5JykoJ0lFX1BST1RPJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24ob2JqZWN0LCBuYW1lcyl7XG4gIHZhciBPICAgICAgPSB0b0lPYmplY3Qob2JqZWN0KVxuICAgICwgaSAgICAgID0gMFxuICAgICwgcmVzdWx0ID0gW11cbiAgICAsIGtleTtcbiAgZm9yKGtleSBpbiBPKWlmKGtleSAhPSBJRV9QUk9UTyloYXMoTywga2V5KSAmJiByZXN1bHQucHVzaChrZXkpO1xuICAvLyBEb24ndCBlbnVtIGJ1ZyAmIGhpZGRlbiBrZXlzXG4gIHdoaWxlKG5hbWVzLmxlbmd0aCA+IGkpaWYoaGFzKE8sIGtleSA9IG5hbWVzW2krK10pKXtcbiAgICB+YXJyYXlJbmRleE9mKHJlc3VsdCwga2V5KSB8fCByZXN1bHQucHVzaChrZXkpO1xuICB9XG4gIHJldHVybiByZXN1bHQ7XG59OyIsIi8vIDE5LjEuMi4xNCAvIDE1LjIuMy4xNCBPYmplY3Qua2V5cyhPKVxudmFyICRrZXlzICAgICAgID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMtaW50ZXJuYWwnKVxuICAsIGVudW1CdWdLZXlzID0gcmVxdWlyZSgnLi9fZW51bS1idWcta2V5cycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IE9iamVjdC5rZXlzIHx8IGZ1bmN0aW9uIGtleXMoTyl7XG4gIHJldHVybiAka2V5cyhPLCBlbnVtQnVnS2V5cyk7XG59OyIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oYml0bWFwLCB2YWx1ZSl7XG4gIHJldHVybiB7XG4gICAgZW51bWVyYWJsZSAgOiAhKGJpdG1hcCAmIDEpLFxuICAgIGNvbmZpZ3VyYWJsZTogIShiaXRtYXAgJiAyKSxcbiAgICB3cml0YWJsZSAgICA6ICEoYml0bWFwICYgNCksXG4gICAgdmFsdWUgICAgICAgOiB2YWx1ZVxuICB9O1xufTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vX2hpZGUnKTsiLCJ2YXIgZGVmID0gcmVxdWlyZSgnLi9fb2JqZWN0LWRwJykuZlxuICAsIGhhcyA9IHJlcXVpcmUoJy4vX2hhcycpXG4gICwgVEFHID0gcmVxdWlyZSgnLi9fd2tzJykoJ3RvU3RyaW5nVGFnJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oaXQsIHRhZywgc3RhdCl7XG4gIGlmKGl0ICYmICFoYXMoaXQgPSBzdGF0ID8gaXQgOiBpdC5wcm90b3R5cGUsIFRBRykpZGVmKGl0LCBUQUcsIHtjb25maWd1cmFibGU6IHRydWUsIHZhbHVlOiB0YWd9KTtcbn07IiwidmFyIHNoYXJlZCA9IHJlcXVpcmUoJy4vX3NoYXJlZCcpKCdrZXlzJylcbiAgLCB1aWQgICAgPSByZXF1aXJlKCcuL191aWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oa2V5KXtcbiAgcmV0dXJuIHNoYXJlZFtrZXldIHx8IChzaGFyZWRba2V5XSA9IHVpZChrZXkpKTtcbn07IiwidmFyIGdsb2JhbCA9IHJlcXVpcmUoJy4vX2dsb2JhbCcpXG4gICwgU0hBUkVEID0gJ19fY29yZS1qc19zaGFyZWRfXydcbiAgLCBzdG9yZSAgPSBnbG9iYWxbU0hBUkVEXSB8fCAoZ2xvYmFsW1NIQVJFRF0gPSB7fSk7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGtleSl7XG4gIHJldHVybiBzdG9yZVtrZXldIHx8IChzdG9yZVtrZXldID0ge30pO1xufTsiLCJ2YXIgdG9JbnRlZ2VyID0gcmVxdWlyZSgnLi9fdG8taW50ZWdlcicpXG4gICwgZGVmaW5lZCAgID0gcmVxdWlyZSgnLi9fZGVmaW5lZCcpO1xuLy8gdHJ1ZSAgLT4gU3RyaW5nI2F0XG4vLyBmYWxzZSAtPiBTdHJpbmcjY29kZVBvaW50QXRcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oVE9fU1RSSU5HKXtcbiAgcmV0dXJuIGZ1bmN0aW9uKHRoYXQsIHBvcyl7XG4gICAgdmFyIHMgPSBTdHJpbmcoZGVmaW5lZCh0aGF0KSlcbiAgICAgICwgaSA9IHRvSW50ZWdlcihwb3MpXG4gICAgICAsIGwgPSBzLmxlbmd0aFxuICAgICAgLCBhLCBiO1xuICAgIGlmKGkgPCAwIHx8IGkgPj0gbClyZXR1cm4gVE9fU1RSSU5HID8gJycgOiB1bmRlZmluZWQ7XG4gICAgYSA9IHMuY2hhckNvZGVBdChpKTtcbiAgICByZXR1cm4gYSA8IDB4ZDgwMCB8fCBhID4gMHhkYmZmIHx8IGkgKyAxID09PSBsIHx8IChiID0gcy5jaGFyQ29kZUF0KGkgKyAxKSkgPCAweGRjMDAgfHwgYiA+IDB4ZGZmZlxuICAgICAgPyBUT19TVFJJTkcgPyBzLmNoYXJBdChpKSA6IGFcbiAgICAgIDogVE9fU1RSSU5HID8gcy5zbGljZShpLCBpICsgMikgOiAoYSAtIDB4ZDgwMCA8PCAxMCkgKyAoYiAtIDB4ZGMwMCkgKyAweDEwMDAwO1xuICB9O1xufTsiLCJ2YXIgdG9JbnRlZ2VyID0gcmVxdWlyZSgnLi9fdG8taW50ZWdlcicpXG4gICwgbWF4ICAgICAgID0gTWF0aC5tYXhcbiAgLCBtaW4gICAgICAgPSBNYXRoLm1pbjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oaW5kZXgsIGxlbmd0aCl7XG4gIGluZGV4ID0gdG9JbnRlZ2VyKGluZGV4KTtcbiAgcmV0dXJuIGluZGV4IDwgMCA/IG1heChpbmRleCArIGxlbmd0aCwgMCkgOiBtaW4oaW5kZXgsIGxlbmd0aCk7XG59OyIsIi8vIDcuMS40IFRvSW50ZWdlclxudmFyIGNlaWwgID0gTWF0aC5jZWlsXG4gICwgZmxvb3IgPSBNYXRoLmZsb29yO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihpdCl7XG4gIHJldHVybiBpc05hTihpdCA9ICtpdCkgPyAwIDogKGl0ID4gMCA/IGZsb29yIDogY2VpbCkoaXQpO1xufTsiLCIvLyB0byBpbmRleGVkIG9iamVjdCwgdG9PYmplY3Qgd2l0aCBmYWxsYmFjayBmb3Igbm9uLWFycmF5LWxpa2UgRVMzIHN0cmluZ3NcbnZhciBJT2JqZWN0ID0gcmVxdWlyZSgnLi9faW9iamVjdCcpXG4gICwgZGVmaW5lZCA9IHJlcXVpcmUoJy4vX2RlZmluZWQnKTtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oaXQpe1xuICByZXR1cm4gSU9iamVjdChkZWZpbmVkKGl0KSk7XG59OyIsIi8vIDcuMS4xNSBUb0xlbmd0aFxudmFyIHRvSW50ZWdlciA9IHJlcXVpcmUoJy4vX3RvLWludGVnZXInKVxuICAsIG1pbiAgICAgICA9IE1hdGgubWluO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihpdCl7XG4gIHJldHVybiBpdCA+IDAgPyBtaW4odG9JbnRlZ2VyKGl0KSwgMHgxZmZmZmZmZmZmZmZmZikgOiAwOyAvLyBwb3coMiwgNTMpIC0gMSA9PSA5MDA3MTk5MjU0NzQwOTkxXG59OyIsIi8vIDcuMS4xMyBUb09iamVjdChhcmd1bWVudClcbnZhciBkZWZpbmVkID0gcmVxdWlyZSgnLi9fZGVmaW5lZCcpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihpdCl7XG4gIHJldHVybiBPYmplY3QoZGVmaW5lZChpdCkpO1xufTsiLCIvLyA3LjEuMSBUb1ByaW1pdGl2ZShpbnB1dCBbLCBQcmVmZXJyZWRUeXBlXSlcbnZhciBpc09iamVjdCA9IHJlcXVpcmUoJy4vX2lzLW9iamVjdCcpO1xuLy8gaW5zdGVhZCBvZiB0aGUgRVM2IHNwZWMgdmVyc2lvbiwgd2UgZGlkbid0IGltcGxlbWVudCBAQHRvUHJpbWl0aXZlIGNhc2Vcbi8vIGFuZCB0aGUgc2Vjb25kIGFyZ3VtZW50IC0gZmxhZyAtIHByZWZlcnJlZCB0eXBlIGlzIGEgc3RyaW5nXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGl0LCBTKXtcbiAgaWYoIWlzT2JqZWN0KGl0KSlyZXR1cm4gaXQ7XG4gIHZhciBmbiwgdmFsO1xuICBpZihTICYmIHR5cGVvZiAoZm4gPSBpdC50b1N0cmluZykgPT0gJ2Z1bmN0aW9uJyAmJiAhaXNPYmplY3QodmFsID0gZm4uY2FsbChpdCkpKXJldHVybiB2YWw7XG4gIGlmKHR5cGVvZiAoZm4gPSBpdC52YWx1ZU9mKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpcmV0dXJuIHZhbDtcbiAgaWYoIVMgJiYgdHlwZW9mIChmbiA9IGl0LnRvU3RyaW5nKSA9PSAnZnVuY3Rpb24nICYmICFpc09iamVjdCh2YWwgPSBmbi5jYWxsKGl0KSkpcmV0dXJuIHZhbDtcbiAgdGhyb3cgVHlwZUVycm9yKFwiQ2FuJ3QgY29udmVydCBvYmplY3QgdG8gcHJpbWl0aXZlIHZhbHVlXCIpO1xufTsiLCJ2YXIgaWQgPSAwXG4gICwgcHggPSBNYXRoLnJhbmRvbSgpO1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihrZXkpe1xuICByZXR1cm4gJ1N5bWJvbCgnLmNvbmNhdChrZXkgPT09IHVuZGVmaW5lZCA/ICcnIDoga2V5LCAnKV8nLCAoKytpZCArIHB4KS50b1N0cmluZygzNikpO1xufTsiLCJ2YXIgc3RvcmUgICAgICA9IHJlcXVpcmUoJy4vX3NoYXJlZCcpKCd3a3MnKVxuICAsIHVpZCAgICAgICAgPSByZXF1aXJlKCcuL191aWQnKVxuICAsIFN5bWJvbCAgICAgPSByZXF1aXJlKCcuL19nbG9iYWwnKS5TeW1ib2xcbiAgLCBVU0VfU1lNQk9MID0gdHlwZW9mIFN5bWJvbCA9PSAnZnVuY3Rpb24nO1xuXG52YXIgJGV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG5hbWUpe1xuICByZXR1cm4gc3RvcmVbbmFtZV0gfHwgKHN0b3JlW25hbWVdID1cbiAgICBVU0VfU1lNQk9MICYmIFN5bWJvbFtuYW1lXSB8fCAoVVNFX1NZTUJPTCA/IFN5bWJvbCA6IHVpZCkoJ1N5bWJvbC4nICsgbmFtZSkpO1xufTtcblxuJGV4cG9ydHMuc3RvcmUgPSBzdG9yZTsiLCJ2YXIgY2xhc3NvZiAgID0gcmVxdWlyZSgnLi9fY2xhc3NvZicpXG4gICwgSVRFUkFUT1IgID0gcmVxdWlyZSgnLi9fd2tzJykoJ2l0ZXJhdG9yJylcbiAgLCBJdGVyYXRvcnMgPSByZXF1aXJlKCcuL19pdGVyYXRvcnMnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9fY29yZScpLmdldEl0ZXJhdG9yTWV0aG9kID0gZnVuY3Rpb24oaXQpe1xuICBpZihpdCAhPSB1bmRlZmluZWQpcmV0dXJuIGl0W0lURVJBVE9SXVxuICAgIHx8IGl0WydAQGl0ZXJhdG9yJ11cbiAgICB8fCBJdGVyYXRvcnNbY2xhc3NvZihpdCldO1xufTsiLCIndXNlIHN0cmljdCc7XG52YXIgY3R4ICAgICAgICAgICAgPSByZXF1aXJlKCcuL19jdHgnKVxuICAsICRleHBvcnQgICAgICAgID0gcmVxdWlyZSgnLi9fZXhwb3J0JylcbiAgLCB0b09iamVjdCAgICAgICA9IHJlcXVpcmUoJy4vX3RvLW9iamVjdCcpXG4gICwgY2FsbCAgICAgICAgICAgPSByZXF1aXJlKCcuL19pdGVyLWNhbGwnKVxuICAsIGlzQXJyYXlJdGVyICAgID0gcmVxdWlyZSgnLi9faXMtYXJyYXktaXRlcicpXG4gICwgdG9MZW5ndGggICAgICAgPSByZXF1aXJlKCcuL190by1sZW5ndGgnKVxuICAsIGNyZWF0ZVByb3BlcnR5ID0gcmVxdWlyZSgnLi9fY3JlYXRlLXByb3BlcnR5JylcbiAgLCBnZXRJdGVyRm4gICAgICA9IHJlcXVpcmUoJy4vY29yZS5nZXQtaXRlcmF0b3ItbWV0aG9kJyk7XG5cbiRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogIXJlcXVpcmUoJy4vX2l0ZXItZGV0ZWN0JykoZnVuY3Rpb24oaXRlcil7IEFycmF5LmZyb20oaXRlcik7IH0pLCAnQXJyYXknLCB7XG4gIC8vIDIyLjEuMi4xIEFycmF5LmZyb20oYXJyYXlMaWtlLCBtYXBmbiA9IHVuZGVmaW5lZCwgdGhpc0FyZyA9IHVuZGVmaW5lZClcbiAgZnJvbTogZnVuY3Rpb24gZnJvbShhcnJheUxpa2UvKiwgbWFwZm4gPSB1bmRlZmluZWQsIHRoaXNBcmcgPSB1bmRlZmluZWQqLyl7XG4gICAgdmFyIE8gICAgICAgPSB0b09iamVjdChhcnJheUxpa2UpXG4gICAgICAsIEMgICAgICAgPSB0eXBlb2YgdGhpcyA9PSAnZnVuY3Rpb24nID8gdGhpcyA6IEFycmF5XG4gICAgICAsIGFMZW4gICAgPSBhcmd1bWVudHMubGVuZ3RoXG4gICAgICAsIG1hcGZuICAgPSBhTGVuID4gMSA/IGFyZ3VtZW50c1sxXSA6IHVuZGVmaW5lZFxuICAgICAgLCBtYXBwaW5nID0gbWFwZm4gIT09IHVuZGVmaW5lZFxuICAgICAgLCBpbmRleCAgID0gMFxuICAgICAgLCBpdGVyRm4gID0gZ2V0SXRlckZuKE8pXG4gICAgICAsIGxlbmd0aCwgcmVzdWx0LCBzdGVwLCBpdGVyYXRvcjtcbiAgICBpZihtYXBwaW5nKW1hcGZuID0gY3R4KG1hcGZuLCBhTGVuID4gMiA/IGFyZ3VtZW50c1syXSA6IHVuZGVmaW5lZCwgMik7XG4gICAgLy8gaWYgb2JqZWN0IGlzbid0IGl0ZXJhYmxlIG9yIGl0J3MgYXJyYXkgd2l0aCBkZWZhdWx0IGl0ZXJhdG9yIC0gdXNlIHNpbXBsZSBjYXNlXG4gICAgaWYoaXRlckZuICE9IHVuZGVmaW5lZCAmJiAhKEMgPT0gQXJyYXkgJiYgaXNBcnJheUl0ZXIoaXRlckZuKSkpe1xuICAgICAgZm9yKGl0ZXJhdG9yID0gaXRlckZuLmNhbGwoTyksIHJlc3VsdCA9IG5ldyBDOyAhKHN0ZXAgPSBpdGVyYXRvci5uZXh0KCkpLmRvbmU7IGluZGV4Kyspe1xuICAgICAgICBjcmVhdGVQcm9wZXJ0eShyZXN1bHQsIGluZGV4LCBtYXBwaW5nID8gY2FsbChpdGVyYXRvciwgbWFwZm4sIFtzdGVwLnZhbHVlLCBpbmRleF0sIHRydWUpIDogc3RlcC52YWx1ZSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGxlbmd0aCA9IHRvTGVuZ3RoKE8ubGVuZ3RoKTtcbiAgICAgIGZvcihyZXN1bHQgPSBuZXcgQyhsZW5ndGgpOyBsZW5ndGggPiBpbmRleDsgaW5kZXgrKyl7XG4gICAgICAgIGNyZWF0ZVByb3BlcnR5KHJlc3VsdCwgaW5kZXgsIG1hcHBpbmcgPyBtYXBmbihPW2luZGV4XSwgaW5kZXgpIDogT1tpbmRleF0pO1xuICAgICAgfVxuICAgIH1cbiAgICByZXN1bHQubGVuZ3RoID0gaW5kZXg7XG4gICAgcmV0dXJuIHJlc3VsdDtcbiAgfVxufSk7XG4iLCJ2YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xuLy8gMTkuMS4yLjQgLyAxNS4yLjMuNiBPYmplY3QuZGVmaW5lUHJvcGVydHkoTywgUCwgQXR0cmlidXRlcylcbiRleHBvcnQoJGV4cG9ydC5TICsgJGV4cG9ydC5GICogIXJlcXVpcmUoJy4vX2Rlc2NyaXB0b3JzJyksICdPYmplY3QnLCB7ZGVmaW5lUHJvcGVydHk6IHJlcXVpcmUoJy4vX29iamVjdC1kcCcpLmZ9KTsiLCIndXNlIHN0cmljdCc7XG52YXIgJGF0ICA9IHJlcXVpcmUoJy4vX3N0cmluZy1hdCcpKHRydWUpO1xuXG4vLyAyMS4xLjMuMjcgU3RyaW5nLnByb3RvdHlwZVtAQGl0ZXJhdG9yXSgpXG5yZXF1aXJlKCcuL19pdGVyLWRlZmluZScpKFN0cmluZywgJ1N0cmluZycsIGZ1bmN0aW9uKGl0ZXJhdGVkKXtcbiAgdGhpcy5fdCA9IFN0cmluZyhpdGVyYXRlZCk7IC8vIHRhcmdldFxuICB0aGlzLl9pID0gMDsgICAgICAgICAgICAgICAgLy8gbmV4dCBpbmRleFxuLy8gMjEuMS41LjIuMSAlU3RyaW5nSXRlcmF0b3JQcm90b3R5cGUlLm5leHQoKVxufSwgZnVuY3Rpb24oKXtcbiAgdmFyIE8gICAgID0gdGhpcy5fdFxuICAgICwgaW5kZXggPSB0aGlzLl9pXG4gICAgLCBwb2ludDtcbiAgaWYoaW5kZXggPj0gTy5sZW5ndGgpcmV0dXJuIHt2YWx1ZTogdW5kZWZpbmVkLCBkb25lOiB0cnVlfTtcbiAgcG9pbnQgPSAkYXQoTywgaW5kZXgpO1xuICB0aGlzLl9pICs9IHBvaW50Lmxlbmd0aDtcbiAgcmV0dXJuIHt2YWx1ZTogcG9pbnQsIGRvbmU6IGZhbHNlfTtcbn0pOyIsIi8vIGltcG9ydFxudmFyIExheWVyICAgPSByZXF1aXJlKCcuL2xheWVyJylcbiwgICBOZXR3b3JrID0gcmVxdWlyZSgnLi9uZXR3b3JrJylcbiwgICBUcmFpbmVyID0gcmVxdWlyZSgnLi90cmFpbmVyJylcblxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBBUkNISVRFQ1RcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbi8vIENvbGxlY3Rpb24gb2YgdXNlZnVsIGJ1aWx0LWluIGFyY2hpdGVjdHVyZXNcbnZhciBBcmNoaXRlY3QgPSB7XG5cbiAgLy8gTXVsdGlsYXllciBQZXJjZXB0cm9uXG4gIFBlcmNlcHRyb246IGZ1bmN0aW9uIFBlcmNlcHRyb24oKSB7XG5cbiAgICB2YXIgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7IC8vIGNvbnZlcnQgYXJndW1lbnRzIHRvIEFycmF5XG4gICAgaWYgKGFyZ3MubGVuZ3RoIDwgMylcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIm5vdCBlbm91Z2ggbGF5ZXJzIChtaW5pbXVtIDMpICEhXCIpO1xuXG4gICAgdmFyIGlucHV0cyA9IGFyZ3Muc2hpZnQoKTsgLy8gZmlyc3QgYXJndW1lbnRcbiAgICB2YXIgb3V0cHV0cyA9IGFyZ3MucG9wKCk7IC8vIGxhc3QgYXJndW1lbnRcbiAgICB2YXIgbGF5ZXJzID0gYXJnczsgLy8gYWxsIHRoZSBhcmd1bWVudHMgaW4gdGhlIG1pZGRsZVxuXG4gICAgdmFyIGlucHV0ID0gbmV3IExheWVyKGlucHV0cyk7XG4gICAgdmFyIGhpZGRlbiA9IFtdO1xuICAgIHZhciBvdXRwdXQgPSBuZXcgTGF5ZXIob3V0cHV0cyk7XG5cbiAgICB2YXIgcHJldmlvdXMgPSBpbnB1dDtcblxuICAgIC8vIGdlbmVyYXRlIGhpZGRlbiBsYXllcnNcbiAgICBmb3IgKHZhciBsZXZlbCBpbiBsYXllcnMpIHtcbiAgICAgIHZhciBzaXplID0gbGF5ZXJzW2xldmVsXTtcbiAgICAgIHZhciBsYXllciA9IG5ldyBMYXllcihzaXplKTtcbiAgICAgIGhpZGRlbi5wdXNoKGxheWVyKTtcbiAgICAgIHByZXZpb3VzLnByb2plY3QobGF5ZXIpO1xuICAgICAgcHJldmlvdXMgPSBsYXllcjtcbiAgICB9XG4gICAgcHJldmlvdXMucHJvamVjdChvdXRwdXQpO1xuXG4gICAgLy8gc2V0IGxheWVycyBvZiB0aGUgbmV1cmFsIG5ldHdvcmtcbiAgICB0aGlzLnNldCh7XG4gICAgICBpbnB1dDogaW5wdXQsXG4gICAgICBoaWRkZW46IGhpZGRlbixcbiAgICAgIG91dHB1dDogb3V0cHV0XG4gICAgfSk7XG5cbiAgICAvLyB0cmFpbmVyIGZvciB0aGUgbmV0d29ya1xuICAgIHRoaXMudHJhaW5lciA9IG5ldyBUcmFpbmVyKHRoaXMpO1xuICB9LFxuXG4gIC8vIE11bHRpbGF5ZXIgTG9uZyBTaG9ydC1UZXJtIE1lbW9yeVxuICBMU1RNOiBmdW5jdGlvbiBMU1RNKCkge1xuXG4gICAgdmFyIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpOyAvLyBjb252ZXJ0IGFyZ3VtZW50cyB0byBhcnJheVxuICAgIGlmIChhcmdzLmxlbmd0aCA8IDMpXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJub3QgZW5vdWdoIGxheWVycyAobWluaW11bSAzKSAhIVwiKTtcblxuICAgIHZhciBsYXN0ID0gYXJncy5wb3AoKTtcbiAgICB2YXIgb3B0aW9uID0ge1xuICAgICAgcGVlcGhvbGVzOiBMYXllci5jb25uZWN0aW9uVHlwZS5BTExfVE9fQUxMLFxuICAgICAgaGlkZGVuVG9IaWRkZW46IGZhbHNlLFxuICAgICAgb3V0cHV0VG9IaWRkZW46IGZhbHNlLFxuICAgICAgb3V0cHV0VG9HYXRlczogZmFsc2UsXG4gICAgICBpbnB1dFRvT3V0cHV0OiB0cnVlLFxuICAgIH07XG4gICAgaWYgKHR5cGVvZiBsYXN0ICE9ICdudW1iZXInKSB7XG4gICAgICB2YXIgb3V0cHV0cyA9IGFyZ3MucG9wKCk7XG4gICAgICBpZiAobGFzdC5oYXNPd25Qcm9wZXJ0eSgncGVlcGhvbGVzJykpXG4gICAgICAgIG9wdGlvbi5wZWVwaG9sZXMgPSBsYXN0LnBlZXBob2xlcztcbiAgICAgIGlmIChsYXN0Lmhhc093blByb3BlcnR5KCdoaWRkZW5Ub0hpZGRlbicpKVxuICAgICAgICBvcHRpb24uaGlkZGVuVG9IaWRkZW4gPSBsYXN0LmhpZGRlblRvSGlkZGVuO1xuICAgICAgaWYgKGxhc3QuaGFzT3duUHJvcGVydHkoJ291dHB1dFRvSGlkZGVuJykpXG4gICAgICAgIG9wdGlvbi5vdXRwdXRUb0hpZGRlbiA9IGxhc3Qub3V0cHV0VG9IaWRkZW47XG4gICAgICBpZiAobGFzdC5oYXNPd25Qcm9wZXJ0eSgnb3V0cHV0VG9HYXRlcycpKVxuICAgICAgICBvcHRpb24ub3V0cHV0VG9HYXRlcyA9IGxhc3Qub3V0cHV0VG9HYXRlcztcbiAgICAgIGlmIChsYXN0Lmhhc093blByb3BlcnR5KCdpbnB1dFRvT3V0cHV0JykpXG4gICAgICAgIG9wdGlvbi5pbnB1dFRvT3V0cHV0ID0gbGFzdC5pbnB1dFRvT3V0cHV0O1xuICAgIH0gZWxzZVxuICAgICAgdmFyIG91dHB1dHMgPSBsYXN0O1xuXG4gICAgdmFyIGlucHV0cyA9IGFyZ3Muc2hpZnQoKTtcbiAgICB2YXIgbGF5ZXJzID0gYXJncztcblxuICAgIHZhciBpbnB1dExheWVyID0gbmV3IExheWVyKGlucHV0cyk7XG4gICAgdmFyIGhpZGRlbkxheWVycyA9IFtdO1xuICAgIHZhciBvdXRwdXRMYXllciA9IG5ldyBMYXllcihvdXRwdXRzKTtcblxuICAgIHZhciBwcmV2aW91cyA9IG51bGw7XG5cbiAgICAvLyBnZW5lcmF0ZSBsYXllcnNcbiAgICBmb3IgKHZhciBsYXllciBpbiBsYXllcnMpIHtcbiAgICAgIC8vIGdlbmVyYXRlIG1lbW9yeSBibG9ja3MgKG1lbW9yeSBjZWxsIGFuZCByZXNwZWN0aXZlIGdhdGVzKVxuICAgICAgdmFyIHNpemUgPSBsYXllcnNbbGF5ZXJdO1xuXG4gICAgICB2YXIgaW5wdXRHYXRlID0gbmV3IExheWVyKHNpemUpLnNldCh7XG4gICAgICAgIGJpYXM6IDFcbiAgICAgIH0pO1xuICAgICAgdmFyIGZvcmdldEdhdGUgPSBuZXcgTGF5ZXIoc2l6ZSkuc2V0KHtcbiAgICAgICAgYmlhczogMVxuICAgICAgfSk7XG4gICAgICB2YXIgbWVtb3J5Q2VsbCA9IG5ldyBMYXllcihzaXplKTtcbiAgICAgIHZhciBvdXRwdXRHYXRlID0gbmV3IExheWVyKHNpemUpLnNldCh7XG4gICAgICAgIGJpYXM6IDFcbiAgICAgIH0pO1xuXG4gICAgICBoaWRkZW5MYXllcnMucHVzaChpbnB1dEdhdGUpO1xuICAgICAgaGlkZGVuTGF5ZXJzLnB1c2goZm9yZ2V0R2F0ZSk7XG4gICAgICBoaWRkZW5MYXllcnMucHVzaChtZW1vcnlDZWxsKTtcbiAgICAgIGhpZGRlbkxheWVycy5wdXNoKG91dHB1dEdhdGUpO1xuXG4gICAgICAvLyBjb25uZWN0aW9ucyBmcm9tIGlucHV0IGxheWVyXG4gICAgICB2YXIgaW5wdXQgPSBpbnB1dExheWVyLnByb2plY3QobWVtb3J5Q2VsbCk7XG4gICAgICBpbnB1dExheWVyLnByb2plY3QoaW5wdXRHYXRlKTtcbiAgICAgIGlucHV0TGF5ZXIucHJvamVjdChmb3JnZXRHYXRlKTtcbiAgICAgIGlucHV0TGF5ZXIucHJvamVjdChvdXRwdXRHYXRlKTtcblxuICAgICAgLy8gY29ubmVjdGlvbnMgZnJvbSBwcmV2aW91cyBtZW1vcnktYmxvY2sgbGF5ZXIgdG8gdGhpcyBvbmVcbiAgICAgIGlmIChwcmV2aW91cyAhPSBudWxsKSB7XG4gICAgICAgIHZhciBjZWxsID0gcHJldmlvdXMucHJvamVjdChtZW1vcnlDZWxsKTtcbiAgICAgICAgcHJldmlvdXMucHJvamVjdChpbnB1dEdhdGUpO1xuICAgICAgICBwcmV2aW91cy5wcm9qZWN0KGZvcmdldEdhdGUpO1xuICAgICAgICBwcmV2aW91cy5wcm9qZWN0KG91dHB1dEdhdGUpO1xuICAgICAgfVxuXG4gICAgICAvLyBjb25uZWN0aW9ucyBmcm9tIG1lbW9yeSBjZWxsXG4gICAgICB2YXIgb3V0cHV0ID0gbWVtb3J5Q2VsbC5wcm9qZWN0KG91dHB1dExheWVyKTtcblxuICAgICAgLy8gc2VsZi1jb25uZWN0aW9uXG4gICAgICB2YXIgc2VsZiA9IG1lbW9yeUNlbGwucHJvamVjdChtZW1vcnlDZWxsKTtcblxuICAgICAgLy8gaGlkZGVuIHRvIGhpZGRlbiByZWN1cnJlbnQgY29ubmVjdGlvblxuICAgICAgaWYgKG9wdGlvbi5oaWRkZW5Ub0hpZGRlbilcbiAgICAgICAgbWVtb3J5Q2VsbC5wcm9qZWN0KG1lbW9yeUNlbGwsIExheWVyLmNvbm5lY3Rpb25UeXBlLkFMTF9UT19FTFNFKTtcblxuICAgICAgLy8gb3V0IHRvIGhpZGRlbiByZWN1cnJlbnQgY29ubmVjdGlvblxuICAgICAgaWYgKG9wdGlvbi5vdXRwdXRUb0hpZGRlbilcbiAgICAgICAgb3V0cHV0TGF5ZXIucHJvamVjdChtZW1vcnlDZWxsKTtcblxuICAgICAgLy8gb3V0IHRvIGdhdGVzIHJlY3VycmVudCBjb25uZWN0aW9uXG4gICAgICBpZiAob3B0aW9uLm91dHB1dFRvR2F0ZXMpIHtcbiAgICAgICAgb3V0cHV0TGF5ZXIucHJvamVjdChpbnB1dEdhdGUpO1xuICAgICAgICBvdXRwdXRMYXllci5wcm9qZWN0KG91dHB1dEdhdGUpO1xuICAgICAgICBvdXRwdXRMYXllci5wcm9qZWN0KGZvcmdldEdhdGUpO1xuICAgICAgfVxuXG4gICAgICAvLyBwZWVwaG9sZXNcbiAgICAgIG1lbW9yeUNlbGwucHJvamVjdChpbnB1dEdhdGUsIG9wdGlvbi5wZWVwaG9sZXMpO1xuICAgICAgbWVtb3J5Q2VsbC5wcm9qZWN0KGZvcmdldEdhdGUsIG9wdGlvbi5wZWVwaG9sZXMpO1xuICAgICAgbWVtb3J5Q2VsbC5wcm9qZWN0KG91dHB1dEdhdGUsIG9wdGlvbi5wZWVwaG9sZXMpO1xuXG4gICAgICAvLyBnYXRlc1xuICAgICAgaW5wdXRHYXRlLmdhdGUoaW5wdXQsIExheWVyLmdhdGVUeXBlLklOUFVUKTtcbiAgICAgIGZvcmdldEdhdGUuZ2F0ZShzZWxmLCBMYXllci5nYXRlVHlwZS5PTkVfVE9fT05FKTtcbiAgICAgIG91dHB1dEdhdGUuZ2F0ZShvdXRwdXQsIExheWVyLmdhdGVUeXBlLk9VVFBVVCk7XG4gICAgICBpZiAocHJldmlvdXMgIT0gbnVsbClcbiAgICAgICAgaW5wdXRHYXRlLmdhdGUoY2VsbCwgTGF5ZXIuZ2F0ZVR5cGUuSU5QVVQpO1xuXG4gICAgICBwcmV2aW91cyA9IG1lbW9yeUNlbGw7XG4gICAgfVxuXG4gICAgLy8gaW5wdXQgdG8gb3V0cHV0IGRpcmVjdCBjb25uZWN0aW9uXG4gICAgaWYgKG9wdGlvbi5pbnB1dFRvT3V0cHV0KVxuICAgICAgaW5wdXRMYXllci5wcm9qZWN0KG91dHB1dExheWVyKTtcblxuICAgIC8vIHNldCB0aGUgbGF5ZXJzIG9mIHRoZSBuZXVyYWwgbmV0d29ya1xuICAgIHRoaXMuc2V0KHtcbiAgICAgIGlucHV0OiBpbnB1dExheWVyLFxuICAgICAgaGlkZGVuOiBoaWRkZW5MYXllcnMsXG4gICAgICBvdXRwdXQ6IG91dHB1dExheWVyXG4gICAgfSk7XG5cbiAgICAvLyB0cmFpbmVyXG4gICAgdGhpcy50cmFpbmVyID0gbmV3IFRyYWluZXIodGhpcyk7XG4gIH0sXG5cbiAgLy8gTGlxdWlkIFN0YXRlIE1hY2hpbmVcbiAgTGlxdWlkOiBmdW5jdGlvbiBMaXF1aWQoaW5wdXRzLCBoaWRkZW4sIG91dHB1dHMsIGNvbm5lY3Rpb25zLCBnYXRlcykge1xuXG4gICAgLy8gY3JlYXRlIGxheWVyc1xuICAgIHZhciBpbnB1dExheWVyID0gbmV3IExheWVyKGlucHV0cyk7XG4gICAgdmFyIGhpZGRlbkxheWVyID0gbmV3IExheWVyKGhpZGRlbik7XG4gICAgdmFyIG91dHB1dExheWVyID0gbmV3IExheWVyKG91dHB1dHMpO1xuXG4gICAgLy8gbWFrZSBjb25uZWN0aW9ucyBhbmQgZ2F0ZXMgcmFuZG9tbHkgYW1vbmcgdGhlIG5ldXJvbnNcbiAgICB2YXIgbmV1cm9ucyA9IGhpZGRlbkxheWVyLm5ldXJvbnMoKTtcbiAgICB2YXIgY29ubmVjdGlvbkxpc3QgPSBbXTtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgY29ubmVjdGlvbnM7IGkrKykge1xuICAgICAgLy8gY29ubmVjdCB0d28gcmFuZG9tIG5ldXJvbnNcbiAgICAgIHZhciBmcm9tID0gTWF0aC5yYW5kb20oKSAqIG5ldXJvbnMubGVuZ3RoIHwgMDtcbiAgICAgIHZhciB0byA9IE1hdGgucmFuZG9tKCkgKiBuZXVyb25zLmxlbmd0aCB8IDA7XG4gICAgICB2YXIgY29ubmVjdGlvbiA9IG5ldXJvbnNbZnJvbV0ucHJvamVjdChuZXVyb25zW3RvXSk7XG4gICAgICBjb25uZWN0aW9uTGlzdC5wdXNoKGNvbm5lY3Rpb24pO1xuICAgIH1cblxuICAgIGZvciAodmFyIGogPSAwOyBqIDwgZ2F0ZXM7IGorKykge1xuICAgICAgLy8gcGljayBhIHJhbmRvbSBnYXRlciBuZXVyb25cbiAgICAgIHZhciBnYXRlciA9IE1hdGgucmFuZG9tKCkgKiBuZXVyb25zLmxlbmd0aCB8IDA7XG4gICAgICAvLyBwaWNrIGEgcmFuZG9tIGNvbm5lY3Rpb24gdG8gZ2F0ZVxuICAgICAgdmFyIGNvbm5lY3Rpb24gPSBNYXRoLnJhbmRvbSgpICogY29ubmVjdGlvbkxpc3QubGVuZ3RoIHwgMDtcbiAgICAgIC8vIGxldCB0aGUgZ2F0ZXIgZ2F0ZSB0aGUgY29ubmVjdGlvblxuICAgICAgbmV1cm9uc1tnYXRlcl0uZ2F0ZShjb25uZWN0aW9uTGlzdFtjb25uZWN0aW9uXSk7XG4gICAgfVxuXG4gICAgLy8gY29ubmVjdCB0aGUgbGF5ZXJzXG4gICAgaW5wdXRMYXllci5wcm9qZWN0KGhpZGRlbkxheWVyKTtcbiAgICBoaWRkZW5MYXllci5wcm9qZWN0KG91dHB1dExheWVyKTtcblxuICAgIC8vIHNldCB0aGUgbGF5ZXJzIG9mIHRoZSBuZXR3b3JrXG4gICAgdGhpcy5zZXQoe1xuICAgICAgaW5wdXQ6IGlucHV0TGF5ZXIsXG4gICAgICBoaWRkZW46IFtoaWRkZW5MYXllcl0sXG4gICAgICBvdXRwdXQ6IG91dHB1dExheWVyXG4gICAgfSk7XG5cbiAgICAvLyB0cmFpbmVyXG4gICAgdGhpcy50cmFpbmVyID0gbmV3IFRyYWluZXIodGhpcyk7XG4gIH0sXG5cbiAgSG9wZmllbGQ6IGZ1bmN0aW9uIEhvcGZpZWxkKHNpemUpIHtcblxuICAgIHZhciBpbnB1dExheWVyID0gbmV3IExheWVyKHNpemUpO1xuICAgIHZhciBvdXRwdXRMYXllciA9IG5ldyBMYXllcihzaXplKTtcblxuICAgIGlucHV0TGF5ZXIucHJvamVjdChvdXRwdXRMYXllciwgTGF5ZXIuY29ubmVjdGlvblR5cGUuQUxMX1RPX0FMTCk7XG5cbiAgICB0aGlzLnNldCh7XG4gICAgICBpbnB1dDogaW5wdXRMYXllcixcbiAgICAgIGhpZGRlbjogW10sXG4gICAgICBvdXRwdXQ6IG91dHB1dExheWVyXG4gICAgfSk7XG5cbiAgICB2YXIgdHJhaW5lciA9IG5ldyBUcmFpbmVyKHRoaXMpO1xuXG4gICAgdmFyIHByb3RvID0gQXJjaGl0ZWN0LkhvcGZpZWxkLnByb3RvdHlwZTtcblxuICAgIHByb3RvLmxlYXJuID0gcHJvdG8ubGVhcm4gfHwgZnVuY3Rpb24ocGF0dGVybnMpXG4gICAge1xuICAgICAgdmFyIHNldCA9IFtdO1xuICAgICAgZm9yICh2YXIgcCBpbiBwYXR0ZXJucylcbiAgICAgICAgc2V0LnB1c2goe1xuICAgICAgICAgIGlucHV0OiBwYXR0ZXJuc1twXSxcbiAgICAgICAgICBvdXRwdXQ6IHBhdHRlcm5zW3BdXG4gICAgICAgIH0pO1xuXG4gICAgICByZXR1cm4gdHJhaW5lci50cmFpbihzZXQsIHtcbiAgICAgICAgaXRlcmF0aW9uczogNTAwMDAwLFxuICAgICAgICBlcnJvcjogLjAwMDA1LFxuICAgICAgICByYXRlOiAxXG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgcHJvdG8uZmVlZCA9IHByb3RvLmZlZWQgfHwgZnVuY3Rpb24ocGF0dGVybilcbiAgICB7XG4gICAgICB2YXIgb3V0cHV0ID0gdGhpcy5hY3RpdmF0ZShwYXR0ZXJuKTtcblxuICAgICAgdmFyIHBhdHRlcm4gPSBbXTtcbiAgICAgIGZvciAodmFyIGkgaW4gb3V0cHV0KVxuICAgICAgICBwYXR0ZXJuW2ldID0gb3V0cHV0W2ldID4gLjUgPyAxIDogMDtcblxuICAgICAgcmV0dXJuIHBhdHRlcm47XG4gICAgfVxuICB9XG59XG5cbi8vIEV4dGVuZCBwcm90b3R5cGUgY2hhaW4gKHNvIGV2ZXJ5IGFyY2hpdGVjdHVyZXMgaXMgYW4gaW5zdGFuY2Ugb2YgTmV0d29yaylcbmZvciAodmFyIGFyY2hpdGVjdHVyZSBpbiBBcmNoaXRlY3QpIHtcbiAgQXJjaGl0ZWN0W2FyY2hpdGVjdHVyZV0ucHJvdG90eXBlID0gbmV3IE5ldHdvcmsoKTtcbiAgQXJjaGl0ZWN0W2FyY2hpdGVjdHVyZV0ucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gQXJjaGl0ZWN0W2FyY2hpdGVjdHVyZV07XG59XG5cbi8vIGV4cG9ydFxuaWYgKG1vZHVsZSkgbW9kdWxlLmV4cG9ydHMgPSBBcmNoaXRlY3Q7IiwiLy8gZXhwb3J0XG5pZiAobW9kdWxlKSBtb2R1bGUuZXhwb3J0cyA9IExheWVyO1xuXG4vLyBpbXBvcnRcbnZhciBOZXVyb24gID0gcmVxdWlyZSgnLi9uZXVyb24nKVxuLCAgIE5ldHdvcmsgPSByZXF1aXJlKCcuL25ldHdvcmsnKVxuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBMQVlFUlxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cblxuZnVuY3Rpb24gTGF5ZXIoc2l6ZSwgbGFiZWwpIHtcbiAgdGhpcy5zaXplID0gc2l6ZSB8IDA7XG4gIHRoaXMubGlzdCA9IFtdO1xuICB0aGlzLmxhYmVsID0gbGFiZWwgfHwgbnVsbDtcbiAgdGhpcy5jb25uZWN0ZWRUbyA9IFtdO1xuXG4gIHdoaWxlIChzaXplLS0pIHtcbiAgICB2YXIgbmV1cm9uID0gbmV3IE5ldXJvbigpO1xuICAgIHRoaXMubGlzdC5wdXNoKG5ldXJvbik7XG4gIH1cbn1cblxuTGF5ZXIucHJvdG90eXBlID0ge1xuXG4gIC8vIGFjdGl2YXRlcyBhbGwgdGhlIG5ldXJvbnMgaW4gdGhlIGxheWVyXG4gIGFjdGl2YXRlOiBmdW5jdGlvbihpbnB1dCkge1xuXG4gICAgdmFyIGFjdGl2YXRpb25zID0gW107XG5cbiAgICBpZiAodHlwZW9mIGlucHV0ICE9ICd1bmRlZmluZWQnKSB7XG4gICAgICBpZiAoaW5wdXQubGVuZ3RoICE9IHRoaXMuc2l6ZSlcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSU5QVVQgc2l6ZSBhbmQgTEFZRVIgc2l6ZSBtdXN0IGJlIHRoZSBzYW1lIHRvIGFjdGl2YXRlIVwiKTtcblxuICAgICAgZm9yICh2YXIgaWQgaW4gdGhpcy5saXN0KSB7XG4gICAgICAgIHZhciBuZXVyb24gPSB0aGlzLmxpc3RbaWRdO1xuICAgICAgICB2YXIgYWN0aXZhdGlvbiA9IG5ldXJvbi5hY3RpdmF0ZShpbnB1dFtpZF0pO1xuICAgICAgICBhY3RpdmF0aW9ucy5wdXNoKGFjdGl2YXRpb24pO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBmb3IgKHZhciBpZCBpbiB0aGlzLmxpc3QpIHtcbiAgICAgICAgdmFyIG5ldXJvbiA9IHRoaXMubGlzdFtpZF07XG4gICAgICAgIHZhciBhY3RpdmF0aW9uID0gbmV1cm9uLmFjdGl2YXRlKCk7XG4gICAgICAgIGFjdGl2YXRpb25zLnB1c2goYWN0aXZhdGlvbik7XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBhY3RpdmF0aW9ucztcbiAgfSxcblxuICAvLyBwcm9wYWdhdGVzIHRoZSBlcnJvciBvbiBhbGwgdGhlIG5ldXJvbnMgb2YgdGhlIGxheWVyXG4gIHByb3BhZ2F0ZTogZnVuY3Rpb24ocmF0ZSwgdGFyZ2V0KSB7XG5cbiAgICBpZiAodHlwZW9mIHRhcmdldCAhPSAndW5kZWZpbmVkJykge1xuICAgICAgaWYgKHRhcmdldC5sZW5ndGggIT0gdGhpcy5zaXplKVxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJUQVJHRVQgc2l6ZSBhbmQgTEFZRVIgc2l6ZSBtdXN0IGJlIHRoZSBzYW1lIHRvIHByb3BhZ2F0ZSFcIik7XG5cbiAgICAgIGZvciAodmFyIGlkID0gdGhpcy5saXN0Lmxlbmd0aCAtIDE7IGlkID49IDA7IGlkLS0pIHtcbiAgICAgICAgdmFyIG5ldXJvbiA9IHRoaXMubGlzdFtpZF07XG4gICAgICAgIG5ldXJvbi5wcm9wYWdhdGUocmF0ZSwgdGFyZ2V0W2lkXSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGZvciAodmFyIGlkID0gdGhpcy5saXN0Lmxlbmd0aCAtIDE7IGlkID49IDA7IGlkLS0pIHtcbiAgICAgICAgdmFyIG5ldXJvbiA9IHRoaXMubGlzdFtpZF07XG4gICAgICAgIG5ldXJvbi5wcm9wYWdhdGUocmF0ZSk7XG4gICAgICB9XG4gICAgfVxuICB9LFxuXG4gIC8vIHByb2plY3RzIGEgY29ubmVjdGlvbiBmcm9tIHRoaXMgbGF5ZXIgdG8gYW5vdGhlciBvbmVcbiAgcHJvamVjdDogZnVuY3Rpb24obGF5ZXIsIHR5cGUsIHdlaWdodHMpIHtcblxuICAgIGlmIChsYXllciBpbnN0YW5jZW9mIE5ldHdvcmspXG4gICAgICBsYXllciA9IGxheWVyLmxheWVycy5pbnB1dDtcblxuICAgIGlmIChsYXllciBpbnN0YW5jZW9mIExheWVyKSB7XG4gICAgICBpZiAoIXRoaXMuY29ubmVjdGVkKGxheWVyKSlcbiAgICAgICAgcmV0dXJuIG5ldyBMYXllci5jb25uZWN0aW9uKHRoaXMsIGxheWVyLCB0eXBlLCB3ZWlnaHRzKTtcbiAgICB9IGVsc2VcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgYXJndW1lbnQsIHlvdSBjYW4gb25seSBwcm9qZWN0IGNvbm5lY3Rpb25zIHRvIExBWUVSUyBhbmQgTkVUV09SS1MhXCIpO1xuXG5cbiAgfSxcblxuICAvLyBnYXRlcyBhIGNvbm5lY3Rpb24gYmV0d2VubiB0d28gbGF5ZXJzXG4gIGdhdGU6IGZ1bmN0aW9uKGNvbm5lY3Rpb24sIHR5cGUpIHtcblxuICAgIGlmICh0eXBlID09IExheWVyLmdhdGVUeXBlLklOUFVUKSB7XG4gICAgICBpZiAoY29ubmVjdGlvbi50by5zaXplICE9IHRoaXMuc2l6ZSlcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiR0FURVIgbGF5ZXIgYW5kIENPTk5FQ1RJT04uVE8gbGF5ZXIgbXVzdCBiZSB0aGUgc2FtZSBzaXplIGluIG9yZGVyIHRvIGdhdGUhXCIpO1xuXG4gICAgICBmb3IgKHZhciBpZCBpbiBjb25uZWN0aW9uLnRvLmxpc3QpIHtcbiAgICAgICAgdmFyIG5ldXJvbiA9IGNvbm5lY3Rpb24udG8ubGlzdFtpZF07XG4gICAgICAgIHZhciBnYXRlciA9IHRoaXMubGlzdFtpZF07XG4gICAgICAgIGZvciAodmFyIGlucHV0IGluIG5ldXJvbi5jb25uZWN0aW9ucy5pbnB1dHMpIHtcbiAgICAgICAgICB2YXIgZ2F0ZWQgPSBuZXVyb24uY29ubmVjdGlvbnMuaW5wdXRzW2lucHV0XTtcbiAgICAgICAgICBpZiAoZ2F0ZWQuSUQgaW4gY29ubmVjdGlvbi5jb25uZWN0aW9ucylcbiAgICAgICAgICAgIGdhdGVyLmdhdGUoZ2F0ZWQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIGlmICh0eXBlID09IExheWVyLmdhdGVUeXBlLk9VVFBVVCkge1xuICAgICAgaWYgKGNvbm5lY3Rpb24uZnJvbS5zaXplICE9IHRoaXMuc2l6ZSlcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiR0FURVIgbGF5ZXIgYW5kIENPTk5FQ1RJT04uRlJPTSBsYXllciBtdXN0IGJlIHRoZSBzYW1lIHNpemUgaW4gb3JkZXIgdG8gZ2F0ZSFcIik7XG5cbiAgICAgIGZvciAodmFyIGlkIGluIGNvbm5lY3Rpb24uZnJvbS5saXN0KSB7XG4gICAgICAgIHZhciBuZXVyb24gPSBjb25uZWN0aW9uLmZyb20ubGlzdFtpZF07XG4gICAgICAgIHZhciBnYXRlciA9IHRoaXMubGlzdFtpZF07XG4gICAgICAgIGZvciAodmFyIHByb2plY3RlZCBpbiBuZXVyb24uY29ubmVjdGlvbnMucHJvamVjdGVkKSB7XG4gICAgICAgICAgdmFyIGdhdGVkID0gbmV1cm9uLmNvbm5lY3Rpb25zLnByb2plY3RlZFtwcm9qZWN0ZWRdO1xuICAgICAgICAgIGlmIChnYXRlZC5JRCBpbiBjb25uZWN0aW9uLmNvbm5lY3Rpb25zKVxuICAgICAgICAgICAgZ2F0ZXIuZ2F0ZShnYXRlZCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKHR5cGUgPT0gTGF5ZXIuZ2F0ZVR5cGUuT05FX1RPX09ORSkge1xuICAgICAgaWYgKGNvbm5lY3Rpb24uc2l6ZSAhPSB0aGlzLnNpemUpXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlRoZSBudW1iZXIgb2YgR0FURVIgVU5JVFMgbXVzdCBiZSB0aGUgc2FtZSBhcyB0aGUgbnVtYmVyIG9mIENPTk5FQ1RJT05TIHRvIGdhdGUhXCIpO1xuXG4gICAgICBmb3IgKHZhciBpZCBpbiBjb25uZWN0aW9uLmxpc3QpIHtcbiAgICAgICAgdmFyIGdhdGVyID0gdGhpcy5saXN0W2lkXTtcbiAgICAgICAgdmFyIGdhdGVkID0gY29ubmVjdGlvbi5saXN0W2lkXTtcbiAgICAgICAgZ2F0ZXIuZ2F0ZShnYXRlZCk7XG4gICAgICB9XG4gICAgfVxuICAgIGNvbm5lY3Rpb24uZ2F0ZWRmcm9tLnB1c2goe2xheWVyOiB0aGlzLCB0eXBlOiB0eXBlfSk7XG4gIH0sXG5cbiAgLy8gdHJ1ZSBvciBmYWxzZSB3aGV0aGVyIHRoZSB3aG9sZSBsYXllciBpcyBzZWxmLWNvbm5lY3RlZCBvciBub3RcbiAgc2VsZmNvbm5lY3RlZDogZnVuY3Rpb24oKSB7XG5cbiAgICBmb3IgKHZhciBpZCBpbiB0aGlzLmxpc3QpIHtcbiAgICAgIHZhciBuZXVyb24gPSB0aGlzLmxpc3RbaWRdO1xuICAgICAgaWYgKCFuZXVyb24uc2VsZmNvbm5lY3RlZCgpKVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9LFxuXG4gIC8vIHRydWUgb2YgZmFsc2Ugd2hldGhlciB0aGUgbGF5ZXIgaXMgY29ubmVjdGVkIHRvIGFub3RoZXIgbGF5ZXIgKHBhcmFtZXRlcikgb3Igbm90XG4gIGNvbm5lY3RlZDogZnVuY3Rpb24obGF5ZXIpIHtcbiAgICAvLyBDaGVjayBpZiBBTEwgdG8gQUxMIGNvbm5lY3Rpb25cbiAgICB2YXIgY29ubmVjdGlvbnMgPSAwO1xuICAgIGZvciAodmFyIGhlcmUgaW4gdGhpcy5saXN0KSB7XG4gICAgICBmb3IgKHZhciB0aGVyZSBpbiBsYXllci5saXN0KSB7XG4gICAgICAgIHZhciBmcm9tID0gdGhpcy5saXN0W2hlcmVdO1xuICAgICAgICB2YXIgdG8gPSBsYXllci5saXN0W3RoZXJlXTtcbiAgICAgICAgdmFyIGNvbm5lY3RlZCA9IGZyb20uY29ubmVjdGVkKHRvKTtcbiAgICAgICAgaWYgKGNvbm5lY3RlZC50eXBlID09ICdwcm9qZWN0ZWQnKVxuICAgICAgICAgIGNvbm5lY3Rpb25zKys7XG4gICAgICB9XG4gICAgfVxuICAgIGlmIChjb25uZWN0aW9ucyA9PSB0aGlzLnNpemUgKiBsYXllci5zaXplKVxuICAgICAgcmV0dXJuIExheWVyLmNvbm5lY3Rpb25UeXBlLkFMTF9UT19BTEw7XG5cbiAgICAvLyBDaGVjayBpZiBPTkUgdG8gT05FIGNvbm5lY3Rpb25cbiAgICBjb25uZWN0aW9ucyA9IDA7XG4gICAgZm9yICh2YXIgbmV1cm9uIGluIHRoaXMubGlzdCkge1xuICAgICAgdmFyIGZyb20gPSB0aGlzLmxpc3RbbmV1cm9uXTtcbiAgICAgIHZhciB0byA9IGxheWVyLmxpc3RbbmV1cm9uXTtcbiAgICAgIHZhciBjb25uZWN0ZWQgPSBmcm9tLmNvbm5lY3RlZCh0byk7XG4gICAgICBpZiAoY29ubmVjdGVkLnR5cGUgPT0gJ3Byb2plY3RlZCcpXG4gICAgICAgIGNvbm5lY3Rpb25zKys7XG4gICAgfVxuICAgIGlmIChjb25uZWN0aW9ucyA9PSB0aGlzLnNpemUpXG4gICAgICByZXR1cm4gTGF5ZXIuY29ubmVjdGlvblR5cGUuT05FX1RPX09ORTtcbiAgfSxcblxuICAvLyBjbGVhcnMgYWxsIHRoZSBuZXVvcm5zIGluIHRoZSBsYXllclxuICBjbGVhcjogZnVuY3Rpb24oKSB7XG4gICAgZm9yICh2YXIgaWQgaW4gdGhpcy5saXN0KSB7XG4gICAgICB2YXIgbmV1cm9uID0gdGhpcy5saXN0W2lkXTtcbiAgICAgIG5ldXJvbi5jbGVhcigpO1xuICAgIH1cbiAgfSxcblxuICAvLyByZXNldHMgYWxsIHRoZSBuZXVyb25zIGluIHRoZSBsYXllclxuICByZXNldDogZnVuY3Rpb24oKSB7XG4gICAgZm9yICh2YXIgaWQgaW4gdGhpcy5saXN0KSB7XG4gICAgICB2YXIgbmV1cm9uID0gdGhpcy5saXN0W2lkXTtcbiAgICAgIG5ldXJvbi5yZXNldCgpO1xuICAgIH1cbiAgfSxcblxuICAvLyByZXR1cm5zIGFsbCB0aGUgbmV1cm9ucyBpbiB0aGUgbGF5ZXIgKGFycmF5KVxuICBuZXVyb25zOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gdGhpcy5saXN0O1xuICB9LFxuXG4gIC8vIGFkZHMgYSBuZXVyb24gdG8gdGhlIGxheWVyXG4gIGFkZDogZnVuY3Rpb24obmV1cm9uKSB7XG4gICAgdGhpcy5uZXVyb25zW25ldXJvbi5JRF0gPSBuZXVyb24gfHwgbmV3IE5ldXJvbigpO1xuICAgIHRoaXMubGlzdC5wdXNoKG5ldXJvbik7XG4gICAgdGhpcy5zaXplKys7XG4gIH0sXG5cbiAgc2V0OiBmdW5jdGlvbihvcHRpb25zKSB7XG4gICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cbiAgICBmb3IgKHZhciBpIGluIHRoaXMubGlzdCkge1xuICAgICAgdmFyIG5ldXJvbiA9IHRoaXMubGlzdFtpXTtcbiAgICAgIGlmIChvcHRpb25zLmxhYmVsKVxuICAgICAgICBuZXVyb24ubGFiZWwgPSBvcHRpb25zLmxhYmVsICsgJ18nICsgbmV1cm9uLklEO1xuICAgICAgaWYgKG9wdGlvbnMuc3F1YXNoKVxuICAgICAgICBuZXVyb24uc3F1YXNoID0gb3B0aW9ucy5zcXVhc2g7XG4gICAgICBpZiAob3B0aW9ucy5iaWFzKVxuICAgICAgICBuZXVyb24uYmlhcyA9IG9wdGlvbnMuYmlhcztcbiAgICB9XG4gICAgcmV0dXJuIHRoaXM7XG4gIH1cbn1cblxuLy8gcmVwcmVzZW50cyBhIGNvbm5lY3Rpb24gZnJvbSBvbmUgbGF5ZXIgdG8gYW5vdGhlciwgYW5kIGtlZXBzIHRyYWNrIG9mIGl0cyB3ZWlnaHQgYW5kIGdhaW5cbkxheWVyLmNvbm5lY3Rpb24gPSBmdW5jdGlvbiBMYXllckNvbm5lY3Rpb24oZnJvbUxheWVyLCB0b0xheWVyLCB0eXBlLCB3ZWlnaHRzKSB7XG4gIHRoaXMuSUQgPSBMYXllci5jb25uZWN0aW9uLnVpZCgpO1xuICB0aGlzLmZyb20gPSBmcm9tTGF5ZXI7XG4gIHRoaXMudG8gPSB0b0xheWVyO1xuICB0aGlzLnNlbGZjb25uZWN0aW9uID0gdG9MYXllciA9PSBmcm9tTGF5ZXI7XG4gIHRoaXMudHlwZSA9IHR5cGU7XG4gIHRoaXMuY29ubmVjdGlvbnMgPSB7fTtcbiAgdGhpcy5saXN0ID0gW107XG4gIHRoaXMuc2l6ZSA9IDA7XG4gIHRoaXMuZ2F0ZWRmcm9tID0gW107XG5cbiAgaWYgKHR5cGVvZiB0aGlzLnR5cGUgPT0gJ3VuZGVmaW5lZCcpXG4gIHtcbiAgICBpZiAoZnJvbUxheWVyID09IHRvTGF5ZXIpXG4gICAgICB0aGlzLnR5cGUgPSBMYXllci5jb25uZWN0aW9uVHlwZS5PTkVfVE9fT05FO1xuICAgIGVsc2VcbiAgICAgIHRoaXMudHlwZSA9IExheWVyLmNvbm5lY3Rpb25UeXBlLkFMTF9UT19BTEw7XG4gIH1cblxuICBpZiAodGhpcy50eXBlID09IExheWVyLmNvbm5lY3Rpb25UeXBlLkFMTF9UT19BTEwgfHxcbiAgICAgIHRoaXMudHlwZSA9PSBMYXllci5jb25uZWN0aW9uVHlwZS5BTExfVE9fRUxTRSkge1xuICAgIGZvciAodmFyIGhlcmUgaW4gdGhpcy5mcm9tLmxpc3QpIHtcbiAgICAgIGZvciAodmFyIHRoZXJlIGluIHRoaXMudG8ubGlzdCkge1xuICAgICAgICB2YXIgZnJvbSA9IHRoaXMuZnJvbS5saXN0W2hlcmVdO1xuICAgICAgICB2YXIgdG8gPSB0aGlzLnRvLmxpc3RbdGhlcmVdO1xuICAgICAgICBpZih0aGlzLnR5cGUgPT0gTGF5ZXIuY29ubmVjdGlvblR5cGUuQUxMX1RPX0VMU0UgJiYgZnJvbSA9PSB0bylcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgdmFyIGNvbm5lY3Rpb24gPSBmcm9tLnByb2plY3QodG8sIHdlaWdodHMpO1xuXG4gICAgICAgIHRoaXMuY29ubmVjdGlvbnNbY29ubmVjdGlvbi5JRF0gPSBjb25uZWN0aW9uO1xuICAgICAgICB0aGlzLnNpemUgPSB0aGlzLmxpc3QucHVzaChjb25uZWN0aW9uKTtcbiAgICAgIH1cbiAgICB9XG4gIH0gZWxzZSBpZiAodGhpcy50eXBlID09IExheWVyLmNvbm5lY3Rpb25UeXBlLk9ORV9UT19PTkUpIHtcblxuICAgIGZvciAodmFyIG5ldXJvbiBpbiB0aGlzLmZyb20ubGlzdCkge1xuICAgICAgdmFyIGZyb20gPSB0aGlzLmZyb20ubGlzdFtuZXVyb25dO1xuICAgICAgdmFyIHRvID0gdGhpcy50by5saXN0W25ldXJvbl07XG4gICAgICB2YXIgY29ubmVjdGlvbiA9IGZyb20ucHJvamVjdCh0bywgd2VpZ2h0cyk7XG5cbiAgICAgIHRoaXMuY29ubmVjdGlvbnNbY29ubmVjdGlvbi5JRF0gPSBjb25uZWN0aW9uO1xuICAgICAgdGhpcy5zaXplID0gdGhpcy5saXN0LnB1c2goY29ubmVjdGlvbik7XG4gICAgfVxuICB9XG5cbiAgZnJvbUxheWVyLmNvbm5lY3RlZFRvLnB1c2godGhpcyk7XG59XG5cbi8vIHR5cGVzIG9mIGNvbm5lY3Rpb25zXG5MYXllci5jb25uZWN0aW9uVHlwZSA9IHt9O1xuTGF5ZXIuY29ubmVjdGlvblR5cGUuQUxMX1RPX0FMTCA9IFwiQUxMIFRPIEFMTFwiO1xuTGF5ZXIuY29ubmVjdGlvblR5cGUuT05FX1RPX09ORSA9IFwiT05FIFRPIE9ORVwiO1xuTGF5ZXIuY29ubmVjdGlvblR5cGUuQUxMX1RPX0VMU0UgPSBcIkFMTCBUTyBFTFNFXCI7XG5cbi8vIHR5cGVzIG9mIGdhdGVzXG5MYXllci5nYXRlVHlwZSA9IHt9O1xuTGF5ZXIuZ2F0ZVR5cGUuSU5QVVQgPSBcIklOUFVUXCI7XG5MYXllci5nYXRlVHlwZS5PVVRQVVQgPSBcIk9VVFBVVFwiO1xuTGF5ZXIuZ2F0ZVR5cGUuT05FX1RPX09ORSA9IFwiT05FIFRPIE9ORVwiO1xuXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBjb25uZWN0aW9ucyA9IDA7XG4gIExheWVyLmNvbm5lY3Rpb24udWlkID0gZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIGNvbm5lY3Rpb25zKys7XG4gIH1cbn0pKCk7XG4iLCIvLyBleHBvcnRcbmlmIChtb2R1bGUpIG1vZHVsZS5leHBvcnRzID0gTmV0d29yaztcblxuLy8gaW1wb3J0XG52YXIgTmV1cm9uICA9IHJlcXVpcmUoJy4vbmV1cm9uJylcbiwgICBMYXllciAgID0gcmVxdWlyZSgnLi9sYXllcicpXG4sICAgVHJhaW5lciA9IHJlcXVpcmUoJy4vdHJhaW5lcicpXG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5FVFdPUktcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbmZ1bmN0aW9uIE5ldHdvcmsobGF5ZXJzKSB7XG4gIGlmICh0eXBlb2YgbGF5ZXJzICE9ICd1bmRlZmluZWQnKSB7XG4gICAgdGhpcy5sYXllcnMgPSBsYXllcnMgfHwge1xuICAgICAgaW5wdXQ6IG51bGwsXG4gICAgICBoaWRkZW46IHt9LFxuICAgICAgb3V0cHV0OiBudWxsXG4gICAgfTtcbiAgICB0aGlzLm9wdGltaXplZCA9IG51bGw7XG4gIH1cbn1cbk5ldHdvcmsucHJvdG90eXBlID0ge1xuXG4gIC8vIGZlZWQtZm9yd2FyZCBhY3RpdmF0aW9uIG9mIGFsbCB0aGUgbGF5ZXJzIHRvIHByb2R1Y2UgYW4gb3VwdXRcbiAgYWN0aXZhdGU6IGZ1bmN0aW9uKGlucHV0KSB7XG5cbiAgICBpZiAodGhpcy5vcHRpbWl6ZWQgPT09IGZhbHNlKVxuICAgIHtcbiAgICAgIHRoaXMubGF5ZXJzLmlucHV0LmFjdGl2YXRlKGlucHV0KTtcbiAgICAgIGZvciAodmFyIGxheWVyIGluIHRoaXMubGF5ZXJzLmhpZGRlbilcbiAgICAgICAgdGhpcy5sYXllcnMuaGlkZGVuW2xheWVyXS5hY3RpdmF0ZSgpO1xuICAgICAgcmV0dXJuIHRoaXMubGF5ZXJzLm91dHB1dC5hY3RpdmF0ZSgpO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgaWYgKHRoaXMub3B0aW1pemVkID09IG51bGwpXG4gICAgICAgIHRoaXMub3B0aW1pemUoKTtcbiAgICAgIHJldHVybiB0aGlzLm9wdGltaXplZC5hY3RpdmF0ZShpbnB1dCk7XG4gICAgfVxuICB9LFxuXG4gIC8vIGJhY2stcHJvcGFnYXRlIHRoZSBlcnJvciB0aHJ1IHRoZSBuZXR3b3JrXG4gIHByb3BhZ2F0ZTogZnVuY3Rpb24ocmF0ZSwgdGFyZ2V0KSB7XG5cbiAgICBpZiAodGhpcy5vcHRpbWl6ZWQgPT09IGZhbHNlKVxuICAgIHtcbiAgICAgIHRoaXMubGF5ZXJzLm91dHB1dC5wcm9wYWdhdGUocmF0ZSwgdGFyZ2V0KTtcbiAgICAgIHZhciByZXZlcnNlID0gW107XG4gICAgICBmb3IgKHZhciBsYXllciBpbiB0aGlzLmxheWVycy5oaWRkZW4pXG4gICAgICAgIHJldmVyc2UucHVzaCh0aGlzLmxheWVycy5oaWRkZW5bbGF5ZXJdKTtcbiAgICAgIHJldmVyc2UucmV2ZXJzZSgpO1xuICAgICAgZm9yICh2YXIgbGF5ZXIgaW4gcmV2ZXJzZSlcbiAgICAgICAgcmV2ZXJzZVtsYXllcl0ucHJvcGFnYXRlKHJhdGUpO1xuICAgIH1cbiAgICBlbHNlXG4gICAge1xuICAgICAgaWYgKHRoaXMub3B0aW1pemVkID09IG51bGwpXG4gICAgICAgIHRoaXMub3B0aW1pemUoKTtcbiAgICAgIHRoaXMub3B0aW1pemVkLnByb3BhZ2F0ZShyYXRlLCB0YXJnZXQpO1xuICAgIH1cbiAgfSxcblxuICAvLyBwcm9qZWN0IGEgY29ubmVjdGlvbiB0byBhbm90aGVyIHVuaXQgKGVpdGhlciBhIG5ldHdvcmsgb3IgYSBsYXllcilcbiAgcHJvamVjdDogZnVuY3Rpb24odW5pdCwgdHlwZSwgd2VpZ2h0cykge1xuXG4gICAgaWYgKHRoaXMub3B0aW1pemVkKVxuICAgICAgdGhpcy5vcHRpbWl6ZWQucmVzZXQoKTtcblxuICAgIGlmICh1bml0IGluc3RhbmNlb2YgTmV0d29yaylcbiAgICAgIHJldHVybiB0aGlzLmxheWVycy5vdXRwdXQucHJvamVjdCh1bml0LmxheWVycy5pbnB1dCwgdHlwZSwgd2VpZ2h0cyk7XG5cbiAgICBpZiAodW5pdCBpbnN0YW5jZW9mIExheWVyKVxuICAgICAgcmV0dXJuIHRoaXMubGF5ZXJzLm91dHB1dC5wcm9qZWN0KHVuaXQsIHR5cGUsIHdlaWdodHMpO1xuXG4gICAgdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBhcmd1bWVudCwgeW91IGNhbiBvbmx5IHByb2plY3QgY29ubmVjdGlvbnMgdG8gTEFZRVJTIGFuZCBORVRXT1JLUyFcIik7XG4gIH0sXG5cbiAgLy8gbGV0IHRoaXMgbmV0d29yayBnYXRlIGEgY29ubmVjdGlvblxuICBnYXRlOiBmdW5jdGlvbihjb25uZWN0aW9uLCB0eXBlKSB7XG4gICAgaWYgKHRoaXMub3B0aW1pemVkKVxuICAgICAgdGhpcy5vcHRpbWl6ZWQucmVzZXQoKTtcbiAgICB0aGlzLmxheWVycy5vdXRwdXQuZ2F0ZShjb25uZWN0aW9uLCB0eXBlKTtcbiAgfSxcblxuICAvLyBjbGVhciBhbGwgZWxlZ2liaWxpdHkgdHJhY2VzIGFuZCBleHRlbmRlZCBlbGVnaWJpbGl0eSB0cmFjZXMgKHRoZSBuZXR3b3JrIGZvcmdldHMgaXRzIGNvbnRleHQsIGJ1dCBub3Qgd2hhdCB3YXMgdHJhaW5lZClcbiAgY2xlYXI6IGZ1bmN0aW9uKCkge1xuXG4gICAgdGhpcy5yZXN0b3JlKCk7XG5cbiAgICB2YXIgaW5wdXRMYXllciA9IHRoaXMubGF5ZXJzLmlucHV0LFxuICAgICAgb3V0cHV0TGF5ZXIgPSB0aGlzLmxheWVycy5vdXRwdXQ7XG5cbiAgICBpbnB1dExheWVyLmNsZWFyKCk7XG4gICAgZm9yICh2YXIgbGF5ZXIgaW4gdGhpcy5sYXllcnMuaGlkZGVuKSB7XG4gICAgICB2YXIgaGlkZGVuTGF5ZXIgPSB0aGlzLmxheWVycy5oaWRkZW5bbGF5ZXJdO1xuICAgICAgaGlkZGVuTGF5ZXIuY2xlYXIoKTtcbiAgICB9XG4gICAgb3V0cHV0TGF5ZXIuY2xlYXIoKTtcblxuICAgIGlmICh0aGlzLm9wdGltaXplZClcbiAgICAgIHRoaXMub3B0aW1pemVkLnJlc2V0KCk7XG4gIH0sXG5cbiAgLy8gcmVzZXQgYWxsIHdlaWdodHMgYW5kIGNsZWFyIGFsbCB0cmFjZXMgKGVuZHMgdXAgbGlrZSBhIG5ldyBuZXR3b3JrKVxuICByZXNldDogZnVuY3Rpb24oKSB7XG5cbiAgICB0aGlzLnJlc3RvcmUoKTtcblxuICAgIHZhciBpbnB1dExheWVyID0gdGhpcy5sYXllcnMuaW5wdXQsXG4gICAgICBvdXRwdXRMYXllciA9IHRoaXMubGF5ZXJzLm91dHB1dDtcblxuICAgIGlucHV0TGF5ZXIucmVzZXQoKTtcbiAgICBmb3IgKHZhciBsYXllciBpbiB0aGlzLmxheWVycy5oaWRkZW4pIHtcbiAgICAgIHZhciBoaWRkZW5MYXllciA9IHRoaXMubGF5ZXJzLmhpZGRlbltsYXllcl07XG4gICAgICBoaWRkZW5MYXllci5yZXNldCgpO1xuICAgIH1cbiAgICBvdXRwdXRMYXllci5yZXNldCgpO1xuXG4gICAgaWYgKHRoaXMub3B0aW1pemVkKVxuICAgICAgdGhpcy5vcHRpbWl6ZWQucmVzZXQoKTtcbiAgfSxcblxuICAvLyBoYXJkY29kZXMgdGhlIGJlaGF2aW91ciBvZiB0aGUgd2hvbGUgbmV0d29yayBpbnRvIGEgc2luZ2xlIG9wdGltaXplZCBmdW5jdGlvblxuICBvcHRpbWl6ZTogZnVuY3Rpb24oKSB7XG5cbiAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgdmFyIG9wdGltaXplZCA9IHt9O1xuICAgIHZhciBuZXVyb25zID0gdGhpcy5uZXVyb25zKCk7XG5cbiAgICBmb3IgKHZhciBpIGluIG5ldXJvbnMpIHtcbiAgICAgIHZhciBuZXVyb24gPSBuZXVyb25zW2ldLm5ldXJvbjtcbiAgICAgIHZhciBsYXllciA9IG5ldXJvbnNbaV0ubGF5ZXI7XG4gICAgICB3aGlsZSAobmV1cm9uLm5ldXJvbilcbiAgICAgICAgbmV1cm9uID0gbmV1cm9uLm5ldXJvbjtcbiAgICAgIG9wdGltaXplZCA9IG5ldXJvbi5vcHRpbWl6ZShvcHRpbWl6ZWQsIGxheWVyKTtcbiAgICB9XG4gICAgZm9yICh2YXIgaSBpbiBvcHRpbWl6ZWQucHJvcGFnYXRpb25fc2VudGVuY2VzKVxuICAgICAgb3B0aW1pemVkLnByb3BhZ2F0aW9uX3NlbnRlbmNlc1tpXS5yZXZlcnNlKCk7XG4gICAgb3B0aW1pemVkLnByb3BhZ2F0aW9uX3NlbnRlbmNlcy5yZXZlcnNlKCk7XG5cbiAgICB2YXIgaGFyZGNvZGUgPSBcIlwiO1xuICAgIGhhcmRjb2RlICs9IFwidmFyIEYgPSBGbG9hdDY0QXJyYXkgPyBuZXcgRmxvYXQ2NEFycmF5KFwiICsgb3B0aW1pemVkLm1lbW9yeSArXG4gICAgICBcIikgOiBbXTsgXCI7XG4gICAgZm9yICh2YXIgaSBpbiBvcHRpbWl6ZWQudmFyaWFibGVzKVxuICAgICAgaGFyZGNvZGUgKz0gXCJGW1wiICsgb3B0aW1pemVkLnZhcmlhYmxlc1tpXS5pZCArIFwiXSA9IFwiICsgKG9wdGltaXplZC52YXJpYWJsZXNbXG4gICAgICAgIGldLnZhbHVlIHx8IDApICsgXCI7IFwiO1xuICAgIGhhcmRjb2RlICs9IFwidmFyIGFjdGl2YXRlID0gZnVuY3Rpb24oaW5wdXQpe1xcblwiO1xuICAgIGZvciAodmFyIGkgaW4gb3B0aW1pemVkLmlucHV0cylcbiAgICAgIGhhcmRjb2RlICs9IFwiRltcIiArIG9wdGltaXplZC5pbnB1dHNbaV0gKyBcIl0gPSBpbnB1dFtcIiArIGkgKyBcIl07IFwiO1xuICAgIGZvciAodmFyIGN1cnJlbnRMYXllciBpbiBvcHRpbWl6ZWQuYWN0aXZhdGlvbl9zZW50ZW5jZXMpIHtcbiAgICAgIGlmIChvcHRpbWl6ZWQuYWN0aXZhdGlvbl9zZW50ZW5jZXNbY3VycmVudExheWVyXS5sZW5ndGggPiAwKSB7XG4gICAgICAgIGZvciAodmFyIGN1cnJlbnROZXVyb24gaW4gb3B0aW1pemVkLmFjdGl2YXRpb25fc2VudGVuY2VzW2N1cnJlbnRMYXllcl0pIHtcbiAgICAgICAgICBoYXJkY29kZSArPSBvcHRpbWl6ZWQuYWN0aXZhdGlvbl9zZW50ZW5jZXNbY3VycmVudExheWVyXVtjdXJyZW50TmV1cm9uXS5qb2luKFwiIFwiKTtcbiAgICAgICAgICBoYXJkY29kZSArPSBvcHRpbWl6ZWQudHJhY2Vfc2VudGVuY2VzW2N1cnJlbnRMYXllcl1bY3VycmVudE5ldXJvbl0uam9pbihcIiBcIik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgaGFyZGNvZGUgKz0gXCIgdmFyIG91dHB1dCA9IFtdOyBcIlxuICAgIGZvciAodmFyIGkgaW4gb3B0aW1pemVkLm91dHB1dHMpXG4gICAgICBoYXJkY29kZSArPSBcIm91dHB1dFtcIiArIGkgKyBcIl0gPSBGW1wiICsgb3B0aW1pemVkLm91dHB1dHNbaV0gKyBcIl07IFwiO1xuICAgIGhhcmRjb2RlICs9IFwicmV0dXJuIG91dHB1dDsgfTsgXCJcbiAgICBoYXJkY29kZSArPSBcInZhciBwcm9wYWdhdGUgPSBmdW5jdGlvbihyYXRlLCB0YXJnZXQpe1xcblwiO1xuICAgIGhhcmRjb2RlICs9IFwiRltcIiArIG9wdGltaXplZC52YXJpYWJsZXMucmF0ZS5pZCArIFwiXSA9IHJhdGU7IFwiO1xuICAgIGZvciAodmFyIGkgaW4gb3B0aW1pemVkLnRhcmdldHMpXG4gICAgICBoYXJkY29kZSArPSBcIkZbXCIgKyBvcHRpbWl6ZWQudGFyZ2V0c1tpXSArIFwiXSA9IHRhcmdldFtcIiArIGkgKyBcIl07IFwiO1xuICAgIGZvciAodmFyIGN1cnJlbnRMYXllciBpbiBvcHRpbWl6ZWQucHJvcGFnYXRpb25fc2VudGVuY2VzKVxuICAgICAgZm9yICh2YXIgY3VycmVudE5ldXJvbiBpbiBvcHRpbWl6ZWQucHJvcGFnYXRpb25fc2VudGVuY2VzW2N1cnJlbnRMYXllcl0pXG4gICAgICAgIGhhcmRjb2RlICs9IG9wdGltaXplZC5wcm9wYWdhdGlvbl9zZW50ZW5jZXNbY3VycmVudExheWVyXVtjdXJyZW50TmV1cm9uXS5qb2luKFwiIFwiKSArIFwiIFwiO1xuICAgIGhhcmRjb2RlICs9IFwiIH07XFxuXCI7XG4gICAgaGFyZGNvZGUgKz1cbiAgICAgIFwidmFyIG93bmVyc2hpcCA9IGZ1bmN0aW9uKG1lbW9yeUJ1ZmZlcil7XFxuRiA9IG1lbW9yeUJ1ZmZlcjtcXG50aGlzLm1lbW9yeSA9IEY7XFxufTtcXG5cIjtcbiAgICBoYXJkY29kZSArPVxuICAgICAgXCJyZXR1cm4ge1xcbm1lbW9yeTogRixcXG5hY3RpdmF0ZTogYWN0aXZhdGUsXFxucHJvcGFnYXRlOiBwcm9wYWdhdGUsXFxub3duZXJzaGlwOiBvd25lcnNoaXBcXG59O1wiO1xuICAgIGhhcmRjb2RlID0gaGFyZGNvZGUuc3BsaXQoXCI7XCIpLmpvaW4oXCI7XFxuXCIpO1xuXG4gICAgdmFyIGNvbnN0cnVjdG9yID0gbmV3IEZ1bmN0aW9uKGhhcmRjb2RlKTtcblxuICAgIHZhciBuZXR3b3JrID0gY29uc3RydWN0b3IoKTtcbiAgICBuZXR3b3JrLmRhdGEgPSB7XG4gICAgICB2YXJpYWJsZXM6IG9wdGltaXplZC52YXJpYWJsZXMsXG4gICAgICBhY3RpdmF0ZTogb3B0aW1pemVkLmFjdGl2YXRpb25fc2VudGVuY2VzLFxuICAgICAgcHJvcGFnYXRlOiBvcHRpbWl6ZWQucHJvcGFnYXRpb25fc2VudGVuY2VzLFxuICAgICAgdHJhY2U6IG9wdGltaXplZC50cmFjZV9zZW50ZW5jZXMsXG4gICAgICBpbnB1dHM6IG9wdGltaXplZC5pbnB1dHMsXG4gICAgICBvdXRwdXRzOiBvcHRpbWl6ZWQub3V0cHV0cyxcbiAgICAgIGNoZWNrX2FjdGl2YXRpb246IHRoaXMuYWN0aXZhdGUsXG4gICAgICBjaGVja19wcm9wYWdhdGlvbjogdGhpcy5wcm9wYWdhdGVcbiAgICB9XG5cbiAgICBuZXR3b3JrLnJlc2V0ID0gZnVuY3Rpb24oKSB7XG4gICAgICBpZiAodGhhdC5vcHRpbWl6ZWQpIHtcbiAgICAgICAgdGhhdC5vcHRpbWl6ZWQgPSBudWxsO1xuICAgICAgICB0aGF0LmFjdGl2YXRlID0gbmV0d29yay5kYXRhLmNoZWNrX2FjdGl2YXRpb247XG4gICAgICAgIHRoYXQucHJvcGFnYXRlID0gbmV0d29yay5kYXRhLmNoZWNrX3Byb3BhZ2F0aW9uO1xuICAgICAgfVxuICAgIH1cblxuICAgIHRoaXMub3B0aW1pemVkID0gbmV0d29yaztcbiAgICB0aGlzLmFjdGl2YXRlID0gbmV0d29yay5hY3RpdmF0ZTtcbiAgICB0aGlzLnByb3BhZ2F0ZSA9IG5ldHdvcmsucHJvcGFnYXRlO1xuICB9LFxuXG4gIC8vIHJlc3RvcmVzIGFsbCB0aGUgdmFsdWVzIGZyb20gdGhlIG9wdGltaXplZCBuZXR3b3JrIHRoZSB0aGVpciByZXNwZWN0aXZlIG9iamVjdHMgaW4gb3JkZXIgdG8gbWFuaXB1bGF0ZSB0aGUgbmV0d29ya1xuICByZXN0b3JlOiBmdW5jdGlvbigpIHtcbiAgICBpZiAoIXRoaXMub3B0aW1pemVkKVxuICAgICAgcmV0dXJuO1xuXG4gICAgdmFyIG9wdGltaXplZCA9IHRoaXMub3B0aW1pemVkO1xuXG4gICAgdmFyIGdldFZhbHVlID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7XG5cbiAgICAgIHZhciB1bml0ID0gYXJncy5zaGlmdCgpO1xuICAgICAgdmFyIHByb3AgPSBhcmdzLnBvcCgpO1xuXG4gICAgICB2YXIgaWQgPSBwcm9wICsgJ18nO1xuICAgICAgZm9yICh2YXIgcHJvcGVydHkgaW4gYXJncylcbiAgICAgICAgaWQgKz0gYXJnc1twcm9wZXJ0eV0gKyAnXyc7XG4gICAgICBpZCArPSB1bml0LklEO1xuXG4gICAgICB2YXIgbWVtb3J5ID0gb3B0aW1pemVkLm1lbW9yeTtcbiAgICAgIHZhciB2YXJpYWJsZXMgPSBvcHRpbWl6ZWQuZGF0YS52YXJpYWJsZXM7XG5cbiAgICAgIGlmIChpZCBpbiB2YXJpYWJsZXMpXG4gICAgICAgIHJldHVybiBtZW1vcnlbdmFyaWFibGVzW2lkXS5pZF07XG4gICAgICByZXR1cm4gMDtcbiAgICB9XG5cbiAgICB2YXIgbGlzdCA9IHRoaXMubmV1cm9ucygpO1xuXG4gICAgLy8gbGluayBpZCdzIHRvIHBvc2l0aW9ucyBpbiB0aGUgYXJyYXlcbiAgICB2YXIgaWRzID0ge307XG4gICAgZm9yICh2YXIgaSBpbiBsaXN0KSB7XG4gICAgICB2YXIgbmV1cm9uID0gbGlzdFtpXS5uZXVyb247XG4gICAgICB3aGlsZSAobmV1cm9uLm5ldXJvbilcbiAgICAgICAgbmV1cm9uID0gbmV1cm9uLm5ldXJvbjtcblxuICAgICAgbmV1cm9uLnN0YXRlID0gZ2V0VmFsdWUobmV1cm9uLCAnc3RhdGUnKTtcbiAgICAgIG5ldXJvbi5vbGQgPSBnZXRWYWx1ZShuZXVyb24sICdvbGQnKTtcbiAgICAgIG5ldXJvbi5hY3RpdmF0aW9uID0gZ2V0VmFsdWUobmV1cm9uLCAnYWN0aXZhdGlvbicpO1xuICAgICAgbmV1cm9uLmJpYXMgPSBnZXRWYWx1ZShuZXVyb24sICdiaWFzJyk7XG5cbiAgICAgIGZvciAodmFyIGlucHV0IGluIG5ldXJvbi50cmFjZS5lbGVnaWJpbGl0eSlcbiAgICAgICAgbmV1cm9uLnRyYWNlLmVsZWdpYmlsaXR5W2lucHV0XSA9IGdldFZhbHVlKG5ldXJvbiwgJ3RyYWNlJyxcbiAgICAgICAgICAnZWxlZ2liaWxpdHknLCBpbnB1dCk7XG5cbiAgICAgIGZvciAodmFyIGdhdGVkIGluIG5ldXJvbi50cmFjZS5leHRlbmRlZClcbiAgICAgICAgZm9yICh2YXIgaW5wdXQgaW4gbmV1cm9uLnRyYWNlLmV4dGVuZGVkW2dhdGVkXSlcbiAgICAgICAgICBuZXVyb24udHJhY2UuZXh0ZW5kZWRbZ2F0ZWRdW2lucHV0XSA9IGdldFZhbHVlKG5ldXJvbiwgJ3RyYWNlJyxcbiAgICAgICAgICAgICdleHRlbmRlZCcsIGdhdGVkLCBpbnB1dCk7XG4gICAgfVxuXG4gICAgLy8gZ2V0IGNvbm5lY3Rpb25zXG4gICAgZm9yICh2YXIgaSBpbiBsaXN0KSB7XG4gICAgICB2YXIgbmV1cm9uID0gbGlzdFtpXS5uZXVyb247XG4gICAgICB3aGlsZSAobmV1cm9uLm5ldXJvbilcbiAgICAgICAgbmV1cm9uID0gbmV1cm9uLm5ldXJvbjtcblxuICAgICAgZm9yICh2YXIgaiBpbiBuZXVyb24uY29ubmVjdGlvbnMucHJvamVjdGVkKSB7XG4gICAgICAgIHZhciBjb25uZWN0aW9uID0gbmV1cm9uLmNvbm5lY3Rpb25zLnByb2plY3RlZFtqXTtcbiAgICAgICAgY29ubmVjdGlvbi53ZWlnaHQgPSBnZXRWYWx1ZShjb25uZWN0aW9uLCAnd2VpZ2h0Jyk7XG4gICAgICAgIGNvbm5lY3Rpb24uZ2FpbiA9IGdldFZhbHVlKGNvbm5lY3Rpb24sICdnYWluJyk7XG4gICAgICB9XG4gICAgfVxuICB9LFxuXG4gIC8vIHJldHVybnMgYWxsIHRoZSBuZXVyb25zIGluIHRoZSBuZXR3b3JrXG4gIG5ldXJvbnM6IGZ1bmN0aW9uKCkge1xuXG4gICAgdmFyIG5ldXJvbnMgPSBbXTtcblxuICAgIHZhciBpbnB1dExheWVyID0gdGhpcy5sYXllcnMuaW5wdXQubmV1cm9ucygpLFxuICAgICAgb3V0cHV0TGF5ZXIgPSB0aGlzLmxheWVycy5vdXRwdXQubmV1cm9ucygpO1xuXG4gICAgZm9yICh2YXIgbmV1cm9uIGluIGlucHV0TGF5ZXIpXG4gICAgICBuZXVyb25zLnB1c2goe1xuICAgICAgICBuZXVyb246IGlucHV0TGF5ZXJbbmV1cm9uXSxcbiAgICAgICAgbGF5ZXI6ICdpbnB1dCdcbiAgICAgIH0pO1xuXG4gICAgZm9yICh2YXIgbGF5ZXIgaW4gdGhpcy5sYXllcnMuaGlkZGVuKSB7XG4gICAgICB2YXIgaGlkZGVuTGF5ZXIgPSB0aGlzLmxheWVycy5oaWRkZW5bbGF5ZXJdLm5ldXJvbnMoKTtcbiAgICAgIGZvciAodmFyIG5ldXJvbiBpbiBoaWRkZW5MYXllcilcbiAgICAgICAgbmV1cm9ucy5wdXNoKHtcbiAgICAgICAgICBuZXVyb246IGhpZGRlbkxheWVyW25ldXJvbl0sXG4gICAgICAgICAgbGF5ZXI6IGxheWVyXG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBmb3IgKHZhciBuZXVyb24gaW4gb3V0cHV0TGF5ZXIpXG4gICAgICBuZXVyb25zLnB1c2goe1xuICAgICAgICBuZXVyb246IG91dHB1dExheWVyW25ldXJvbl0sXG4gICAgICAgIGxheWVyOiAnb3V0cHV0J1xuICAgICAgfSk7XG5cbiAgICByZXR1cm4gbmV1cm9ucztcbiAgfSxcblxuICAvLyByZXR1cm5zIG51bWJlciBvZiBpbnB1dHMgb2YgdGhlIG5ldHdvcmtcbiAgaW5wdXRzOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gdGhpcy5sYXllcnMuaW5wdXQuc2l6ZTtcbiAgfSxcblxuICAvLyByZXR1cm5zIG51bWJlciBvZiBvdXRwdXRzIG9mIGh0ZSBuZXR3b3JrXG4gIG91dHB1dHM6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB0aGlzLmxheWVycy5vdXRwdXQuc2l6ZTtcbiAgfSxcblxuICAvLyBzZXRzIHRoZSBsYXllcnMgb2YgdGhlIG5ldHdvcmtcbiAgc2V0OiBmdW5jdGlvbihsYXllcnMpIHtcblxuICAgIHRoaXMubGF5ZXJzID0gbGF5ZXJzO1xuICAgIGlmICh0aGlzLm9wdGltaXplZClcbiAgICAgIHRoaXMub3B0aW1pemVkLnJlc2V0KCk7XG4gIH0sXG5cbiAgc2V0T3B0aW1pemU6IGZ1bmN0aW9uKGJvb2wpe1xuICAgIHRoaXMucmVzdG9yZSgpO1xuICAgIGlmICh0aGlzLm9wdGltaXplZClcbiAgICAgIHRoaXMub3B0aW1pemVkLnJlc2V0KCk7XG4gICAgdGhpcy5vcHRpbWl6ZWQgPSBib29sPyBudWxsIDogZmFsc2U7XG4gIH0sXG5cbiAgLy8gcmV0dXJucyBhIGpzb24gdGhhdCByZXByZXNlbnRzIGFsbCB0aGUgbmV1cm9ucyBhbmQgY29ubmVjdGlvbnMgb2YgdGhlIG5ldHdvcmtcbiAgdG9KU09OOiBmdW5jdGlvbihpZ25vcmVUcmFjZXMpIHtcblxuICAgIHRoaXMucmVzdG9yZSgpO1xuXG4gICAgdmFyIGxpc3QgPSB0aGlzLm5ldXJvbnMoKTtcbiAgICB2YXIgbmV1cm9ucyA9IFtdO1xuICAgIHZhciBjb25uZWN0aW9ucyA9IFtdO1xuXG4gICAgLy8gbGluayBpZCdzIHRvIHBvc2l0aW9ucyBpbiB0aGUgYXJyYXlcbiAgICB2YXIgaWRzID0ge307XG4gICAgZm9yICh2YXIgaSBpbiBsaXN0KSB7XG4gICAgICB2YXIgbmV1cm9uID0gbGlzdFtpXS5uZXVyb247XG4gICAgICB3aGlsZSAobmV1cm9uLm5ldXJvbilcbiAgICAgICAgbmV1cm9uID0gbmV1cm9uLm5ldXJvbjtcbiAgICAgIGlkc1tuZXVyb24uSURdID0gaTtcblxuICAgICAgdmFyIGNvcHkgPSB7XG4gICAgICAgIHRyYWNlOiB7XG4gICAgICAgICAgZWxlZ2liaWxpdHk6IHt9LFxuICAgICAgICAgIGV4dGVuZGVkOiB7fVxuICAgICAgICB9LFxuICAgICAgICBzdGF0ZTogbmV1cm9uLnN0YXRlLFxuICAgICAgICBvbGQ6IG5ldXJvbi5vbGQsXG4gICAgICAgIGFjdGl2YXRpb246IG5ldXJvbi5hY3RpdmF0aW9uLFxuICAgICAgICBiaWFzOiBuZXVyb24uYmlhcyxcbiAgICAgICAgbGF5ZXI6IGxpc3RbaV0ubGF5ZXJcbiAgICAgIH07XG5cbiAgICAgIGNvcHkuc3F1YXNoID0gbmV1cm9uLnNxdWFzaCA9PSBOZXVyb24uc3F1YXNoLkxPR0lTVElDID8gXCJMT0dJU1RJQ1wiIDpcbiAgICAgICAgbmV1cm9uLnNxdWFzaCA9PSBOZXVyb24uc3F1YXNoLlRBTkggPyBcIlRBTkhcIiA6XG4gICAgICAgIG5ldXJvbi5zcXVhc2ggPT0gTmV1cm9uLnNxdWFzaC5JREVOVElUWSA/IFwiSURFTlRJVFlcIiA6XG4gICAgICAgIG5ldXJvbi5zcXVhc2ggPT0gTmV1cm9uLnNxdWFzaC5ITElNID8gXCJITElNXCIgOlxuICAgICAgICBudWxsO1xuXG4gICAgICBuZXVyb25zLnB1c2goY29weSk7XG4gICAgfVxuXG4gICAgLy8gZ2V0IGNvbm5lY3Rpb25zXG4gICAgZm9yICh2YXIgaSBpbiBsaXN0KSB7XG4gICAgICB2YXIgbmV1cm9uID0gbGlzdFtpXS5uZXVyb247XG4gICAgICB3aGlsZSAobmV1cm9uLm5ldXJvbilcbiAgICAgICAgbmV1cm9uID0gbmV1cm9uLm5ldXJvbjtcblxuICAgICAgZm9yICh2YXIgaiBpbiBuZXVyb24uY29ubmVjdGlvbnMucHJvamVjdGVkKSB7XG4gICAgICAgIHZhciBjb25uZWN0aW9uID0gbmV1cm9uLmNvbm5lY3Rpb25zLnByb2plY3RlZFtqXTtcbiAgICAgICAgY29ubmVjdGlvbnMucHVzaCh7XG4gICAgICAgICAgZnJvbTogaWRzW2Nvbm5lY3Rpb24uZnJvbS5JRF0sXG4gICAgICAgICAgdG86IGlkc1tjb25uZWN0aW9uLnRvLklEXSxcbiAgICAgICAgICB3ZWlnaHQ6IGNvbm5lY3Rpb24ud2VpZ2h0LFxuICAgICAgICAgIGdhdGVyOiBjb25uZWN0aW9uLmdhdGVyID8gaWRzW2Nvbm5lY3Rpb24uZ2F0ZXIuSURdIDogbnVsbCxcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICBpZiAobmV1cm9uLnNlbGZjb25uZWN0ZWQoKSlcbiAgICAgICAgY29ubmVjdGlvbnMucHVzaCh7XG4gICAgICAgICAgZnJvbTogaWRzW25ldXJvbi5JRF0sXG4gICAgICAgICAgdG86IGlkc1tuZXVyb24uSURdLFxuICAgICAgICAgIHdlaWdodDogbmV1cm9uLnNlbGZjb25uZWN0aW9uLndlaWdodCxcbiAgICAgICAgICBnYXRlcjogbmV1cm9uLnNlbGZjb25uZWN0aW9uLmdhdGVyID8gaWRzW25ldXJvbi5zZWxmY29ubmVjdGlvbi5nYXRlci5JRF0gOiBudWxsLFxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICByZXR1cm4ge1xuICAgICAgbmV1cm9uczogbmV1cm9ucyxcbiAgICAgIGNvbm5lY3Rpb25zOiBjb25uZWN0aW9uc1xuICAgIH1cbiAgfSxcblxuICAvLyBleHBvcnQgdGhlIHRvcG9sb2d5IGludG8gZG90IGxhbmd1YWdlIHdoaWNoIGNhbiBiZSB2aXN1YWxpemVkIGFzIGdyYXBocyB1c2luZyBkb3RcbiAgLyogZXhhbXBsZTogLi4uIGNvbnNvbGUubG9nKG5ldC50b0RvdExhbmcoKSk7XG4gICAgICAgICAgICAgICQgbm9kZSBleGFtcGxlLmpzID4gZXhhbXBsZS5kb3RcbiAgICAgICAgICAgICAgJCBkb3QgZXhhbXBsZS5kb3QgLVRwbmcgPiBvdXQucG5nXG4gICovXG4gIHRvRG90OiBmdW5jdGlvbihlZGdlQ29ubmVjdGlvbikge1xuICAgIGlmICghIHR5cGVvZiBlZGdlQ29ubmVjdGlvbilcbiAgICAgIGVkZ2VDb25uZWN0aW9uID0gZmFsc2U7XG4gICAgdmFyIGNvZGUgPSBcImRpZ3JhcGggbm4ge1xcbiAgICByYW5rZGlyID0gQlRcXG5cIjtcbiAgICB2YXIgbGF5ZXJzID0gW3RoaXMubGF5ZXJzLmlucHV0XS5jb25jYXQodGhpcy5sYXllcnMuaGlkZGVuLCB0aGlzLmxheWVycy5vdXRwdXQpO1xuICAgIGZvciAodmFyIGxheWVyIGluIGxheWVycykge1xuICAgICAgZm9yICh2YXIgdG8gaW4gbGF5ZXJzW2xheWVyXS5jb25uZWN0ZWRUbykgeyAvLyBwcm9qZWN0aW9uc1xuICAgICAgICB2YXIgY29ubmVjdGlvbiA9IGxheWVyc1tsYXllcl0uY29ubmVjdGVkVG9bdG9dO1xuICAgICAgICB2YXIgbGF5ZXJUbyA9IGNvbm5lY3Rpb24udG87XG4gICAgICAgIHZhciBzaXplID0gY29ubmVjdGlvbi5zaXplO1xuICAgICAgICB2YXIgbGF5ZXJJRCA9IGxheWVycy5pbmRleE9mKGxheWVyc1tsYXllcl0pO1xuICAgICAgICB2YXIgbGF5ZXJUb0lEID0gbGF5ZXJzLmluZGV4T2YobGF5ZXJUbyk7XG4gICAgICAgIC8qIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMjY4NDU1NDAvY29ubmVjdC1lZGdlcy13aXRoLWdyYXBoLWRvdFxuICAgICAgICAgKiBET1QgZG9lcyBub3Qgc3VwcG9ydCBlZGdlLXRvLWVkZ2UgY29ubmVjdGlvbnNcbiAgICAgICAgICogVGhpcyB3b3JrYXJvdW5kIHByb2R1Y2VzIHNvbWV3aGF0IHdlaXJkIGdyYXBocyAuLi5cbiAgICAgICAgKi9cbiAgICAgICAgaWYgKCBlZGdlQ29ubmVjdGlvbikge1xuICAgICAgICAgIGlmIChjb25uZWN0aW9uLmdhdGVkZnJvbS5sZW5ndGgpIHtcbiAgICAgICAgICAgIHZhciBmYWtlTm9kZSA9IFwiZmFrZVwiICsgbGF5ZXJJRCArIFwiX1wiICsgbGF5ZXJUb0lEO1xuICAgICAgICAgICAgY29kZSArPSBcIiAgICBcIiArIGZha2VOb2RlICtcbiAgICAgICAgICAgICAgXCIgW2xhYmVsID0gXFxcIlxcXCIsIHNoYXBlID0gcG9pbnQsIHdpZHRoID0gMC4wMSwgaGVpZ2h0ID0gMC4wMV1cXG5cIjtcbiAgICAgICAgICAgIGNvZGUgKz0gXCIgICAgXCIgKyBsYXllcklEICsgXCIgLT4gXCIgKyBmYWtlTm9kZSArIFwiIFtsYWJlbCA9IFwiICsgc2l6ZSArIFwiLCBhcnJvd2hlYWQgPSBub25lXVxcblwiO1xuICAgICAgICAgICAgY29kZSArPSBcIiAgICBcIiArIGZha2VOb2RlICsgXCIgLT4gXCIgKyBsYXllclRvSUQgKyBcIlxcblwiO1xuICAgICAgICAgIH0gZWxzZVxuICAgICAgICAgICAgY29kZSArPSBcIiAgICBcIiArIGxheWVySUQgKyBcIiAtPiBcIiArIGxheWVyVG9JRCArIFwiIFtsYWJlbCA9IFwiICsgc2l6ZSArIFwiXVxcblwiO1xuICAgICAgICAgIGZvciAodmFyIGZyb20gaW4gY29ubmVjdGlvbi5nYXRlZGZyb20pIHsgLy8gZ2F0aW5nc1xuICAgICAgICAgICAgdmFyIGxheWVyZnJvbSA9IGNvbm5lY3Rpb24uZ2F0ZWRmcm9tW2Zyb21dLmxheWVyO1xuICAgICAgICAgICAgdmFyIGxheWVyZnJvbUlEID0gbGF5ZXJzLmluZGV4T2YobGF5ZXJmcm9tKTtcbiAgICAgICAgICAgIGNvZGUgKz0gXCIgICAgXCIgKyBsYXllcmZyb21JRCArIFwiIC0+IFwiICsgZmFrZU5vZGUgKyBcIiBbY29sb3IgPSBibHVlXVxcblwiO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb2RlICs9IFwiICAgIFwiICsgbGF5ZXJJRCArIFwiIC0+IFwiICsgbGF5ZXJUb0lEICsgXCIgW2xhYmVsID0gXCIgKyBzaXplICsgXCJdXFxuXCI7XG4gICAgICAgICAgZm9yICh2YXIgZnJvbSBpbiBjb25uZWN0aW9uLmdhdGVkZnJvbSkgeyAvLyBnYXRpbmdzXG4gICAgICAgICAgICB2YXIgbGF5ZXJmcm9tID0gY29ubmVjdGlvbi5nYXRlZGZyb21bZnJvbV0ubGF5ZXI7XG4gICAgICAgICAgICB2YXIgbGF5ZXJmcm9tSUQgPSBsYXllcnMuaW5kZXhPZihsYXllcmZyb20pO1xuICAgICAgICAgICAgY29kZSArPSBcIiAgICBcIiArIGxheWVyZnJvbUlEICsgXCIgLT4gXCIgKyBsYXllclRvSUQgKyBcIiBbY29sb3IgPSBibHVlXVxcblwiO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICBjb2RlICs9IFwifVxcblwiO1xuICAgIHJldHVybiB7XG4gICAgICBjb2RlOiBjb2RlLFxuICAgICAgbGluazogXCJodHRwczovL2NoYXJ0Lmdvb2dsZWFwaXMuY29tL2NoYXJ0P2NobD1cIiArIGVzY2FwZShjb2RlLnJlcGxhY2UoXCIvIC9nXCIsIFwiK1wiKSkgKyBcIiZjaHQ9Z3ZcIlxuICAgIH1cbiAgfSxcblxuICAvLyByZXR1cm5zIGEgZnVuY3Rpb24gdGhhdCB3b3JrcyBhcyB0aGUgYWN0aXZhdGlvbiBvZiB0aGUgbmV0d29yayBhbmQgY2FuIGJlIHVzZWQgd2l0aG91dCBkZXBlbmRpbmcgb24gdGhlIGxpYnJhcnlcbiAgc3RhbmRhbG9uZTogZnVuY3Rpb24oKSB7XG4gICAgaWYgKCF0aGlzLm9wdGltaXplZClcbiAgICAgIHRoaXMub3B0aW1pemUoKTtcblxuICAgIHZhciBkYXRhID0gdGhpcy5vcHRpbWl6ZWQuZGF0YTtcblxuICAgIC8vIGJ1aWxkIGFjdGl2YXRpb24gZnVuY3Rpb25cbiAgICB2YXIgYWN0aXZhdGlvbiA9IFwiZnVuY3Rpb24gKGlucHV0KSB7XFxuXCI7XG5cbiAgICAvLyBidWlsZCBpbnB1dHNcbiAgICBmb3IgKHZhciBpIGluIGRhdGEuaW5wdXRzKVxuICAgICAgYWN0aXZhdGlvbiArPSBcIkZbXCIgKyBkYXRhLmlucHV0c1tpXSArIFwiXSA9IGlucHV0W1wiICsgaSArIFwiXTtcXG5cIjtcblxuICAgIC8vIGJ1aWxkIG5ldHdvcmsgYWN0aXZhdGlvblxuICAgIGZvciAodmFyIG5ldXJvbiBpbiBkYXRhLmFjdGl2YXRlKSB7IC8vIHNob3VsZG4ndCB0aGlzIGJlIGxheWVyP1xuICAgICAgZm9yICh2YXIgc2VudGVuY2UgaW4gZGF0YS5hY3RpdmF0ZVtuZXVyb25dKVxuICAgICAgICBhY3RpdmF0aW9uICs9IGRhdGEuYWN0aXZhdGVbbmV1cm9uXVtzZW50ZW5jZV0uam9pbignJykgKyBcIlxcblwiO1xuICAgIH1cblxuICAgIC8vIGJ1aWxkIG91dHB1dHNcbiAgICBhY3RpdmF0aW9uICs9IFwidmFyIG91dHB1dCA9IFtdO1xcblwiO1xuICAgIGZvciAodmFyIGkgaW4gZGF0YS5vdXRwdXRzKVxuICAgICAgYWN0aXZhdGlvbiArPSBcIm91dHB1dFtcIiArIGkgKyBcIl0gPSBGW1wiICsgZGF0YS5vdXRwdXRzW2ldICsgXCJdO1xcblwiO1xuICAgIGFjdGl2YXRpb24gKz0gXCJyZXR1cm4gb3V0cHV0O1xcbn1cIjtcblxuICAgIC8vIHJlZmVyZW5jZSBhbGwgdGhlIHBvc2l0aW9ucyBpbiBtZW1vcnlcbiAgICB2YXIgbWVtb3J5ID0gYWN0aXZhdGlvbi5tYXRjaCgvRlxcWyhcXGQrKVxcXS9nKTtcbiAgICB2YXIgZGltZW5zaW9uID0gMDtcbiAgICB2YXIgaWRzID0ge307XG4gICAgZm9yICh2YXIgYWRkcmVzcyBpbiBtZW1vcnkpIHtcbiAgICAgIHZhciB0bXAgPSBtZW1vcnlbYWRkcmVzc10ubWF0Y2goL1xcZCsvKVswXTtcbiAgICAgIGlmICghKHRtcCBpbiBpZHMpKSB7XG4gICAgICAgIGlkc1t0bXBdID0gZGltZW5zaW9uKys7XG4gICAgICB9XG4gICAgfVxuICAgIHZhciBoYXJkY29kZSA9IFwiRiA9IHtcXG5cIjtcbiAgICBmb3IgKHZhciBpIGluIGlkcylcbiAgICAgIGhhcmRjb2RlICs9IGlkc1tpXSArIFwiOiBcIiArIHRoaXMub3B0aW1pemVkLm1lbW9yeVtpXSArIFwiLFxcblwiO1xuICAgIGhhcmRjb2RlID0gaGFyZGNvZGUuc3Vic3RyaW5nKDAsIGhhcmRjb2RlLmxlbmd0aCAtIDIpICsgXCJcXG59O1xcblwiO1xuICAgIGhhcmRjb2RlID0gXCJ2YXIgcnVuID0gXCIgKyBhY3RpdmF0aW9uLnJlcGxhY2UoL0ZcXFsoXFxkKyldL2csIGZ1bmN0aW9uKFxuICAgICAgaW5kZXgpIHtcbiAgICAgIHJldHVybiAnRlsnICsgaWRzW2luZGV4Lm1hdGNoKC9cXGQrLylbMF1dICsgJ10nXG4gICAgfSkucmVwbGFjZShcIntcXG5cIiwgXCJ7XFxuXCIgKyBoYXJkY29kZSArIFwiXCIpICsgXCI7XFxuXCI7XG4gICAgaGFyZGNvZGUgKz0gXCJyZXR1cm4gcnVuXCI7XG5cbiAgICAvLyByZXR1cm4gc3RhbmRhbG9uZSBmdW5jdGlvblxuICAgIHJldHVybiBuZXcgRnVuY3Rpb24oaGFyZGNvZGUpKCk7XG4gIH0sXG5cblxuICAvLyBSZXR1cm4gYSBIVE1MNSBXZWJXb3JrZXIgc3BlY2lhbGl6ZWQgb24gdHJhaW5pbmcgdGhlIG5ldHdvcmsgc3RvcmVkIGluIGBtZW1vcnlgLlxuICAvLyBUcmFpbiBiYXNlZCBvbiB0aGUgZ2l2ZW4gZGF0YVNldCBhbmQgb3B0aW9ucy5cbiAgLy8gVGhlIHdvcmtlciByZXR1cm5zIHRoZSB1cGRhdGVkIGBtZW1vcnlgIHdoZW4gZG9uZS5cbiAgd29ya2VyOiBmdW5jdGlvbihtZW1vcnksIHNldCwgb3B0aW9ucykge1xuXG4gICAgLy8gQ29weSB0aGUgb3B0aW9ucyBhbmQgc2V0IGRlZmF1bHRzIChvcHRpb25zIG1pZ2h0IGJlIGRpZmZlcmVudCBmb3IgZWFjaCB3b3JrZXIpXG4gICAgdmFyIHdvcmtlck9wdGlvbnMgPSB7fTtcbiAgICBpZihvcHRpb25zKSB3b3JrZXJPcHRpb25zID0gb3B0aW9ucztcbiAgICB3b3JrZXJPcHRpb25zLnJhdGUgPSBvcHRpb25zLnJhdGUgfHwgLjI7XG4gICAgd29ya2VyT3B0aW9ucy5pdGVyYXRpb25zID0gb3B0aW9ucy5pdGVyYXRpb25zIHx8IDEwMDAwMDtcbiAgICB3b3JrZXJPcHRpb25zLmVycm9yID0gb3B0aW9ucy5lcnJvciB8fCAuMDA1O1xuICAgIHdvcmtlck9wdGlvbnMuY29zdCA9IG9wdGlvbnMuY29zdCB8fCBudWxsO1xuICAgIHdvcmtlck9wdGlvbnMuY3Jvc3NWYWxpZGF0ZSA9IG9wdGlvbnMuY3Jvc3NWYWxpZGF0ZSB8fCBudWxsO1xuXG4gICAgLy8gQ29zdCBmdW5jdGlvbiBtaWdodCBiZSBkaWZmZXJlbnQgZm9yIGVhY2ggd29ya2VyXG4gICAgY29zdEZ1bmN0aW9uID0gXCJ2YXIgY29zdCA9IFwiICsgKG9wdGlvbnMgJiYgb3B0aW9ucy5jb3N0IHx8IHRoaXMuY29zdCB8fCBUcmFpbmVyLmNvc3QuTVNFKSArIFwiO1xcblwiO1xuICAgIHZhciB3b3JrZXJGdW5jdGlvbiA9IE5ldHdvcmsuZ2V0V29ya2VyU2hhcmVkRnVuY3Rpb25zKCk7XG4gICAgd29ya2VyRnVuY3Rpb24gPSB3b3JrZXJGdW5jdGlvbi5yZXBsYWNlKC92YXIgY29zdCA9IG9wdGlvbnMgJiYgb3B0aW9uc1xcLmNvc3QgXFx8XFx8IHRoaXNcXC5jb3N0IFxcfFxcfCBUcmFpbmVyXFwuY29zdFxcLk1TRTsvZywgY29zdEZ1bmN0aW9uKTtcblxuICAgIC8vIFNldCB3aGF0IHdlIGRvIHdoZW4gdHJhaW5pbmcgaXMgZmluaXNoZWRcbiAgICB3b3JrZXJGdW5jdGlvbiA9IHdvcmtlckZ1bmN0aW9uLnJlcGxhY2UoJ3JldHVybiByZXN1bHRzOycsXG4gICAgICAgICAgICAgICAgICAgICAgJ3Bvc3RNZXNzYWdlKHthY3Rpb246IFwiZG9uZVwiLCBtZXNzYWdlOiByZXN1bHRzLCBtZW1vcnlCdWZmZXI6IEZ9LCBbRi5idWZmZXJdKTsnKTtcblxuICAgIC8vIFJlcGxhY2UgbG9nIHdpdGggcG9zdG1lc3NhZ2VcbiAgICB3b3JrZXJGdW5jdGlvbiA9IHdvcmtlckZ1bmN0aW9uLnJlcGxhY2UoXCJjb25zb2xlLmxvZygnaXRlcmF0aW9ucycsIGl0ZXJhdGlvbnMsICdlcnJvcicsIGVycm9yLCAncmF0ZScsIGN1cnJlbnRSYXRlKVwiLFxuICAgICAgICAgICAgICBcInBvc3RNZXNzYWdlKHthY3Rpb246ICdsb2cnLCBtZXNzYWdlOiB7XFxuXCIgK1xuICAgICAgICAgICAgICAgICAgXCJpdGVyYXRpb25zOiBpdGVyYXRpb25zLFxcblwiICtcbiAgICAgICAgICAgICAgICAgIFwiZXJyb3I6IGVycm9yLFxcblwiICtcbiAgICAgICAgICAgICAgICAgIFwicmF0ZTogY3VycmVudFJhdGVcXG5cIiArXG4gICAgICAgICAgICAgICAgXCJ9XFxuXCIgK1xuICAgICAgICAgICAgICBcIn0pXCIpO1xuXG4gICAgLy8gUmVwbGFjZSBzY2hlZHVsZSB3aXRoIHBvc3RtZXNzYWdlXG4gICAgd29ya2VyRnVuY3Rpb24gPSB3b3JrZXJGdW5jdGlvbi5yZXBsYWNlKFwiYWJvcnQgPSB0aGlzLnNjaGVkdWxlLmRvKHsgZXJyb3I6IGVycm9yLCBpdGVyYXRpb25zOiBpdGVyYXRpb25zLCByYXRlOiBjdXJyZW50UmF0ZSB9KVwiLFxuICAgICAgICAgICAgICBcInBvc3RNZXNzYWdlKHthY3Rpb246ICdzY2hlZHVsZScsIG1lc3NhZ2U6IHtcXG5cIiArXG4gICAgICAgICAgICAgICAgICBcIml0ZXJhdGlvbnM6IGl0ZXJhdGlvbnMsXFxuXCIgK1xuICAgICAgICAgICAgICAgICAgXCJlcnJvcjogZXJyb3IsXFxuXCIgK1xuICAgICAgICAgICAgICAgICAgXCJyYXRlOiBjdXJyZW50UmF0ZVxcblwiICtcbiAgICAgICAgICAgICAgICBcIn1cXG5cIiArXG4gICAgICAgICAgICAgIFwifSlcIik7XG5cbiAgICBpZiAoIXRoaXMub3B0aW1pemVkKVxuICAgICAgdGhpcy5vcHRpbWl6ZSgpO1xuXG4gICAgdmFyIGhhcmRjb2RlID0gXCJ2YXIgaW5wdXRzID0gXCIgKyB0aGlzLm9wdGltaXplZC5kYXRhLmlucHV0cy5sZW5ndGggKyBcIjtcXG5cIjtcbiAgICBoYXJkY29kZSArPSBcInZhciBvdXRwdXRzID0gXCIgKyB0aGlzLm9wdGltaXplZC5kYXRhLm91dHB1dHMubGVuZ3RoICsgXCI7XFxuXCI7XG4gICAgaGFyZGNvZGUgKz0gXCJ2YXIgRiA9ICBuZXcgRmxvYXQ2NEFycmF5KFtcIiArIHRoaXMub3B0aW1pemVkLm1lbW9yeS50b1N0cmluZygpICsgXCJdKTtcXG5cIjtcbiAgICBoYXJkY29kZSArPSBcInZhciBhY3RpdmF0ZSA9IFwiICsgdGhpcy5vcHRpbWl6ZWQuYWN0aXZhdGUudG9TdHJpbmcoKSArIFwiO1xcblwiO1xuICAgIGhhcmRjb2RlICs9IFwidmFyIHByb3BhZ2F0ZSA9IFwiICsgdGhpcy5vcHRpbWl6ZWQucHJvcGFnYXRlLnRvU3RyaW5nKCkgKyBcIjtcXG5cIjtcbiAgICBoYXJkY29kZSArPVxuICAgICAgICBcIm9ubWVzc2FnZSA9IGZ1bmN0aW9uKGUpIHtcXG5cIiArXG4gICAgICAgICAgXCJpZiAoZS5kYXRhLmFjdGlvbiA9PSAnc3RhcnRUcmFpbmluZycpIHtcXG5cIiArXG4gICAgICAgICAgICBcInRyYWluKFwiICsgSlNPTi5zdHJpbmdpZnkoc2V0KSArIFwiLFwiICsgSlNPTi5zdHJpbmdpZnkod29ya2VyT3B0aW9ucykgKyBcIik7XFxuXCIgK1xuICAgICAgICAgIFwifVxcblwiICtcbiAgICAgICAgXCJ9XCI7XG5cbiAgICB2YXIgd29ya2VyU291cmNlQ29kZSA9IHdvcmtlckZ1bmN0aW9uICsgJ1xcbicgKyBoYXJkY29kZTtcbiAgICB2YXIgYmxvYiA9IG5ldyBCbG9iKFt3b3JrZXJTb3VyY2VDb2RlXSk7XG4gICAgdmFyIGJsb2JVUkwgPSB3aW5kb3cuVVJMLmNyZWF0ZU9iamVjdFVSTChibG9iKTtcblxuICAgIHJldHVybiBuZXcgV29ya2VyKGJsb2JVUkwpO1xuICB9LFxuXG4gIC8vIHJldHVybnMgYSBjb3B5IG9mIHRoZSBuZXR3b3JrXG4gIGNsb25lOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gTmV0d29yay5mcm9tSlNPTih0aGlzLnRvSlNPTigpKTtcbiAgfVxufTtcblxuLyoqXG4gKiBDcmVhdGVzIGEgc3RhdGljIFN0cmluZyB0byBzdG9yZSB0aGUgc291cmNlIGNvZGUgb2YgdGhlIGZ1bmN0aW9uc1xuICogIHRoYXQgYXJlIGlkZW50aWNhbCBmb3IgYWxsIHRoZSB3b3JrZXJzICh0cmFpbiwgX3RyYWluU2V0LCB0ZXN0KVxuICpcbiAqIEByZXR1cm4ge1N0cmluZ30gU291cmNlIGNvZGUgdGhhdCBjYW4gdHJhaW4gYSBuZXR3b3JrIGluc2lkZSBhIHdvcmtlci5cbiAqIEBzdGF0aWNcbiAqL1xuTmV0d29yay5nZXRXb3JrZXJTaGFyZWRGdW5jdGlvbnMgPSBmdW5jdGlvbigpIHtcbiAgLy8gSWYgd2UgYWxyZWFkeSBjb21wdXRlZCB0aGUgc291cmNlIGNvZGUgZm9yIHRoZSBzaGFyZWQgZnVuY3Rpb25zXG4gIGlmKHR5cGVvZiBOZXR3b3JrLl9TSEFSRURfV09SS0VSX0ZVTkNUSU9OUyAhPT0gJ3VuZGVmaW5lZCcpXG4gICAgcmV0dXJuIE5ldHdvcmsuX1NIQVJFRF9XT1JLRVJfRlVOQ1RJT05TO1xuXG4gIC8vIE90aGVyd2lzZSBjb21wdXRlIGFuZCByZXR1cm4gdGhlIHNvdXJjZSBjb2RlXG4gIC8vIFdlIGNvbXB1dGUgdGhlbSBieSBzaW1wbHkgY29weWluZyB0aGUgc291cmNlIGNvZGUgb2YgdGhlIHRyYWluLCBfdHJhaW5TZXQgYW5kIHRlc3QgZnVuY3Rpb25zXG4gIC8vICB1c2luZyB0aGUgLnRvU3RyaW5nKCkgbWV0aG9kXG5cbiAgLy8gTG9hZCBhbmQgbmFtZSB0aGUgdHJhaW4gZnVuY3Rpb25cbiAgdmFyIHRyYWluX2YgPSBUcmFpbmVyLnByb3RvdHlwZS50cmFpbi50b1N0cmluZygpO1xuICB0cmFpbl9mID0gdHJhaW5fZi5yZXBsYWNlKCdmdW5jdGlvbiAoc2V0JywgJ2Z1bmN0aW9uIHRyYWluKHNldCcpICsgJ1xcbic7XG5cbiAgLy8gTG9hZCBhbmQgbmFtZSB0aGUgX3RyYWluU2V0IGZ1bmN0aW9uXG4gIHZhciBfdHJhaW5TZXRfZiA9IFRyYWluZXIucHJvdG90eXBlLl90cmFpblNldC50b1N0cmluZygpLnJlcGxhY2UoL3RoaXMubmV0d29yay4vZywgJycpO1xuICBfdHJhaW5TZXRfZiA9IF90cmFpblNldF9mLnJlcGxhY2UoJ2Z1bmN0aW9uIChzZXQnLCAnZnVuY3Rpb24gX3RyYWluU2V0KHNldCcpICsgJ1xcbic7XG4gIF90cmFpblNldF9mID0gX3RyYWluU2V0X2YucmVwbGFjZSgndGhpcy5jcm9zc1ZhbGlkYXRlJywgJ2Nyb3NzVmFsaWRhdGUnKTtcbiAgX3RyYWluU2V0X2YgPSBfdHJhaW5TZXRfZi5yZXBsYWNlKCdjcm9zc1ZhbGlkYXRlID0gdHJ1ZScsICdjcm9zc1ZhbGlkYXRlID0geyB9Jyk7XG5cbiAgLy8gTG9hZCBhbmQgbmFtZSB0aGUgdGVzdCBmdW5jdGlvblxuICB2YXIgdGVzdF9mID0gVHJhaW5lci5wcm90b3R5cGUudGVzdC50b1N0cmluZygpLnJlcGxhY2UoL3RoaXMubmV0d29yay4vZywgJycpO1xuICB0ZXN0X2YgPSB0ZXN0X2YucmVwbGFjZSgnZnVuY3Rpb24gKHNldCcsICdmdW5jdGlvbiB0ZXN0KHNldCcpICsgJ1xcbic7XG5cbiAgcmV0dXJuIE5ldHdvcmsuX1NIQVJFRF9XT1JLRVJfRlVOQ1RJT05TID0gdHJhaW5fZiArIF90cmFpblNldF9mICsgdGVzdF9mO1xufTtcblxuLy8gcmVidWlsZCBhIG5ldHdvcmsgdGhhdCBoYXMgYmVlbiBzdG9yZWQgaW4gYSBqc29uIHVzaW5nIHRoZSBtZXRob2QgdG9KU09OKClcbk5ldHdvcmsuZnJvbUpTT04gPSBmdW5jdGlvbihqc29uKSB7XG5cbiAgdmFyIG5ldXJvbnMgPSBbXTtcblxuICB2YXIgbGF5ZXJzID0ge1xuICAgIGlucHV0OiBuZXcgTGF5ZXIoKSxcbiAgICBoaWRkZW46IFtdLFxuICAgIG91dHB1dDogbmV3IExheWVyKClcbiAgfTtcblxuICBmb3IgKHZhciBpIGluIGpzb24ubmV1cm9ucykge1xuICAgIHZhciBjb25maWcgPSBqc29uLm5ldXJvbnNbaV07XG5cbiAgICB2YXIgbmV1cm9uID0gbmV3IE5ldXJvbigpO1xuICAgIG5ldXJvbi50cmFjZS5lbGVnaWJpbGl0eSA9IHt9O1xuICAgIG5ldXJvbi50cmFjZS5leHRlbmRlZCA9IHt9O1xuICAgIG5ldXJvbi5zdGF0ZSA9IGNvbmZpZy5zdGF0ZTtcbiAgICBuZXVyb24ub2xkID0gY29uZmlnLm9sZDtcbiAgICBuZXVyb24uYWN0aXZhdGlvbiA9IGNvbmZpZy5hY3RpdmF0aW9uO1xuICAgIG5ldXJvbi5iaWFzID0gY29uZmlnLmJpYXM7XG4gICAgbmV1cm9uLnNxdWFzaCA9IGNvbmZpZy5zcXVhc2ggaW4gTmV1cm9uLnNxdWFzaCA/IE5ldXJvbi5zcXVhc2hbY29uZmlnLnNxdWFzaF0gOiBOZXVyb24uc3F1YXNoLkxPR0lTVElDO1xuICAgIG5ldXJvbnMucHVzaChuZXVyb24pO1xuXG4gICAgaWYgKGNvbmZpZy5sYXllciA9PSAnaW5wdXQnKVxuICAgICAgbGF5ZXJzLmlucHV0LmFkZChuZXVyb24pO1xuICAgIGVsc2UgaWYgKGNvbmZpZy5sYXllciA9PSAnb3V0cHV0JylcbiAgICAgIGxheWVycy5vdXRwdXQuYWRkKG5ldXJvbik7XG4gICAgZWxzZSB7XG4gICAgICBpZiAodHlwZW9mIGxheWVycy5oaWRkZW5bY29uZmlnLmxheWVyXSA9PSAndW5kZWZpbmVkJylcbiAgICAgICAgbGF5ZXJzLmhpZGRlbltjb25maWcubGF5ZXJdID0gbmV3IExheWVyKCk7XG4gICAgICBsYXllcnMuaGlkZGVuW2NvbmZpZy5sYXllcl0uYWRkKG5ldXJvbik7XG4gICAgfVxuICB9XG5cbiAgZm9yICh2YXIgaSBpbiBqc29uLmNvbm5lY3Rpb25zKSB7XG4gICAgdmFyIGNvbmZpZyA9IGpzb24uY29ubmVjdGlvbnNbaV07XG4gICAgdmFyIGZyb20gPSBuZXVyb25zW2NvbmZpZy5mcm9tXTtcbiAgICB2YXIgdG8gPSBuZXVyb25zW2NvbmZpZy50b107XG4gICAgdmFyIHdlaWdodCA9IGNvbmZpZy53ZWlnaHQ7XG4gICAgdmFyIGdhdGVyID0gbmV1cm9uc1tjb25maWcuZ2F0ZXJdO1xuXG4gICAgdmFyIGNvbm5lY3Rpb24gPSBmcm9tLnByb2plY3QodG8sIHdlaWdodCk7XG4gICAgaWYgKGdhdGVyKVxuICAgICAgZ2F0ZXIuZ2F0ZShjb25uZWN0aW9uKTtcbiAgfVxuXG4gIHJldHVybiBuZXcgTmV0d29yayhsYXllcnMpO1xufTtcbiIsIi8vIGV4cG9ydFxuaWYgKG1vZHVsZSkgbW9kdWxlLmV4cG9ydHMgPSBOZXVyb247XG5cbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTkVVUk9OXG4qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG5mdW5jdGlvbiBOZXVyb24oKSB7XG4gIHRoaXMuSUQgPSBOZXVyb24udWlkKCk7XG4gIHRoaXMubGFiZWwgPSBudWxsO1xuICB0aGlzLmNvbm5lY3Rpb25zID0ge1xuICAgIGlucHV0czoge30sXG4gICAgcHJvamVjdGVkOiB7fSxcbiAgICBnYXRlZDoge31cbiAgfTtcbiAgdGhpcy5lcnJvciA9IHtcbiAgICByZXNwb25zaWJpbGl0eTogMCxcbiAgICBwcm9qZWN0ZWQ6IDAsXG4gICAgZ2F0ZWQ6IDBcbiAgfTtcbiAgdGhpcy50cmFjZSA9IHtcbiAgICBlbGVnaWJpbGl0eToge30sXG4gICAgZXh0ZW5kZWQ6IHt9LFxuICAgIGluZmx1ZW5jZXM6IHt9XG4gIH07XG4gIHRoaXMuc3RhdGUgPSAwO1xuICB0aGlzLm9sZCA9IDA7XG4gIHRoaXMuYWN0aXZhdGlvbiA9IDA7XG4gIHRoaXMuc2VsZmNvbm5lY3Rpb24gPSBuZXcgTmV1cm9uLmNvbm5lY3Rpb24odGhpcywgdGhpcywgMCk7IC8vIHdlaWdodCA9IDAgLT4gbm90IGNvbm5lY3RlZFxuICB0aGlzLnNxdWFzaCA9IE5ldXJvbi5zcXVhc2guTE9HSVNUSUM7XG4gIHRoaXMubmVpZ2hib29ycyA9IHt9O1xuICB0aGlzLmJpYXMgPSBNYXRoLnJhbmRvbSgpICogLjIgLSAuMTtcbn1cblxuTmV1cm9uLnByb3RvdHlwZSA9IHtcblxuICAvLyBhY3RpdmF0ZSB0aGUgbmV1cm9uXG4gIGFjdGl2YXRlOiBmdW5jdGlvbihpbnB1dCkge1xuICAgIC8vIGFjdGl2YXRpb24gZnJvbSBlbnZpcm9tZW50IChmb3IgaW5wdXQgbmV1cm9ucylcbiAgICBpZiAodHlwZW9mIGlucHV0ICE9ICd1bmRlZmluZWQnKSB7XG4gICAgICB0aGlzLmFjdGl2YXRpb24gPSBpbnB1dDtcbiAgICAgIHRoaXMuZGVyaXZhdGl2ZSA9IDA7XG4gICAgICB0aGlzLmJpYXMgPSAwO1xuICAgICAgcmV0dXJuIHRoaXMuYWN0aXZhdGlvbjtcbiAgICB9XG5cbiAgICAvLyBvbGQgc3RhdGVcbiAgICB0aGlzLm9sZCA9IHRoaXMuc3RhdGU7XG5cbiAgICAvLyBlcS4gMTVcbiAgICB0aGlzLnN0YXRlID0gdGhpcy5zZWxmY29ubmVjdGlvbi5nYWluICogdGhpcy5zZWxmY29ubmVjdGlvbi53ZWlnaHQgKlxuICAgICAgdGhpcy5zdGF0ZSArIHRoaXMuYmlhcztcblxuICAgIGZvciAodmFyIGkgaW4gdGhpcy5jb25uZWN0aW9ucy5pbnB1dHMpIHtcbiAgICAgIHZhciBpbnB1dCA9IHRoaXMuY29ubmVjdGlvbnMuaW5wdXRzW2ldO1xuICAgICAgdGhpcy5zdGF0ZSArPSBpbnB1dC5mcm9tLmFjdGl2YXRpb24gKiBpbnB1dC53ZWlnaHQgKiBpbnB1dC5nYWluO1xuICAgIH1cblxuICAgIC8vIGVxLiAxNlxuICAgIHRoaXMuYWN0aXZhdGlvbiA9IHRoaXMuc3F1YXNoKHRoaXMuc3RhdGUpO1xuXG4gICAgLy8gZicocylcbiAgICB0aGlzLmRlcml2YXRpdmUgPSB0aGlzLnNxdWFzaCh0aGlzLnN0YXRlLCB0cnVlKTtcblxuICAgIC8vIHVwZGF0ZSB0cmFjZXNcbiAgICB2YXIgaW5mbHVlbmNlcyA9IFtdO1xuICAgIGZvciAodmFyIGlkIGluIHRoaXMudHJhY2UuZXh0ZW5kZWQpIHtcbiAgICAgIC8vIGV4dGVuZGVkIGVsZWdpYmlsaXR5IHRyYWNlXG4gICAgICB2YXIgbmV1cm9uID0gdGhpcy5uZWlnaGJvb3JzW2lkXTtcblxuICAgICAgLy8gaWYgZ2F0ZWQgbmV1cm9uJ3Mgc2VsZmNvbm5lY3Rpb24gaXMgZ2F0ZWQgYnkgdGhpcyB1bml0LCB0aGUgaW5mbHVlbmNlIGtlZXBzIHRyYWNrIG9mIHRoZSBuZXVyb24ncyBvbGQgc3RhdGVcbiAgICAgIHZhciBpbmZsdWVuY2UgPSBuZXVyb24uc2VsZmNvbm5lY3Rpb24uZ2F0ZXIgPT0gdGhpcyA/IG5ldXJvbi5vbGQgOiAwO1xuXG4gICAgICAvLyBpbmRleCBydW5zIG92ZXIgYWxsIHRoZSBpbmNvbWluZyBjb25uZWN0aW9ucyB0byB0aGUgZ2F0ZWQgbmV1cm9uIHRoYXQgYXJlIGdhdGVkIGJ5IHRoaXMgdW5pdFxuICAgICAgZm9yICh2YXIgaW5jb21pbmcgaW4gdGhpcy50cmFjZS5pbmZsdWVuY2VzW25ldXJvbi5JRF0pIHsgLy8gY2FwdHVyZXMgdGhlIGVmZmVjdCB0aGF0IGhhcyBhbiBpbnB1dCBjb25uZWN0aW9uIHRvIHRoaXMgdW5pdCwgb24gYSBuZXVyb24gdGhhdCBpcyBnYXRlZCBieSB0aGlzIHVuaXRcbiAgICAgICAgaW5mbHVlbmNlICs9IHRoaXMudHJhY2UuaW5mbHVlbmNlc1tuZXVyb24uSURdW2luY29taW5nXS53ZWlnaHQgKlxuICAgICAgICAgIHRoaXMudHJhY2UuaW5mbHVlbmNlc1tuZXVyb24uSURdW2luY29taW5nXS5mcm9tLmFjdGl2YXRpb247XG4gICAgICB9XG4gICAgICBpbmZsdWVuY2VzW25ldXJvbi5JRF0gPSBpbmZsdWVuY2U7XG4gICAgfVxuXG4gICAgZm9yICh2YXIgaSBpbiB0aGlzLmNvbm5lY3Rpb25zLmlucHV0cykge1xuICAgICAgdmFyIGlucHV0ID0gdGhpcy5jb25uZWN0aW9ucy5pbnB1dHNbaV07XG5cbiAgICAgIC8vIGVsZWdpYmlsaXR5IHRyYWNlIC0gRXEuIDE3XG4gICAgICB0aGlzLnRyYWNlLmVsZWdpYmlsaXR5W2lucHV0LklEXSA9IHRoaXMuc2VsZmNvbm5lY3Rpb24uZ2FpbiAqIHRoaXMuc2VsZmNvbm5lY3Rpb25cbiAgICAgICAgLndlaWdodCAqIHRoaXMudHJhY2UuZWxlZ2liaWxpdHlbaW5wdXQuSURdICsgaW5wdXQuZ2FpbiAqIGlucHV0LmZyb21cbiAgICAgICAgLmFjdGl2YXRpb247XG5cbiAgICAgIGZvciAodmFyIGlkIGluIHRoaXMudHJhY2UuZXh0ZW5kZWQpIHtcbiAgICAgICAgLy8gZXh0ZW5kZWQgZWxlZ2liaWxpdHkgdHJhY2VcbiAgICAgICAgdmFyIHh0cmFjZSA9IHRoaXMudHJhY2UuZXh0ZW5kZWRbaWRdO1xuICAgICAgICB2YXIgbmV1cm9uID0gdGhpcy5uZWlnaGJvb3JzW2lkXTtcbiAgICAgICAgdmFyIGluZmx1ZW5jZSA9IGluZmx1ZW5jZXNbbmV1cm9uLklEXTtcblxuICAgICAgICAvLyBlcS4gMThcbiAgICAgICAgeHRyYWNlW2lucHV0LklEXSA9IG5ldXJvbi5zZWxmY29ubmVjdGlvbi5nYWluICogbmV1cm9uLnNlbGZjb25uZWN0aW9uXG4gICAgICAgICAgLndlaWdodCAqIHh0cmFjZVtpbnB1dC5JRF0gKyB0aGlzLmRlcml2YXRpdmUgKiB0aGlzLnRyYWNlLmVsZWdpYmlsaXR5W1xuICAgICAgICAgICAgaW5wdXQuSURdICogaW5mbHVlbmNlO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vICB1cGRhdGUgZ2F0ZWQgY29ubmVjdGlvbidzIGdhaW5zXG4gICAgZm9yICh2YXIgY29ubmVjdGlvbiBpbiB0aGlzLmNvbm5lY3Rpb25zLmdhdGVkKSB7XG4gICAgICB0aGlzLmNvbm5lY3Rpb25zLmdhdGVkW2Nvbm5lY3Rpb25dLmdhaW4gPSB0aGlzLmFjdGl2YXRpb247XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuYWN0aXZhdGlvbjtcbiAgfSxcblxuICAvLyBiYWNrLXByb3BhZ2F0ZSB0aGUgZXJyb3JcbiAgcHJvcGFnYXRlOiBmdW5jdGlvbihyYXRlLCB0YXJnZXQpIHtcbiAgICAvLyBlcnJvciBhY2N1bXVsYXRvclxuICAgIHZhciBlcnJvciA9IDA7XG5cbiAgICAvLyB3aGV0aGVyIG9yIG5vdCB0aGlzIG5ldXJvbiBpcyBpbiB0aGUgb3V0cHV0IGxheWVyXG4gICAgdmFyIGlzT3V0cHV0ID0gdHlwZW9mIHRhcmdldCAhPSAndW5kZWZpbmVkJztcblxuICAgIC8vIG91dHB1dCBuZXVyb25zIGdldCB0aGVpciBlcnJvciBmcm9tIHRoZSBlbnZpcm9tZW50XG4gICAgaWYgKGlzT3V0cHV0KVxuICAgICAgdGhpcy5lcnJvci5yZXNwb25zaWJpbGl0eSA9IHRoaXMuZXJyb3IucHJvamVjdGVkID0gdGFyZ2V0IC0gdGhpcy5hY3RpdmF0aW9uOyAvLyBFcS4gMTBcblxuICAgIGVsc2UgLy8gdGhlIHJlc3Qgb2YgdGhlIG5ldXJvbiBjb21wdXRlIHRoZWlyIGVycm9yIHJlc3BvbnNpYmlsaXRpZXMgYnkgYmFja3Byb3BhZ2F0aW9uXG4gICAge1xuICAgICAgLy8gZXJyb3IgcmVzcG9uc2liaWxpdGllcyBmcm9tIGFsbCB0aGUgY29ubmVjdGlvbnMgcHJvamVjdGVkIGZyb20gdGhpcyBuZXVyb25cbiAgICAgIGZvciAodmFyIGlkIGluIHRoaXMuY29ubmVjdGlvbnMucHJvamVjdGVkKSB7XG4gICAgICAgIHZhciBjb25uZWN0aW9uID0gdGhpcy5jb25uZWN0aW9ucy5wcm9qZWN0ZWRbaWRdO1xuICAgICAgICB2YXIgbmV1cm9uID0gY29ubmVjdGlvbi50bztcbiAgICAgICAgLy8gRXEuIDIxXG4gICAgICAgIGVycm9yICs9IG5ldXJvbi5lcnJvci5yZXNwb25zaWJpbGl0eSAqIGNvbm5lY3Rpb24uZ2FpbiAqIGNvbm5lY3Rpb24ud2VpZ2h0O1xuICAgICAgfVxuXG4gICAgICAvLyBwcm9qZWN0ZWQgZXJyb3IgcmVzcG9uc2liaWxpdHlcbiAgICAgIHRoaXMuZXJyb3IucHJvamVjdGVkID0gdGhpcy5kZXJpdmF0aXZlICogZXJyb3I7XG5cbiAgICAgIGVycm9yID0gMDtcbiAgICAgIC8vIGVycm9yIHJlc3BvbnNpYmlsaXRpZXMgZnJvbSBhbGwgdGhlIGNvbm5lY3Rpb25zIGdhdGVkIGJ5IHRoaXMgbmV1cm9uXG4gICAgICBmb3IgKHZhciBpZCBpbiB0aGlzLnRyYWNlLmV4dGVuZGVkKSB7XG4gICAgICAgIHZhciBuZXVyb24gPSB0aGlzLm5laWdoYm9vcnNbaWRdOyAvLyBnYXRlZCBuZXVyb25cbiAgICAgICAgdmFyIGluZmx1ZW5jZSA9IG5ldXJvbi5zZWxmY29ubmVjdGlvbi5nYXRlciA9PSB0aGlzID8gbmV1cm9uLm9sZCA6IDA7IC8vIGlmIGdhdGVkIG5ldXJvbidzIHNlbGZjb25uZWN0aW9uIGlzIGdhdGVkIGJ5IHRoaXMgbmV1cm9uXG5cbiAgICAgICAgLy8gaW5kZXggcnVucyBvdmVyIGFsbCB0aGUgY29ubmVjdGlvbnMgdG8gdGhlIGdhdGVkIG5ldXJvbiB0aGF0IGFyZSBnYXRlZCBieSB0aGlzIG5ldXJvblxuICAgICAgICBmb3IgKHZhciBpbnB1dCBpbiB0aGlzLnRyYWNlLmluZmx1ZW5jZXNbaWRdKSB7IC8vIGNhcHR1cmVzIHRoZSBlZmZlY3QgdGhhdCB0aGUgaW5wdXQgY29ubmVjdGlvbiBvZiB0aGlzIG5ldXJvbiBoYXZlLCBvbiBhIG5ldXJvbiB3aGljaCBpdHMgaW5wdXQvcyBpcy9hcmUgZ2F0ZWQgYnkgdGhpcyBuZXVyb25cbiAgICAgICAgICBpbmZsdWVuY2UgKz0gdGhpcy50cmFjZS5pbmZsdWVuY2VzW2lkXVtpbnB1dF0ud2VpZ2h0ICogdGhpcy50cmFjZS5pbmZsdWVuY2VzW1xuICAgICAgICAgICAgbmV1cm9uLklEXVtpbnB1dF0uZnJvbS5hY3RpdmF0aW9uO1xuICAgICAgICB9XG4gICAgICAgIC8vIGVxLiAyMlxuICAgICAgICBlcnJvciArPSBuZXVyb24uZXJyb3IucmVzcG9uc2liaWxpdHkgKiBpbmZsdWVuY2U7XG4gICAgICB9XG5cbiAgICAgIC8vIGdhdGVkIGVycm9yIHJlc3BvbnNpYmlsaXR5XG4gICAgICB0aGlzLmVycm9yLmdhdGVkID0gdGhpcy5kZXJpdmF0aXZlICogZXJyb3I7XG5cbiAgICAgIC8vIGVycm9yIHJlc3BvbnNpYmlsaXR5IC0gRXEuIDIzXG4gICAgICB0aGlzLmVycm9yLnJlc3BvbnNpYmlsaXR5ID0gdGhpcy5lcnJvci5wcm9qZWN0ZWQgKyB0aGlzLmVycm9yLmdhdGVkO1xuICAgIH1cblxuICAgIC8vIGxlYXJuaW5nIHJhdGVcbiAgICByYXRlID0gcmF0ZSB8fCAuMTtcblxuICAgIC8vIGFkanVzdCBhbGwgdGhlIG5ldXJvbidzIGluY29taW5nIGNvbm5lY3Rpb25zXG4gICAgZm9yICh2YXIgaWQgaW4gdGhpcy5jb25uZWN0aW9ucy5pbnB1dHMpIHtcbiAgICAgIHZhciBpbnB1dCA9IHRoaXMuY29ubmVjdGlvbnMuaW5wdXRzW2lkXTtcblxuICAgICAgLy8gRXEuIDI0XG4gICAgICB2YXIgZ3JhZGllbnQgPSB0aGlzLmVycm9yLnByb2plY3RlZCAqIHRoaXMudHJhY2UuZWxlZ2liaWxpdHlbaW5wdXQuSURdO1xuICAgICAgZm9yICh2YXIgaWQgaW4gdGhpcy50cmFjZS5leHRlbmRlZCkge1xuICAgICAgICB2YXIgbmV1cm9uID0gdGhpcy5uZWlnaGJvb3JzW2lkXTtcbiAgICAgICAgZ3JhZGllbnQgKz0gbmV1cm9uLmVycm9yLnJlc3BvbnNpYmlsaXR5ICogdGhpcy50cmFjZS5leHRlbmRlZFtcbiAgICAgICAgICBuZXVyb24uSURdW2lucHV0LklEXTtcbiAgICAgIH1cbiAgICAgIGlucHV0LndlaWdodCArPSByYXRlICogZ3JhZGllbnQ7IC8vIGFkanVzdCB3ZWlnaHRzIC0gYWthIGxlYXJuXG4gICAgfVxuXG4gICAgLy8gYWRqdXN0IGJpYXNcbiAgICB0aGlzLmJpYXMgKz0gcmF0ZSAqIHRoaXMuZXJyb3IucmVzcG9uc2liaWxpdHk7XG4gIH0sXG5cbiAgcHJvamVjdDogZnVuY3Rpb24obmV1cm9uLCB3ZWlnaHQpIHtcbiAgICAvLyBzZWxmLWNvbm5lY3Rpb25cbiAgICBpZiAobmV1cm9uID09IHRoaXMpIHtcbiAgICAgIHRoaXMuc2VsZmNvbm5lY3Rpb24ud2VpZ2h0ID0gMTtcbiAgICAgIHJldHVybiB0aGlzLnNlbGZjb25uZWN0aW9uO1xuICAgIH1cblxuICAgIC8vIGNoZWNrIGlmIGNvbm5lY3Rpb24gYWxyZWFkeSBleGlzdHNcbiAgICB2YXIgY29ubmVjdGVkID0gdGhpcy5jb25uZWN0ZWQobmV1cm9uKTtcbiAgICBpZiAoY29ubmVjdGVkICYmIGNvbm5lY3RlZC50eXBlID09IFwicHJvamVjdGVkXCIpIHtcbiAgICAgIC8vIHVwZGF0ZSBjb25uZWN0aW9uXG4gICAgICBpZiAodHlwZW9mIHdlaWdodCAhPSAndW5kZWZpbmVkJylcbiAgICAgICAgY29ubmVjdGVkLmNvbm5lY3Rpb24ud2VpZ2h0ID0gd2VpZ2h0O1xuICAgICAgLy8gcmV0dXJuIGV4aXN0aW5nIGNvbm5lY3Rpb25cbiAgICAgIHJldHVybiBjb25uZWN0ZWQuY29ubmVjdGlvbjtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gY3JlYXRlIGEgbmV3IGNvbm5lY3Rpb25cbiAgICAgIHZhciBjb25uZWN0aW9uID0gbmV3IE5ldXJvbi5jb25uZWN0aW9uKHRoaXMsIG5ldXJvbiwgd2VpZ2h0KTtcbiAgICB9XG5cbiAgICAvLyByZWZlcmVuY2UgYWxsIHRoZSBjb25uZWN0aW9ucyBhbmQgdHJhY2VzXG4gICAgdGhpcy5jb25uZWN0aW9ucy5wcm9qZWN0ZWRbY29ubmVjdGlvbi5JRF0gPSBjb25uZWN0aW9uO1xuICAgIHRoaXMubmVpZ2hib29yc1tuZXVyb24uSURdID0gbmV1cm9uO1xuICAgIG5ldXJvbi5jb25uZWN0aW9ucy5pbnB1dHNbY29ubmVjdGlvbi5JRF0gPSBjb25uZWN0aW9uO1xuICAgIG5ldXJvbi50cmFjZS5lbGVnaWJpbGl0eVtjb25uZWN0aW9uLklEXSA9IDA7XG5cbiAgICBmb3IgKHZhciBpZCBpbiBuZXVyb24udHJhY2UuZXh0ZW5kZWQpIHtcbiAgICAgIHZhciB0cmFjZSA9IG5ldXJvbi50cmFjZS5leHRlbmRlZFtpZF07XG4gICAgICB0cmFjZVtjb25uZWN0aW9uLklEXSA9IDA7XG4gICAgfVxuXG4gICAgcmV0dXJuIGNvbm5lY3Rpb247XG4gIH0sXG5cbiAgZ2F0ZTogZnVuY3Rpb24oY29ubmVjdGlvbikge1xuICAgIC8vIGFkZCBjb25uZWN0aW9uIHRvIGdhdGVkIGxpc3RcbiAgICB0aGlzLmNvbm5lY3Rpb25zLmdhdGVkW2Nvbm5lY3Rpb24uSURdID0gY29ubmVjdGlvbjtcblxuICAgIHZhciBuZXVyb24gPSBjb25uZWN0aW9uLnRvO1xuICAgIGlmICghKG5ldXJvbi5JRCBpbiB0aGlzLnRyYWNlLmV4dGVuZGVkKSkge1xuICAgICAgLy8gZXh0ZW5kZWQgdHJhY2VcbiAgICAgIHRoaXMubmVpZ2hib29yc1tuZXVyb24uSURdID0gbmV1cm9uO1xuICAgICAgdmFyIHh0cmFjZSA9IHRoaXMudHJhY2UuZXh0ZW5kZWRbbmV1cm9uLklEXSA9IHt9O1xuICAgICAgZm9yICh2YXIgaWQgaW4gdGhpcy5jb25uZWN0aW9ucy5pbnB1dHMpIHtcbiAgICAgICAgdmFyIGlucHV0ID0gdGhpcy5jb25uZWN0aW9ucy5pbnB1dHNbaWRdO1xuICAgICAgICB4dHJhY2VbaW5wdXQuSURdID0gMDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBrZWVwIHRyYWNrXG4gICAgaWYgKG5ldXJvbi5JRCBpbiB0aGlzLnRyYWNlLmluZmx1ZW5jZXMpXG4gICAgICB0aGlzLnRyYWNlLmluZmx1ZW5jZXNbbmV1cm9uLklEXS5wdXNoKGNvbm5lY3Rpb24pO1xuICAgIGVsc2VcbiAgICAgIHRoaXMudHJhY2UuaW5mbHVlbmNlc1tuZXVyb24uSURdID0gW2Nvbm5lY3Rpb25dO1xuXG4gICAgLy8gc2V0IGdhdGVyXG4gICAgY29ubmVjdGlvbi5nYXRlciA9IHRoaXM7XG4gIH0sXG5cbiAgLy8gcmV0dXJucyB0cnVlIG9yIGZhbHNlIHdoZXRoZXIgdGhlIG5ldXJvbiBpcyBzZWxmLWNvbm5lY3RlZCBvciBub3RcbiAgc2VsZmNvbm5lY3RlZDogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHRoaXMuc2VsZmNvbm5lY3Rpb24ud2VpZ2h0ICE9PSAwO1xuICB9LFxuXG4gIC8vIHJldHVybnMgdHJ1ZSBvciBmYWxzZSB3aGV0aGVyIHRoZSBuZXVyb24gaXMgY29ubmVjdGVkIHRvIGFub3RoZXIgbmV1cm9uIChwYXJhbWV0ZXIpXG4gIGNvbm5lY3RlZDogZnVuY3Rpb24obmV1cm9uKSB7XG4gICAgdmFyIHJlc3VsdCA9IHtcbiAgICAgIHR5cGU6IG51bGwsXG4gICAgICBjb25uZWN0aW9uOiBmYWxzZVxuICAgIH07XG5cbiAgICBpZiAodGhpcyA9PSBuZXVyb24pIHtcbiAgICAgIGlmICh0aGlzLnNlbGZjb25uZWN0ZWQoKSkge1xuICAgICAgICByZXN1bHQudHlwZSA9ICdzZWxmY29ubmVjdGlvbic7XG4gICAgICAgIHJlc3VsdC5jb25uZWN0aW9uID0gdGhpcy5zZWxmY29ubmVjdGlvbjtcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgIH0gZWxzZVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgZm9yICh2YXIgdHlwZSBpbiB0aGlzLmNvbm5lY3Rpb25zKSB7XG4gICAgICBmb3IgKHZhciBjb25uZWN0aW9uIGluIHRoaXMuY29ubmVjdGlvbnNbdHlwZV0pIHtcbiAgICAgICAgdmFyIGNvbm5lY3Rpb24gPSB0aGlzLmNvbm5lY3Rpb25zW3R5cGVdW2Nvbm5lY3Rpb25dO1xuICAgICAgICBpZiAoY29ubmVjdGlvbi50byA9PSBuZXVyb24pIHtcbiAgICAgICAgICByZXN1bHQudHlwZSA9IHR5cGU7XG4gICAgICAgICAgcmVzdWx0LmNvbm5lY3Rpb24gPSBjb25uZWN0aW9uO1xuICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgIH0gZWxzZSBpZiAoY29ubmVjdGlvbi5mcm9tID09IG5ldXJvbikge1xuICAgICAgICAgIHJlc3VsdC50eXBlID0gdHlwZTtcbiAgICAgICAgICByZXN1bHQuY29ubmVjdGlvbiA9IGNvbm5lY3Rpb247XG4gICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBmYWxzZTtcbiAgfSxcblxuICAvLyBjbGVhcnMgYWxsIHRoZSB0cmFjZXMgKHRoZSBuZXVyb24gZm9yZ2V0cyBpdCdzIGNvbnRleHQsIGJ1dCB0aGUgY29ubmVjdGlvbnMgcmVtYWluIGludGFjdClcbiAgY2xlYXI6IGZ1bmN0aW9uKCkge1xuXG4gICAgZm9yICh2YXIgdHJhY2UgaW4gdGhpcy50cmFjZS5lbGVnaWJpbGl0eSlcbiAgICAgIHRoaXMudHJhY2UuZWxlZ2liaWxpdHlbdHJhY2VdID0gMDtcblxuICAgIGZvciAodmFyIHRyYWNlIGluIHRoaXMudHJhY2UuZXh0ZW5kZWQpXG4gICAgICBmb3IgKHZhciBleHRlbmRlZCBpbiB0aGlzLnRyYWNlLmV4dGVuZGVkW3RyYWNlXSlcbiAgICAgICAgdGhpcy50cmFjZS5leHRlbmRlZFt0cmFjZV1bZXh0ZW5kZWRdID0gMDtcblxuICAgIHRoaXMuZXJyb3IucmVzcG9uc2liaWxpdHkgPSB0aGlzLmVycm9yLnByb2plY3RlZCA9IHRoaXMuZXJyb3IuZ2F0ZWQgPSAwO1xuICB9LFxuXG4gIC8vIGFsbCB0aGUgY29ubmVjdGlvbnMgYXJlIHJhbmRvbWl6ZWQgYW5kIHRoZSB0cmFjZXMgYXJlIGNsZWFyZWRcbiAgcmVzZXQ6IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMuY2xlYXIoKTtcblxuICAgIGZvciAodmFyIHR5cGUgaW4gdGhpcy5jb25uZWN0aW9ucylcbiAgICAgIGZvciAodmFyIGNvbm5lY3Rpb24gaW4gdGhpcy5jb25uZWN0aW9uc1t0eXBlXSlcbiAgICAgICAgdGhpcy5jb25uZWN0aW9uc1t0eXBlXVtjb25uZWN0aW9uXS53ZWlnaHQgPSBNYXRoLnJhbmRvbSgpICogLjIgLSAuMTtcbiAgICB0aGlzLmJpYXMgPSBNYXRoLnJhbmRvbSgpICogLjIgLSAuMTtcblxuICAgIHRoaXMub2xkID0gdGhpcy5zdGF0ZSA9IHRoaXMuYWN0aXZhdGlvbiA9IDA7XG4gIH0sXG5cbiAgLy8gaGFyZGNvZGVzIHRoZSBiZWhhdmlvdXIgb2YgdGhlIG5ldXJvbiBpbnRvIGFuIG9wdGltaXplZCBmdW5jdGlvblxuICBvcHRpbWl6ZTogZnVuY3Rpb24ob3B0aW1pemVkLCBsYXllcikge1xuXG4gICAgb3B0aW1pemVkID0gb3B0aW1pemVkIHx8IHt9O1xuICAgIHZhciBzdG9yZV9hY3RpdmF0aW9uID0gW107XG4gICAgdmFyIHN0b3JlX3RyYWNlID0gW107XG4gICAgdmFyIHN0b3JlX3Byb3BhZ2F0aW9uID0gW107XG4gICAgdmFyIHZhcklEID0gb3B0aW1pemVkLm1lbW9yeSB8fCAwO1xuICAgIHZhciBuZXVyb25zID0gb3B0aW1pemVkLm5ldXJvbnMgfHwgMTtcbiAgICB2YXIgaW5wdXRzID0gb3B0aW1pemVkLmlucHV0cyB8fCBbXTtcbiAgICB2YXIgdGFyZ2V0cyA9IG9wdGltaXplZC50YXJnZXRzIHx8IFtdO1xuICAgIHZhciBvdXRwdXRzID0gb3B0aW1pemVkLm91dHB1dHMgfHwgW107XG4gICAgdmFyIHZhcmlhYmxlcyA9IG9wdGltaXplZC52YXJpYWJsZXMgfHwge307XG4gICAgdmFyIGFjdGl2YXRpb25fc2VudGVuY2VzID0gb3B0aW1pemVkLmFjdGl2YXRpb25fc2VudGVuY2VzIHx8IFtdO1xuICAgIHZhciB0cmFjZV9zZW50ZW5jZXMgPSBvcHRpbWl6ZWQudHJhY2Vfc2VudGVuY2VzIHx8IFtdO1xuICAgIHZhciBwcm9wYWdhdGlvbl9zZW50ZW5jZXMgPSBvcHRpbWl6ZWQucHJvcGFnYXRpb25fc2VudGVuY2VzIHx8IFtdO1xuICAgIHZhciBsYXllcnMgPSBvcHRpbWl6ZWQubGF5ZXJzIHx8IHsgX19jb3VudDogMCwgX19uZXVyb246IDAgfTtcblxuICAgIC8vIGFsbG9jYXRlIHNlbnRlbmNlc1xuICAgIHZhciBhbGxvY2F0ZSA9IGZ1bmN0aW9uKHN0b3JlKXtcbiAgICAgIHZhciBhbGxvY2F0ZWQgPSBsYXllciBpbiBsYXllcnMgJiYgc3RvcmVbbGF5ZXJzLl9fY291bnRdO1xuICAgICAgaWYgKCFhbGxvY2F0ZWQpXG4gICAgICB7XG4gICAgICAgIGxheWVycy5fX2NvdW50ID0gc3RvcmUucHVzaChbXSkgLSAxO1xuICAgICAgICBsYXllcnNbbGF5ZXJdID0gbGF5ZXJzLl9fY291bnQ7XG4gICAgICB9XG4gICAgfTtcbiAgICBhbGxvY2F0ZShhY3RpdmF0aW9uX3NlbnRlbmNlcyk7XG4gICAgYWxsb2NhdGUodHJhY2Vfc2VudGVuY2VzKTtcbiAgICBhbGxvY2F0ZShwcm9wYWdhdGlvbl9zZW50ZW5jZXMpO1xuICAgIHZhciBjdXJyZW50TGF5ZXIgPSBsYXllcnMuX19jb3VudDtcblxuICAgIC8vIGdldC9yZXNlcnZlIHNwYWNlIGluIG1lbW9yeSBieSBjcmVhdGluZyBhIHVuaXF1ZSBJRCBmb3IgYSB2YXJpYWJsZWxcbiAgICB2YXIgZ2V0VmFyID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgYXJncyA9IEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7XG5cbiAgICAgIGlmIChhcmdzLmxlbmd0aCA9PSAxKSB7XG4gICAgICAgIGlmIChhcmdzWzBdID09ICd0YXJnZXQnKSB7XG4gICAgICAgICAgdmFyIGlkID0gJ3RhcmdldF8nICsgdGFyZ2V0cy5sZW5ndGg7XG4gICAgICAgICAgdGFyZ2V0cy5wdXNoKHZhcklEKTtcbiAgICAgICAgfSBlbHNlXG4gICAgICAgICAgdmFyIGlkID0gYXJnc1swXTtcbiAgICAgICAgaWYgKGlkIGluIHZhcmlhYmxlcylcbiAgICAgICAgICByZXR1cm4gdmFyaWFibGVzW2lkXTtcbiAgICAgICAgcmV0dXJuIHZhcmlhYmxlc1tpZF0gPSB7XG4gICAgICAgICAgdmFsdWU6IDAsXG4gICAgICAgICAgaWQ6IHZhcklEKytcbiAgICAgICAgfTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHZhciBleHRlbmRlZCA9IGFyZ3MubGVuZ3RoID4gMjtcbiAgICAgICAgaWYgKGV4dGVuZGVkKVxuICAgICAgICAgIHZhciB2YWx1ZSA9IGFyZ3MucG9wKCk7XG5cbiAgICAgICAgdmFyIHVuaXQgPSBhcmdzLnNoaWZ0KCk7XG4gICAgICAgIHZhciBwcm9wID0gYXJncy5wb3AoKTtcblxuICAgICAgICBpZiAoIWV4dGVuZGVkKVxuICAgICAgICAgIHZhciB2YWx1ZSA9IHVuaXRbcHJvcF07XG5cbiAgICAgICAgdmFyIGlkID0gcHJvcCArICdfJztcbiAgICAgICAgZm9yICh2YXIgcHJvcGVydHkgaW4gYXJncylcbiAgICAgICAgICBpZCArPSBhcmdzW3Byb3BlcnR5XSArICdfJztcbiAgICAgICAgaWQgKz0gdW5pdC5JRDtcbiAgICAgICAgaWYgKGlkIGluIHZhcmlhYmxlcylcbiAgICAgICAgICByZXR1cm4gdmFyaWFibGVzW2lkXTtcblxuICAgICAgICByZXR1cm4gdmFyaWFibGVzW2lkXSA9IHtcbiAgICAgICAgICB2YWx1ZTogdmFsdWUsXG4gICAgICAgICAgaWQ6IHZhcklEKytcbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLy8gYnVpbGQgc2VudGVuY2VcbiAgICB2YXIgYnVpbGRTZW50ZW5jZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGFyZ3MgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuICAgICAgdmFyIHN0b3JlID0gYXJncy5wb3AoKTtcbiAgICAgIHZhciBzZW50ZW5jZSA9IFwiXCI7XG4gICAgICBmb3IgKHZhciBpIGluIGFyZ3MpXG4gICAgICAgIGlmICh0eXBlb2YgYXJnc1tpXSA9PSAnc3RyaW5nJylcbiAgICAgICAgICBzZW50ZW5jZSArPSBhcmdzW2ldO1xuICAgICAgICBlbHNlXG4gICAgICAgICAgc2VudGVuY2UgKz0gJ0ZbJyArIGFyZ3NbaV0uaWQgKyAnXSc7XG5cbiAgICAgIHN0b3JlLnB1c2goc2VudGVuY2UgKyAnOycpO1xuICAgIH07XG5cbiAgICAvLyBoZWxwZXIgdG8gY2hlY2sgaWYgYW4gb2JqZWN0IGlzIGVtcHR5XG4gICAgdmFyIGlzRW1wdHkgPSBmdW5jdGlvbihvYmopIHtcbiAgICAgIGZvciAodmFyIHByb3AgaW4gb2JqKSB7XG4gICAgICAgIGlmIChvYmouaGFzT3duUHJvcGVydHkocHJvcCkpXG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTtcblxuICAgIC8vIGNoYXJhY3RlcmlzdGljcyBvZiB0aGUgbmV1cm9uXG4gICAgdmFyIG5vUHJvamVjdGlvbnMgPSBpc0VtcHR5KHRoaXMuY29ubmVjdGlvbnMucHJvamVjdGVkKTtcbiAgICB2YXIgbm9HYXRlcyA9IGlzRW1wdHkodGhpcy5jb25uZWN0aW9ucy5nYXRlZCk7XG4gICAgdmFyIGlzSW5wdXQgPSBsYXllciA9PSAnaW5wdXQnID8gdHJ1ZSA6IGlzRW1wdHkodGhpcy5jb25uZWN0aW9ucy5pbnB1dHMpO1xuICAgIHZhciBpc091dHB1dCA9IGxheWVyID09ICdvdXRwdXQnID8gdHJ1ZSA6IG5vUHJvamVjdGlvbnMgJiYgbm9HYXRlcztcblxuICAgIC8vIG9wdGltaXplIG5ldXJvbidzIGJlaGF2aW91clxuICAgIHZhciByYXRlID0gZ2V0VmFyKCdyYXRlJyk7XG4gICAgdmFyIGFjdGl2YXRpb24gPSBnZXRWYXIodGhpcywgJ2FjdGl2YXRpb24nKTtcbiAgICBpZiAoaXNJbnB1dClcbiAgICAgIGlucHV0cy5wdXNoKGFjdGl2YXRpb24uaWQpO1xuICAgIGVsc2Uge1xuICAgICAgYWN0aXZhdGlvbl9zZW50ZW5jZXNbY3VycmVudExheWVyXS5wdXNoKHN0b3JlX2FjdGl2YXRpb24pO1xuICAgICAgdHJhY2Vfc2VudGVuY2VzW2N1cnJlbnRMYXllcl0ucHVzaChzdG9yZV90cmFjZSk7XG4gICAgICBwcm9wYWdhdGlvbl9zZW50ZW5jZXNbY3VycmVudExheWVyXS5wdXNoKHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgIHZhciBvbGQgPSBnZXRWYXIodGhpcywgJ29sZCcpO1xuICAgICAgdmFyIHN0YXRlID0gZ2V0VmFyKHRoaXMsICdzdGF0ZScpO1xuICAgICAgdmFyIGJpYXMgPSBnZXRWYXIodGhpcywgJ2JpYXMnKTtcbiAgICAgIGlmICh0aGlzLnNlbGZjb25uZWN0aW9uLmdhdGVyKVxuICAgICAgICB2YXIgc2VsZl9nYWluID0gZ2V0VmFyKHRoaXMuc2VsZmNvbm5lY3Rpb24sICdnYWluJyk7XG4gICAgICBpZiAodGhpcy5zZWxmY29ubmVjdGVkKCkpXG4gICAgICAgIHZhciBzZWxmX3dlaWdodCA9IGdldFZhcih0aGlzLnNlbGZjb25uZWN0aW9uLCAnd2VpZ2h0Jyk7XG4gICAgICBidWlsZFNlbnRlbmNlKG9sZCwgJyA9ICcsIHN0YXRlLCBzdG9yZV9hY3RpdmF0aW9uKTtcbiAgICAgIGlmICh0aGlzLnNlbGZjb25uZWN0ZWQoKSlcbiAgICAgICAgaWYgKHRoaXMuc2VsZmNvbm5lY3Rpb24uZ2F0ZXIpXG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShzdGF0ZSwgJyA9ICcsIHNlbGZfZ2FpbiwgJyAqICcsIHNlbGZfd2VpZ2h0LCAnICogJyxcbiAgICAgICAgICAgIHN0YXRlLCAnICsgJywgYmlhcywgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICAgIGVsc2VcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKHN0YXRlLCAnID0gJywgc2VsZl93ZWlnaHQsICcgKiAnLCBzdGF0ZSwgJyArICcsXG4gICAgICAgICAgICBiaWFzLCBzdG9yZV9hY3RpdmF0aW9uKTtcbiAgICAgIGVsc2VcbiAgICAgICAgYnVpbGRTZW50ZW5jZShzdGF0ZSwgJyA9ICcsIGJpYXMsIHN0b3JlX2FjdGl2YXRpb24pO1xuICAgICAgZm9yICh2YXIgaSBpbiB0aGlzLmNvbm5lY3Rpb25zLmlucHV0cykge1xuICAgICAgICB2YXIgaW5wdXQgPSB0aGlzLmNvbm5lY3Rpb25zLmlucHV0c1tpXTtcbiAgICAgICAgdmFyIGlucHV0X2FjdGl2YXRpb24gPSBnZXRWYXIoaW5wdXQuZnJvbSwgJ2FjdGl2YXRpb24nKTtcbiAgICAgICAgdmFyIGlucHV0X3dlaWdodCA9IGdldFZhcihpbnB1dCwgJ3dlaWdodCcpO1xuICAgICAgICBpZiAoaW5wdXQuZ2F0ZXIpXG4gICAgICAgICAgdmFyIGlucHV0X2dhaW4gPSBnZXRWYXIoaW5wdXQsICdnYWluJyk7XG4gICAgICAgIGlmICh0aGlzLmNvbm5lY3Rpb25zLmlucHV0c1tpXS5nYXRlcilcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKHN0YXRlLCAnICs9ICcsIGlucHV0X2FjdGl2YXRpb24sICcgKiAnLFxuICAgICAgICAgICAgaW5wdXRfd2VpZ2h0LCAnICogJywgaW5wdXRfZ2Fpbiwgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICAgIGVsc2VcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKHN0YXRlLCAnICs9ICcsIGlucHV0X2FjdGl2YXRpb24sICcgKiAnLFxuICAgICAgICAgICAgaW5wdXRfd2VpZ2h0LCBzdG9yZV9hY3RpdmF0aW9uKTtcbiAgICAgIH1cbiAgICAgIHZhciBkZXJpdmF0aXZlID0gZ2V0VmFyKHRoaXMsICdkZXJpdmF0aXZlJyk7XG4gICAgICBzd2l0Y2ggKHRoaXMuc3F1YXNoKSB7XG4gICAgICAgIGNhc2UgTmV1cm9uLnNxdWFzaC5MT0dJU1RJQzpcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKGFjdGl2YXRpb24sICcgPSAoMSAvICgxICsgTWF0aC5leHAoLScsIHN0YXRlLCAnKSkpJyxcbiAgICAgICAgICAgIHN0b3JlX2FjdGl2YXRpb24pO1xuICAgICAgICAgIGJ1aWxkU2VudGVuY2UoZGVyaXZhdGl2ZSwgJyA9ICcsIGFjdGl2YXRpb24sICcgKiAoMSAtICcsXG4gICAgICAgICAgICBhY3RpdmF0aW9uLCAnKScsIHN0b3JlX2FjdGl2YXRpb24pO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIE5ldXJvbi5zcXVhc2guVEFOSDpcbiAgICAgICAgICB2YXIgZVAgPSBnZXRWYXIoJ2F1eCcpO1xuICAgICAgICAgIHZhciBlTiA9IGdldFZhcignYXV4XzInKTtcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKGVQLCAnID0gTWF0aC5leHAoJywgc3RhdGUsICcpJywgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShlTiwgJyA9IDEgLyAnLCBlUCwgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShhY3RpdmF0aW9uLCAnID0gKCcsIGVQLCAnIC0gJywgZU4sICcpIC8gKCcsIGVQLCAnICsgJywgZU4sICcpJywgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShkZXJpdmF0aXZlLCAnID0gMSAtICgnLCBhY3RpdmF0aW9uLCAnICogJywgYWN0aXZhdGlvbiwgJyknLCBzdG9yZV9hY3RpdmF0aW9uKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSBOZXVyb24uc3F1YXNoLklERU5USVRZOlxuICAgICAgICAgIGJ1aWxkU2VudGVuY2UoYWN0aXZhdGlvbiwgJyA9ICcsIHN0YXRlLCBzdG9yZV9hY3RpdmF0aW9uKTtcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKGRlcml2YXRpdmUsICcgPSAxJywgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgTmV1cm9uLnNxdWFzaC5ITElNOlxuICAgICAgICAgIGJ1aWxkU2VudGVuY2UoYWN0aXZhdGlvbiwgJyA9ICsoJywgc3RhdGUsICcgPiAwKScsIHN0b3JlX2FjdGl2YXRpb24pO1xuICAgICAgICAgIGJ1aWxkU2VudGVuY2UoZGVyaXZhdGl2ZSwgJyA9IDEnLCBzdG9yZV9hY3RpdmF0aW9uKTtcbiAgICAgICAgY2FzZSBOZXVyb24uc3F1YXNoLlJFTFU6XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShhY3RpdmF0aW9uLCAnID0gJywgc3RhdGUsICcgPiAwID8gJywgc3RhdGUsICcgOiAwJywgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShkZXJpdmF0aXZlLCAnID0gJywgc3RhdGUsICcgPiAwID8gMSA6IDAnLCBzdG9yZV9hY3RpdmF0aW9uKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgIH1cblxuICAgICAgZm9yICh2YXIgaWQgaW4gdGhpcy50cmFjZS5leHRlbmRlZCkge1xuICAgICAgICAvLyBjYWxjdWxhdGUgZXh0ZW5kZWQgZWxlZ2liaWxpdHkgdHJhY2VzIGluIGFkdmFuY2VcblxuICAgICAgICB2YXIgbmV1cm9uID0gdGhpcy5uZWlnaGJvb3JzW2lkXTtcbiAgICAgICAgdmFyIGluZmx1ZW5jZSA9IGdldFZhcignaW5mbHVlbmNlc1snICsgbmV1cm9uLklEICsgJ10nKTtcbiAgICAgICAgdmFyIG5ldXJvbl9vbGQgPSBnZXRWYXIobmV1cm9uLCAnb2xkJyk7XG4gICAgICAgIHZhciBpbml0aWFsaXplZCA9IGZhbHNlO1xuICAgICAgICBpZiAobmV1cm9uLnNlbGZjb25uZWN0aW9uLmdhdGVyID09IHRoaXMpXG4gICAgICAgIHtcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKGluZmx1ZW5jZSwgJyA9ICcsIG5ldXJvbl9vbGQsIHN0b3JlX3RyYWNlKTtcbiAgICAgICAgICBpbml0aWFsaXplZCA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgZm9yICh2YXIgaW5jb21pbmcgaW4gdGhpcy50cmFjZS5pbmZsdWVuY2VzW25ldXJvbi5JRF0pIHtcbiAgICAgICAgICB2YXIgaW5jb21pbmdfd2VpZ2h0ID0gZ2V0VmFyKHRoaXMudHJhY2UuaW5mbHVlbmNlc1tuZXVyb24uSURdXG4gICAgICAgICAgICBbaW5jb21pbmddLCAnd2VpZ2h0Jyk7XG4gICAgICAgICAgdmFyIGluY29taW5nX2FjdGl2YXRpb24gPSBnZXRWYXIodGhpcy50cmFjZS5pbmZsdWVuY2VzW25ldXJvbi5JRF1cbiAgICAgICAgICAgIFtpbmNvbWluZ10uZnJvbSwgJ2FjdGl2YXRpb24nKTtcblxuICAgICAgICAgIGlmIChpbml0aWFsaXplZClcbiAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoaW5mbHVlbmNlLCAnICs9ICcsIGluY29taW5nX3dlaWdodCwgJyAqICcsIGluY29taW5nX2FjdGl2YXRpb24sIHN0b3JlX3RyYWNlKTtcbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoaW5mbHVlbmNlLCAnID0gJywgaW5jb21pbmdfd2VpZ2h0LCAnICogJywgaW5jb21pbmdfYWN0aXZhdGlvbiwgc3RvcmVfdHJhY2UpO1xuICAgICAgICAgICAgaW5pdGlhbGl6ZWQgPSB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBmb3IgKHZhciBpIGluIHRoaXMuY29ubmVjdGlvbnMuaW5wdXRzKSB7XG4gICAgICAgIHZhciBpbnB1dCA9IHRoaXMuY29ubmVjdGlvbnMuaW5wdXRzW2ldO1xuICAgICAgICBpZiAoaW5wdXQuZ2F0ZXIpXG4gICAgICAgICAgdmFyIGlucHV0X2dhaW4gPSBnZXRWYXIoaW5wdXQsICdnYWluJyk7XG4gICAgICAgIHZhciBpbnB1dF9hY3RpdmF0aW9uID0gZ2V0VmFyKGlucHV0LmZyb20sICdhY3RpdmF0aW9uJyk7XG4gICAgICAgIHZhciB0cmFjZSA9IGdldFZhcih0aGlzLCAndHJhY2UnLCAnZWxlZ2liaWxpdHknLCBpbnB1dC5JRCwgdGhpcy50cmFjZVxuICAgICAgICAgIC5lbGVnaWJpbGl0eVtpbnB1dC5JRF0pO1xuICAgICAgICBpZiAodGhpcy5zZWxmY29ubmVjdGVkKCkpIHtcbiAgICAgICAgICBpZiAodGhpcy5zZWxmY29ubmVjdGlvbi5nYXRlcikge1xuICAgICAgICAgICAgaWYgKGlucHV0LmdhdGVyKVxuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKHRyYWNlLCAnID0gJywgc2VsZl9nYWluLCAnICogJywgc2VsZl93ZWlnaHQsXG4gICAgICAgICAgICAgICAgJyAqICcsIHRyYWNlLCAnICsgJywgaW5wdXRfZ2FpbiwgJyAqICcsIGlucHV0X2FjdGl2YXRpb24sXG4gICAgICAgICAgICAgICAgc3RvcmVfdHJhY2UpO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKHRyYWNlLCAnID0gJywgc2VsZl9nYWluLCAnICogJywgc2VsZl93ZWlnaHQsXG4gICAgICAgICAgICAgICAgJyAqICcsIHRyYWNlLCAnICsgJywgaW5wdXRfYWN0aXZhdGlvbiwgc3RvcmVfdHJhY2UpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoaW5wdXQuZ2F0ZXIpXG4gICAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UodHJhY2UsICcgPSAnLCBzZWxmX3dlaWdodCwgJyAqICcsIHRyYWNlLCAnICsgJyxcbiAgICAgICAgICAgICAgICBpbnB1dF9nYWluLCAnICogJywgaW5wdXRfYWN0aXZhdGlvbiwgc3RvcmVfdHJhY2UpO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKHRyYWNlLCAnID0gJywgc2VsZl93ZWlnaHQsICcgKiAnLCB0cmFjZSwgJyArICcsXG4gICAgICAgICAgICAgICAgaW5wdXRfYWN0aXZhdGlvbiwgc3RvcmVfdHJhY2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAoaW5wdXQuZ2F0ZXIpXG4gICAgICAgICAgICBidWlsZFNlbnRlbmNlKHRyYWNlLCAnID0gJywgaW5wdXRfZ2FpbiwgJyAqICcsIGlucHV0X2FjdGl2YXRpb24sXG4gICAgICAgICAgICAgIHN0b3JlX3RyYWNlKTtcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICBidWlsZFNlbnRlbmNlKHRyYWNlLCAnID0gJywgaW5wdXRfYWN0aXZhdGlvbiwgc3RvcmVfdHJhY2UpO1xuICAgICAgICB9XG4gICAgICAgIGZvciAodmFyIGlkIGluIHRoaXMudHJhY2UuZXh0ZW5kZWQpIHtcbiAgICAgICAgICAvLyBleHRlbmRlZCBlbGVnaWJpbGl0eSB0cmFjZVxuICAgICAgICAgIHZhciBuZXVyb24gPSB0aGlzLm5laWdoYm9vcnNbaWRdO1xuICAgICAgICAgIHZhciBpbmZsdWVuY2UgPSBnZXRWYXIoJ2luZmx1ZW5jZXNbJyArIG5ldXJvbi5JRCArICddJyk7XG5cbiAgICAgICAgICB2YXIgdHJhY2UgPSBnZXRWYXIodGhpcywgJ3RyYWNlJywgJ2VsZWdpYmlsaXR5JywgaW5wdXQuSUQsIHRoaXMudHJhY2VcbiAgICAgICAgICAgIC5lbGVnaWJpbGl0eVtpbnB1dC5JRF0pO1xuICAgICAgICAgIHZhciB4dHJhY2UgPSBnZXRWYXIodGhpcywgJ3RyYWNlJywgJ2V4dGVuZGVkJywgbmV1cm9uLklELCBpbnB1dC5JRCxcbiAgICAgICAgICAgIHRoaXMudHJhY2UuZXh0ZW5kZWRbbmV1cm9uLklEXVtpbnB1dC5JRF0pO1xuICAgICAgICAgIGlmIChuZXVyb24uc2VsZmNvbm5lY3RlZCgpKVxuICAgICAgICAgICAgdmFyIG5ldXJvbl9zZWxmX3dlaWdodCA9IGdldFZhcihuZXVyb24uc2VsZmNvbm5lY3Rpb24sICd3ZWlnaHQnKTtcbiAgICAgICAgICBpZiAobmV1cm9uLnNlbGZjb25uZWN0aW9uLmdhdGVyKVxuICAgICAgICAgICAgdmFyIG5ldXJvbl9zZWxmX2dhaW4gPSBnZXRWYXIobmV1cm9uLnNlbGZjb25uZWN0aW9uLCAnZ2FpbicpO1xuICAgICAgICAgIGlmIChuZXVyb24uc2VsZmNvbm5lY3RlZCgpKVxuICAgICAgICAgICAgaWYgKG5ldXJvbi5zZWxmY29ubmVjdGlvbi5nYXRlcilcbiAgICAgICAgICAgICAgYnVpbGRTZW50ZW5jZSh4dHJhY2UsICcgPSAnLCBuZXVyb25fc2VsZl9nYWluLCAnICogJyxcbiAgICAgICAgICAgICAgICBuZXVyb25fc2VsZl93ZWlnaHQsICcgKiAnLCB4dHJhY2UsICcgKyAnLCBkZXJpdmF0aXZlLCAnICogJyxcbiAgICAgICAgICAgICAgICB0cmFjZSwgJyAqICcsIGluZmx1ZW5jZSwgc3RvcmVfdHJhY2UpO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKHh0cmFjZSwgJyA9ICcsIG5ldXJvbl9zZWxmX3dlaWdodCwgJyAqICcsXG4gICAgICAgICAgICAgICAgeHRyYWNlLCAnICsgJywgZGVyaXZhdGl2ZSwgJyAqICcsIHRyYWNlLCAnICogJyxcbiAgICAgICAgICAgICAgICBpbmZsdWVuY2UsIHN0b3JlX3RyYWNlKTtcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICBidWlsZFNlbnRlbmNlKHh0cmFjZSwgJyA9ICcsIGRlcml2YXRpdmUsICcgKiAnLCB0cmFjZSwgJyAqICcsXG4gICAgICAgICAgICAgIGluZmx1ZW5jZSwgc3RvcmVfdHJhY2UpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBmb3IgKHZhciBjb25uZWN0aW9uIGluIHRoaXMuY29ubmVjdGlvbnMuZ2F0ZWQpIHtcbiAgICAgICAgdmFyIGdhdGVkX2dhaW4gPSBnZXRWYXIodGhpcy5jb25uZWN0aW9ucy5nYXRlZFtjb25uZWN0aW9uXSwgJ2dhaW4nKTtcbiAgICAgICAgYnVpbGRTZW50ZW5jZShnYXRlZF9nYWluLCAnID0gJywgYWN0aXZhdGlvbiwgc3RvcmVfYWN0aXZhdGlvbik7XG4gICAgICB9XG4gICAgfVxuICAgIGlmICghaXNJbnB1dCkge1xuICAgICAgdmFyIHJlc3BvbnNpYmlsaXR5ID0gZ2V0VmFyKHRoaXMsICdlcnJvcicsICdyZXNwb25zaWJpbGl0eScsIHRoaXMuZXJyb3JcbiAgICAgICAgLnJlc3BvbnNpYmlsaXR5KTtcbiAgICAgIGlmIChpc091dHB1dCkge1xuICAgICAgICB2YXIgdGFyZ2V0ID0gZ2V0VmFyKCd0YXJnZXQnKTtcbiAgICAgICAgYnVpbGRTZW50ZW5jZShyZXNwb25zaWJpbGl0eSwgJyA9ICcsIHRhcmdldCwgJyAtICcsIGFjdGl2YXRpb24sXG4gICAgICAgICAgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICBmb3IgKHZhciBpZCBpbiB0aGlzLmNvbm5lY3Rpb25zLmlucHV0cykge1xuICAgICAgICAgIHZhciBpbnB1dCA9IHRoaXMuY29ubmVjdGlvbnMuaW5wdXRzW2lkXTtcbiAgICAgICAgICB2YXIgdHJhY2UgPSBnZXRWYXIodGhpcywgJ3RyYWNlJywgJ2VsZWdpYmlsaXR5JywgaW5wdXQuSUQsIHRoaXMudHJhY2VcbiAgICAgICAgICAgIC5lbGVnaWJpbGl0eVtpbnB1dC5JRF0pO1xuICAgICAgICAgIHZhciBpbnB1dF93ZWlnaHQgPSBnZXRWYXIoaW5wdXQsICd3ZWlnaHQnKTtcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKGlucHV0X3dlaWdodCwgJyArPSAnLCByYXRlLCAnICogKCcsIHJlc3BvbnNpYmlsaXR5LFxuICAgICAgICAgICAgJyAqICcsIHRyYWNlLCAnKScsIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgfVxuICAgICAgICBvdXRwdXRzLnB1c2goYWN0aXZhdGlvbi5pZCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoIW5vUHJvamVjdGlvbnMgJiYgIW5vR2F0ZXMpIHtcbiAgICAgICAgICB2YXIgZXJyb3IgPSBnZXRWYXIoJ2F1eCcpO1xuICAgICAgICAgIGZvciAodmFyIGlkIGluIHRoaXMuY29ubmVjdGlvbnMucHJvamVjdGVkKSB7XG4gICAgICAgICAgICB2YXIgY29ubmVjdGlvbiA9IHRoaXMuY29ubmVjdGlvbnMucHJvamVjdGVkW2lkXTtcbiAgICAgICAgICAgIHZhciBuZXVyb24gPSBjb25uZWN0aW9uLnRvO1xuICAgICAgICAgICAgdmFyIGNvbm5lY3Rpb25fd2VpZ2h0ID0gZ2V0VmFyKGNvbm5lY3Rpb24sICd3ZWlnaHQnKTtcbiAgICAgICAgICAgIHZhciBuZXVyb25fcmVzcG9uc2liaWxpdHkgPSBnZXRWYXIobmV1cm9uLCAnZXJyb3InLFxuICAgICAgICAgICAgICAncmVzcG9uc2liaWxpdHknLCBuZXVyb24uZXJyb3IucmVzcG9uc2liaWxpdHkpO1xuICAgICAgICAgICAgaWYgKGNvbm5lY3Rpb24uZ2F0ZXIpIHtcbiAgICAgICAgICAgICAgdmFyIGNvbm5lY3Rpb25fZ2FpbiA9IGdldFZhcihjb25uZWN0aW9uLCAnZ2FpbicpO1xuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKGVycm9yLCAnICs9ICcsIG5ldXJvbl9yZXNwb25zaWJpbGl0eSwgJyAqICcsXG4gICAgICAgICAgICAgICAgY29ubmVjdGlvbl9nYWluLCAnICogJywgY29ubmVjdGlvbl93ZWlnaHQsXG4gICAgICAgICAgICAgICAgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgICAgfSBlbHNlXG4gICAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoZXJyb3IsICcgKz0gJywgbmV1cm9uX3Jlc3BvbnNpYmlsaXR5LCAnICogJyxcbiAgICAgICAgICAgICAgICBjb25uZWN0aW9uX3dlaWdodCwgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgcHJvamVjdGVkID0gZ2V0VmFyKHRoaXMsICdlcnJvcicsICdwcm9qZWN0ZWQnLCB0aGlzLmVycm9yLnByb2plY3RlZCk7XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShwcm9qZWN0ZWQsICcgPSAnLCBkZXJpdmF0aXZlLCAnICogJywgZXJyb3IsXG4gICAgICAgICAgICBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShlcnJvciwgJyA9IDAnLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgZm9yICh2YXIgaWQgaW4gdGhpcy50cmFjZS5leHRlbmRlZCkge1xuICAgICAgICAgICAgdmFyIG5ldXJvbiA9IHRoaXMubmVpZ2hib29yc1tpZF07XG4gICAgICAgICAgICB2YXIgaW5mbHVlbmNlID0gZ2V0VmFyKCdhdXhfMicpO1xuICAgICAgICAgICAgdmFyIG5ldXJvbl9vbGQgPSBnZXRWYXIobmV1cm9uLCAnb2xkJyk7XG4gICAgICAgICAgICBpZiAobmV1cm9uLnNlbGZjb25uZWN0aW9uLmdhdGVyID09IHRoaXMpXG4gICAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoaW5mbHVlbmNlLCAnID0gJywgbmV1cm9uX29sZCwgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKGluZmx1ZW5jZSwgJyA9IDAnLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgICBmb3IgKHZhciBpbnB1dCBpbiB0aGlzLnRyYWNlLmluZmx1ZW5jZXNbbmV1cm9uLklEXSkge1xuICAgICAgICAgICAgICB2YXIgY29ubmVjdGlvbiA9IHRoaXMudHJhY2UuaW5mbHVlbmNlc1tuZXVyb24uSURdW2lucHV0XTtcbiAgICAgICAgICAgICAgdmFyIGNvbm5lY3Rpb25fd2VpZ2h0ID0gZ2V0VmFyKGNvbm5lY3Rpb24sICd3ZWlnaHQnKTtcbiAgICAgICAgICAgICAgdmFyIG5ldXJvbl9hY3RpdmF0aW9uID0gZ2V0VmFyKGNvbm5lY3Rpb24uZnJvbSwgJ2FjdGl2YXRpb24nKTtcbiAgICAgICAgICAgICAgYnVpbGRTZW50ZW5jZShpbmZsdWVuY2UsICcgKz0gJywgY29ubmVjdGlvbl93ZWlnaHQsICcgKiAnLFxuICAgICAgICAgICAgICAgIG5ldXJvbl9hY3RpdmF0aW9uLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgbmV1cm9uX3Jlc3BvbnNpYmlsaXR5ID0gZ2V0VmFyKG5ldXJvbiwgJ2Vycm9yJyxcbiAgICAgICAgICAgICAgJ3Jlc3BvbnNpYmlsaXR5JywgbmV1cm9uLmVycm9yLnJlc3BvbnNpYmlsaXR5KTtcbiAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoZXJyb3IsICcgKz0gJywgbmV1cm9uX3Jlc3BvbnNpYmlsaXR5LCAnICogJyxcbiAgICAgICAgICAgICAgaW5mbHVlbmNlLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBnYXRlZCA9IGdldFZhcih0aGlzLCAnZXJyb3InLCAnZ2F0ZWQnLCB0aGlzLmVycm9yLmdhdGVkKTtcbiAgICAgICAgICBidWlsZFNlbnRlbmNlKGdhdGVkLCAnID0gJywgZGVyaXZhdGl2ZSwgJyAqICcsIGVycm9yLFxuICAgICAgICAgICAgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgIGJ1aWxkU2VudGVuY2UocmVzcG9uc2liaWxpdHksICcgPSAnLCBwcm9qZWN0ZWQsICcgKyAnLCBnYXRlZCxcbiAgICAgICAgICAgIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICBmb3IgKHZhciBpZCBpbiB0aGlzLmNvbm5lY3Rpb25zLmlucHV0cykge1xuICAgICAgICAgICAgdmFyIGlucHV0ID0gdGhpcy5jb25uZWN0aW9ucy5pbnB1dHNbaWRdO1xuICAgICAgICAgICAgdmFyIGdyYWRpZW50ID0gZ2V0VmFyKCdhdXgnKTtcbiAgICAgICAgICAgIHZhciB0cmFjZSA9IGdldFZhcih0aGlzLCAndHJhY2UnLCAnZWxlZ2liaWxpdHknLCBpbnB1dC5JRCwgdGhpc1xuICAgICAgICAgICAgICAudHJhY2UuZWxlZ2liaWxpdHlbaW5wdXQuSURdKTtcbiAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoZ3JhZGllbnQsICcgPSAnLCBwcm9qZWN0ZWQsICcgKiAnLCB0cmFjZSxcbiAgICAgICAgICAgICAgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgICAgZm9yICh2YXIgaWQgaW4gdGhpcy50cmFjZS5leHRlbmRlZCkge1xuICAgICAgICAgICAgICB2YXIgbmV1cm9uID0gdGhpcy5uZWlnaGJvb3JzW2lkXTtcbiAgICAgICAgICAgICAgdmFyIG5ldXJvbl9yZXNwb25zaWJpbGl0eSA9IGdldFZhcihuZXVyb24sICdlcnJvcicsXG4gICAgICAgICAgICAgICAgJ3Jlc3BvbnNpYmlsaXR5JywgbmV1cm9uLmVycm9yLnJlc3BvbnNpYmlsaXR5KTtcbiAgICAgICAgICAgICAgdmFyIHh0cmFjZSA9IGdldFZhcih0aGlzLCAndHJhY2UnLCAnZXh0ZW5kZWQnLCBuZXVyb24uSUQsXG4gICAgICAgICAgICAgICAgaW5wdXQuSUQsIHRoaXMudHJhY2UuZXh0ZW5kZWRbbmV1cm9uLklEXVtpbnB1dC5JRF0pO1xuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKGdyYWRpZW50LCAnICs9ICcsIG5ldXJvbl9yZXNwb25zaWJpbGl0eSwgJyAqICcsXG4gICAgICAgICAgICAgICAgeHRyYWNlLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgaW5wdXRfd2VpZ2h0ID0gZ2V0VmFyKGlucHV0LCAnd2VpZ2h0Jyk7XG4gICAgICAgICAgICBidWlsZFNlbnRlbmNlKGlucHV0X3dlaWdodCwgJyArPSAnLCByYXRlLCAnICogJywgZ3JhZGllbnQsXG4gICAgICAgICAgICAgIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgfSBlbHNlIGlmIChub0dhdGVzKSB7XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShyZXNwb25zaWJpbGl0eSwgJyA9IDAnLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgZm9yICh2YXIgaWQgaW4gdGhpcy5jb25uZWN0aW9ucy5wcm9qZWN0ZWQpIHtcbiAgICAgICAgICAgIHZhciBjb25uZWN0aW9uID0gdGhpcy5jb25uZWN0aW9ucy5wcm9qZWN0ZWRbaWRdO1xuICAgICAgICAgICAgdmFyIG5ldXJvbiA9IGNvbm5lY3Rpb24udG87XG4gICAgICAgICAgICB2YXIgY29ubmVjdGlvbl93ZWlnaHQgPSBnZXRWYXIoY29ubmVjdGlvbiwgJ3dlaWdodCcpO1xuICAgICAgICAgICAgdmFyIG5ldXJvbl9yZXNwb25zaWJpbGl0eSA9IGdldFZhcihuZXVyb24sICdlcnJvcicsXG4gICAgICAgICAgICAgICdyZXNwb25zaWJpbGl0eScsIG5ldXJvbi5lcnJvci5yZXNwb25zaWJpbGl0eSk7XG4gICAgICAgICAgICBpZiAoY29ubmVjdGlvbi5nYXRlcikge1xuICAgICAgICAgICAgICB2YXIgY29ubmVjdGlvbl9nYWluID0gZ2V0VmFyKGNvbm5lY3Rpb24sICdnYWluJyk7XG4gICAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UocmVzcG9uc2liaWxpdHksICcgKz0gJywgbmV1cm9uX3Jlc3BvbnNpYmlsaXR5LFxuICAgICAgICAgICAgICAgICcgKiAnLCBjb25uZWN0aW9uX2dhaW4sICcgKiAnLCBjb25uZWN0aW9uX3dlaWdodCxcbiAgICAgICAgICAgICAgICBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgICB9IGVsc2VcbiAgICAgICAgICAgICAgYnVpbGRTZW50ZW5jZShyZXNwb25zaWJpbGl0eSwgJyArPSAnLCBuZXVyb25fcmVzcG9uc2liaWxpdHksXG4gICAgICAgICAgICAgICAgJyAqICcsIGNvbm5lY3Rpb25fd2VpZ2h0LCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgfVxuICAgICAgICAgIGJ1aWxkU2VudGVuY2UocmVzcG9uc2liaWxpdHksICcgKj0gJywgZGVyaXZhdGl2ZSxcbiAgICAgICAgICAgIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICBmb3IgKHZhciBpZCBpbiB0aGlzLmNvbm5lY3Rpb25zLmlucHV0cykge1xuICAgICAgICAgICAgdmFyIGlucHV0ID0gdGhpcy5jb25uZWN0aW9ucy5pbnB1dHNbaWRdO1xuICAgICAgICAgICAgdmFyIHRyYWNlID0gZ2V0VmFyKHRoaXMsICd0cmFjZScsICdlbGVnaWJpbGl0eScsIGlucHV0LklELCB0aGlzXG4gICAgICAgICAgICAgIC50cmFjZS5lbGVnaWJpbGl0eVtpbnB1dC5JRF0pO1xuICAgICAgICAgICAgdmFyIGlucHV0X3dlaWdodCA9IGdldFZhcihpbnB1dCwgJ3dlaWdodCcpO1xuICAgICAgICAgICAgYnVpbGRTZW50ZW5jZShpbnB1dF93ZWlnaHQsICcgKz0gJywgcmF0ZSwgJyAqICgnLFxuICAgICAgICAgICAgICByZXNwb25zaWJpbGl0eSwgJyAqICcsIHRyYWNlLCAnKScsIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAobm9Qcm9qZWN0aW9ucykge1xuICAgICAgICAgIGJ1aWxkU2VudGVuY2UocmVzcG9uc2liaWxpdHksICcgPSAwJywgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgIGZvciAodmFyIGlkIGluIHRoaXMudHJhY2UuZXh0ZW5kZWQpIHtcbiAgICAgICAgICAgIHZhciBuZXVyb24gPSB0aGlzLm5laWdoYm9vcnNbaWRdO1xuICAgICAgICAgICAgdmFyIGluZmx1ZW5jZSA9IGdldFZhcignYXV4Jyk7XG4gICAgICAgICAgICB2YXIgbmV1cm9uX29sZCA9IGdldFZhcihuZXVyb24sICdvbGQnKTtcbiAgICAgICAgICAgIGlmIChuZXVyb24uc2VsZmNvbm5lY3Rpb24uZ2F0ZXIgPT0gdGhpcylcbiAgICAgICAgICAgICAgYnVpbGRTZW50ZW5jZShpbmZsdWVuY2UsICcgPSAnLCBuZXVyb25fb2xkLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoaW5mbHVlbmNlLCAnID0gMCcsIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICAgIGZvciAodmFyIGlucHV0IGluIHRoaXMudHJhY2UuaW5mbHVlbmNlc1tuZXVyb24uSURdKSB7XG4gICAgICAgICAgICAgIHZhciBjb25uZWN0aW9uID0gdGhpcy50cmFjZS5pbmZsdWVuY2VzW25ldXJvbi5JRF1baW5wdXRdO1xuICAgICAgICAgICAgICB2YXIgY29ubmVjdGlvbl93ZWlnaHQgPSBnZXRWYXIoY29ubmVjdGlvbiwgJ3dlaWdodCcpO1xuICAgICAgICAgICAgICB2YXIgbmV1cm9uX2FjdGl2YXRpb24gPSBnZXRWYXIoY29ubmVjdGlvbi5mcm9tLCAnYWN0aXZhdGlvbicpO1xuICAgICAgICAgICAgICBidWlsZFNlbnRlbmNlKGluZmx1ZW5jZSwgJyArPSAnLCBjb25uZWN0aW9uX3dlaWdodCwgJyAqICcsXG4gICAgICAgICAgICAgICAgbmV1cm9uX2FjdGl2YXRpb24sIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZhciBuZXVyb25fcmVzcG9uc2liaWxpdHkgPSBnZXRWYXIobmV1cm9uLCAnZXJyb3InLFxuICAgICAgICAgICAgICAncmVzcG9uc2liaWxpdHknLCBuZXVyb24uZXJyb3IucmVzcG9uc2liaWxpdHkpO1xuICAgICAgICAgICAgYnVpbGRTZW50ZW5jZShyZXNwb25zaWJpbGl0eSwgJyArPSAnLCBuZXVyb25fcmVzcG9uc2liaWxpdHksXG4gICAgICAgICAgICAgICcgKiAnLCBpbmZsdWVuY2UsIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgYnVpbGRTZW50ZW5jZShyZXNwb25zaWJpbGl0eSwgJyAqPSAnLCBkZXJpdmF0aXZlLFxuICAgICAgICAgICAgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgIGZvciAodmFyIGlkIGluIHRoaXMuY29ubmVjdGlvbnMuaW5wdXRzKSB7XG4gICAgICAgICAgICB2YXIgaW5wdXQgPSB0aGlzLmNvbm5lY3Rpb25zLmlucHV0c1tpZF07XG4gICAgICAgICAgICB2YXIgZ3JhZGllbnQgPSBnZXRWYXIoJ2F1eCcpO1xuICAgICAgICAgICAgYnVpbGRTZW50ZW5jZShncmFkaWVudCwgJyA9IDAnLCBzdG9yZV9wcm9wYWdhdGlvbik7XG4gICAgICAgICAgICBmb3IgKHZhciBpZCBpbiB0aGlzLnRyYWNlLmV4dGVuZGVkKSB7XG4gICAgICAgICAgICAgIHZhciBuZXVyb24gPSB0aGlzLm5laWdoYm9vcnNbaWRdO1xuICAgICAgICAgICAgICB2YXIgbmV1cm9uX3Jlc3BvbnNpYmlsaXR5ID0gZ2V0VmFyKG5ldXJvbiwgJ2Vycm9yJyxcbiAgICAgICAgICAgICAgICAncmVzcG9uc2liaWxpdHknLCBuZXVyb24uZXJyb3IucmVzcG9uc2liaWxpdHkpO1xuICAgICAgICAgICAgICB2YXIgeHRyYWNlID0gZ2V0VmFyKHRoaXMsICd0cmFjZScsICdleHRlbmRlZCcsIG5ldXJvbi5JRCxcbiAgICAgICAgICAgICAgICBpbnB1dC5JRCwgdGhpcy50cmFjZS5leHRlbmRlZFtuZXVyb24uSURdW2lucHV0LklEXSk7XG4gICAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoZ3JhZGllbnQsICcgKz0gJywgbmV1cm9uX3Jlc3BvbnNpYmlsaXR5LCAnICogJyxcbiAgICAgICAgICAgICAgICB4dHJhY2UsIHN0b3JlX3Byb3BhZ2F0aW9uKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHZhciBpbnB1dF93ZWlnaHQgPSBnZXRWYXIoaW5wdXQsICd3ZWlnaHQnKTtcbiAgICAgICAgICAgIGJ1aWxkU2VudGVuY2UoaW5wdXRfd2VpZ2h0LCAnICs9ICcsIHJhdGUsICcgKiAnLCBncmFkaWVudCxcbiAgICAgICAgICAgICAgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgYnVpbGRTZW50ZW5jZShiaWFzLCAnICs9ICcsIHJhdGUsICcgKiAnLCByZXNwb25zaWJpbGl0eSxcbiAgICAgICAgc3RvcmVfcHJvcGFnYXRpb24pO1xuICAgIH1cbiAgICByZXR1cm4ge1xuICAgICAgbWVtb3J5OiB2YXJJRCxcbiAgICAgIG5ldXJvbnM6IG5ldXJvbnMgKyAxLFxuICAgICAgaW5wdXRzOiBpbnB1dHMsXG4gICAgICBvdXRwdXRzOiBvdXRwdXRzLFxuICAgICAgdGFyZ2V0czogdGFyZ2V0cyxcbiAgICAgIHZhcmlhYmxlczogdmFyaWFibGVzLFxuICAgICAgYWN0aXZhdGlvbl9zZW50ZW5jZXM6IGFjdGl2YXRpb25fc2VudGVuY2VzLFxuICAgICAgdHJhY2Vfc2VudGVuY2VzOiB0cmFjZV9zZW50ZW5jZXMsXG4gICAgICBwcm9wYWdhdGlvbl9zZW50ZW5jZXM6IHByb3BhZ2F0aW9uX3NlbnRlbmNlcyxcbiAgICAgIGxheWVyczogbGF5ZXJzXG4gICAgfVxuICB9XG59XG5cblxuLy8gcmVwcmVzZW50cyBhIGNvbm5lY3Rpb24gYmV0d2VlbiB0d28gbmV1cm9uc1xuTmV1cm9uLmNvbm5lY3Rpb24gPSBmdW5jdGlvbiBDb25uZWN0aW9uKGZyb20sIHRvLCB3ZWlnaHQpIHtcblxuICBpZiAoIWZyb20gfHwgIXRvKVxuICAgIHRocm93IG5ldyBFcnJvcihcIkNvbm5lY3Rpb24gRXJyb3I6IEludmFsaWQgbmV1cm9uc1wiKTtcblxuICB0aGlzLklEID0gTmV1cm9uLmNvbm5lY3Rpb24udWlkKCk7XG4gIHRoaXMuZnJvbSA9IGZyb207XG4gIHRoaXMudG8gPSB0bztcbiAgdGhpcy53ZWlnaHQgPSB0eXBlb2Ygd2VpZ2h0ID09ICd1bmRlZmluZWQnID8gTWF0aC5yYW5kb20oKSAqIC4yIC0gLjEgOlxuICAgIHdlaWdodDtcbiAgdGhpcy5nYWluID0gMTtcbiAgdGhpcy5nYXRlciA9IG51bGw7XG59XG5cblxuLy8gc3F1YXNoaW5nIGZ1bmN0aW9uc1xuTmV1cm9uLnNxdWFzaCA9IHt9O1xuXG4vLyBlcS4gNSAmIDUnXG5OZXVyb24uc3F1YXNoLkxPR0lTVElDID0gZnVuY3Rpb24oeCwgZGVyaXZhdGUpIHtcbiAgaWYgKCFkZXJpdmF0ZSlcbiAgICByZXR1cm4gMSAvICgxICsgTWF0aC5leHAoLXgpKTtcbiAgdmFyIGZ4ID0gTmV1cm9uLnNxdWFzaC5MT0dJU1RJQyh4KTtcbiAgcmV0dXJuIGZ4ICogKDEgLSBmeCk7XG59O1xuTmV1cm9uLnNxdWFzaC5UQU5IID0gZnVuY3Rpb24oeCwgZGVyaXZhdGUpIHtcbiAgaWYgKGRlcml2YXRlKVxuICAgIHJldHVybiAxIC0gTWF0aC5wb3coTmV1cm9uLnNxdWFzaC5UQU5IKHgpLCAyKTtcbiAgdmFyIGVQID0gTWF0aC5leHAoeCk7XG4gIHZhciBlTiA9IDEgLyBlUDtcbiAgcmV0dXJuIChlUCAtIGVOKSAvIChlUCArIGVOKTtcbn07XG5OZXVyb24uc3F1YXNoLklERU5USVRZID0gZnVuY3Rpb24oeCwgZGVyaXZhdGUpIHtcbiAgcmV0dXJuIGRlcml2YXRlID8gMSA6IHg7XG59O1xuTmV1cm9uLnNxdWFzaC5ITElNID0gZnVuY3Rpb24oeCwgZGVyaXZhdGUpIHtcbiAgcmV0dXJuIGRlcml2YXRlID8gMSA6IHggPiAwID8gMSA6IDA7XG59O1xuTmV1cm9uLnNxdWFzaC5SRUxVID0gZnVuY3Rpb24oeCwgZGVyaXZhdGUpIHtcbiAgaWYgKGRlcml2YXRlKVxuICAgIHJldHVybiB4ID4gMCA/IDEgOiAwO1xuICByZXR1cm4geCA+IDAgPyB4IDogMDtcbn07XG5cbi8vIHVuaXF1ZSBJRCdzXG4oZnVuY3Rpb24oKSB7XG4gIHZhciBuZXVyb25zID0gMDtcbiAgdmFyIGNvbm5lY3Rpb25zID0gMDtcbiAgTmV1cm9uLnVpZCA9IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBuZXVyb25zKys7XG4gIH1cbiAgTmV1cm9uLmNvbm5lY3Rpb24udWlkID0gZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIGNvbm5lY3Rpb25zKys7XG4gIH1cbiAgTmV1cm9uLnF1YW50aXR5ID0gZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIG5ldXJvbnM6IG5ldXJvbnMsXG4gICAgICBjb25uZWN0aW9uczogY29ubmVjdGlvbnNcbiAgICB9XG4gIH1cbn0pKCk7XG4iLCJ2YXIgU3luYXB0aWMgPSB7XG4gICAgTmV1cm9uOiByZXF1aXJlKCcuL25ldXJvbicpLFxuICAgIExheWVyOiByZXF1aXJlKCcuL2xheWVyJyksXG4gICAgTmV0d29yazogcmVxdWlyZSgnLi9uZXR3b3JrJyksXG4gICAgVHJhaW5lcjogcmVxdWlyZSgnLi90cmFpbmVyJyksXG4gICAgQXJjaGl0ZWN0OiByZXF1aXJlKCcuL2FyY2hpdGVjdCcpXG59O1xuXG4vLyBDb21tb25KUyAmIEFNRFxuaWYgKHR5cGVvZiBkZWZpbmUgIT09ICd1bmRlZmluZWQnICYmIGRlZmluZS5hbWQpXG57XG4gIGRlZmluZShbXSwgZnVuY3Rpb24oKXsgcmV0dXJuIFN5bmFwdGljIH0pO1xufVxuXG4vLyBOb2RlLmpzXG5pZiAodHlwZW9mIG1vZHVsZSAhPT0gJ3VuZGVmaW5lZCcgJiYgbW9kdWxlLmV4cG9ydHMpXG57XG4gIG1vZHVsZS5leHBvcnRzID0gU3luYXB0aWM7XG59XG5cbi8vIEJyb3dzZXJcbmlmICh0eXBlb2Ygd2luZG93ID09ICdvYmplY3QnKVxue1xuICAoZnVuY3Rpb24oKXtcbiAgICB2YXIgb2xkU3luYXB0aWMgPSB3aW5kb3dbJ3N5bmFwdGljJ107XG4gICAgU3luYXB0aWMubmluamEgPSBmdW5jdGlvbigpe1xuICAgICAgd2luZG93WydzeW5hcHRpYyddID0gb2xkU3luYXB0aWM7XG4gICAgICByZXR1cm4gU3luYXB0aWM7XG4gICAgfTtcbiAgfSkoKTtcblxuICB3aW5kb3dbJ3N5bmFwdGljJ10gPSBTeW5hcHRpYztcbn1cbiIsIi8vIGV4cG9ydFxuaWYgKG1vZHVsZSkgbW9kdWxlLmV4cG9ydHMgPSBUcmFpbmVyO1xuXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFRSQUlORVJcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbmZ1bmN0aW9uIFRyYWluZXIobmV0d29yaywgb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdGhpcy5uZXR3b3JrID0gbmV0d29yaztcbiAgdGhpcy5yYXRlID0gb3B0aW9ucy5yYXRlIHx8IC4yO1xuICB0aGlzLml0ZXJhdGlvbnMgPSBvcHRpb25zLml0ZXJhdGlvbnMgfHwgMTAwMDAwO1xuICB0aGlzLmVycm9yID0gb3B0aW9ucy5lcnJvciB8fCAuMDA1O1xuICB0aGlzLmNvc3QgPSBvcHRpb25zLmNvc3QgfHwgbnVsbDtcbiAgdGhpcy5jcm9zc1ZhbGlkYXRlID0gb3B0aW9ucy5jcm9zc1ZhbGlkYXRlIHx8IG51bGw7XG59XG5cblRyYWluZXIucHJvdG90eXBlID0ge1xuXG4gIC8vIHRyYWlucyBhbnkgZ2l2ZW4gc2V0IHRvIGEgbmV0d29ya1xuICB0cmFpbjogZnVuY3Rpb24oc2V0LCBvcHRpb25zKSB7XG5cbiAgICB2YXIgZXJyb3IgPSAxO1xuICAgIHZhciBpdGVyYXRpb25zID0gYnVja2V0U2l6ZSA9IDA7XG4gICAgdmFyIGFib3J0ID0gZmFsc2U7XG4gICAgdmFyIGN1cnJlbnRSYXRlO1xuICAgIHZhciBjb3N0ID0gb3B0aW9ucyAmJiBvcHRpb25zLmNvc3QgfHwgdGhpcy5jb3N0IHx8IFRyYWluZXIuY29zdC5NU0U7XG4gICAgdmFyIGNyb3NzVmFsaWRhdGUgPSBmYWxzZSwgdGVzdFNldCwgdHJhaW5TZXQ7XG5cbiAgICB2YXIgc3RhcnQgPSBEYXRlLm5vdygpO1xuXG4gICAgaWYgKG9wdGlvbnMpIHtcbiAgICAgIGlmIChvcHRpb25zLnNodWZmbGUpIHtcbiAgICAgICAgLy8rIEpvbmFzIFJhb25pIFNvYXJlcyBTaWx2YVxuICAgICAgICAvL0AgaHR0cDovL2pzZnJvbWhlbGwuY29tL2FycmF5L3NodWZmbGUgW3YxLjBdXG4gICAgICAgIGZ1bmN0aW9uIHNodWZmbGUobykgeyAvL3YxLjBcbiAgICAgICAgICBmb3IgKHZhciBqLCB4LCBpID0gby5sZW5ndGg7IGk7IGogPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBpKSwgeCA9IG9bLS1pXSwgb1tpXSA9IG9bal0sIG9bal0gPSB4KTtcbiAgICAgICAgICByZXR1cm4gbztcbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIGlmIChvcHRpb25zLml0ZXJhdGlvbnMpXG4gICAgICAgIHRoaXMuaXRlcmF0aW9ucyA9IG9wdGlvbnMuaXRlcmF0aW9ucztcbiAgICAgIGlmIChvcHRpb25zLmVycm9yKVxuICAgICAgICB0aGlzLmVycm9yID0gb3B0aW9ucy5lcnJvcjtcbiAgICAgIGlmIChvcHRpb25zLnJhdGUpXG4gICAgICAgIHRoaXMucmF0ZSA9IG9wdGlvbnMucmF0ZTtcbiAgICAgIGlmIChvcHRpb25zLmNvc3QpXG4gICAgICAgIHRoaXMuY29zdCA9IG9wdGlvbnMuY29zdDtcbiAgICAgIGlmIChvcHRpb25zLnNjaGVkdWxlKVxuICAgICAgICB0aGlzLnNjaGVkdWxlID0gb3B0aW9ucy5zY2hlZHVsZTtcbiAgICAgIGlmIChvcHRpb25zLmN1c3RvbUxvZyl7XG4gICAgICAgIC8vIGZvciBiYWNrd2FyZCBjb21wYXRpYmlsaXR5IHdpdGggY29kZSB0aGF0IHVzZWQgY3VzdG9tTG9nXG4gICAgICAgIGNvbnNvbGUubG9nKCdEZXByZWNhdGVkOiB1c2Ugc2NoZWR1bGUgaW5zdGVhZCBvZiBjdXN0b21Mb2cnKVxuICAgICAgICB0aGlzLnNjaGVkdWxlID0gb3B0aW9ucy5jdXN0b21Mb2c7XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5jcm9zc1ZhbGlkYXRlIHx8IG9wdGlvbnMuY3Jvc3NWYWxpZGF0ZSkge1xuICAgICAgICBpZighdGhpcy5jcm9zc1ZhbGlkYXRlKSB0aGlzLmNyb3NzVmFsaWRhdGUgPSB7fTtcbiAgICAgICAgY3Jvc3NWYWxpZGF0ZSA9IHRydWU7XG4gICAgICAgIGlmIChvcHRpb25zLmNyb3NzVmFsaWRhdGUudGVzdFNpemUpXG4gICAgICAgICAgdGhpcy5jcm9zc1ZhbGlkYXRlLnRlc3RTaXplID0gb3B0aW9ucy5jcm9zc1ZhbGlkYXRlLnRlc3RTaXplO1xuICAgICAgICBpZiAob3B0aW9ucy5jcm9zc1ZhbGlkYXRlLnRlc3RFcnJvcilcbiAgICAgICAgICB0aGlzLmNyb3NzVmFsaWRhdGUudGVzdEVycm9yID0gb3B0aW9ucy5jcm9zc1ZhbGlkYXRlLnRlc3RFcnJvcjtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBjdXJyZW50UmF0ZSA9IHRoaXMucmF0ZTtcbiAgICBpZihBcnJheS5pc0FycmF5KHRoaXMucmF0ZSkpIHtcbiAgICAgIHZhciBidWNrZXRTaXplID0gTWF0aC5mbG9vcih0aGlzLml0ZXJhdGlvbnMgLyB0aGlzLnJhdGUubGVuZ3RoKTtcbiAgICB9XG5cbiAgICBpZihjcm9zc1ZhbGlkYXRlKSB7XG4gICAgICB2YXIgbnVtVHJhaW4gPSBNYXRoLmNlaWwoKDEgLSB0aGlzLmNyb3NzVmFsaWRhdGUudGVzdFNpemUpICogc2V0Lmxlbmd0aCk7XG4gICAgICB0cmFpblNldCA9IHNldC5zbGljZSgwLCBudW1UcmFpbik7XG4gICAgICB0ZXN0U2V0ID0gc2V0LnNsaWNlKG51bVRyYWluKTtcbiAgICB9XG5cbiAgICB3aGlsZSAoKCFhYm9ydCAmJiBpdGVyYXRpb25zIDwgdGhpcy5pdGVyYXRpb25zICYmIGVycm9yID4gdGhpcy5lcnJvcikpIHtcbiAgICAgIGlmIChjcm9zc1ZhbGlkYXRlICYmIGVycm9yIDw9IHRoaXMuY3Jvc3NWYWxpZGF0ZS50ZXN0RXJyb3IpIHtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG5cbiAgICAgIHZhciBjdXJyZW50U2V0U2l6ZSA9IHNldC5sZW5ndGg7XG4gICAgICBlcnJvciA9IDA7XG4gICAgICBpdGVyYXRpb25zKys7XG5cbiAgICAgIGlmKGJ1Y2tldFNpemUgPiAwKSB7XG4gICAgICAgIHZhciBjdXJyZW50QnVja2V0ID0gTWF0aC5mbG9vcihpdGVyYXRpb25zIC8gYnVja2V0U2l6ZSk7XG4gICAgICAgIGN1cnJlbnRSYXRlID0gdGhpcy5yYXRlW2N1cnJlbnRCdWNrZXRdIHx8IGN1cnJlbnRSYXRlO1xuICAgICAgfVxuICAgICAgXG4gICAgICBpZih0eXBlb2YgdGhpcy5yYXRlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGN1cnJlbnRSYXRlID0gdGhpcy5yYXRlKGl0ZXJhdGlvbnMsIGVycm9yKTtcbiAgICAgIH1cblxuICAgICAgaWYgKGNyb3NzVmFsaWRhdGUpIHtcbiAgICAgICAgdGhpcy5fdHJhaW5TZXQodHJhaW5TZXQsIGN1cnJlbnRSYXRlLCBjb3N0KTtcbiAgICAgICAgZXJyb3IgKz0gdGhpcy50ZXN0KHRlc3RTZXQpLmVycm9yO1xuICAgICAgICBjdXJyZW50U2V0U2l6ZSA9IDE7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlcnJvciArPSB0aGlzLl90cmFpblNldChzZXQsIGN1cnJlbnRSYXRlLCBjb3N0KTtcbiAgICAgICAgY3VycmVudFNldFNpemUgPSBzZXQubGVuZ3RoO1xuICAgICAgfVxuXG4gICAgICAvLyBjaGVjayBlcnJvclxuICAgICAgZXJyb3IgLz0gY3VycmVudFNldFNpemU7XG5cbiAgICAgIGlmIChvcHRpb25zKSB7XG4gICAgICAgIGlmICh0aGlzLnNjaGVkdWxlICYmIHRoaXMuc2NoZWR1bGUuZXZlcnkgJiYgaXRlcmF0aW9ucyAlXG4gICAgICAgICAgdGhpcy5zY2hlZHVsZS5ldmVyeSA9PSAwKVxuICAgICAgICAgIGFib3J0ID0gdGhpcy5zY2hlZHVsZS5kbyh7IGVycm9yOiBlcnJvciwgaXRlcmF0aW9uczogaXRlcmF0aW9ucywgcmF0ZTogY3VycmVudFJhdGUgfSk7XG4gICAgICAgIGVsc2UgaWYgKG9wdGlvbnMubG9nICYmIGl0ZXJhdGlvbnMgJSBvcHRpb25zLmxvZyA9PSAwKSB7XG4gICAgICAgICAgY29uc29sZS5sb2coJ2l0ZXJhdGlvbnMnLCBpdGVyYXRpb25zLCAnZXJyb3InLCBlcnJvciwgJ3JhdGUnLCBjdXJyZW50UmF0ZSk7XG4gICAgICAgIH07XG4gICAgICAgIGlmIChvcHRpb25zLnNodWZmbGUpXG4gICAgICAgICAgc2h1ZmZsZShzZXQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHZhciByZXN1bHRzID0ge1xuICAgICAgZXJyb3I6IGVycm9yLFxuICAgICAgaXRlcmF0aW9uczogaXRlcmF0aW9ucyxcbiAgICAgIHRpbWU6IERhdGUubm93KCkgLSBzdGFydFxuICAgIH07XG5cbiAgICByZXR1cm4gcmVzdWx0cztcbiAgfSxcblxuICAvLyB0cmFpbnMgYW55IGdpdmVuIHNldCB0byBhIG5ldHdvcmssIHVzaW5nIGEgV2ViV29ya2VyIChvbmx5IGZvciB0aGUgYnJvd3NlcikuIFJldHVybnMgYSBQcm9taXNlIG9mIHRoZSByZXN1bHRzLlxuICB0cmFpbkFzeW5jOiBmdW5jdGlvbihzZXQsIG9wdGlvbnMpIHtcbiAgICB2YXIgdHJhaW4gPSB0aGlzLndvcmtlclRyYWluLmJpbmQodGhpcyk7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgdHJ5IHtcbiAgICAgICAgdHJhaW4oc2V0LCByZXNvbHZlLCBvcHRpb25zLCB0cnVlKVxuICAgICAgfSBjYXRjaChlKSB7XG4gICAgICAgIHJlamVjdChlKVxuICAgICAgfVxuICAgIH0pXG4gIH0sXG5cbiAgLy8gcHJlZm9ybXMgb25lIHRyYWluaW5nIGVwb2NoIGFuZCByZXR1cm5zIHRoZSBlcnJvciAocHJpdmF0ZSBmdW5jdGlvbiB1c2VkIGluIHRoaXMudHJhaW4pXG4gIF90cmFpblNldDogZnVuY3Rpb24oc2V0LCBjdXJyZW50UmF0ZSwgY29zdEZ1bmN0aW9uKSB7XG4gICAgdmFyIGVycm9yU3VtID0gMDtcbiAgICBmb3IgKHZhciB0cmFpbiBpbiBzZXQpIHtcbiAgICAgIHZhciBpbnB1dCA9IHNldFt0cmFpbl0uaW5wdXQ7XG4gICAgICB2YXIgdGFyZ2V0ID0gc2V0W3RyYWluXS5vdXRwdXQ7XG5cbiAgICAgIHZhciBvdXRwdXQgPSB0aGlzLm5ldHdvcmsuYWN0aXZhdGUoaW5wdXQpO1xuICAgICAgdGhpcy5uZXR3b3JrLnByb3BhZ2F0ZShjdXJyZW50UmF0ZSwgdGFyZ2V0KTtcblxuICAgICAgZXJyb3JTdW0gKz0gY29zdEZ1bmN0aW9uKHRhcmdldCwgb3V0cHV0KTtcbiAgICB9XG4gICAgcmV0dXJuIGVycm9yU3VtO1xuICB9LFxuXG4gIC8vIHRlc3RzIGEgc2V0IGFuZCByZXR1cm5zIHRoZSBlcnJvciBhbmQgZWxhcHNlZCB0aW1lXG4gIHRlc3Q6IGZ1bmN0aW9uKHNldCwgb3B0aW9ucykge1xuXG4gICAgdmFyIGVycm9yID0gMDtcbiAgICB2YXIgaW5wdXQsIG91dHB1dCwgdGFyZ2V0O1xuICAgIHZhciBjb3N0ID0gb3B0aW9ucyAmJiBvcHRpb25zLmNvc3QgfHwgdGhpcy5jb3N0IHx8IFRyYWluZXIuY29zdC5NU0U7XG5cbiAgICB2YXIgc3RhcnQgPSBEYXRlLm5vdygpO1xuXG4gICAgZm9yICh2YXIgdGVzdCBpbiBzZXQpIHtcbiAgICAgIGlucHV0ID0gc2V0W3Rlc3RdLmlucHV0O1xuICAgICAgdGFyZ2V0ID0gc2V0W3Rlc3RdLm91dHB1dDtcbiAgICAgIG91dHB1dCA9IHRoaXMubmV0d29yay5hY3RpdmF0ZShpbnB1dCk7XG4gICAgICBlcnJvciArPSBjb3N0KHRhcmdldCwgb3V0cHV0KTtcbiAgICB9XG5cbiAgICBlcnJvciAvPSBzZXQubGVuZ3RoO1xuXG4gICAgdmFyIHJlc3VsdHMgPSB7XG4gICAgICBlcnJvcjogZXJyb3IsXG4gICAgICB0aW1lOiBEYXRlLm5vdygpIC0gc3RhcnRcbiAgICB9O1xuXG4gICAgcmV0dXJuIHJlc3VsdHM7XG4gIH0sXG5cbiAgLy8gdHJhaW5zIGFueSBnaXZlbiBzZXQgdG8gYSBuZXR3b3JrIHVzaW5nIGEgV2ViV29ya2VyIFtkZXByZWNhdGVkOiB1c2UgdHJhaW5Bc3luYyBpbnN0ZWFkXVxuICB3b3JrZXJUcmFpbjogZnVuY3Rpb24oc2V0LCBjYWxsYmFjaywgb3B0aW9ucywgc3VwcHJlc3NXYXJuaW5nKSB7XG5cbiAgICBpZiAoIXN1cHByZXNzV2FybmluZykge1xuICAgICAgY29uc29sZS53YXJuKCdEZXByZWNhdGVkOiBkbyBub3QgdXNlIGB3b3JrZXJUcmFpbmAsIHVzZSBgdHJhaW5Bc3luY2AgaW5zdGVhZC4nKVxuICAgIH1cbiAgICB2YXIgdGhhdCA9IHRoaXM7XG5cbiAgICBpZiAoIXRoaXMubmV0d29yay5vcHRpbWl6ZWQpXG4gICAgICB0aGlzLm5ldHdvcmsub3B0aW1pemUoKTtcblxuICAgIC8vIENyZWF0ZSBhIG5ldyB3b3JrZXJcbiAgICB2YXIgd29ya2VyID0gdGhpcy5uZXR3b3JrLndvcmtlcih0aGlzLm5ldHdvcmsub3B0aW1pemVkLm1lbW9yeSwgc2V0LCBvcHRpb25zKTtcblxuICAgIC8vIHRyYWluIHRoZSB3b3JrZXJcbiAgICB3b3JrZXIub25tZXNzYWdlID0gZnVuY3Rpb24oZSkge1xuICAgICAgc3dpdGNoKGUuZGF0YS5hY3Rpb24pIHtcbiAgICAgICAgICBjYXNlICdkb25lJzpcbiAgICAgICAgICAgIHZhciBpdGVyYXRpb25zID0gZS5kYXRhLm1lc3NhZ2UuaXRlcmF0aW9ucztcbiAgICAgICAgICAgIHZhciBlcnJvciA9IGUuZGF0YS5tZXNzYWdlLmVycm9yO1xuICAgICAgICAgICAgdmFyIHRpbWUgPSBlLmRhdGEubWVzc2FnZS50aW1lO1xuXG4gICAgICAgICAgICB0aGF0Lm5ldHdvcmsub3B0aW1pemVkLm93bmVyc2hpcChlLmRhdGEubWVtb3J5QnVmZmVyKTtcblxuICAgICAgICAgICAgLy8gRG9uZSBjYWxsYmFja1xuICAgICAgICAgICAgY2FsbGJhY2soe1xuICAgICAgICAgICAgICBlcnJvcjogZXJyb3IsXG4gICAgICAgICAgICAgIGl0ZXJhdGlvbnM6IGl0ZXJhdGlvbnMsXG4gICAgICAgICAgICAgIHRpbWU6IHRpbWVcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAvLyBEZWxldGUgdGhlIHdvcmtlciBhbmQgYWxsIGl0cyBhc3NvY2lhdGVkIG1lbW9yeVxuICAgICAgICAgICAgd29ya2VyLnRlcm1pbmF0ZSgpO1xuICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgY2FzZSAnbG9nJzpcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGUuZGF0YS5tZXNzYWdlKTtcblxuICAgICAgICAgIGNhc2UgJ3NjaGVkdWxlJzpcbiAgICAgICAgICAgIGlmIChvcHRpb25zICYmIG9wdGlvbnMuc2NoZWR1bGUgJiYgdHlwZW9mIG9wdGlvbnMuc2NoZWR1bGUuZG8gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgdmFyIHNjaGVkdWxlZCA9IG9wdGlvbnMuc2NoZWR1bGUuZG9cbiAgICAgICAgICAgICAgc2NoZWR1bGVkKGUuZGF0YS5tZXNzYWdlKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH07XG5cbiAgICAvLyBTdGFydCB0aGUgd29ya2VyXG4gICAgd29ya2VyLnBvc3RNZXNzYWdlKHthY3Rpb246ICdzdGFydFRyYWluaW5nJ30pO1xuICB9LFxuXG4gIC8vIHRyYWlucyBhbiBYT1IgdG8gdGhlIG5ldHdvcmtcbiAgWE9SOiBmdW5jdGlvbihvcHRpb25zKSB7XG5cbiAgICBpZiAodGhpcy5uZXR3b3JrLmlucHV0cygpICE9IDIgfHwgdGhpcy5uZXR3b3JrLm91dHB1dHMoKSAhPSAxKVxuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSW5jb21wYXRpYmxlIG5ldHdvcmsgKDIgaW5wdXRzLCAxIG91dHB1dClcIik7XG5cbiAgICB2YXIgZGVmYXVsdHMgPSB7XG4gICAgICBpdGVyYXRpb25zOiAxMDAwMDAsXG4gICAgICBsb2c6IGZhbHNlLFxuICAgICAgc2h1ZmZsZTogdHJ1ZSxcbiAgICAgIGNvc3Q6IFRyYWluZXIuY29zdC5NU0VcbiAgICB9O1xuXG4gICAgaWYgKG9wdGlvbnMpXG4gICAgICBmb3IgKHZhciBpIGluIG9wdGlvbnMpXG4gICAgICAgIGRlZmF1bHRzW2ldID0gb3B0aW9uc1tpXTtcblxuICAgIHJldHVybiB0aGlzLnRyYWluKFt7XG4gICAgICBpbnB1dDogWzAsIDBdLFxuICAgICAgb3V0cHV0OiBbMF1cbiAgICB9LCB7XG4gICAgICBpbnB1dDogWzEsIDBdLFxuICAgICAgb3V0cHV0OiBbMV1cbiAgICB9LCB7XG4gICAgICBpbnB1dDogWzAsIDFdLFxuICAgICAgb3V0cHV0OiBbMV1cbiAgICB9LCB7XG4gICAgICBpbnB1dDogWzEsIDFdLFxuICAgICAgb3V0cHV0OiBbMF1cbiAgICB9XSwgZGVmYXVsdHMpO1xuICB9LFxuXG4gIC8vIHRyYWlucyB0aGUgbmV0d29yayB0byBwYXNzIGEgRGlzdHJhY3RlZCBTZXF1ZW5jZSBSZWNhbGwgdGVzdFxuICBEU1I6IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuICAgIHZhciB0YXJnZXRzID0gb3B0aW9ucy50YXJnZXRzIHx8IFsyLCA0LCA3LCA4XTtcbiAgICB2YXIgZGlzdHJhY3RvcnMgPSBvcHRpb25zLmRpc3RyYWN0b3JzIHx8IFszLCA1LCA2LCA5XTtcbiAgICB2YXIgcHJvbXB0cyA9IG9wdGlvbnMucHJvbXB0cyB8fCBbMCwgMV07XG4gICAgdmFyIGxlbmd0aCA9IG9wdGlvbnMubGVuZ3RoIHx8IDI0O1xuICAgIHZhciBjcml0ZXJpb24gPSBvcHRpb25zLnN1Y2Nlc3MgfHwgMC45NTtcbiAgICB2YXIgaXRlcmF0aW9ucyA9IG9wdGlvbnMuaXRlcmF0aW9ucyB8fCAxMDAwMDA7XG4gICAgdmFyIHJhdGUgPSBvcHRpb25zLnJhdGUgfHwgLjE7XG4gICAgdmFyIGxvZyA9IG9wdGlvbnMubG9nIHx8IDA7XG4gICAgdmFyIHNjaGVkdWxlID0gb3B0aW9ucy5zY2hlZHVsZSB8fCB7fTtcbiAgICB2YXIgY29zdCA9IG9wdGlvbnMuY29zdCB8fCB0aGlzLmNvc3QgfHwgVHJhaW5lci5jb3N0LkNST1NTX0VOVFJPUFk7XG5cbiAgICB2YXIgdHJpYWwsIGNvcnJlY3QsIGksIGosIHN1Y2Nlc3M7XG4gICAgdHJpYWwgPSBjb3JyZWN0ID0gaSA9IGogPSBzdWNjZXNzID0gMDtcbiAgICB2YXIgZXJyb3IgPSAxLFxuICAgICAgc3ltYm9scyA9IHRhcmdldHMubGVuZ3RoICsgZGlzdHJhY3RvcnMubGVuZ3RoICsgcHJvbXB0cy5sZW5ndGg7XG5cbiAgICB2YXIgbm9SZXBlYXQgPSBmdW5jdGlvbihyYW5nZSwgYXZvaWQpIHtcbiAgICAgIHZhciBudW1iZXIgPSBNYXRoLnJhbmRvbSgpICogcmFuZ2UgfCAwO1xuICAgICAgdmFyIHVzZWQgPSBmYWxzZTtcbiAgICAgIGZvciAodmFyIGkgaW4gYXZvaWQpXG4gICAgICAgIGlmIChudW1iZXIgPT0gYXZvaWRbaV0pXG4gICAgICAgICAgdXNlZCA9IHRydWU7XG4gICAgICByZXR1cm4gdXNlZCA/IG5vUmVwZWF0KHJhbmdlLCBhdm9pZCkgOiBudW1iZXI7XG4gICAgfTtcblxuICAgIHZhciBlcXVhbCA9IGZ1bmN0aW9uKHByZWRpY3Rpb24sIG91dHB1dCkge1xuICAgICAgZm9yICh2YXIgaSBpbiBwcmVkaWN0aW9uKVxuICAgICAgICBpZiAoTWF0aC5yb3VuZChwcmVkaWN0aW9uW2ldKSAhPSBvdXRwdXRbaV0pXG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTtcblxuICAgIHZhciBzdGFydCA9IERhdGUubm93KCk7XG5cbiAgICB3aGlsZSAodHJpYWwgPCBpdGVyYXRpb25zICYmIChzdWNjZXNzIDwgY3JpdGVyaW9uIHx8IHRyaWFsICUgMTAwMCAhPSAwKSkge1xuICAgICAgLy8gZ2VuZXJhdGUgc2VxdWVuY2VcbiAgICAgIHZhciBzZXF1ZW5jZSA9IFtdLFxuICAgICAgICBzZXF1ZW5jZUxlbmd0aCA9IGxlbmd0aCAtIHByb21wdHMubGVuZ3RoO1xuICAgICAgZm9yIChpID0gMDsgaSA8IHNlcXVlbmNlTGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGFueSA9IE1hdGgucmFuZG9tKCkgKiBkaXN0cmFjdG9ycy5sZW5ndGggfCAwO1xuICAgICAgICBzZXF1ZW5jZS5wdXNoKGRpc3RyYWN0b3JzW2FueV0pO1xuICAgICAgfVxuICAgICAgdmFyIGluZGV4ZXMgPSBbXSxcbiAgICAgICAgcG9zaXRpb25zID0gW107XG4gICAgICBmb3IgKGkgPSAwOyBpIDwgcHJvbXB0cy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpbmRleGVzLnB1c2goTWF0aC5yYW5kb20oKSAqIHRhcmdldHMubGVuZ3RoIHwgMCk7XG4gICAgICAgIHBvc2l0aW9ucy5wdXNoKG5vUmVwZWF0KHNlcXVlbmNlTGVuZ3RoLCBwb3NpdGlvbnMpKTtcbiAgICAgIH1cbiAgICAgIHBvc2l0aW9ucyA9IHBvc2l0aW9ucy5zb3J0KCk7XG4gICAgICBmb3IgKGkgPSAwOyBpIDwgcHJvbXB0cy5sZW5ndGg7IGkrKykge1xuICAgICAgICBzZXF1ZW5jZVtwb3NpdGlvbnNbaV1dID0gdGFyZ2V0c1tpbmRleGVzW2ldXTtcbiAgICAgICAgc2VxdWVuY2UucHVzaChwcm9tcHRzW2ldKTtcbiAgICAgIH1cblxuICAgICAgLy90cmFpbiBzZXF1ZW5jZVxuICAgICAgdmFyIGRpc3RyYWN0b3JzQ29ycmVjdDtcbiAgICAgIHZhciB0YXJnZXRzQ29ycmVjdCA9IGRpc3RyYWN0b3JzQ29ycmVjdCA9IDA7XG4gICAgICBlcnJvciA9IDA7XG4gICAgICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgICAgLy8gZ2VuZXJhdGUgaW5wdXQgZnJvbSBzZXF1ZW5jZVxuICAgICAgICB2YXIgaW5wdXQgPSBbXTtcbiAgICAgICAgZm9yIChqID0gMDsgaiA8IHN5bWJvbHM7IGorKylcbiAgICAgICAgICBpbnB1dFtqXSA9IDA7XG4gICAgICAgIGlucHV0W3NlcXVlbmNlW2ldXSA9IDE7XG5cbiAgICAgICAgLy8gZ2VuZXJhdGUgdGFyZ2V0IG91dHB1dFxuICAgICAgICB2YXIgb3V0cHV0ID0gW107XG4gICAgICAgIGZvciAoaiA9IDA7IGogPCB0YXJnZXRzLmxlbmd0aDsgaisrKVxuICAgICAgICAgIG91dHB1dFtqXSA9IDA7XG5cbiAgICAgICAgaWYgKGkgPj0gc2VxdWVuY2VMZW5ndGgpIHtcbiAgICAgICAgICB2YXIgaW5kZXggPSBpIC0gc2VxdWVuY2VMZW5ndGg7XG4gICAgICAgICAgb3V0cHV0W2luZGV4ZXNbaW5kZXhdXSA9IDE7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBjaGVjayByZXN1bHRcbiAgICAgICAgdmFyIHByZWRpY3Rpb24gPSB0aGlzLm5ldHdvcmsuYWN0aXZhdGUoaW5wdXQpO1xuXG4gICAgICAgIGlmIChlcXVhbChwcmVkaWN0aW9uLCBvdXRwdXQpKVxuICAgICAgICAgIGlmIChpIDwgc2VxdWVuY2VMZW5ndGgpXG4gICAgICAgICAgICBkaXN0cmFjdG9yc0NvcnJlY3QrKztcbiAgICAgICAgICBlbHNlXG4gICAgICAgICAgICB0YXJnZXRzQ29ycmVjdCsrO1xuICAgICAgICBlbHNlIHtcbiAgICAgICAgICB0aGlzLm5ldHdvcmsucHJvcGFnYXRlKHJhdGUsIG91dHB1dCk7XG4gICAgICAgIH1cblxuICAgICAgICBlcnJvciArPSBjb3N0KG91dHB1dCwgcHJlZGljdGlvbik7XG5cbiAgICAgICAgaWYgKGRpc3RyYWN0b3JzQ29ycmVjdCArIHRhcmdldHNDb3JyZWN0ID09IGxlbmd0aClcbiAgICAgICAgICBjb3JyZWN0Kys7XG4gICAgICB9XG5cbiAgICAgIC8vIGNhbGN1bGF0ZSBlcnJvclxuICAgICAgaWYgKHRyaWFsICUgMTAwMCA9PSAwKVxuICAgICAgICBjb3JyZWN0ID0gMDtcbiAgICAgIHRyaWFsKys7XG4gICAgICB2YXIgZGl2aWRlRXJyb3IgPSB0cmlhbCAlIDEwMDA7XG4gICAgICBkaXZpZGVFcnJvciA9IGRpdmlkZUVycm9yID09IDAgPyAxMDAwIDogZGl2aWRlRXJyb3I7XG4gICAgICBzdWNjZXNzID0gY29ycmVjdCAvIGRpdmlkZUVycm9yO1xuICAgICAgZXJyb3IgLz0gbGVuZ3RoO1xuXG4gICAgICAvLyBsb2dcbiAgICAgIGlmIChsb2cgJiYgdHJpYWwgJSBsb2cgPT0gMClcbiAgICAgICAgY29uc29sZS5sb2coXCJpdGVyYXRpb25zOlwiLCB0cmlhbCwgXCIgc3VjY2VzczpcIiwgc3VjY2VzcywgXCIgY29ycmVjdDpcIixcbiAgICAgICAgICBjb3JyZWN0LCBcIiB0aW1lOlwiLCBEYXRlLm5vdygpIC0gc3RhcnQsIFwiIGVycm9yOlwiLCBlcnJvcik7XG4gICAgICBpZiAoc2NoZWR1bGUuZG8gJiYgc2NoZWR1bGUuZXZlcnkgJiYgdHJpYWwgJSBzY2hlZHVsZS5ldmVyeSA9PSAwKVxuICAgICAgICBzY2hlZHVsZS5kbyh7XG4gICAgICAgICAgaXRlcmF0aW9uczogdHJpYWwsXG4gICAgICAgICAgc3VjY2Vzczogc3VjY2VzcyxcbiAgICAgICAgICBlcnJvcjogZXJyb3IsXG4gICAgICAgICAgdGltZTogRGF0ZS5ub3coKSAtIHN0YXJ0LFxuICAgICAgICAgIGNvcnJlY3Q6IGNvcnJlY3RcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgIGl0ZXJhdGlvbnM6IHRyaWFsLFxuICAgICAgc3VjY2Vzczogc3VjY2VzcyxcbiAgICAgIGVycm9yOiBlcnJvcixcbiAgICAgIHRpbWU6IERhdGUubm93KCkgLSBzdGFydFxuICAgIH1cbiAgfSxcblxuICAvLyB0cmFpbiB0aGUgbmV0d29yayB0byBsZWFybiBhbiBFbWJlZGVkIFJlYmVyIEdyYW1tYXJcbiAgRVJHOiBmdW5jdGlvbihvcHRpb25zKSB7XG5cbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgICB2YXIgaXRlcmF0aW9ucyA9IG9wdGlvbnMuaXRlcmF0aW9ucyB8fCAxNTAwMDA7XG4gICAgdmFyIGNyaXRlcmlvbiA9IG9wdGlvbnMuZXJyb3IgfHwgLjA1O1xuICAgIHZhciByYXRlID0gb3B0aW9ucy5yYXRlIHx8IC4xO1xuICAgIHZhciBsb2cgPSBvcHRpb25zLmxvZyB8fCA1MDA7XG4gICAgdmFyIGNvc3QgPSBvcHRpb25zLmNvc3QgfHwgdGhpcy5jb3N0IHx8IFRyYWluZXIuY29zdC5DUk9TU19FTlRST1BZO1xuXG4gICAgLy8gZ3JhbWFyIG5vZGVcbiAgICB2YXIgTm9kZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5wYXRocyA9IFtdO1xuICAgIH07XG4gICAgTm9kZS5wcm90b3R5cGUgPSB7XG4gICAgICBjb25uZWN0OiBmdW5jdGlvbihub2RlLCB2YWx1ZSkge1xuICAgICAgICB0aGlzLnBhdGhzLnB1c2goe1xuICAgICAgICAgIG5vZGU6IG5vZGUsXG4gICAgICAgICAgdmFsdWU6IHZhbHVlXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH0sXG4gICAgICBhbnk6IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAodGhpcy5wYXRocy5sZW5ndGggPT0gMClcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIHZhciBpbmRleCA9IE1hdGgucmFuZG9tKCkgKiB0aGlzLnBhdGhzLmxlbmd0aCB8IDA7XG4gICAgICAgIHJldHVybiB0aGlzLnBhdGhzW2luZGV4XTtcbiAgICAgIH0sXG4gICAgICB0ZXN0OiBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICBmb3IgKHZhciBpIGluIHRoaXMucGF0aHMpXG4gICAgICAgICAgaWYgKHRoaXMucGF0aHNbaV0udmFsdWUgPT0gdmFsdWUpXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5wYXRoc1tpXTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH07XG5cbiAgICB2YXIgcmViZXJHcmFtbWFyID0gZnVuY3Rpb24oKSB7XG5cbiAgICAgIC8vIGJ1aWxkIGEgcmViZXIgZ3JhbW1hclxuICAgICAgdmFyIG91dHB1dCA9IG5ldyBOb2RlKCk7XG4gICAgICB2YXIgbjEgPSAobmV3IE5vZGUoKSkuY29ubmVjdChvdXRwdXQsIFwiRVwiKTtcbiAgICAgIHZhciBuMiA9IChuZXcgTm9kZSgpKS5jb25uZWN0KG4xLCBcIlNcIik7XG4gICAgICB2YXIgbjMgPSAobmV3IE5vZGUoKSkuY29ubmVjdChuMSwgXCJWXCIpLmNvbm5lY3QobjIsIFwiUFwiKTtcbiAgICAgIHZhciBuNCA9IChuZXcgTm9kZSgpKS5jb25uZWN0KG4yLCBcIlhcIik7XG4gICAgICBuNC5jb25uZWN0KG40LCBcIlNcIik7XG4gICAgICB2YXIgbjUgPSAobmV3IE5vZGUoKSkuY29ubmVjdChuMywgXCJWXCIpO1xuICAgICAgbjUuY29ubmVjdChuNSwgXCJUXCIpO1xuICAgICAgbjIuY29ubmVjdChuNSwgXCJYXCIpO1xuICAgICAgdmFyIG42ID0gKG5ldyBOb2RlKCkpLmNvbm5lY3QobjQsIFwiVFwiKS5jb25uZWN0KG41LCBcIlBcIik7XG4gICAgICB2YXIgaW5wdXQgPSAobmV3IE5vZGUoKSkuY29ubmVjdChuNiwgXCJCXCIpO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBpbnB1dDogaW5wdXQsXG4gICAgICAgIG91dHB1dDogb3V0cHV0XG4gICAgICB9XG4gICAgfTtcblxuICAgIC8vIGJ1aWxkIGFuIGVtYmVkZWQgcmViZXIgZ3JhbW1hclxuICAgIHZhciBlbWJlZGVkUmViZXJHcmFtbWFyID0gZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgcmViZXIxID0gcmViZXJHcmFtbWFyKCk7XG4gICAgICB2YXIgcmViZXIyID0gcmViZXJHcmFtbWFyKCk7XG5cbiAgICAgIHZhciBvdXRwdXQgPSBuZXcgTm9kZSgpO1xuICAgICAgdmFyIG4xID0gKG5ldyBOb2RlKS5jb25uZWN0KG91dHB1dCwgXCJFXCIpO1xuICAgICAgcmViZXIxLm91dHB1dC5jb25uZWN0KG4xLCBcIlRcIik7XG4gICAgICByZWJlcjIub3V0cHV0LmNvbm5lY3QobjEsIFwiUFwiKTtcbiAgICAgIHZhciBuMiA9IChuZXcgTm9kZSkuY29ubmVjdChyZWJlcjEuaW5wdXQsIFwiUFwiKS5jb25uZWN0KHJlYmVyMi5pbnB1dCxcbiAgICAgICAgXCJUXCIpO1xuICAgICAgdmFyIGlucHV0ID0gKG5ldyBOb2RlKS5jb25uZWN0KG4yLCBcIkJcIik7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIGlucHV0OiBpbnB1dCxcbiAgICAgICAgb3V0cHV0OiBvdXRwdXRcbiAgICAgIH1cblxuICAgIH07XG5cbiAgICAvLyBnZW5lcmF0ZSBhbiBFUkcgc2VxdWVuY2VcbiAgICB2YXIgZ2VuZXJhdGUgPSBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBub2RlID0gZW1iZWRlZFJlYmVyR3JhbW1hcigpLmlucHV0O1xuICAgICAgdmFyIG5leHQgPSBub2RlLmFueSgpO1xuICAgICAgdmFyIHN0ciA9IFwiXCI7XG4gICAgICB3aGlsZSAobmV4dCkge1xuICAgICAgICBzdHIgKz0gbmV4dC52YWx1ZTtcbiAgICAgICAgbmV4dCA9IG5leHQubm9kZS5hbnkoKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzdHI7XG4gICAgfTtcblxuICAgIC8vIHRlc3QgaWYgYSBzdHJpbmcgbWF0Y2hlcyBhbiBlbWJlZGVkIHJlYmVyIGdyYW1tYXJcbiAgICB2YXIgdGVzdCA9IGZ1bmN0aW9uKHN0cikge1xuICAgICAgdmFyIG5vZGUgPSBlbWJlZGVkUmViZXJHcmFtbWFyKCkuaW5wdXQ7XG4gICAgICB2YXIgaSA9IDA7XG4gICAgICB2YXIgY2ggPSBzdHIuY2hhckF0KGkpO1xuICAgICAgd2hpbGUgKGkgPCBzdHIubGVuZ3RoKSB7XG4gICAgICAgIHZhciBuZXh0ID0gbm9kZS50ZXN0KGNoKTtcbiAgICAgICAgaWYgKCFuZXh0KVxuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgbm9kZSA9IG5leHQubm9kZTtcbiAgICAgICAgY2ggPSBzdHIuY2hhckF0KCsraSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9O1xuXG4gICAgLy8gaGVscGVyIHRvIGNoZWNrIGlmIHRoZSBvdXRwdXQgYW5kIHRoZSB0YXJnZXQgdmVjdG9ycyBtYXRjaFxuICAgIHZhciBkaWZmZXJlbnQgPSBmdW5jdGlvbihhcnJheTEsIGFycmF5Mikge1xuICAgICAgdmFyIG1heDEgPSAwO1xuICAgICAgdmFyIGkxID0gLTE7XG4gICAgICB2YXIgbWF4MiA9IDA7XG4gICAgICB2YXIgaTIgPSAtMTtcbiAgICAgIGZvciAodmFyIGkgaW4gYXJyYXkxKSB7XG4gICAgICAgIGlmIChhcnJheTFbaV0gPiBtYXgxKSB7XG4gICAgICAgICAgbWF4MSA9IGFycmF5MVtpXTtcbiAgICAgICAgICBpMSA9IGk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGFycmF5MltpXSA+IG1heDIpIHtcbiAgICAgICAgICBtYXgyID0gYXJyYXkyW2ldO1xuICAgICAgICAgIGkyID0gaTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gaTEgIT0gaTI7XG4gICAgfTtcblxuICAgIHZhciBpdGVyYXRpb24gPSAwO1xuICAgIHZhciBlcnJvciA9IDE7XG4gICAgdmFyIHRhYmxlID0ge1xuICAgICAgXCJCXCI6IDAsXG4gICAgICBcIlBcIjogMSxcbiAgICAgIFwiVFwiOiAyLFxuICAgICAgXCJYXCI6IDMsXG4gICAgICBcIlNcIjogNCxcbiAgICAgIFwiRVwiOiA1XG4gICAgfTtcblxuICAgIHZhciBzdGFydCA9IERhdGUubm93KCk7XG4gICAgd2hpbGUgKGl0ZXJhdGlvbiA8IGl0ZXJhdGlvbnMgJiYgZXJyb3IgPiBjcml0ZXJpb24pIHtcbiAgICAgIHZhciBpID0gMDtcbiAgICAgIGVycm9yID0gMDtcblxuICAgICAgLy8gRVJHIHNlcXVlbmNlIHRvIGxlYXJuXG4gICAgICB2YXIgc2VxdWVuY2UgPSBnZW5lcmF0ZSgpO1xuXG4gICAgICAvLyBpbnB1dFxuICAgICAgdmFyIHJlYWQgPSBzZXF1ZW5jZS5jaGFyQXQoaSk7XG4gICAgICAvLyB0YXJnZXRcbiAgICAgIHZhciBwcmVkaWN0ID0gc2VxdWVuY2UuY2hhckF0KGkgKyAxKTtcblxuICAgICAgLy8gdHJhaW5cbiAgICAgIHdoaWxlIChpIDwgc2VxdWVuY2UubGVuZ3RoIC0gMSkge1xuICAgICAgICB2YXIgaW5wdXQgPSBbXTtcbiAgICAgICAgdmFyIHRhcmdldCA9IFtdO1xuICAgICAgICBmb3IgKHZhciBqID0gMDsgaiA8IDY7IGorKykge1xuICAgICAgICAgIGlucHV0W2pdID0gMDtcbiAgICAgICAgICB0YXJnZXRbal0gPSAwO1xuICAgICAgICB9XG4gICAgICAgIGlucHV0W3RhYmxlW3JlYWRdXSA9IDE7XG4gICAgICAgIHRhcmdldFt0YWJsZVtwcmVkaWN0XV0gPSAxO1xuXG4gICAgICAgIHZhciBvdXRwdXQgPSB0aGlzLm5ldHdvcmsuYWN0aXZhdGUoaW5wdXQpO1xuXG4gICAgICAgIGlmIChkaWZmZXJlbnQob3V0cHV0LCB0YXJnZXQpKVxuICAgICAgICAgIHRoaXMubmV0d29yay5wcm9wYWdhdGUocmF0ZSwgdGFyZ2V0KTtcblxuICAgICAgICByZWFkID0gc2VxdWVuY2UuY2hhckF0KCsraSk7XG4gICAgICAgIHByZWRpY3QgPSBzZXF1ZW5jZS5jaGFyQXQoaSArIDEpO1xuXG4gICAgICAgIGVycm9yICs9IGNvc3QodGFyZ2V0LCBvdXRwdXQpO1xuICAgICAgfVxuICAgICAgZXJyb3IgLz0gc2VxdWVuY2UubGVuZ3RoO1xuICAgICAgaXRlcmF0aW9uKys7XG4gICAgICBpZiAoaXRlcmF0aW9uICUgbG9nID09IDApIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJpdGVyYXRpb25zOlwiLCBpdGVyYXRpb24sIFwiIHRpbWU6XCIsIERhdGUubm93KCkgLSBzdGFydCxcbiAgICAgICAgICBcIiBlcnJvcjpcIiwgZXJyb3IpO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICBpdGVyYXRpb25zOiBpdGVyYXRpb24sXG4gICAgICBlcnJvcjogZXJyb3IsXG4gICAgICB0aW1lOiBEYXRlLm5vdygpIC0gc3RhcnQsXG4gICAgICB0ZXN0OiB0ZXN0LFxuICAgICAgZ2VuZXJhdGU6IGdlbmVyYXRlXG4gICAgfVxuICB9LFxuXG4gIHRpbWluZ1Rhc2s6IGZ1bmN0aW9uKG9wdGlvbnMpe1xuXG4gICAgaWYgKHRoaXMubmV0d29yay5pbnB1dHMoKSAhPSAyIHx8IHRoaXMubmV0d29yay5vdXRwdXRzKCkgIT0gMSlcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkludmFsaWQgTmV0d29yazogbXVzdCBoYXZlIDIgaW5wdXRzIGFuZCBvbmUgb3V0cHV0XCIpO1xuXG4gICAgaWYgKHR5cGVvZiBvcHRpb25zID09ICd1bmRlZmluZWQnKVxuICAgICAgb3B0aW9ucyA9IHt9O1xuXG4gICAgLy8gaGVscGVyXG4gICAgZnVuY3Rpb24gZ2V0U2FtcGxlcyAodHJhaW5pbmdTaXplLCB0ZXN0U2l6ZSl7XG5cbiAgICAgIC8vIHNhbXBsZSBzaXplXG4gICAgICB2YXIgc2l6ZSA9IHRyYWluaW5nU2l6ZSArIHRlc3RTaXplO1xuXG4gICAgICAvLyBnZW5lcmF0ZSBzYW1wbGVzXG4gICAgICB2YXIgdCA9IDA7XG4gICAgICB2YXIgc2V0ID0gW107XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHNpemU7IGkrKykge1xuICAgICAgICBzZXQucHVzaCh7IGlucHV0OiBbMCwwXSwgb3V0cHV0OiBbMF0gfSk7XG4gICAgICB9XG4gICAgICB3aGlsZSh0IDwgc2l6ZSAtIDIwKSB7XG4gICAgICAgICAgdmFyIG4gPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAyMCk7XG4gICAgICAgICAgc2V0W3RdLmlucHV0WzBdID0gMTtcbiAgICAgICAgICBmb3IgKHZhciBqID0gdDsgaiA8PSB0ICsgbjsgaisrKXtcbiAgICAgICAgICAgICAgc2V0W2pdLmlucHV0WzFdID0gbiAvIDIwO1xuICAgICAgICAgICAgICBzZXRbal0ub3V0cHV0WzBdID0gMC41O1xuICAgICAgICAgIH1cbiAgICAgICAgICB0ICs9IG47XG4gICAgICAgICAgbiA9IE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIDIwKTtcbiAgICAgICAgICBmb3IgKHZhciBrID0gdCsxOyBrIDw9ICh0ICsgbikgJiYgIGsgPCBzaXplOyBrKyspXG4gICAgICAgICAgICAgIHNldFtrXS5pbnB1dFsxXSA9IHNldFt0XS5pbnB1dFsxXTtcbiAgICAgICAgICB0ICs9IG47XG4gICAgICB9XG5cbiAgICAgIC8vIHNlcGFyYXRlIHNhbXBsZXMgYmV0d2VlbiB0cmFpbiBhbmQgdGVzdCBzZXRzXG4gICAgICB2YXIgdHJhaW5pbmdTZXQgPSBbXTsgdmFyIHRlc3RTZXQgPSBbXTtcbiAgICAgIGZvciAodmFyIGwgPSAwOyBsIDwgc2l6ZTsgbCsrKVxuICAgICAgICAgIChsIDwgdHJhaW5pbmdTaXplID8gdHJhaW5pbmdTZXQgOiB0ZXN0U2V0KS5wdXNoKHNldFtsXSk7XG5cbiAgICAgIC8vIHJldHVybiBzYW1wbGVzXG4gICAgICByZXR1cm4ge1xuICAgICAgICAgIHRyYWluOiB0cmFpbmluZ1NldCxcbiAgICAgICAgICB0ZXN0OiB0ZXN0U2V0XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIGl0ZXJhdGlvbnMgPSBvcHRpb25zLml0ZXJhdGlvbnMgfHwgMjAwO1xuICAgIHZhciBlcnJvciA9IG9wdGlvbnMuZXJyb3IgfHwgLjAwNTtcbiAgICB2YXIgcmF0ZSA9IG9wdGlvbnMucmF0ZSB8fCBbLjAzLCAuMDJdO1xuICAgIHZhciBsb2cgPSBvcHRpb25zLmxvZyA9PT0gZmFsc2UgPyBmYWxzZSA6IG9wdGlvbnMubG9nIHx8IDEwO1xuICAgIHZhciBjb3N0ID0gb3B0aW9ucy5jb3N0IHx8IHRoaXMuY29zdCB8fCBUcmFpbmVyLmNvc3QuTVNFO1xuICAgIHZhciB0cmFpbmluZ1NhbXBsZXMgPSBvcHRpb25zLnRyYWluU2FtcGxlcyB8fCA3MDAwO1xuICAgIHZhciB0ZXN0U2FtcGxlcyA9IG9wdGlvbnMudHJhaW5TYW1wbGVzIHx8IDEwMDA7XG5cbiAgICAvLyBzYW1wbGVzIGZvciB0cmFpbmluZyBhbmQgdGVzdGluZ1xuICAgIHZhciBzYW1wbGVzID0gZ2V0U2FtcGxlcyh0cmFpbmluZ1NhbXBsZXMsIHRlc3RTYW1wbGVzKTtcblxuICAgIC8vIHRyYWluXG4gICAgdmFyIHJlc3VsdCA9IHRoaXMudHJhaW4oc2FtcGxlcy50cmFpbiwge1xuICAgICAgcmF0ZTogcmF0ZSxcbiAgICAgIGxvZzogbG9nLFxuICAgICAgaXRlcmF0aW9uczogaXRlcmF0aW9ucyxcbiAgICAgIGVycm9yOiBlcnJvcixcbiAgICAgIGNvc3Q6IGNvc3RcbiAgICB9KTtcblxuICAgIHJldHVybiB7XG4gICAgICB0cmFpbjogcmVzdWx0LFxuICAgICAgdGVzdDogdGhpcy50ZXN0KHNhbXBsZXMudGVzdClcbiAgICB9XG4gIH1cbn07XG5cbi8vIEJ1aWx0LWluIGNvc3QgZnVuY3Rpb25zXG5UcmFpbmVyLmNvc3QgPSB7XG4gIC8vIEVxLiA5XG4gIENST1NTX0VOVFJPUFk6IGZ1bmN0aW9uKHRhcmdldCwgb3V0cHV0KVxuICB7XG4gICAgdmFyIGNyb3NzZW50cm9weSA9IDA7XG4gICAgZm9yICh2YXIgaSBpbiBvdXRwdXQpXG4gICAgICBjcm9zc2VudHJvcHkgLT0gKHRhcmdldFtpXSAqIE1hdGgubG9nKG91dHB1dFtpXSsxZS0xNSkpICsgKCgxLXRhcmdldFtpXSkgKiBNYXRoLmxvZygoMSsxZS0xNSktb3V0cHV0W2ldKSk7IC8vICsxZS0xNSBpcyBhIHRpbnkgcHVzaCBhd2F5IHRvIGF2b2lkIE1hdGgubG9nKDApXG4gICAgcmV0dXJuIGNyb3NzZW50cm9weTtcbiAgfSxcbiAgTVNFOiBmdW5jdGlvbih0YXJnZXQsIG91dHB1dClcbiAge1xuICAgIHZhciBtc2UgPSAwO1xuICAgIGZvciAodmFyIGkgaW4gb3V0cHV0KVxuICAgICAgbXNlICs9IE1hdGgucG93KHRhcmdldFtpXSAtIG91dHB1dFtpXSwgMik7XG4gICAgcmV0dXJuIG1zZSAvIG91dHB1dC5sZW5ndGg7XG4gIH0sXG4gIEJJTkFSWTogZnVuY3Rpb24odGFyZ2V0LCBvdXRwdXQpe1xuICAgIHZhciBtaXNzZXMgPSAwO1xuICAgIGZvciAodmFyIGkgaW4gb3V0cHV0KVxuICAgICAgbWlzc2VzICs9IE1hdGgucm91bmQodGFyZ2V0W2ldICogMikgIT0gTWF0aC5yb3VuZChvdXRwdXRbaV0gKiAyKTtcbiAgICByZXR1cm4gbWlzc2VzO1xuICB9XG59XG4iXX0=
